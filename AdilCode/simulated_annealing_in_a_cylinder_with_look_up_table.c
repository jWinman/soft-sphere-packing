#include <stdio.h>
#include <time.h>
#include <math.h>

#define large_int 1000
#define diameter 1.0
#define radius diameter/2.0
#define tiny 0.000000000000001

#define UClim 20.0
#define LClim 0.00000001

#define large 10.0

//RANDOM NUMBER GENERATOR
#define pi 3.14159265358979323846264338327
#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)
double ran2(long *idum)
{
	int j;
	long k;
	static long idum2=123456789;
	static long iy=0;
	static long iv[NTAB];
	double temp;

	if (*idum <= 0) {
		if (-(*idum) < 1) *idum=1;
		else *idum = -(*idum);
		idum2=(*idum);
		for (j=NTAB+7;j>=0;j--) {
			k=(*idum)/IQ1;
			*idum=IA1*(*idum-k*IQ1)-k*IR1;
			if (*idum < 0) *idum += IM1;
			if (j < NTAB) iv[j] = *idum;
		}
		iy=iv[0];
	}
	k=(*idum)/IQ1;
	*idum=IA1*(*idum-k*IQ1)-k*IR1;
	if (*idum < 0) *idum += IM1;
	k=idum2/IQ2;
	idum2=IA2*(idum2-k*IQ2)-k*IR2;
	if (idum2 < 0) idum2 += IM2;
	j=iy/NDIV;
	iy=iv[j]-idum2;
	iv[j] = *idum;
	if (iy < 1) iy += IMM1;
	if ((temp=AM*iy) > RNMX) return RNMX;
	else return temp;
}
#undef IM1
#undef IM2
#undef AM
#undef IMM1
#undef IA1
#undef IA2
#undef IQ1
#undef IQ2
#undef IR1
#undef IR2
#undef NTAB
#undef NDIV
#undef EPS
#undef RNMX
/* (C) Copr. 1986-92 Numerical Recipes Software 5.){2p491&].#RsL. */


//Global variables
int NBOX_X, NBOX_Y, NBOX_Z; //Cartessian grid size
int NRUN_TRIAL, NRUN_FULL;
double XSTART, XEND, YSTART, YEND, ZSTART, ZEND;
double BOX_LEN_X, BOX_LEN_Y, BOX_LEN_Z; //length of box
double CELL_SIZE; //size of cell
double E;
double acptN, tol, Aval[5]; 
double Rlim, Clength;
double Angle;
double P;
double D; //diamter of tube
int grid[25][25][100][1000]; //store the id of the first and last particles with this grid position 
int NUM_PAR; //number of particles
long seed; //seed for random number generator
int accept, reject;

////////////////// FUNCTIONS TO COMPUTE THE ENERGY OF THE SYSTEM ///////////////////////

//Compute initial energy
double compute_initial_energy(void);

//Compute final energy
double compute_final_energy(void);

//To compute the total interaction energy of a give particle:
double penergy(int);

//pairwise energy
double pairwise_energy(int, int);

//boundary energy
double boundary_energy(int);

//total particle-particle energy (using grid)
double total_particle_energy(void);

//total energy due to cylindrical boundary
double total_boundary_energy(void);

//////////////////////////////////////////////////////////////////////////////////////////

////////////////// FUNCTIONS TO MANAGE THE LOOK UP TABLE /////////////////////////////////

//Initialise the table
void start_table(void);

//Compute the grid index of the particle
int ind(double, double, double); 

//To initially build the linked list use:
void build_linked_list(void);

//To add particles to the linked list:
void add(int);

//To delete particles to the linked list:
void delete(int);

//print out the linked list
void print_linked_list(void);

////////////////////////////////////////////////////////////////////////////////////////////

////////////////// SIMULATED ANNEALING FUNCTIONS ///////////////////////////////////////////

//initial coordinates of particles
void  initial_positions(void);

//make a trial move
void move(int, double);

//change angle
void change_angle(double);

//change length
void change_length(double);

//perform a monte carlo move
void MC(double, double);

//Find the right step length for a given temperature
double FindStepLength(double);

////////////////////////////////////////////////////////////////////////////////////////////

////////////////// GENERATE OUTPUT  ////////////////////////////////////////////////////////

//Output
void output(void);

////////////////////////////////////////////////////////////////////////////////////////////

////////////////// FUNCTIONS  /////////////////////////////////////////////////////////////

//Output
double modulo(double,double);

////////////////////////////////////////////////////////////////////////////////////////////

struct particle{
  double x;
  double y;
  double z;
  double r;
  double th;
  int x_index;
  int y_index;
  int z_index;
  int head;
  int tail;
};
struct particle p[1000];


int main(void)
{
  int i, T, Tlimit, sim_time;
  int NRUN_TRIAL_PER_P, NRUN_FULL_PER_P;
  double StepLength, temperature[50000];
  double initial_energy, final_energy, Ptemp, Btemp;
  char filename[100];
  FILE *fpt;
 
  seed=-time(NULL); //start random number generator
  //seed=-1.0;
 
  /////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////  READ INPUT FILES  //////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    
    initial_positions();

    fpt=fopen("run_stats", "r");
    fscanf(fpt, "%d", &NRUN_TRIAL_PER_P);
    fscanf(fpt, "%d", &NRUN_FULL_PER_P);
    fscanf(fpt, "%lf", &acptN  );
    fscanf(fpt, "%lf", &tol  ); 
    fclose(fpt);
    NRUN_TRIAL=NUM_PAR*NRUN_TRIAL_PER_P;
    NRUN_FULL=NUM_PAR*NRUN_FULL_PER_P;
    
    fpt=fopen("cooling_schedule", "r");
    fscanf(fpt, "%d", &Tlimit);
    printf("Tlimit=%d\n",Tlimit);
    for(T=1; T<=Tlimit; T++){
      fscanf(fpt, "%lf", &temperature[T]);
    } 
    fclose(fpt);
    
    //Define initial particle positions   
    start_table();
    //build table
    build_linked_list();
    
    //compute initial energy
    E=compute_initial_energy();
    initial_energy=E;
    //set the initial step lengths
    Aval[1]=0.0;
    Aval[2]=large*0.5;
    Aval[3]=large;

    for(T=1; T<=Tlimit; T++){
      //printf("T=%d Temp=%6.15f\n", T, temperature[T]);
      //fflush (stdin);
      //(void) getchar ();
      //printf("check 1\n");

      StepLength=FindStepLength(temperature[T]);

      //printf("check 2\n");

      fpt=fopen("temp_vs_step_length", "a");
      fprintf(fpt, "%6.15f %6.15f\n",  StepLength, temperature[T]);
      fclose(fpt);
      
      for(sim_time=1; sim_time<=NRUN_FULL; sim_time++){
	//printf("Sim Step %d\n", sim_time);
	MC(temperature[T], StepLength);
      }
    }
    final_energy=compute_final_energy();    

    printf("initial energy = %6.15f E=%6.15f final energy=%6.15f\n", initial_energy, E, final_energy);
    E=compute_initial_energy();
    printf("check=%6.15f\n", E);

    fpt=fopen("simulated_annealing_energy", "w");
    fprintf(fpt, "%6.15f\n", final_energy/(1.0*NUM_PAR) );
    fclose(fpt);

    output();
}

////////////////////////////////////////////////////////////////////////////////////////
////////////////// FUNCTIONS TO COMPUTE THE ENERGY OF THE SYSTEM ///////////////////////
////////////////////////////////////////////////////////////////////////////////////////

double compute_initial_energy()
{
  int i, j;
  double xi, yi, zi;
  double xj, yj, zj;
  double dx, dy, dz, dist, distance_from_boundary, volume;
  double E, energy, boundary, pressure;
  double Etemp;

  printf("**** COMPUTE INITIAL ENERGY ***\n");

  energy=0.0;
  for(i=1;i<=3*NUM_PAR;i++){  
    xi=p[i].x;
    yi=p[i].y;
    zi=p[i].z;
    
    for(j=1; j<=3*NUM_PAR; j++){
      
      if(i!=j){
	xj=p[j].x;
	yj=p[j].y;
	zj=p[j].z;
	
	dx=xi-xj;
	dy=yi-yj;
	dz=zi-zj;      
	dist=sqrt( (dx*dx) + (dy*dy) + (dz*dz) );
	if(dist<=diameter){
	  E=0.5*(0.5*(dist-diameter)*(dist-diameter)); 
	  energy+=E; 
	}
	else{
	  energy+=0.0;
	}
      }
    }
  }

  boundary=0.0;
  for(i=1; i<=3*NUM_PAR; i++){
    distance_from_boundary=Rlim-p[i].r;
    if(distance_from_boundary<=diameter){
      Etemp=0.5*(distance_from_boundary-diameter)*(distance_from_boundary-diameter);
      boundary+=Etemp;
      printf("boundary energy=%d (%6.15f %6.15f %6.15f): %6.15f\n", i, p[i].r, p[i].th, p[i].z,  Etemp);
    }
  }


  volume=pi*((D/2.0)*(D/2.0))*Clength;
  pressure=P*volume;

  printf("energy=%6.15f boundary=%6.15f pressure=%6.15f\n", energy, boundary, pressure);

  printf("**** END INITIAL ENERGY ***\n");

  return energy+boundary+pressure;
}

double compute_final_energy()
{


  
  int i, j;
  double xi, yi, zi;
  double xj, yj, zj;
  double dx, dy, dz, dist, distance_from_boundary, volume;
  double Etemp;
  double E, energy, boundary, pressure;

  printf("**** COMPUTE FINAL ENERGY ***\n");

  energy=0.0;
  for(i=1;i<=NUM_PAR;i++){  
    xi=p[i].x;
    yi=p[i].y;
    zi=p[i].z;
    
    for(j=1; j<=3*NUM_PAR; j++){
      
      if(i!=j){
	xj=p[j].x;
	yj=p[j].y;
	zj=p[j].z;
	
	dx=xi-xj;
	dy=yi-yj;
	dz=zi-zj;      
	dist=sqrt( (dx*dx) + (dy*dy) + (dz*dz) );
	if(dist<=diameter){
	  E=0.5*(0.5*(dist-diameter)*(dist-diameter)); 
	  energy+=E; 
	}
	else{
	  energy+=0.0;
	}
      }
    }
  }

  boundary=0.0;
  for(i=1; i<=NUM_PAR; i++){
    distance_from_boundary=Rlim-p[i].r;
    if(distance_from_boundary<=diameter){
      Etemp=0.5*(distance_from_boundary-diameter)*(distance_from_boundary-diameter);
      boundary+=Etemp;
      printf("boundary energy=%d (%6.15f %6.15f %6.15f): %6.15f\n", i, p[i].r, p[i].th, p[i].z,  Etemp);
    }
  }

  volume=pi*((D/2.0)*(D/2.0))*Clength;
  pressure=P*volume;

  printf("energy=%6.15f boundary=%6.15f pressure=%6.15f\n", energy, boundary, pressure);

  printf("**** END FINAL ENERGY ***\n");



  return energy+boundary+pressure;
}



//compute the interaction energy of the qth particle with all other particles
// in the same cell and in neighbouring cells
double penergy(int q)
{
  int i, j, k;
  int l;
  int N;
  int xo, yo, zo;
  int xind, yind, zind;
  double E;
  double Etemp;

  E=0.0; //set the initial energy to zero.
  //these are the x, y and z grid coordinates of the qth particle
  //this is the central box surrounded by 28 other boxes.
  xo=p[q].x_index;
  yo=p[q].y_index;
  zo=p[q].z_index;
  
  //go through all boxes including the central box
  for(i=-1;i<=1;i++){ //iterate over x, y, and z
    for(j=-1;j<=1;j++){
      for(k=-1;k<=1;k++){
	
	xind=xo+i;
	yind=yo+j;
	zind=zo+k;
	
	N=grid[xind][yind][zind][0]; //head particle of the box under consideration
	if(N!=0){//provided the current cell is not empty
	  for(l=1;l<=large_int;l++){
	    if(q!=N){//do not include self interaction 
	      Etemp=pairwise_energy(q, N);
	      E+=Etemp;
	    }
	    N=p[N].tail; // this gives the next particle in the chain. 
	    if(N==0){//stop when the end of the list has been reached
	      break;
	    }
	  }
	}
      }
    }
  }
  
  return E;
}


double pairwise_energy(int i, int j)
{

  double xi, yi, zi;
  double xj, yj, zj;

  double dx, dy, dz;
  double dist;
  double energy;

  xi=p[i].x;
  yi=p[i].y;
  zi=p[i].z;

  xj=p[j].x;
  yj=p[j].y;
  zj=p[j].z;

  dx=xi-xj;
  dy=yi-yj;
  dz=zi-zj;

  dist=sqrt( (dx*dx) + (dy*dy) + (dz*dz) );
  
  if(dist<=diameter){
    energy=(0.5*(dist-diameter)*(dist-diameter)); 
  }
  else{
    energy=0.0;
  }
  return energy;
}


//compute the interaction energy of the ith particle with the confining boundary
double boundary_energy(int i)
{
  double distance_from_boundary, energy;

  energy=0.0;
  distance_from_boundary=Rlim-p[i].r;
  if(distance_from_boundary<=diameter){
    energy+=0.5*(distance_from_boundary-diameter)*(distance_from_boundary-diameter);
  }
  return energy;  
}

//using the grid compute the total particle-particle energy of the system 
// --- this is used to compute the change in enefrgy due a change in angle
double total_particle_energy()
{
  int i;
  double E, energy;

  energy=0.0;
  for(i=1;i<=3*NUM_PAR;i++){
    E=0.5*penergy(i);
    energy+=E;
  }
 
  return energy;

}

double total_boundary_energy()
{
  int i;
  double E, energy;

  energy=0.0;
  for(i=1;i<=3*NUM_PAR;i++){
    E=boundary_energy(i);
    energy+=E;
  }
  
  return energy;
}

//////////////////////////////////////////////////////////////////////////////////////////
////////////////// FUNCTIONS TO MANAGE THE LOOK UP TABLE /////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

void start_table()
{
  int offset; 

  //COMPUTE GRID SIZE
  offset=(int)(-floor(XSTART/CELL_SIZE))+1; 
  NBOX_X=(int)(floor(XEND))+offset;
  
  offset=(int)(-floor(YSTART/CELL_SIZE))+1; 
  NBOX_Y=(int)(floor(YEND))+offset;
  
  offset=(int)(-floor(ZSTART/CELL_SIZE))+1; 
  NBOX_Z=(int)(floor(ZEND))+offset;
}

int ind(double x, double L, double start)
{  
  int i;
  double findex;
  int offset;

  offset=(int)(-floor(start/L))+1; // the range of the cartessian coordinates are from -Ve to +Ve
  //thus the offset is added onto the index to insure the start of the grid is at i=1.
  
  findex=x/L;
  i=(int)(floor(findex)) + offset;
  
  return i;
}

void build_linked_list (void)
{
  int i,j,k;
  //set the all head nodes on the grid equal to zero
  for(i=0;i<=NBOX_X+1;i++){
    for(j=0;j<=NBOX_Y+1;j++){
      for(k=0;k<=NBOX_Z+1;k++){
	  grid[i][j][k][0]=0;
	  grid[i][j][k][1]=0;
      }
    } 
  }
  //add each particle in turn to its linked list
  for(i=1;i<=3*NUM_PAR;i++){
    add(i);
  } 
}

//add particle i to the linked list
void add(i)
{

  int xind, yind, zind;
  int previous;

  xind=p[i].x_index;
  yind=p[i].y_index;
  zind=p[i].z_index;

  //if the grid is empty then modify the head node
  if(grid[xind][yind][zind][0]==0){
    grid[xind][yind][zind][0]=i; //this particle is the first
    grid[xind][yind][zind][1]=i; //and also last particle at this grid index

    p[i].head=0; // make a note that the ith particle is first in the list
    //p[i].tail=-1; //and that there are no other particles after it.
    p[i].tail=0;
  }//....otherwise....
  else{
    previous=grid[xind][yind][zind][1]; // this is the last particle at this site
    //printf("previous=%d\n", previous);
    p[i].head=previous;  // set the head of the new particle equal to the id of the previous one
    //p[i].tail=-1; // make this new particle the last one in the linked list
    p[i].tail=0;

    p[previous].tail=i; //change the tail of the previous particle so it now leads to the new one
    grid[xind][yind][zind][1]=i; // also note in the grid that this is the last particle now.
  }

}

//remove particle i from the linked list
void delete(i)
{
  int xind, yind, zind;
  int previous, subsequent;

  previous=p[i].head; // this is the particle before the one about to be deleted
  subsequent=p[i].tail; // this is the particle after the one to be deleted

  //check - is this particle at the head of a cell?
  if(previous==0){// if so then set the head to be the next particle
    xind=p[i].x_index;
    yind=p[i].y_index;
    zind=p[i].z_index;
    grid[xind][yind][zind][0]=subsequent;
  }

  //check - is this particle at the tail of a cell?
  if(subsequent==0){// if so then set the tail to be the previous
    xind=p[i].x_index;
    yind=p[i].y_index;
    zind=p[i].z_index;
    grid[xind][yind][zind][1]=previous;
  }

  //update the previous particle 
  p[previous].tail=subsequent;
  //update the subsequent particle
  p[subsequent].head=previous;

  //remove the particle from the list by setting its head and tail to zero
  p[i].head=0;
  p[i].tail=0;
}

void print_linked_list(void)
{

  int i,j,k,l;
  int N;

  //print out the state of the grid
  for(i=1;i<=NBOX_X;i++){
    for(j=1;j<=NBOX_Y;j++){
      for(k=1;k<=NBOX_Z;k++){
	printf("[%d][%d][%d]: %d", i, j, k, grid[i][j][k][0]);
	N=grid[i][j][k][0]; //number of elements in this linked list
	if(N!=0){//provided the current cell is not empty
	  for(l=1;l<=large_int;l++){
	    printf(" ([%d]--[%d]--[%d])  ", p[N].head, N, p[N].tail );
	    N=p[N].tail;
	    if(N==0){//stop when the end of the list has been reached
	      break;
	    }
	  }
	}
	printf("    %d\n", grid[i][j][k][1]);
      }
    } 
  }


}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////// SIMULATED ANNEALING FUNCTIONS ///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

//Assign initial random positions to N particles 
void initial_positions()
{
  int i;
  double pos1, pos2, pos3;
  double xcoord, ycoord, zcoord;
  double rand1;
  char filename[100];
  FILE *fpt;

  //load polar coordinates for particles
  fpt=fopen("seed", "r");
  fscanf(fpt, "%lf", &P);
  fscanf(fpt, "%d", &NUM_PAR);
  fscanf(fpt, "%lf", &Rlim);
  fscanf(fpt, "%lf", &Clength);
  for(i=1; i<=NUM_PAR; i++){
    fscanf(fpt, "%lf", &pos1);
    p[i].r=sqrt(pos1*pos1);
    fscanf(fpt, "%lf", &pos2);
    p[i].th=pos2;
    fscanf(fpt, "%lf", &pos3);
    p[i].z=pos3;
  } 
  fscanf(fpt, "%lf", &Angle);
  fclose(fpt);

  D=(2.0*Rlim)-1.0;
  //UClim=2*Clength;
  //LClim=Clim;

  //generate image particles - polar coordinates only
  for(i=1; i<=NUM_PAR; i++){
    p[(NUM_PAR)+i].r=p[i].r;
    p[(NUM_PAR)+i].th=(p[i].th)-Angle;
    p[(NUM_PAR)+i].z=(p[i].z) - Clength;

    p[(2*NUM_PAR)+i].r=p[i].r;
    p[(2*NUM_PAR)+i].th=(p[i].th)+Angle;
    p[(2*NUM_PAR)+i].z=(p[i].z) + Clength;   
  }

  //compute cartessian coordinates
  for(i=1; i<=3*NUM_PAR; i++){
    p[i].x=(p[i].r)*cos(p[i].th);
    p[i].y=(p[i].r)*sin(p[i].th);
  }

  //DEFINE CELL SIZE
  CELL_SIZE=diameter;  
  XSTART=-Rlim; XEND=Rlim; 
  YSTART=-Rlim; YEND=Rlim; 
  ZSTART=-UClim; ZEND=2*UClim;

  for(i=1;i<=3*NUM_PAR;i++){
    p[i].x_index=ind(p[i].x, CELL_SIZE, XSTART); // x coordinate, cell size, minimum x value of cell
    p[i].y_index=ind(p[i].y, CELL_SIZE, YSTART); // y coordinate, cell size, minimum y value of cell
    p[i].z_index=ind(p[i].z, CELL_SIZE, ZSTART); // z coordinate, cell size, minimum z value of cell
  }
  
}

// move particle i randomly
void move(int i, double A)
{
  double new_x, new_y, new_z, new_r, new_th;
  double Bx, By, Bz, Br, Bth;
  double Tx, Ty, Tz, Tr, Tth;
  double delta_x, delta_y, delta_z;
  double sx, sy, sz;
  double signX, signY, signZ;

  signX=ran2(&seed);
  signY=ran2(&seed);
  signZ=ran2(&seed);
  
  if(signX<0.5){sx=-1.0;}
  else{sx=1.0;}
  if(signY<0.5){sy=-1.0;}
  else{sy=1.0;}
  if(signZ<0.5){sz=-1.0;}
  else{sz=1.0;}
 
  delta_x=ran2(&seed);
  delta_y=ran2(&seed);
  delta_z=ran2(&seed);
 
  /* printf("A=%6.15f\n", A); */

/*   printf("old coordinates: x=%6.15f y=%6.15f z=%6.15f th=%6.15f r=%6.15f\n", p[i].x, p[i].y, p[i].z, p[i].th, p[i].r); */

  //move the particle a small distance in a random direction

  //delta_x=0.0; //TEMPORARY - REMOVE!!!!!
  //delta_y=0.0;
  new_x=(p[i].x)+(sx*A*delta_x);
  new_y=(p[i].y)+(sy*A*delta_y);


  new_z=(p[i].z)+(sz*A*delta_z);
  new_r=sqrt( (new_x*new_x) + (new_y*new_y) );
  new_th=atan2(new_y,new_x);


 /*  printf("new coordinates before BC: x=%6.15f y=%6.15f z=%6.15f th=%6.15f r=%6.15f\n", new_x, new_y, new_z, new_th, new_r); */

  //apply boundary conditions
  if(new_r>Rlim){
    new_r=Rlim;   
    //after imposing the boundary condition calculate 
    //the new new_x and new_y again - from new_r and new_th
    new_x=new_r*cos(new_th);
    new_y=new_r*sin(new_th);
  }
  new_z=modulo(new_z,Clength);
  
 /* printf("new coordinates after BC: x=%6.15f y=%6.15f z=%6.15f th=%6.15f r=%6.15f\n", new_x, new_y, new_z, new_th, new_r); */

  //remove particles (original, bottom image & top image) from previous linked list
  delete(i); delete((NUM_PAR)+i); delete((2*NUM_PAR)+i);

  //update coordinates of particle
  p[i].x=new_x;
  p[i].y=new_y;
  p[i].z=new_z;
  p[i].r=new_r;
  p[i].th=new_th;

  //update coordinates of the bottom image particle
  Br=p[i].r;
  Bth=modulo((p[i].th)-Angle,2*pi);
  Bz=p[i].z-Clength;
  Bx=Br*cos(Bth);
  By=Br*sin(Bth);

  p[(NUM_PAR)+i].r=Br;
  p[(NUM_PAR)+i].th=Bth;
  p[(NUM_PAR)+i].z=Bz;
  p[(NUM_PAR)+i].x=Bx;
  p[(NUM_PAR)+i].y=By;

  //update coordinates of the top image particle
  Tr=p[i].r;
  Tth=modulo((p[i].th)+Angle,2*pi);
  Tz=p[i].z+Clength;
  Tx=Tr*cos(Tth);
  Ty=Tr*sin(Tth);

  p[(2*NUM_PAR)+i].r=Tr;
  p[(2*NUM_PAR)+i].th=Tth;
  p[(2*NUM_PAR)+i].z=Tz;
  p[(2*NUM_PAR)+i].x=Tx;
  p[(2*NUM_PAR)+i].y=Ty;

  //update grid: original particle
  p[i].x_index=ind(p[i].x, CELL_SIZE, XSTART); // x coordinate, cell size, minimum x value of cell
  p[i].y_index=ind(p[i].y, CELL_SIZE, YSTART); // y coordinate, cell size, minimum y value of cell
  p[i].z_index=ind(p[i].z, CELL_SIZE, ZSTART); // z coordinate, cell size, minimum z value of cell

  //update grid: bottom image
  p[(NUM_PAR)+i].x_index=ind(p[(NUM_PAR)+i].x, CELL_SIZE, XSTART); // x coordinate, cell size, minimum x value of cell
  p[(NUM_PAR)+i].y_index=ind(p[(NUM_PAR)+i].y, CELL_SIZE, YSTART); // y coordinate, cell size, minimum y value of cell
  p[(NUM_PAR)+i].z_index=ind(p[(NUM_PAR)+i].z, CELL_SIZE, ZSTART); // z coordinate, cell size, minimum z value of cell

  //update grid: top image
  p[(2*NUM_PAR)+i].x_index=ind(p[(2*NUM_PAR)+i].x, CELL_SIZE, XSTART); // x coordinate, cell size, minimum x value of cell
  p[(2*NUM_PAR)+i].y_index=ind(p[(2*NUM_PAR)+i].y, CELL_SIZE, YSTART); // y coordinate, cell size, minimum y value of cell
  p[(2*NUM_PAR)+i].z_index=ind(p[(2*NUM_PAR)+i].z, CELL_SIZE, ZSTART); // z coordinate, cell size, minimum z value of cell

  //add particles (original, bottom image & top image) to new linked list
  add(i); add((NUM_PAR)+i); add((2*NUM_PAR)+i);
}

void change_angle(double A)
{
  int i, j, k;
  double signA, sign;
  double sa, delta_a;
  double new_angle;
  double newx, newy, newz, newth, newr;

  signA=ran2(&seed);
  if(signA<0.5){sa=-1.0;}
  else{sa=1.0;}
  delta_a=ran2(&seed);
  
  new_angle=Angle+(sa*A*delta_a);
  new_angle=modulo(new_angle, 2.0*pi); 
  if(new_angle<0.0)
    {
      new_angle=(2*pi)+new_angle;
    }  
  Angle=new_angle;

  //remove the particles from linked list
  for(i=1;i<=3*NUM_PAR;i++){
    delete(i);
  }

  //change the coordinates
  k=0;
  for(j=1;j<=3;j++){
    if(j==1){sign=0.0;}
    if(j==2){sign=-1.0;}
    if(j==3){sign=1.0;}
    for(i=1;i<=NUM_PAR;i++){
      k++;         
      newr=p[i].r;
      newth=modulo( (p[i].th) + (sign*Angle), 2*pi);
      newz=p[i].z + (sign*Clength);
      newx=newr*cos(newth);
      newy=newr*sin(newth);
      
      p[k].r=newr;
      p[k].th=newth;
      p[k].z=newz;
      p[k].x=newx;
      p[k].y=newy;
    }   
  }

  //update grid: image particles
  for(i=1;i<=3*NUM_PAR;i++){
    p[i].x_index=ind(p[i].x, CELL_SIZE, XSTART); // x coordinate, cell size, minimum x value of cell
    p[i].y_index=ind(p[i].y, CELL_SIZE, YSTART); // y coordinate, cell size, minimum y value of cell
    p[i].z_index=ind(p[i].z, CELL_SIZE, ZSTART); // z coordinate, cell size, minimum z value of cell
  }

  //update the grid
  for(i=1;i<=3*NUM_PAR;i++){
    add(i);
  }

}


void change_length(double A)
{

  int i, j, k;
  double signL, sign;
  double sl, delta_l;
  double new_length;
  double newx, newy, newz, newth, newr;
  
  signL=ran2(&seed);
  if(signL<0.5){sl=-1.0;}
  else{sl=1.0;}
  delta_l=ran2(&seed);

  new_length=Clength+(sl*A*delta_l);
  if(new_length<0.0)
    {
      new_length=LClim;
    }  
  if(new_length>UClim)
    {
      new_length=UClim;
    }  
  Clength=new_length;
  
 //remove the particles from linked list
  for(i=1;i<=3*NUM_PAR;i++){
    delete(i);
  }

  //change the coordinates
  k=0;
  for(i=1;i<=NUM_PAR;i++){  
    newr=p[i].r;
    newth=p[i].th;
    newz=modulo(p[i].z, Clength);
    newx=newr*cos(newth);
    newy=newr*sin(newth);
    
    k++;
    p[k].r=newr;
    p[k].th=newth;
    p[k].z=newz;
    p[k].x=newx;
    p[k].y=newy;
  }   
  for(i=1;i<=NUM_PAR;i++){
    k++;
    p[k].r=p[i].r;
    p[k].th=modulo( (p[i].th) - (Angle), 2*pi);
    p[k].z=p[i].z - (Clength);
    p[k].x=(p[k].r)*cos(p[k].th);
    p[k].y=(p[k].r)*sin(p[k].th);
  }
  for(i=1;i<=NUM_PAR;i++){
    k++;
    p[k].r=p[i].r;
    p[k].th=modulo( (p[i].th) + (Angle), 2*pi);
    p[k].z=p[i].z + (Clength);
    p[k].x=(p[k].r)*cos(p[k].th);
    p[k].y=(p[k].r)*sin(p[k].th);
  }


  
  
  //update grid: image particles
  for(i=1;i<=3*NUM_PAR;i++){
    p[i].x_index=ind(p[i].x, CELL_SIZE, XSTART); // x coordinate, cell size, minimum x value of cell
    p[i].y_index=ind(p[i].y, CELL_SIZE, YSTART); // y coordinate, cell size, minimum y value of cell
    p[i].z_index=ind(p[i].z, CELL_SIZE, ZSTART); // z coordinate, cell size, minimum z value of cell
  }
  
  //update the grid
  for(i=1;i<=3*NUM_PAR;i++){
    add(i);
  }

 

}

void MC(double T, double A)
{
  double indx;
  double old_energy, new_energy, dE;
  double bz, prob;
  int i, j;
  double xi, yi, zi, ri, thi;
  double Bx, By, Bz, Br, Bth;
  double Tx, Ty, Tz, Tr, Tth;
  double oldx[3000], oldy[3000], oldz[3000], oldr[3000], oldth[3000];
  double E1, E2, E3, E4, E5, E6, E7, E8;
  double volume, pressure;
  double old_angle, old_length;
  FILE *fpt;

  //pick a random particle 
  indx=ran2(&seed); 
  i=(int)(indx*(NUM_PAR+2.0)+1.0);

  //printf("i=%d\n", i);
  //fflush (stdin);
  //(void) getchar ();

  if(i<=NUM_PAR){ // Perform a MC move for a particle in the system

    //printf("i=%d\n", i);

    //store the old coordinates for the particle
    xi=p[i].x;
    yi=p[i].y;
    zi=p[i].z;
    ri=p[i].r;
    thi=p[i].th;

    //store the coordinates for the bottom image
    Br=p[(NUM_PAR)+i].r;
    Bth=p[(NUM_PAR)+i].th;
    Bz=p[(NUM_PAR)+i].z;
    Bx=p[(NUM_PAR)+i].x;
    By=p[(NUM_PAR)+i].y;

    //store the coordinates for the top image
    Tr=p[(2*NUM_PAR)+i].r;
    Tth=p[(2*NUM_PAR)+i].th;
    Tz=p[(2*NUM_PAR)+i].z;
    Tx=p[(2*NUM_PAR)+i].x;
    Ty=p[(2*NUM_PAR)+i].y;

    E1=penergy(i);
    E2=penergy((NUM_PAR)+i);
    E3=penergy((2*NUM_PAR)+i);
    E4=pairwise_energy(i, NUM_PAR+i);
    E5=pairwise_energy(i, (2*NUM_PAR)+i);
    E6=pairwise_energy(NUM_PAR+i, (2*NUM_PAR)+i);
    E7=boundary_energy(i)+boundary_energy( (NUM_PAR)+i )+boundary_energy( (2*NUM_PAR)+i );
    volume=pi*((D/2.0)*(D/2.0))*Clength;
    pressure=P*volume;
    E8=pressure;
    
    old_energy=E1+E2+E3-E4-E5-E6+E7+E8;
    //printf("\n\n\nOld Eneregy\n");

    /* printf("%6.15f\n", Clength); */
/*     for(j = 1; j <= 3*NUM_PAR; j++){ */
/*       printf("%d: %6.15f %6.15f %6.15f\n",j, p[j].r, p[j].th, p[j].z); */
/*     } */
/*     printf("%6.15f\n", Angle); */

/*     printf("E1=%6.15f E2=%6.15f E3=%6.15f\n", E1, E2, E3); */
/*     printf("E4=%6.15f E5=%6.15f E6=%6.15f\n", E4, E5, E6); */
/*     printf("E7=%6.15f\n", E7);     */
/*     printf("E8=%6.15f\n", E8);  */

    //move choosen particle
    move(i, A);

    E1=penergy(i);
    E2=penergy((NUM_PAR)+i);
    E3=penergy((2*NUM_PAR)+i);
    E4=pairwise_energy(i, NUM_PAR+i);
    E5=pairwise_energy(i, (2*NUM_PAR)+i);
    E6=pairwise_energy(NUM_PAR+i, (2*NUM_PAR)+i);
    E7=boundary_energy(i)+boundary_energy( (NUM_PAR)+i )+boundary_energy( (2*NUM_PAR)+i );
    volume=pi*((D/2.0)*(D/2.0))*Clength;
    pressure=P*volume;
    E8=pressure;
   
    new_energy=E1+E2+E3-E4-E5-E6+E7+E8;
   /*  printf("\nNew Eneregy\n"); */

    /* printf("%6.15f\n", Clength); */
/*     for(j = 1; j <= 3*NUM_PAR; j++){ */
/*       printf("%d: %6.15f %6.15f %6.15f\n", j, p[j].r, p[j].th, p[j].z); */
/*     } */
/*     printf("%6.15f\n", Angle); */

/*     printf("E1=%6.15f E2=%6.15f E3=%6.15f\n", E1, E2, E3); */
/*     printf("E4=%6.15f E5=%6.15f E6=%6.15f\n", E4, E5, E6); */
/*     printf("E7=%6.15f\n", E7);     */
/*     printf("E8=%6.15f\n", E8);  */
    
    dE=old_energy-new_energy;
   
    //compute the boltzman probability
    bz=exp((dE)/T);
    //roll the dice
    prob=ran2(&seed);
    
    if(prob<=bz){
      //accept 
      E-=dE;
      accept++;
    }
    else{    

      /* if(A<tiny){ */
/* 	printf("mismatch in PARTICLE energies dE=%6.6f old=%6.15f New=%6.15f\n", dE, old_energy, new_energy); */
/* 	fflush (stdin); */
/* 	(void) getchar (); */
/*       } */

      //reject
      reject++;
      dE=0.0;

      //remove particles from previous linked list
      delete(i); delete((NUM_PAR)+i); delete((2*NUM_PAR)+i);

      //return the particle to its original coordinates
      p[i].x=xi;
      p[i].y=yi;
      p[i].z=zi;
      p[i].r=ri;
      p[i].th=thi;

      //return bottom image to its original coordinates
      p[(NUM_PAR)+i].r=Br;
      p[(NUM_PAR)+i].th=Bth;
      p[(NUM_PAR)+i].z=Bz;
      p[(NUM_PAR)+i].x=Bx;
      p[(NUM_PAR)+i].y=By;

      //return top image to its original coodinates
      p[(2*NUM_PAR)+i].r=Tr;
      p[(2*NUM_PAR)+i].th=Tth;
      p[(2*NUM_PAR)+i].z=Tz;
      p[(2*NUM_PAR)+i].x=Tx;
      p[(2*NUM_PAR)+i].y=Ty;

      //update grid
      p[i].x_index=ind(p[i].x, CELL_SIZE, XSTART); // x coordinate, cell size, minimum x value of cell
      p[i].y_index=ind(p[i].y, CELL_SIZE, YSTART); // y coordinate, cell size, minimum y value of cell
      p[i].z_index=ind(p[i].z, CELL_SIZE, ZSTART); // z coordinate, cell size, minimum z value of cell

      p[(NUM_PAR)+i].x_index=ind(p[(NUM_PAR)+i].x, CELL_SIZE, XSTART); // x coordinate, cell size, minimum x value of cell
      p[(NUM_PAR)+i].y_index=ind(p[(NUM_PAR)+i].y, CELL_SIZE, YSTART); // y coordinate, cell size, minimum y value of cell
      p[(NUM_PAR)+i].z_index=ind(p[(NUM_PAR)+i].z, CELL_SIZE, ZSTART); // z coordinate, cell size, minimum z value of cell
      
      p[(2*NUM_PAR)+i].x_index=ind(p[(2*NUM_PAR)+i].x, CELL_SIZE, XSTART); // x coordinate, cell size, minimum x value of cell
      p[(2*NUM_PAR)+i].y_index=ind(p[(2*NUM_PAR)+i].y, CELL_SIZE, YSTART); // y coordinate, cell size, minimum y value of cell
      p[(2*NUM_PAR)+i].z_index=ind(p[(2*NUM_PAR)+i].z, CELL_SIZE, ZSTART); // z coordinate, cell size, minimum z value of cell

      //add particles to new linked list
      add(i); add((NUM_PAR)+i); add((2*NUM_PAR)+i);
    }
  }


  if(i==NUM_PAR+1){ // Perform a MC move for the angle
    
    //store the old angle
    old_angle=Angle;
    //compute the old energy of the system
    old_energy=0.0;
    old_energy+=total_particle_energy();
    volume=pi*((D/2.0)*(D/2.0))*Clength;
    pressure=P*volume;
    old_energy+=pressure;
      
    //store the old coordinates
    old_length=Clength;
    for(j=1;j<=3*NUM_PAR;j++){
      oldr[j] =p[j].r;
      oldth[j]=p[j].th;
      oldz[j] =p[j].z;
      oldx[j] =p[j].x;
      oldy[j] =p[j].y;
    }
    
    //change the twist angle
    change_angle(A);

    //compute the new energy of the system
    new_energy=0.0;
    new_energy+=total_particle_energy();
    volume=pi*((D/2.0)*(D/2.0))*Clength;
    pressure=P*volume;
    new_energy+=pressure;

    //difference in energy
    dE=old_energy-new_energy;

    //compute the boltzman probability
    bz=exp((dE)/T);
    //roll the dice
    prob=ran2(&seed);

    if(prob<=bz){
      //accept 
      E-=dE;
      accept++;
    }
    else{    
      //reject

      /* if(A<tiny){ */
/* 	printf("\nmismatch in Angle energies dE=%6.6f old=%6.15f New=%6.15f\n", dE, old_energy, new_energy); */
/* 	fflush (stdin); */
/* 	(void) getchar (); */
/*       } */

      reject++;
      dE=0.0;

      Angle=old_angle;
      Clength=old_length;
      for(j=1;j<=3*NUM_PAR;j++){
	delete(j);
      }
      //return particles to their original positions
      for(j=1;j<=3*NUM_PAR;j++){
	p[j].r=oldr[j];
	p[j].th=oldth[j];
	p[j].z=oldz[j];
	p[j].x=oldx[j];
	p[j].y=oldy[j];
      }
      //compute grid indices
      for(j=1;j<=3*NUM_PAR;j++){
	p[j].x_index=ind(p[j].x, CELL_SIZE, XSTART); // x coordinate, cell size, minimum x value of cell
	p[j].y_index=ind(p[j].y, CELL_SIZE, YSTART); // y coordinate, cell size, minimum y value of cell
	p[j].z_index=ind(p[j].z, CELL_SIZE, ZSTART); // z coordinate, cell size, minimum z value of cell
      } 
      //update grid
      for(j=1;j<=3*NUM_PAR;j++){
	add(j);
      }
    }
  }

  if(i==NUM_PAR+2){
    //printf("Change Length\n");

    //store the old length
    old_length=Clength;
    //compute the old energy of the system
    old_energy=0.0;
    old_energy+=total_particle_energy();
    volume=pi*((D/2.0)*(D/2.0))*Clength;
    pressure=P*volume;
    old_energy+=pressure;

    //store the old coordinates
    old_angle=Angle;
    for(j=1;j<=3*NUM_PAR;j++){
      oldr[j] =p[j].r;
      oldth[j]=p[j].th;
      oldz[j] =p[j].z;
      oldx[j] =p[j].x;
      oldy[j] =p[j].y;
    }

    //change the cylinder length
    change_length(A);
    
    //compute the new energy of the system
    new_energy=0.0;
    new_energy+=total_particle_energy();
    volume=pi*((D/2.0)*(D/2.0))*Clength;
    pressure=P*volume;
    new_energy+=pressure;

    //difference in energy
    dE=old_energy-new_energy;

    //compute the boltzman probability
    bz=exp((dE)/T);
    //roll the dice
    prob=ran2(&seed);

    if(prob<=bz){
      //accept 
      E-=dE;
      accept++;
    }


    else{    

      /* if(A<tiny){ */
/* 	printf("mismatch in LENGTH energies dE=%6.6f old=%6.15f New=%6.15f\n", dE, old_energy, new_energy); */
/* 	fflush (stdin); */
/* 	(void) getchar (); */
/*       } */


      //reject
      reject++;
      dE=0.0;
      
      Clength=old_length;
      Angle=old_angle;
      for(j=1;j<=3*NUM_PAR;j++){
	delete(j);
      }
      //return particles to their original positions
      for(j=1;j<=3*NUM_PAR;j++){
	p[j].r=oldr[j];
	p[j].th=oldth[j];
	p[j].z=oldz[j];
	p[j].x=oldx[j];
	p[j].y=oldy[j];
      }
      //compute grid indices
      for(j=1;j<=3*NUM_PAR;j++){
	p[j].x_index=ind(p[j].x, CELL_SIZE, XSTART); // x coordinate, cell size, minimum x value of cell
	p[j].y_index=ind(p[j].y, CELL_SIZE, YSTART); // y coordinate, cell size, minimum y value of cell
	p[j].z_index=ind(p[j].z, CELL_SIZE, ZSTART); // z coordinate, cell size, minimum z value of cell
      } 
      //update grid
      for(j=1;j<=3*NUM_PAR;j++){
	add(j);
      }
    }

  }

  //fpt=fopen("Clength", "a");
  //fprintf(fpt, "%6.15f\n",  Clength);
  //fclose(fpt);


}

double FindStepLength(double T)
{
  int i, j, t;
  double  A;
  double initialx[5000], initialy[5000], initialz[5000], initialr[5000], initialth[5000], initial_angle, initial_length, initialE;
  int initial_x_index[5000], initial_y_index[5000], initial_z_index[5000];
  int initial_head[5000], initial_tail[5000];
  int rescue;
  double probability[5];
 
  rescue=0;

  for( ; ; ){
    rescue++;

    if(rescue==100){
      Aval[1]=0.0;
      Aval[2]=large*0.5;
      Aval[3]=large;
      rescue=0;
    }
   


    //begin search
    for(j=1 ;j<=3; j++ ){
      A=Aval[j];
      
      //store the initial state of the system
      for(i=1;i<=3*NUM_PAR;i++){
	initialx[i]=p[i].x;
	initialy[i]=p[i].y;
	initialz[i]=p[i].z;
	initialr[i]=p[i].r;
	initialth[i]=p[i].th;
	
	initial_x_index[i]=p[i].x_index;
	initial_y_index[i]=p[i].y_index;
	initial_z_index[i]=p[i].z_index;
	
	initial_head[i]=p[i].head;
	initial_tail[i]=p[i].tail;
      }
      initial_angle=Angle;
      initial_length=Clength;
      initialE=E;
      
      //perform NTRIAL monte-carlo moves
      accept=0;
      reject=0;
      for(t=1;t<=NRUN_TRIAL; t++){
	//printf("Trial Step %d\n", t);
	MC(T, A);// temperature, steplength
      }
      
      //return system to initial state
      for(i=1;i<=3*NUM_PAR;i++){
	p[i].x=initialx[i];
	p[i].y=initialy[i];
	p[i].z=initialz[i];
	p[i].r=initialr[i];
	p[i].th=initialth[i];

	p[i].x_index=initial_x_index[i];
	p[i].y_index=initial_y_index[i];
	p[i].z_index=initial_z_index[i];
	
	p[i].head=initial_head[i];
	p[i].tail=initial_tail[i];
      }
      Angle=initial_angle;
      Clength=initial_length;
      E=initialE;
      
      //rebuild the linked list
      build_linked_list();
      
      probability[j]=(1.0*accept)/(1.0*NRUN_TRIAL);
      printf("Trial step length=%6.15f Success probability=%6.15f\n", Aval[j], probability[j]);
    }
    
    printf("Lower limit (%6.15f) ---- Actual(%6.15f)---Upper Limit(%6.15f)\n",acptN-tol, probability[2], acptN+tol);
    
    if( probability[2]<=acptN+tol && probability[2]>=acptN-tol) break;
    
    if(acptN<probability[1] &&  acptN>probability[2]){//upper half
      printf("old range %6.6f---%6.6f---%6.6f\n",Aval[1],Aval[2],Aval[3]);
      Aval[3]=Aval[2];
      Aval[2]=(Aval[1]+Aval[3])/2;
      printf("New range %6.6f---%6.6f---%6.6f\n",Aval[1],Aval[2],Aval[3]);
    }
    if(acptN<probability[2] &&  acptN>probability[3]){//lower half
      printf("old range %6.6f---%6.6f---%6.6f\n",Aval[1],Aval[2],Aval[3]);
      Aval[1]=Aval[2];
      Aval[2]=(Aval[1]+Aval[3])/2;
      printf("New range %6.6f---%6.6f---%6.6f\n",Aval[1],Aval[2],Aval[3]);
    }
    if(acptN<probability[1] &&  acptN<probability[3]){
      printf("****  INCREASE STEP LENGTH ****\n");
      printf("old range %6.6f---%6.6f---%6.6f\n",Aval[1],Aval[2],Aval[3]);
      Aval[3]+=0.1;
      printf("New range %6.6f---%6.6f---%6.6f\n",Aval[1],Aval[2],Aval[3]);
    }
    if(acptN>probability[1] &&  acptN>probability[3]){
      printf("****  DECREASE STEP LENGTH ****\n");
      printf("old range %6.6f---%6.6f---%6.6f\n",Aval[1],Aval[2],Aval[3]);
      Aval[1]-=0.1;
      if(Aval[1]<0){Aval[1]=0;}
      printf("New range %6.6f---%6.6f---%6.6f\n",Aval[1],Aval[2],Aval[3]);
    }
    printf("\n");
  }

  A=Aval[2];
  printf("Final length A =%6.15f\n\n\n\n\n\n", A);

  

  return A;
}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////// GENERATE OUTPUT  ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

void output()
{
  int i, j;
  /* input - output xxxxxxxxxxxxxxxxxxxxxxxxx */
  char filename[100];
  FILE *fpt;
  /* xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx */

  double xi, yi, zi;
  double xj, yj, zj;
  double dist;
  
  /*
  fpt=fopen("state.vmd", "w");
  for(i = 1; i <= NUM_PAR; i++){
    xi=(p[i].r)*cos(p[i].th);
    yi=(p[i].r)*sin(p[i].th);
    zi=p[i].z;

    fprintf(fpt, "draw color     1\n");
    fprintf(fpt, "draw sphere {    ");
    fprintf(fpt,"%6.15f %6.15f %6.15f}", xi, yi, zi );
    fprintf(fpt," radius        %6.6f", diameter/2.0);
    fprintf(fpt," resolution  20\n");
  }
  for(i = 1; i <= NUM_PAR; i++){
    xi=(p[i].r)*cos((p[i].th)+Angle);
    yi=(p[i].r)*sin((p[i].th)+Angle);
    zi=(p[i].z)+Clength;

    fprintf(fpt, "draw color     2\n");
    fprintf(fpt, "draw sphere {    ");
    fprintf(fpt,"%6.15f %6.15f %6.15f}", xi, yi, zi );
    fprintf(fpt," radius        %6.6f", diameter/2.0);
    fprintf(fpt," resolution  20\n");
  }
  for(i = 1; i <= NUM_PAR; i++){
    xi=(p[i].r)*cos((p[i].th)+(2*Angle));
    yi=(p[i].r)*sin((p[i].th)+(2*Angle));
    zi=(p[i].z)+(2*Clength);

    fprintf(fpt, "draw color     3\n");
    fprintf(fpt, "draw sphere {    ");
    fprintf(fpt,"%6.15f %6.15f %6.15f}", xi, yi, zi );
    fprintf(fpt," radius        %6.6f", diameter/2.0);
    fprintf(fpt," resolution  20\n");
  }
  for(i = 1; i <= NUM_PAR; i++){
    xi=(p[i].r)*cos((p[i].th)-Angle);
    yi=(p[i].r)*sin((p[i].th)-Angle);
    zi=(p[i].z)-Clength;
    
    fprintf(fpt, "draw color     4\n");
    fprintf(fpt, "draw sphere {    ");
    fprintf(fpt,"%6.15f %6.15f %6.15f}", xi, yi, zi );
    fprintf(fpt," radius        %6.6f", diameter/2.0);
    fprintf(fpt," resolution  20\n");
  }
  for(i = 1; i <= NUM_PAR; i++){
    xi=(p[i].r)*cos((p[i].th)-(2*Angle));
    yi=(p[i].r)*sin((p[i].th)-(2*Angle));
    zi=(p[i].z)-(2*Clength);
    
    fprintf(fpt, "draw color     5\n");
    fprintf(fpt, "draw sphere {    ");
    fprintf(fpt,"%6.15f %6.15f %6.15f}", xi, yi, zi );
    fprintf(fpt," radius        %6.6f", diameter/2.0);
    fprintf(fpt," resolution  20\n");
  }
  fclose(fpt);
  */

  fpt=fopen("sa_gpic", "w");  
  fprintf(fpt, "LIST\n");
  for(i = 1; i <= NUM_PAR; i++){
    xi=(p[i].r)*cos(p[i].th);
    yi=(p[i].r)*sin(p[i].th);
    zi=p[i].z;
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", diameter/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xi, zi, yi );
    fprintf(fpt,"}\n");
    

    xi=(p[i].r)*cos((p[i].th)+Angle);
    yi=(p[i].r)*sin((p[i].th)+Angle);
    zi=(p[i].z)+Clength;
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", diameter/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xi, zi, yi );
    fprintf(fpt,"}\n");
    
    xi=(p[i].r)*cos((p[i].th)-Angle);
    yi=(p[i].r)*sin((p[i].th)-Angle);
    zi=(p[i].z)-Clength;
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", diameter/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xi, zi, yi );
    fprintf(fpt,"}\n");

  }
  fclose(fpt);
  

  fpt=fopen("sa_out", "w");
  fprintf(fpt, "%6.15f\n", P);
  fprintf(fpt, "%d\n", NUM_PAR);
  fprintf(fpt, "%6.15f\n", Rlim);
  fprintf(fpt, "%6.15f\n", Clength);
  for(i = 1; i <= NUM_PAR; i++){
    xi=p[i].x;
    yi=p[i].y;
    zi=p[i].z;
    fprintf(fpt, "%6.15f %6.15f %6.15f\n", p[i].r, p[i].th, p[i].z);
  }
  fprintf(fpt, "%6.15f", Angle);
  fclose(fpt);

  fpt=fopen("SAEmatrix", "w");
  for(i = 1; i <= NUM_PAR; i++){
    xi=(p[i].r)*cos(p[i].th);
    yi=(p[i].r)*sin(p[i].th);
    zi=p[i].z;
    for(j = 1; j <= NUM_PAR; j++){
      xj=(p[j].r)*cos(p[j].th);
      yj=(p[j].r)*sin(p[j].th);
      zj=p[j].z;
      dist=sqrt( ((xi-xj)*(xi-xj)) + ((yi-yj)*(yi-yj)) + ((zi-zj)*(zi-zj)) );
      if(dist<=diameter){
	fprintf(fpt,"%6.5f ", dist);
      }
      if(dist>diameter){
	fprintf(fpt,"x.xxxxx ");
      }
    }
    fprintf(fpt,"  ");
    for(j = 1; j <= NUM_PAR; j++){
      xj=(p[j].r)*cos((p[j].th)+Angle);
      yj=(p[j].r)*sin((p[j].th)+Angle);
      zj=(p[j].z)+Clength;
      dist=sqrt( ((xi-xj)*(xi-xj)) + ((yi-yj)*(yi-yj)) + ((zi-zj)*(zi-zj)) );
      if(dist<=diameter){
	fprintf(fpt,"%6.5f ", dist);
      }
      if(dist>diameter){
	fprintf(fpt,"x.xxxxx ");
      }
    }
    fprintf(fpt,"  ");
    for(j = 1; j <= NUM_PAR; j++){
      xj=(p[j].r)*cos((p[j].th)-Angle);
      yj=(p[j].r)*sin((p[j].th)-Angle);
      zj=(p[j].z)-Clength;
      dist=sqrt( ((xi-xj)*(xi-xj)) + ((yi-yj)*(yi-yj)) + ((zi-zj)*(zi-zj)) );
      if(dist<=diameter){
	fprintf(fpt,"%6.5f ", dist);
      }
      if(dist>diameter){
	fprintf(fpt,"x.xxxxx ");
      }
    }
    fprintf(fpt, "\n");
  }
  fclose(fpt);
  
}

////////////////////////////////////////////////////////////////////////////////////////////
//////////////////    FUNCTIONS   //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

double modulo(double num,double limit)
{
  return num-floor(num/limit)*limit;
}


/*
fpt=fopen("state.vmd", "w");
  for(i = 1; i <= NUM_PAR; i++){
    xi=p[i].x;
    yi=p[i].y;
    zi=p[i].z;
    fprintf(fpt, "draw color     1\n");
    fprintf(fpt, "draw sphere {    ");
    fprintf(fpt,"%6.15f %6.15f %6.15f}", xi, yi, zi );
    fprintf(fpt," radius        %6.6f", diameter/2.0);
    fprintf(fpt," resolution  20\n");
  }
  for(i = NUM_PAR+1; i <= 2*NUM_PAR; i++){
    xi=p[i].x;
    yi=p[i].y;
    zi=p[i].z;
    fprintf(fpt, "draw color     2\n");
    fprintf(fpt, "draw sphere {    ");
    fprintf(fpt,"%6.15f %6.15f %6.15f}", xi, yi, zi );
    fprintf(fpt," radius        %6.6f", diameter/2.0);
    fprintf(fpt," resolution  20\n");
  }
  for(i = (2*NUM_PAR)+1; i <= 3*NUM_PAR; i++){
    xi=p[i].x;
    yi=p[i].y;
    zi=p[i].z;
    fprintf(fpt, "draw color     3\n");
    fprintf(fpt, "draw sphere {    ");
    fprintf(fpt,"%6.15f %6.15f %6.15f}", xi, yi, zi );
    fprintf(fpt," radius        %6.6f", diameter/2.0);
    fprintf(fpt," resolution  20\n");
  }
  fclose(fpt);


*/
