##!/bin/ksh

if [ -f "energylist" ]
then
rm energylist
fi 

for file in `\ls data*`
do

energy=`more $file|head -4`
echo $energy >> temp
energy=`awk '{print $4}' temp`
rm temp

filenum=`echo $file|awk -Fa '{print $3}'`
echo $energy $filenum >>energylist
done
more energylist|sort -g >sorted_energylist
#mv temp energylist

sed '1,3d' sorted_energylist > temp
mv temp sorted_energylist
