#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#define pi 3.141592653589793238462643383279502884
#define BS 1.0

#define Emax 100
#define L 1.0

double modulo(double num,double limit)
{
  return num-floor(num/limit)*limit;
}

int main (int argc, char *argv[])	
{

  int i, j, k;
  int t;
  int NUM_PAR, NT_images, NB_images;
  int counter, ecounter;
  int acc;

  double pos;
  double P, A, Rlim, initial_Clength, Clength;
  double r[5000], th[5000], z[5000];
  double Tr[5000], Tth[5000], Tz[5000];
  double Br[5000], Bth[5000], Bz[5000];
  double energy, fr[5000], fth[5000], fz[5000], fl, fA;
  double dr, dth, dz, dl, dA;
  double xo, yo, zo;
  double part1, part2, part3, Etemp;
  double dist, pre_dist;
  double cylinder, image, boundary, pressure, D;
  double volume;
  double distance_from_boundary;

  double xi, yi, zi;
  double xj, yj, zj;

  double ziprime, zjprime;
  double Bziprime, Bzjprime;
  double Tziprime, Tzjprime;
  double dlpart1, dlpart2;
  
  double time;

  int TLIM;
  double lambda, initial_lambda;

  char filename[100];
  FILE *fpt;

  sscanf(argv[1],"%lf",&initial_lambda);
  lambda=initial_lambda;
  sscanf(argv[2],"%d",&TLIM);
  
  ecounter=Emax-1;
  time=0.0;

  fpt=fopen("cylinder_seed", "r");
  fscanf(fpt, "%lf", &P);
  fscanf(fpt, "%d", &NUM_PAR);
  fscanf(fpt, "%lf", &Rlim);
  fscanf(fpt, "%lf", &initial_Clength);
  Clength=initial_Clength;
  for(i = 1; i <= NUM_PAR; i++){
    fscanf(fpt, "%lf", &pos);
    r[i]=pos;
    fscanf(fpt, "%lf", &pos);
    th[i]=pos;
    fscanf(fpt, "%lf", &pos);
    z[i]=pos;
  }
  fscanf(fpt, "%lf", &A);
  fclose(fpt);
  D=(2.0*Rlim)-1.0;

  for(t=0; t<=TLIM;t++){ 

    time=lambda*t;
    ecounter++;
   
    Clength=sqrt(Clength*Clength);
    for(i=1;i<=NUM_PAR;i++){
      //z[i]=modulo(z[i], Clength);
      if(z[i]>Clength){
	z[i]=Clength;
	fz[i]=0.0;
      }
      if(z[i]<0.0){
	z[i]=0.0;
	fz[i]=0.0;
      }
    }
    for(i=1;i<=NUM_PAR;i++){
      r[i]=sqrt(r[i]*r[i]);    
      if(r[i] > Rlim){
	r[i]=Rlim;
      }
    }
    A=modulo(A, 2*pi);

    ////////////////////////////////////////////////////////////////////////////
    //set energy and all forces to zero.
    ////////////////////////////////////////////////////////////////////////////
    energy=0.0;
    for(i=1;i<=NUM_PAR;i++){
      fr[i]=0.0; fth[i]=0.0; fz[i]=0.0;
    }
    fl=0.0;
    fA=0.0;
    ////////////////////////////////////////////////////////////////////////////
    

    ////////////////////////////////////////////////////////////////////////////
    //generate image particles
    ///////////////////////////////////////////////////////////////////////////
    //find all the particles at the bottom of the cylinder
    k=0;
    for (i = 1; i <= NUM_PAR; i++){
      if(z[i] < L){
	k++;
	Bz[k]=Clength+z[i];
	Bth[k]=modulo(th[i] + A, 2*pi);
	Br[k]=r[i];
      }
    }
    NB_images=k;
    //find all the particles at the top of the cylinder
    k=0;
    for (i = 1; i <= NUM_PAR; i++){
      if(z[i] > Clength - L){
	k++;
	Tz[k]=-(Clength - z[i]);
	Tth[k]=modulo(th[i] - A, 2*pi);
	Tr[k]=r[i];
      }
    }
    NT_images=k;
    //////////////////////////////////////////////////////////////////////////    
    
    //////////////////////////////////////////////////////////////////////////
    //comupte the energy of the system
    //////////////////////////////////////////////////////////////////////////
    energy=0.0;    

    cylinder=0.0;
    for (i = 1; i <= NUM_PAR; i++){
      for (j = 1; j <= NUM_PAR; j++){
	if(i != j ){
	  part1= (r[i]*r[i]) + (r[j]*r[j]) - ( 2*r[i]*r[j]*cos(th[i]-th[j]) );
	  part2= ( (z[i]*z[i]) + (z[j]*z[j]) - (2*z[i]*z[j]) );
	  pre_dist=part1+part2;
	  dist=sqrt(pre_dist);
	  if(dist<=L){
	    Etemp = 0.5*(0.5*(dist-L)*(dist-L)); 
	    cylinder+=Etemp;
	    //printf("cell energy=%d (%6.15f %6.15f %6.15f)--- %d (%6.15f %6.15f %6.15f): %6.15f\n", i, r[i], th[i], z[i], j, r[j], th[j], z[j],  Etemp);
	  }
	}
      }
    }
    //printf("\n");    

    image=0.0;
    for(i=1;i<=NUM_PAR;i++){
      for(j=1;j<=NB_images;j++){
	part1= (r[i]*r[i]) + (Br[j]*Br[j]) - ( 2*r[i]*Br[j]*cos(th[i]-Bth[j]) );
	part2= (z[i]*z[i]) + (Bz[j]*Bz[j]) - (2*z[i]*Bz[j]);
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
	if(dist<=L){
	  //Etemp = 0.5*(0.5*(dist-L)*(dist-L));
	  Etemp = (0.5*(dist-L)*(dist-L));
	  //printf("Bimage energy=%d (%6.15f %6.15f %6.15f)--- %d (%6.15f %6.15f %6.15f): %6.15f\n", i, r[i], th[i], z[i], j, Br[j], Bth[j], Bz[j],  Etemp);
	  image+=Etemp;
	}
      }
    }

    //printf("\n");    
    for(i=1;i<=NUM_PAR;i++){
      for(j=1;j<=NT_images;j++){
	part1= (r[i]*r[i]) + (Tr[j]*Tr[j]) - ( 2*r[i]*Tr[j]*cos(th[i]-Tth[j]) );
	part2= (z[i]*z[i]) + (Tz[j]*Tz[j]) - (2*z[i]*Tz[j]);
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
	if(dist<=L){
	  //Etemp = 0.5*(0.5*(dist-L)*(dist-L));
	  Etemp = (0.5*(dist-L)*(dist-L));
	  //printf("Timage energy=%d (%6.15f %6.15f %6.15f)--- %d (%6.15f %6.15f %6.15f): %6.15f\n", i, r[i], th[i], z[i], j, Tr[j], Tth[j], Tz[j],  Etemp);
	  image+=Etemp;
	}
      }
    }
    //printf("\n");    
    
    boundary=0.0;
    for(i=1; i<=NUM_PAR; i++){
      dist=r[i];
      distance_from_boundary=Rlim-dist;
      if(distance_from_boundary<=L){
	Etemp=BS*0.5*(distance_from_boundary-L)*(distance_from_boundary-L);
	boundary+=Etemp;
	//printf("boundary energy=%d (%6.15f %6.15f %6.15f): %6.15f\n", i, r[i], th[i], z[i],  Etemp);
      }
    }
    //printf("\n");    

    pressure=0.0;
    volume=pi*((D/2.0)*(D/2.0))*Clength;
    pressure=P*volume;
    
    energy = cylinder+(0.5*image)+boundary+pressure;
    //printf("cylinder=%6.15f image=%6.15f boundary=%6.15f pressure=%6.15f\n", cylinder, image, boundary, pressure);  
    //printf("Total Eneregy=%6.15f Energy per particles=%6.15f\n", energy, energy/(NUM_PAR*1.0));   
    //fflush (stdin);
    //(void) getchar ();

    //////////////////////////////////////////////////////////////////////////
    //comupte the force on each particle
    //////////////////////////////////////////////////////////////////////////

    
    for(i=1;i<= NUM_PAR;i++){
      for(j=1;j<= NUM_PAR;j++){
	if(i != j){
	  	  
	  part1= (r[i]*r[i]) + (r[j]*r[j]) - ( 2*r[i]*r[j]*cos(th[i]-th[j]) );	  
	  part2=( (z[i]*z[i]) + (z[j]*z[j]) - (2*z[i]*z[j]) );
	  pre_dist=part1+part2;
	  dist=sqrt(pre_dist);
	  
	  if(dist<=L){
	    part3=((dist-L)/dist);
	    dr=0.5*( r[i] - (  r[j]*(cos(th[i]-th[j]))  ) )*part3;
	    fr[i]-=dr; 
	    dth=0.5*(r[i]*r[j])*sin(th[i]-th[j])*part3;
	    fth[i]-=dth; 
	    dz=0.5*(z[i]-z[j])*part3;
	    fz[i]-=dz; 
	  }
	}     
      }
    }

    for(i=1;i<=NUM_PAR;i++){
      for(j=1;j<=NB_images;j++){
	
	part1= (r[i]*r[i]) + (Br[j]*Br[j]) - ( 2*r[i]*Br[j]*cos(th[i]-Bth[j]) );
	part2= ( (z[i]*z[i]) + (Bz[j]*Bz[j]) - (2*z[i]*Bz[j]) );
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
	
	if(dist<=L){
	  part3=( (dist-L)/dist );
	  //part3=( (dist-L)/dist );
	  dr = 0.5*( r[i]  -   (  Br[j]*(cos(th[i]-Bth[j]))  ) )*part3;
	  fr[i] -= dr; //Since the image particles themselves do NOT contribute to the energy ther isn't a factor of 2 here
	  dth=0.5*(r[i]*Br[j])*sin(th[i]-Bth[j])*part3;
	  fth[i] -= dth; //same as above
	  dz=0.5*(z[i]-Bz[j])*part3;
	  fz[i] -=dz; //and again
	  dA=-dth;
	  fA-= dA;
	  dl=-dz;   
	  fl -= dl;
	}
      }
    }    
    for(i=1;i<=NUM_PAR;i++){
      for(j=1;j<=NT_images;j++){
		
	part1= (r[i]*r[i]) + (Tr[j]*Tr[j]) - ( 2*r[i]*Tr[j]*cos(th[i]-Tth[j]) );
	part2= (z[i]*z[i]) + (Tz[j]*Tz[j]) - (2*z[i]*Tz[j]);
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
		
	if(dist<=L){
	  part3=(dist-L)/dist;
	  //part3=(dist-L)/dist;
	  dr= 0.5*( r[i]  -   (  Tr[j]*(cos(th[i]-Tth[j]))  ) )*part3;
	  fr[i] -= dr;
	  dth=0.5*(r[i]*Tr[j])*sin(th[i]-Tth[j])*part3;
	  fth[i] -= dth;
	  dz=0.5*(z[i]-Tz[j])*part3;	
	  fz[i] -= dz;
	  dA=dth;
	  fA -= dA;
	  dl= dz;
	  fl -= dl;
	}
      }
    }
    //printf("particle fl=%6.15f\n", fl);

    dl=P*pi*((D/2.0)*(D/2.0));
    //printf("pressure fl=%6.15f\n", -dl);
    fl-=dl;
    //printf("total fl=%6.15f\n", fl);

    //compute derivative due to boundary
    for(i=1; i<=NUM_PAR; i++){
      dist=r[i];
      distance_from_boundary=Rlim-dist;
      if(distance_from_boundary<=L){
	dr=-(BS)*(distance_from_boundary-L);
	fr[i]-=dr;
      }
    }
    
    for(i=1;i<=NUM_PAR;i++){
      r[i]=r[i]+(lambda*fr[i]);
      th[i]=th[i]+(lambda*fth[i]);
      z[i]=z[i]+(lambda*fz[i]);      
    }
    A=A+(lambda*fA);
    Clength=Clength+(lambda*fl);
    //printf("Clength=%6.15f\n\n", Clength);

    Clength=sqrt(Clength*Clength);
    for(i=1;i<=NUM_PAR;i++){
      //z[i]=modulo(z[i], Clength);
      if(z[i]>Clength){
	z[i]=Clength;
	fz[i]=0.0;
      }
      if(z[i]<0.0){
	z[i]=0.0;
	fz[i]=0.0;
      }
    }
    for(i=1;i<=NUM_PAR;i++){
      r[i]=sqrt(r[i]*r[i]);    
      if(r[i] > Rlim){
	r[i]=Rlim;
      }
    }
    A=modulo(A, 2*pi);

    
    //printf("time=%d energy=%6.15f\n", t, energy);
    //for(i=1;i<=NUM_PAR;i++){
    //printf("%d: r=%6.15f [fr=%6.15f] th=%6.15f [fth=%6.15f] z=%6.15f [fz=%6.15f]\n", i, r[i], fr[i], th[i], fth[i], z[i], fz[i]);
    //}
    //printf("A=%6.15f [fA=%6.15f]\n", A, fA);
    //printf("l=%6.15f [fl=%6.15f]\n", Clength, fl);
        


    //fflush (stdin);
    //(void) getchar ();
    

    //fflush (stdin);
    //(void) getchar ();

    if(ecounter==Emax){
      ecounter=0;
      fpt=fopen("energy", "a");
      fprintf(fpt, "%6.15f %6.15f\n", time, energy/(1.0*NUM_PAR));
      fclose(fpt);
    }
    
  }

  fpt=fopen("gradient_descent_energy", "w");
  fprintf(fpt, "%6.15f\n",energy/(1.0*NUM_PAR));
  fclose(fpt);

  fpt=fopen("DATA", "w");
  fprintf(fpt, "%6.15f\n",energy/(1.0*NUM_PAR));
  fprintf(fpt, "%6.15f\n",P);
  fprintf(fpt, "%d\n", NUM_PAR);
  fprintf(fpt, "%6.15f\n", Rlim);
  fprintf(fpt, "%6.15f\n", Clength);
  for(i = 1; i <= NUM_PAR; i++){
    fprintf(fpt, "%6.15f %6.15f %6.15f\n",r[i], th[i], z[i]);
  }
  fprintf(fpt, "%6.15f\n", A);
  fclose(fpt);
  
  fpt=fopen("cg_gpic", "w");  
  fprintf(fpt, "LIST\n");
  for(i = 1; i <= NUM_PAR; i++){
    xo=r[i]*cos(th[i]);
    yo=r[i]*sin(th[i]);
    zo=z[i];
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", L/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fpt,"}\n");
    
    xo=r[i]*cos(th[i]+A);
    yo=r[i]*sin(th[i]+A);
    zo=Clength+z[i];
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", L/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fpt,"}\n");
    
    xo=r[i]*cos(th[i]-A);
    yo=r[i]*sin(th[i]-A);
    zo=-(Clength - z[i]);
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", L/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fpt,"}\n");

  }
  fclose(fpt);

  fpt=fopen("CGEmatrix", "w");
  for(i = 1; i <= NUM_PAR; i++){
    xi=r[i]*cos(th[i]);
    yi=r[i]*sin(th[i]);
    zi=z[i];
    for(j = 1; j <= NUM_PAR; j++){
      xj=r[j]*cos(th[j]);
      yj=r[j]*sin(th[j]);
      zj=z[j];
      dist=sqrt( ((xi-xj)*(xi-xj)) + ((yi-yj)*(yi-yj)) + ((zi-zj)*(zi-zj)) );
      if(dist<=L){
	fprintf(fpt,"%6.15f ", dist);
      }
      if(dist>L){
	fprintf(fpt,"x.xxxxxxxxxxxxxxx ");
      }
    }
    fprintf(fpt,"  ");
    for(j = 1; j <= NUM_PAR; j++){      
      xj=r[j]*cos(th[j]+A);
      yj=r[j]*sin(th[j]+A);
      zj=Clength+z[j];
      dist=sqrt( ((xi-xj)*(xi-xj)) + ((yi-yj)*(yi-yj)) + ((zi-zj)*(zi-zj)) );
      if(dist<=L){
	fprintf(fpt,"%6.15f ", dist);
      }
      if(dist>L){
	fprintf(fpt,"x.xxxxxxxxxxxxxxx ");
      }
    }
    fprintf(fpt,"  ");
    for(j = 1; j <= NUM_PAR; j++){
      xj=r[j]*cos(th[j]-A);
      yj=r[j]*sin(th[j]-A);
      zj=-(Clength-z[j]);
      dist=sqrt( ((xi-xj)*(xi-xj)) + ((yi-yj)*(yi-yj)) + ((zi-zj)*(zi-zj)) );
     if(dist<=L){
	fprintf(fpt,"%6.15f ", dist);
      }
      if(dist>L){
	fprintf(fpt,"x.xxxxxxxxxxxxxxx ");
      } 
    }
    fprintf(fpt, "\n");
  }
  fclose(fpt);



}

