##!/bin/ksh

if [ -f "energylist" ]
then
rm energylist
fi 

for file in `\ls data*`
do
energy=`more $file|head -1`
filenum=`echo $file|awk -Fa '{print $3}'`
echo $energy $filenum >>energylist
done
more energylist|sort -g >temp
#mv temp energylist
mv temp sorted_energylist
