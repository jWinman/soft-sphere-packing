import numpy as np
import matplotlib.pyplot as plt

_, ratio26, phi26, _ = np.loadtxt("PackingRatios/packing_2_6.csv", unpack=True, delimiter=",")
_, ratio, phi, _ = np.loadtxt("PackingRatios/packing.csv", unpack=True, delimiter=",")

ratioAdil, phiAdil = np.loadtxt("PackingRatios/VF_graph_xmgrace_file_2", unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(ratio, phi, "rD")
ax.plot(ratio26, phi26, "rD", label="Basin Hopping")
ax.plot(ratioAdil, phiAdil, "b-", label="Mughal's simulated annealing")

ax.set_xlim(ratioAdil[0], ratioAdil[-1])
ax.set_ylim(.32, 0.56)
#ax.set_xlim(min(ratioAdil), max(ratioAdil))
#ax.set_ylim(min(phiAdil) - 0.01, max(phiAdil) + 0.01)
ax.legend(loc="best")
ax.grid()

ax.set_xlabel("diameter ratio $D / d$")
ax.set_ylabel("packing fraction $\phi$")

fig.savefig("Plots/packingRatio.pdf")
