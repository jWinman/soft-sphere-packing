# -*- coding: utf-8 -*-
"""
Created on Sun Oct 04 12:49:03 2015

@author: Shaun
"""

import numpy as np
from scipy.optimize import minimize
import loop_functions_2_6 as lf
from scipy.spatial.distance import pdist
import functools as ft
import pandas as pd

def Pot(r):
    #the sphere interaction, currently goes to 0 at r=1
    return 500*(2-r)* ((1.0/r) ** 12 - 2 * (1.0/r) ** 6)
    


r_min = lf.minimum(Pot,1)                   #finds mind of potential
radius = r_min * 0.5                        #radius of sphere
rc=1.0                                      #cut off value for the potential



def Total_Pot(x, R, z, theta):
    #Takes in 1d array of [x1,y1,z1,x2,y2,z2..,theta] and computes the total potential
    
    theta = x[-1]
    x = np.delete(x, -1)
    
    b = np.reshape(x,(np.shape(state)[0],3))
    r=np.sqrt(b[:,0]**2 + b[:,1]**2)
    
    b = lf.ghost_array(b,z,theta)
    rp=pdist(b)    
    
    cylinder_pot=np.where(r+radius <= R, 0.0, np.exp(50*(r + radius -R))-1)
    sphere_pot=np.where(rp <= rc, Pot(rp), 0.0)
    
    return  sum(sphere_pot) + sum(cylinder_pot)
    

    
def Minimization(x, theta, R, z):
    #Takes Cartesian guess array then minimizes and returns array
    x0=(np.reshape(x,(np.shape(x)[0],3))).ravel()
    x0=np.append(x0,theta)
    
    bnds = [(None,None), (None,None), (0,z)] * (np.shape(state)[0])
    bnds.append((None,None))
    Pot = ft.partial(Total_Pot, theta = theta, R = R, z = z)
    
    res = minimize(Pot, x0, method='L-BFGS-B', bounds=bnds, tol= 10**(-11)).x
    
    alpha = res[-1]
    res = np.delete(res, -1)
    a=np.reshape(res,(np.shape(x)[0],3))
    return a, alpha
    




for k in range(10,11):
    
    df = pd.read_csv('packing_2_6.csv')
    spheres = k+1
    print spheres
    
    #state = lf.initial_state_c_2(0.9,radius, spheres)
    #state = lf.ran_ini(0.9,spheres)

    for j in range(0,21):
        
        theta = np.pi/spheres        
        z = 0.4
        ratio = 2.6 + ((0.4 * float(j))/20)
        R = ratio * radius
        print ratio
        
        #state = lf.initial_state_c(R,radius, spheres)
        state = lf.ran_ini(R,spheres)
        #state = lf.initial_state_c_3(0.9,radius, spheres)
        
        for i in range(0,4):
            #state = lf.ran_ini(0.9,spheres)
            print i
            d = 0.1 * 1.0/(10 ** i)
            cond = True
            while cond:
                    
                for i in range(0,5):
                    state, theta =Minimization(state, theta, R, z)
                    
            
            
                    
                #Overlap conditions to break loop, a is overlap between spheres, b is overlap between boundary and spheres    
                a = np.any(np.where(pdist(lf.ghost_array(state,z,theta))+0.0005 <r_min  , True, False))
                b = np.any(np.where(R - (np.sqrt(state[:,0]**2 + state[:,1]**2) + radius) <-0.0005,True, False))                   
                cond = (a or b)
                z += d
                if z > 3.0:
                    break
                
            z -= 2.0 * d
            h = z + d
            
            if z > 3.0 - (2.0 * d):
                print 'failed'
                break
            
            
        if cond == False:
            #comparing previous packing fraction values to newly obtained ones and overwriting if it is better
            if df.loc[j]['packing'] < float(lf.packing_ratio(radius,R,h,spheres))-0.00001:
                
                df.loc[j] = k+1, ratio,  float(lf.packing_ratio(radius,R,h,spheres)), theta
                lf.save(state,R,radius,h,theta,j)

    #write datastructure to the file   
    df.to_csv('packing_2_6.csv', index=False)
            
    
        
lf.packing_plot(20)
        
    






















