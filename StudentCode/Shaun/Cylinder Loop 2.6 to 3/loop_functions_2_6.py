# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 16:16:15 2015

@author: Shaun
"""

import numpy as np
from scipy.optimize import fmin
import mayavi.mlab as mlab
import matplotlib.pyplot as plt
import pandas as pd
mlab.options.offscreen = True



def pol_to_cart(polar_array):
    # Converts the polar array of the form [[r1,theta1,z1],[r2,theta2,z2],...] to a cartesian array of the form [[x1,y1,z1],[x2,y2,z2],...]
    x= polar_array[:,0] * np.cos(polar_array[:,1])
    y= polar_array[:,0] * np.sin(polar_array[:,1])
    z= polar_array[:,2]
    return np.transpose(np.vstack((x,y,z)))
    
    

def ghost_array(X,z,theta):
    #creates image array of the original array at +/- z with twist theta
    a=X.copy()
    a[:,2] +=z
    rotMatrix = np.array([[np.cos(theta),np.sin(theta),0],[-1.0*np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        a[i,:] = np.dot(rotMatrix,a[i,:])
    c=X.copy()
    c[:,2] -=z
    rotMatrix_inv = np.array([[np.cos(theta),-1.0*np.sin(theta),0],[np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        c[i,:] = np.dot(rotMatrix_inv,c[i,:])
    b = np.concatenate((np.concatenate((a,X)),c))
    return b
    
def plot_array(X,z,theta):
    #creates image arrays at +/- z with twist theta
    a=X.copy()
    a[:,2] +=z
    rotMatrix = np.array([[np.cos(theta),np.sin(theta),0],[-1.0*np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        a[i,:] = np.dot(rotMatrix,a[i,:])
    c=X.copy()
    c[:,2] -=z
    rotMatrix_inv = np.array([[np.cos(theta),-1.0*np.sin(theta),0],[np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        c[i,:] = np.dot(rotMatrix_inv,c[i,:])
    b = np.concatenate((a,c))
    return b
    
    
def minimum(V,x):
    #Finding the radius for a given potential V and trial guess x
    x0=x
    res = fmin(V,x0,xtol=0.0001,ftol=0.0001,maxiter=None, maxfun=None)
    return res


#State initialisers of various kinds

def ran_ini(R, N):
    a=np.random.rand(N,3)
    a[:,0] = a[:,0] * R
    a[:,1] = a[:,1] * np.pi
    a[:,2] = 0.1
    
    return pol_to_cart(a)
    
def ran_ini_2(R, N):
    a=np.random.rand(N,3)
    a[:,0] = a[:,0] * R
    a[:,1] = a[:,1] * 2 * np.pi
    a[:,2] = 0.1
    a[0,:] = [0.,0.,0.05]
    
    return pol_to_cart(a)

    
def initial_state(r,N):
    #z is height, R is cylinder radius, r is the sphere radius, N is number of spheres
    a=np.zeros((N,3))
    a[:,1]=0
    for i in range(0,N):
        if (i % 2.) == 0.0:
            a[i,0]= 0.4
        else:
            a[i,0]=-0.4
        a[i,2]= (r-0.2)*(1+i)
        
    return a
    

def initial_state_c_2(R,r,N):
    #z is height, R is cylinder radius, r is the sphere radius, N is number of spheres
    a=np.ones((N,3),float)
    a[0,:] = [0.,0.,0.1]
    a[1:,0]=(R-r)
    a[1:,2]= 0.1
    for i in range (N-1):
        a[i+1,1]= ((2*np.pi) /float(N-1)) * float(i)
    a=pol_to_cart(a)
    return a
    
def initial_state_c(R,r,N):
    #z is height, R is cylinder radius, r is the sphere radius, N is number of spheres
    a=np.ones((N,3),float)
    a[:,0]=(R-r)
    a[:,2]= 0.1
    for i in range (N):
        a[i,1]= ((2*np.pi) /float(N)) * float(i)
    a=pol_to_cart(a)
    return a
    
def initial_state_c_3(R,r,N):
    #z is height, R is cylinder radius, r is the sphere radius, N is number of spheres
    a=np.ones((N,3),float)
    a[0,:] = [0.,0.,0.1]
    a[1:,0]=(R-r)
    a[1:,2]= 0.1
    for i in range (N-1):
        if (i % 2.) == 0.0:
            a[i+1,1]= ((2*np.pi) /float(N-1)) * float(i)
            a[i+1:,2]= 0.1
        else:
            a[i+1,1]= ((2*np.pi) /float(N-1)) * float(i) + np.pi/float(N-1)
            a[i+1:,2]= 0.3
    a=pol_to_cart(a)
    return a
    
    
def sphere(x, y, z):

    return x**2 + y**2
    
        
def save(b,R,r,d,theta,l):
    #b is the state, R is the cylinder radius, r is sphere radius, d is the height
    x = b[:,0]
    y = b[:,1]
    z = b[:,2]
    s = 2.0 * r * np.ones(len(x))
    
    c = plot_array(b,d,theta)    
    
    x1 = c[:,0]
    y1 = c[:,1]
    z1 = c[:,2]
    s1 = 2.0 * r * np.ones(len(x1))
    
    f = np.mean(z) - d/2.
    a = np.mean(z) + d/2.
    
    h1 = f - d
    h2 = a + d

    

    mlab.figure(figure=1, bgcolor=(1, 1, 1), size=(1000, 1000))
    #cylinder
    xS, yS, zS = np.mgrid[-R:R:64j, -R:R:64j, h1:h2:64j]
    mlab.contour3d(xS, yS, zS, sphere, opacity=0.5)  
    #spheres
    mlab.points3d(x, y, z, s, resolution=40, color=(0, 0, 1), scale_factor=1)
    mlab.points3d(x1, y1, z1, s1, resolution=40, color=(1, 0, 0), scale_factor=1)
    
    N = np.shape(b)[0]
    ratio = R / r
    mlab.savefig(str(l)+ "_Spheres_" +str(N) + "_Ratio_" + str(float(ratio))+ "_Packing_"+str(float(packing_ratio(r,R,d,N))) +".png")
    mlab.close()
    
    
    
    
def packing_ratio(r,R,h,N):
    #r is sphere radius, R cylinder radius, h is cylinder height, N is number of spheres
    vol_sphere = 4.0/3.0 * np.pi * (r ** 3)
    vol_cyl = np.pi * (R ** 2) *  h
    return (N * vol_sphere)/(vol_cyl)
    

def packing_plot(N):
    #Generates Packing vs Ratio and Twist vs Ratio from packing.csv file
    d = {}
    plt.figure(1)
    
    df = pd.read_csv('packing_2_6.csv')
    a = len(df.index)
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    for i in range(1,N+1):
        
        d["x" + str(i)]=[]
        d["y" + str(i)]=[]
        d["z" + str(i)]=[]
        
        for j in range(0,a):
        
            if df.loc[j]['Number'] == i:
                
                d["x" + str(i)].append(df.loc[j]['ratio'])
                d["y" + str(i)].append(df.loc[j]['packing'])
                d["z" + str(i)].append(df.loc[j]['theta']% (np.pi -0.001))
                
        
                
        if not not d["x" + str(i)]:
            plt.plot(d["x" + str(i)],d["y" + str(i)], 'o', label='Sphere Number: '+ str(i))

	
    plt.legend(loc='4')
    plt.xlabel(r"$\frac{R}{r}$", fontsize=16)
    plt.ylabel(r"$\displaystyle\Phi$", fontsize=20)
    plt.axis([2.6,3.01,0.45,0.7])

    
    plt.figure(3)
    
    for i in range(1,N+1):
        if not not d["x" + str(i)]:
            plt.plot(d["x" + str(i)],d["z" + str(i)], 'o', label='Sphere Number: '+ str(i))
    
    
    
    plt.legend()
    plt.xlabel(r"$\frac{R}{r}$",fontsize=16)
    plt.ylabel(r"$\displaystyle\alpha$",fontsize=20)
    plt.axis([2.6,3.01,0.0,3.146])
    
    
    plt.show()
    
packing_plot(20)
    
    
 
def dataset(N,x,y,z):
    packing = zip(N,x,y,z)
    f = pd.DataFrame(data = packing, columns=['Number', 'ratio', 'packing', 'theta'])
    f.to_csv('packing_2_6.csv', index=False)
    


