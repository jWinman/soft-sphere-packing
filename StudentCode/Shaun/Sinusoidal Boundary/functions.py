# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 16:16:15 2015

@author: Shaun
"""

import numpy as np
from scipy.optimize import fmin
import mayavi.mlab as mlab
import functools as ft
from scipy.optimize import fsolve
from scipy.integrate import quad



def pol_to_cart(polar_array):
    # Converts the polar array of the form [[r1,theta1,z1],[r2,theta2,z2],...] to a cartesian array of the form [[x1,y1,z1],[x2,y2,z2],...]
    x= polar_array[:,0] * np.cos(polar_array[:,1])
    y= polar_array[:,0] * np.sin(polar_array[:,1])
    z= polar_array[:,2]
    return np.transpose(np.vstack((x,y,z)))
    
    

def ghost_array(X,z,theta):
    #creates image array of the original array at +/- z with twist theta
    a=X.copy()
    a[:,2] +=z
    rotMatrix = np.array([[np.cos(theta),np.sin(theta),0],[-1.0*np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        a[i,:] = np.dot(rotMatrix,a[i,:])
    c=X.copy()
    c[:,2] -=z
    rotMatrix_inv = np.array([[np.cos(theta),-1.0*np.sin(theta),0],[np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        c[i,:] = np.dot(rotMatrix_inv,c[i,:])
    b = np.concatenate((np.concatenate((a,X)),c))
    return b
    
def plot_array(X,z,theta):
    #creates image arrays at +/- z with twist theta
    a=X.copy()
    a[:,2] +=z
    rotMatrix = np.array([[np.cos(theta),np.sin(theta),0],[-1.0*np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        a[i,:] = np.dot(rotMatrix,a[i,:])
    c=X.copy()
    c[:,2] -=z
    rotMatrix_inv = np.array([[np.cos(theta),-1.0*np.sin(theta),0],[np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        c[i,:] = np.dot(rotMatrix_inv,c[i,:])
    b = np.concatenate((a,c))
    return b
    
    
def minimum(V,x):
    #Finding the radius for a given potential V and trial guess x
    x0=x
    res = fmin(V,x0,xtol=0.0001,ftol=0.0001,maxiter=None, maxfun=None)
    return res
    
    
def initial_state(r,N):
    #z is height, R is cylinder radius, r is the sphere radius, N is number of spheres
    a=np.zeros((N,3))
    a[:,1]=0
    for i in range(0,N):
        if (i % 2.) == 0.0:
            a[i,0]= 0.4
        else:
            a[i,0]=-0.4
        a[i,2]= (r-0.2)*(1+i)
        
    return a
    
def ran_ini(R, N):
    a=np.random.rand(N,3)
    a[:,0] = a[:,0] * R
    a[:,1] = a[:,1] * 2 * np.pi
    a[:,2] = 0.1
    
    return pol_to_cart(a)
    
    
    
def cosine(z,Rmin,Rmax,h):
    #Equation of a cos wave in (r,z) plane with wavelength h, r = Rmin at z=0

    return (Rmax+Rmin)/2.0 - (Rmax-Rmin)/2.0 * np.cos((2.0 * np.pi * z)/h)
    
def cosine_squared(z,Rmin,Rmax,h):
    #Squared Equation of a cos wave in (r,z) plane with wavelength h, r = Rmin at z=0 for integration

    return ((Rmax+Rmin)/2.0 - (Rmax-Rmin)/2.0 * np.cos((2.0 * np.pi * z)/h))**2


def bd(y, xs, ys, h, Rmin, Rmax):
    #Root of this equation gives the z value of the nearest point on the cosine wave, (r,z) = (x,y)
    #Derived by minimization of the square of the distance function with respect to y
    return y-ys +((Rmax+Rmin)/2.0 - (Rmax-Rmin)/2.0*np.cos((2.0 * np.pi * y)/h)-xs) * ((((Rmax+Rmin)/2.0 * np.pi)/h) * np.sin((2.0 * np.pi * y)/h))
    
def dist(b, h, Rmin, Rmax):
    #returns the point (xb,yb) on the cosine wave of min distance as well as the min distance for each sphere centre
    xs = np.sqrt(b[:,0]**2 + b[:,1]**2)
    ys= b[:,2]
    
    vfunc = np.vectorize(ft.partial)
    fun1 = vfunc(bd, xs=xs, ys=ys, h=h, Rmin=Rmin, Rmax=Rmax)
    
    vrootsolve = np.vectorize(fsolve)
    
    yb = vrootsolve(fun1,ys)
    xb = cosine(yb, Rmin, Rmax, h)
    
    dist= np.sqrt((xs-xb)**2 + (ys-yb)**2)
    
    return xb, yb, dist
    

def plot(b,Rmin, Rmax, r,d,theta):
    #b is the state, R is the cylinder radius, r is sphere radius, d is the height
    x = b[:,0]
    y = b[:,1]
    z = b[:,2]
    s = 2.0 * r * np.ones(len(x))
    
    c = plot_array(b,d,theta)    
    
    x1 = c[:,0]
    y1 = c[:,1]
    z1 = c[:,2]
    s1 = 2.0 * r * np.ones(len(x1))
    
    f = np.mean(z) - d/2.
    a = np.mean(z) + d/2.
    f=np.min(z)
    #a=f+d
    
    h1 = f - d
    h2 = a + d



    mlab.figure(figure=1, bgcolor=(1, 1, 1), size=(1000, 1000))
    
    mlab.points3d(x, y, z, s, resolution=40, color=(0, 0, 1), scale_factor=1)
    mlab.points3d(x1, y1, z1, s1, resolution=40, color=(1, 0, 0), scale_factor=1)    
    
    
    pi = np.pi
    cos = np.cos
    sin = np.sin
    dphi, dtheta = pi / 500.0, pi / 500.0
    [phi, theta] = np.mgrid[0:2 * pi:dphi,-d:2*d:dtheta]


    x = ((Rmax+Rmin)/2.0 - (Rmax-Rmin)/2.0 * cos(2 * pi * theta / d)) * sin(phi)
    y = ((Rmax+Rmin)/2.0 - (Rmax-Rmin)/2.0 * cos(2 * pi * theta / d)) * cos(phi)
    z = theta


    mlab.mesh(x, y, z, color=(0, 1, 0), opacity=0.3)
    mlab.show()
    

    
        

def packing_ratio(r, Rmin, Rmax, h, N):
    #r is sphere radius, R cylinder radius, h is cylinder height, N is number of spheres
    vol_sphere = 4.0/3.0 * np.pi * (r ** 3)
    func = ft.partial(cosine_squared, Rmin = Rmin, Rmax = Rmax, h=h)
    vol_cos = np.pi * quad(func, 0, h)[0] 
    
    
    return (N * vol_sphere)/(vol_cos)
    
  
    
    

