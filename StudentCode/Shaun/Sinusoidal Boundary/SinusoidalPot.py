# -*- coding: utf-8 -*-
"""
Created on Sun Oct 04 12:49:03 2015

@author: Shaun
"""

import numpy as np
from scipy.optimize import minimize
import functions as fn
from scipy.spatial.distance import pdist
import functools as ft

def Pot(r):
    #the sphere interaction, currently goes to 0 at r=1
    return 50*(2-r) * ((1.0/r) ** 12 - 2 * (1.0/r) ** 6)
    


r_min = fn.minimum(Pot,1)                   
radius = r_min * 0.5  

#Max and min ratio of the radius of the sinusoidal boundary relative to the sphere size                     
min_ratio = 1.0
max_ratio = 1.4
Rmin = min_ratio * radius 
Rmax = max_ratio * radius
                  
rc=1.0                                      
z = 0.4
theta = np.pi/2.


state = fn.initial_state(radius,2)

#Plotting Initial state
fn.plot(np.reshape(state,(np.shape(state)[0],3)),Rmin, Rmax,radius,z,theta)




def Total_Pot(x, Rmin, Rmax, radius, z, theta):
    #Takes in 1d array of [x1,y1,z1,x2,y2,z2..,theta] and computes the total potential
    
    theta = x[-1]
    x = np.delete(x, -1)
    
    b = np.reshape(x,(np.shape(state)[0],3))
    xs=np.sqrt(b[:,0]**2 + b[:,1]**2)
    
    xb, yb, min_dist = fn.dist(b, z, Rmin, Rmax)
    
    b = fn.ghost_array(b,z,theta)
    rp=pdist(b)  
    
    
    sinusoidal_pot= np.where(xs < xb , np.where(min_dist-radius > 0, 0, np.exp(80.0*np.absolute(min_dist-radius))-1 ), np.exp(80.0 * np.absolute(min_dist+radius))-1)
    sphere_pot=np.where(rp <= rc, Pot(rp), 0.0)
    
    return  sum(sphere_pot) + sum(sinusoidal_pot)
    

    
def Minimization(x, theta, Rmin, Rmax, z):
    #Takes Cartesian guess array then minimizes and returns array
    x0=(np.reshape(x,(np.shape(x)[0],3))).ravel()
    x0=np.append(x0,theta)
    
    bnds = [(None,None), (None,None), (0,z)] * (np.shape(state)[0])
    bnds.append((None,None))
    Pot = ft.partial(Total_Pot, radius = radius, theta = theta, Rmin = Rmin, Rmax = Rmax, z = z)
    
    res = minimize(Pot, x0, method='L-BFGS-B', bounds=bnds, tol= 10**(-11)).x
    
    alpha = res[-1]
    res = np.delete(res, -1)
    a=np.reshape(res,(np.shape(x)[0],3))
    return a, alpha

for i in range(0,3):
    d = 0.1 * 1.0/(10 ** i)
    print "height at", z, "- step set to", d
    cond = True
    #loop over states
    while cond:
        for i in range(0,5):
            
            state, theta =Minimization(state, theta, Rmin, Rmax, z)
            
            
        
        
        a = np.any(np.where(pdist(fn.ghost_array(state,z,theta))+0.0005 <r_min  , True, False))
        #not sure if b overlap conidition is working
        #b = np.any(np.where(fn.dist(state, z, Rmin, Rmax)[2] +0.0005 < radius,True, False)) 
        print fn.dist(state, z, Rmin, Rmax)[2]
        print pdist(state)                 
        #cond = (a or b)   
        cond = a

        print "tested",  z
        #print "Pairwise Distances:"
        #print pdist(fn.ghost_array(state,z,theta)) 
        print "Sphere Radius: "+ str(radius) + ", Min distance: "+ str(r_min) 
        #fn.plot(state, Rmin, Rmax, radius, z, theta)
        z += d
    z -= 2.0 * d
    h = z + d



print state
print np.degrees(theta)    
print "Packing Ratio", fn.packing_ratio(radius,Rmin, Rmax,z,np.shape(state)[0])
fn.plot(state, Rmin, Rmax, radius, z, theta)





















