# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 16:16:15 2015

@author: Shaun
"""

import numpy as np
from scipy.optimize import fmin
import mayavi.mlab as mlab
from numpy.linalg import matrix_power



def pol_to_cart(polar_array):
    # Converts the polar array of the form [[r1,theta1,z1],[r2,theta2,z2],...] to a cartesian array of the form [[x1,y1,z1],[x2,y2,z2],...]
    x= polar_array[:,0] * np.cos(polar_array[:,1])
    y= polar_array[:,0] * np.sin(polar_array[:,1])
    z= polar_array[:,2]
    return np.transpose(np.vstack((x,y,z)))
    
    

def ghost_array(X,z,theta):
    #creates image array of the original array at +/- z with twist theta
    a=X.copy()
    a[:,2] +=z
    rotMatrix = np.array([[np.cos(theta),np.sin(theta),0],[-1.0*np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        a[i,:] = np.dot(rotMatrix,a[i,:])
    c=X.copy()
    c[:,2] -=z
    rotMatrix_inv = np.array([[np.cos(theta),-1.0*np.sin(theta),0],[np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        c[i,:] = np.dot(rotMatrix_inv,c[i,:])
    b = np.concatenate((np.concatenate((a,X)),c))
    return b
    
def ghost_array_2(X,z,theta,l):
    #creates image array of the original array at +/- z with twist theta
    d = {}
    for i in range(l):
        
        d["a"+str(l)]=X.copy()
        d["a"+str(l)][:,2] += z*(1+l)
        rotMatrix = np.array([[np.cos(theta),np.sin(theta),0],[-1.0*np.sin(theta),np.cos(theta),0],[0,0,1]])
        rotMatrix = matrix_power(rotMatrix,l+1)
        for i in range(np.shape(X)[0]):
            d["a"+str(l)][i,:] = np.dot(rotMatrix,d["a"+str(l)][i,:])
    
        d["c"+str(l)]=X.copy()
        d["c"+str(l)][:,2] -= z*(1+l)
        rotMatrix = np.array([[np.cos(theta),-1.0 * np.sin(theta),0],[np.sin(theta),np.cos(theta),0],[0,0,1]])
        rotMatrix = matrix_power(rotMatrix,l+1)
        for i in range(np.shape(X)[0]):
            d["c"+str(l)][i,:] = np.dot(rotMatrix,d["a"+str(l)][i,:])
            
        X = np.concatenate((np.concatenate((d["a"+str(l)],X)),d["a"+str(l)]))
        
    return X
    
def plot_array(X,z,theta):
    #creates image arrays at +/- z with twist theta
    a=X.copy()
    a[:,2] +=z
    rotMatrix = np.array([[np.cos(theta),np.sin(theta),0],[-1.0*np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        a[i,:] = np.dot(rotMatrix,a[i,:])
    c=X.copy()
    c[:,2] -=z
    rotMatrix_inv = np.array([[np.cos(theta),-1.0*np.sin(theta),0],[np.sin(theta),np.cos(theta),0],[0,0,1]])
    for i in range(np.shape(X)[0]):
        c[i,:] = np.dot(rotMatrix_inv,c[i,:])
    b = np.concatenate((a,c))
    return b
    
    
def minimum(V,x):
    #Finding the radius for a given potential V and trial guess x
    x0=x
    res = fmin(V,x0,xtol=0.0001,ftol=0.0001,maxiter=None, maxfun=None)
    return res
    
    
def initial_state(r,N):
    #z is height, R is cylinder radius, r is the sphere radius, N is number of spheres
    a=np.zeros((N,3))
    a[:,1]=0
    for i in range(0,N):
        if (i % 2.) == 0.0:
            a[i,0]= 0.4
        else:
            a[i,0]=-0.4
        a[i,2]= (r-0.2)*(1+i)
        
    return a

def ran_ini_2(R, N):
    a=np.random.rand(N,3)
    a[:,0] = a[:,0] * R
    a[:,1] = a[:,1] * 2 * np.pi
    a[:,2] = 0.1
    a[0,:] = [0.,0.,0.05]
    
    return pol_to_cart(a)
    
def ran_ini(R, N):
    a=np.random.rand(N,3)
    a[:,0] = a[:,0] * R
    a[:,1] = a[:,1] * 2 * np.pi
    a[:,2] = 0.1
    
    return pol_to_cart(a)
    
    
def initial_state_c(R,r,N):
    #z is height, R is cylinder radius, r is the sphere radius, N is number of spheres
    a=np.ones((N,3),float)
    a[:,0]=(R-r)
    a[:,2]= r
    for i in range (N):
        a[i,1]= ((2*np.pi) /float(N)) * float(i)
    a=pol_to_cart(a)
    return a
    
def initial_state_c_2(R,r,N):
    #z is height, R is cylinder radius, r is the sphere radius, N is number of spheres
    a=np.ones((N,3),float)
    a[0,:] = [0.,0.,0.2]
    a[1:,0]=(R-r)
    a[1:,2]= 0.2
    for i in range (N-1):
        
        a[i+1,1]= ((2*np.pi) /float(N-1)) * float(i)
    a=pol_to_cart(a)
    return a
    
    
def sphere(x, y, z):

    return x**2 + y**2

def plot(b,R,r,d,theta):
    #b is the state, R is the cylinder radius, r is sphere radius, d is the height
    x = b[:,0]
    y = b[:,1]
    z = b[:,2]
    s =  np.ones(len(x)) * 2.0 * r
    
    c = plot_array(b,d,theta)    
    
    x1 = c[:,0]
    y1 = c[:,1]
    z1 = c[:,2]
    s1 = np.ones(len(x1)) * 2.0 * r
    
    f = np.mean(z) - d/2.
    a = np.mean(z) + d/2.
    
    h1 = f - d
    h2 = a + d



    pty=mlab.figure(figure=1, bgcolor=(1, 1, 1), size=(1000, 1000))
    xS, yS, zS = np.mgrid[-R:R:64j, -R:R:64j, h1:h2:64j]
    #xS1, yS1, zS1 = np.mgrid[-R:R:64j, -R:R:64j, h1:f:64j]
    #xS2, yS2, zS2 = np.mgrid[-R:R:64j, -R:R:64j, a:h2:64j]
    
    po=mlab.contour3d(xS, yS, zS, sphere, opacity=0.3)
    #p1=mlab.contour3d(xS1, yS1, zS1, sphere, opacity=0.5, color=(1, 0, 1))
    #p2=mlab.contour3d(xS2, yS2, zS2, sphere, opacity=0.5 ,color=(1, 0, 1))    
    
    
    p3=mlab.points3d(x, y, z, s, resolution=40, color=(0, 0, 1), scale_factor=1)
    p4=mlab.points3d(x1, y1, z1, s1, resolution=40, color=(1, 0, 0), scale_factor=1)
    
    mlab.view(0, 90, 10)   
    mlab.show()
    
        

def packing_ratio(r,R,h,N):
    #r is sphere radius, R cylinder radius, h is cylinder height, N is number of spheres
    vol_sphere = 4.0/3.0 * np.pi * (r ** 3)
    vol_cyl = np.pi * (R ** 2) * np.maximum(h,r)
    return (N * vol_sphere)/(vol_cyl)
    

