# -*- coding: utf-8 -*-
"""
Created on Sun Oct 04 12:49:03 2015

@author: Shaun
"""

import numpy as np
from scipy.optimize import minimize
import functions as fn
from scipy.spatial.distance import pdist

def Pot(r):
    #the sphere interaction, currently goes to 0 at r=1
    return 500*(2-r)*((1.0/r) ** 12 - 2 * (1.0/r) ** 6)
    


r_min = fn.minimum(Pot,1)                   #finds mind of potential
radius = r_min * 0.5                        #radius of sphere
#ratio = (1.0+1.0/np.sin(np.pi/5.))          #ratio of cylinder to sphere radius, current function is analytic formula for c(i)s
ratio = 3
R= ratio * radius                           #Cylinder radius
rc=1.0                                      #cut off value for the potential
z = 0.8
theta =  np.pi/7


#state= fn.ran_ini(R,5)
state = fn.initial_state_c_2(R,radius,7)
#state = [[0,0,0.001]]
#state = [[0.4,0,0.1],[-0.4,0,0.1]]
#state = [[0.4,0,0.5],[-0.4,0,0.5],[0,0.4,0.5]]
#state = [[ 0.37738312, -0.50024212,  0.40533203],[-0.5106052,  -0.36323899,  0.15970141],[-0.15054338,  0.60827369,  0.00996261]]
#state = [[0.4,0,0.5],[-0.4,0,0.8],[0,-.5,1.1],[0,0,1.5]]
#state = [[.5,.5,0.5],[-.3,-.7,0.5],[-.3,0.2,0.75],[-0.7,-0.4,0.75],[0,0,0.9],[-0.4,0.3,0.9],[0.42,-0.67,0.5],[-0.89,-0.12,0.5]]
#state = fn.initial_state(radius,3)

#plotting initial state


fn.plot(np.reshape(state,(np.shape(state)[0],3)),R,radius,z,theta)




def Total_Pot(x):
    #Takes in 1d array of [x1,y1,z1,x2,y2,z2..,theta] and computes the total potential
    
    theta = x[-1]
    x = np.delete(x, -1)
    
    b = np.reshape(x,(np.shape(state)[0],3))
    r=np.sqrt(b[:,0]**2 + b[:,1]**2)
    
    b = fn.ghost_array(b,z,theta)
    rp=pdist(b)    
    
    cylinder_pot=np.where(r+radius <= R, 0.0, np.exp(50*(r + radius -R))-1)
    sphere_pot=np.where(rp <= rc, Pot(rp), 0.0)
    
    return  sum(sphere_pot) + sum(cylinder_pot)
    

    
def Minimization(x,theta):
    #Takes Cartesian guess array then minimizes and returns array
    x0=(np.reshape(x,(np.shape(x)[0],3))).ravel()
    x0=np.append(x0,theta)
    
    bnds = [(None,None), (None,None), (0,z)] * (np.shape(state)[0])
    bnds.append((None,None))
    
    res = minimize(Total_Pot, x0, method='L-BFGS-B', bounds=bnds, tol= 10**(-11)).x
    
    alpha = res[-1]
    res = np.delete(res, -1)
    a=np.reshape(res,(np.shape(x)[0],3))
    return a, alpha

#loops over decreasing height increments

for i in range(0,5):
    d = 0.1 * 1.0/(10 ** i)
    print "height at", z, "- step set to", d
    cond = True
    #loop over states
    while cond:
        for i in range(0,5):
            state, theta =Minimization(state,theta)
            
            
        
        a = np.any(np.where(pdist(fn.ghost_array(state,z,theta))+0.0005 <r_min  , True, False))
        b = np.any(np.where(R - (np.sqrt(state[:,0]**2 + state[:,1]**2) + radius) <-0.0005,True, False))                   
        cond = (a or b)   
        #cond = a

        print "tested",  z
        print "Pairwise Distances:"
        #print pdist(fn.ghost_array(state,z,theta)) 
        print "Sphere Radius: "+ str(radius) + ", Min distance: "+ str(r_min) + ",  Cylinder Radius: " + str(R) +",  Ratio: " + str(ratio)
        print R - (np.sqrt(state[:,0]**2 + state[:,1]**2) + radius)
        z += d
    z -= 2.0 * d
    h = z + d



    

#print state
print np.degrees(theta)    
print "Packing Ratio", fn.packing_ratio(radius,R,h,np.shape(state)[0])
fn.plot(state,R,radius,h,theta)











