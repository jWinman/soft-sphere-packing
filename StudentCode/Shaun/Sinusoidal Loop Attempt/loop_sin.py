# -*- coding: utf-8 -*-
"""
Created on Sun Oct 04 12:49:03 2015

@author: Shaun
"""

import numpy as np
from scipy.optimize import minimize
import loop_functions_sin as fn
from scipy.spatial.distance import pdist
import functools as ft
import pandas as pd

def Pot(r):
    #the sphere interaction, currently goes to 0 at r=1
    return 500*(2-r)* ((1.0/r) ** 12 - 2 * (1.0/r) ** 6)
    


r_min = fn.minimum(Pot,1)                   #finds mind of potential
radius = r_min * 0.5  
rc=1.0                                      #cut off value for the potential


def Total_Pot(x, Rmin, Rmax, radius, z):
    #Takes in 1d array of [x1,y1,z1,x2,y2,z2..,theta] and computes the total potential
    
    
    b = np.reshape(x,(np.shape(state)[0],3))
    xs=np.sqrt(b[:,0]**2 + b[:,1]**2)
    
    xb, yb, min_dist = fn.dist(b, z, Rmin, Rmax)
    
    rp=pdist(b)  
    
    
    cylinder_pot= np.where(xs < xb , np.where(min_dist-radius > 0, 0, np.exp(80.0*np.absolute(min_dist-radius))-1 ), np.exp(80.0 * np.absolute(min_dist+radius))-1)
    sphere_pot=np.where(rp <= rc, Pot(rp), 0.0)
    
    return  sum(sphere_pot) + sum(cylinder_pot)
    

    
def Minimization(x, Rmin, Rmax, z):
    #Takes Cartesian guess array then minimizes and returns array
    x0=(np.reshape(x,(np.shape(x)[0],3))).ravel()
    
    bnds = [(None,None), (None,None), (0,z)] * (np.shape(state)[0])
    
    Pot = ft.partial(Total_Pot, radius = radius, Rmin = Rmin, Rmax = Rmax, z = z)
    
    res = minimize(Pot, x0, method='L-BFGS-B', bounds=bnds, tol= 10**(-11)).x
    
    a=np.reshape(res,(np.shape(x)[0],3))
    return a
    




for k in range(0,4):
    
    df = pd.read_csv('packing_2_6.csv')
    spheres = k+1
    
    print spheres
    
    #state = lf.initial_state_c_2(0.9,radius, spheres)
    #state = lf.ran_ini(0.9,spheres)

    for j in range(0,20):
        

        z = 0.3
        min_ratio = 1.0
        max_ratio = 1 + ((2 * float(j))/20)
        Rmin = min_ratio * radius 
        Rmax = max_ratio * radius
        
        #state = lf.initial_state_c(R,radius, spheres)
        state = fn.ran_ini(Rmax,spheres)
        #state = lf.initial_state_c_3(0.9,radius, spheres)
        
        print max_ratio
        
        for i in range(0,4):
            #state = lf.ran_ini(0.9,spheres)
            print i
            d = 0.1 * 1.0/(10 ** i)
            cond = True
            while cond:
                    
                for i in range(0,1):
                    state = Minimization(state, Rmin, Rmax, z)
                    
            
            
                    
                    
                a = np.any(np.where(pdist(fn.ghost_array(state,z))+0.0005 <r_min , True, False))
                b = np.any(np.where(fn.dist(state, z, Rmin, Rmax)[2] +0.0005 < radius,True, False))
                cond = (a or b)
                z += d
                if z > 6.0:
                    break
                
            z -= 2.0 * d
            h = z + d
            
            if z > 6.0 - (2.0 * d):
                print 'failed'
                break
            
            
        if cond == False:

            if df.loc[j]['packing'] < float(fn.packing_ratio(radius,Rmin, Rmax,z,np.shape(state)[0]))-0.00001:
                
                df.loc[j] = spheres, max_ratio,  float(fn.packing_ratio(radius,Rmin, Rmax,z,np.shape(state)[0])), 0
                fn.save(state, Rmin, Rmax, radius, z, j)

        
    df.to_csv('packing_2_6.csv', index=False)
            
    
        
fn.packing_plot(20)
        
    






















