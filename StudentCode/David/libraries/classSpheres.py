import numpy as np
import functools as ft
import scipy.spatial.distance as ssd
from scipy import optimize
import miscellaneous as misc

class SpheresCylinder:
    def __init__(selfy, fileName, interaction="softsphere", borders="softborder"):
        import pylibconfig2 as pylib

        with open(fileName, "r") as f:
            cfgStr = f.read()

        cfg = pylib.Config(cfgStr)

        # static parameters
        selfy.R_z = 10
        selfy.Norm_par_number = cfg.cylinder.Norm_par_num
        selfy.p = cfg.cylinder.p
        selfy.N = cfg.spheres.N
        selfy.R_c = cfg.spheres.R_c
        selfy.eps = cfg.spheres.eps
        selfy.r_min = cfg.spheres.r_min
        selfy.niter = cfg.minimize.niter
        selfy.stepsize = cfg.minimize.stepsize
        selfy.T = cfg.minimize.T
        selfy.size = -2 # number of additional variable parameters
        selfy.RR = cfg.cylinder.RR
        #fixxing L for a given normed particle number
        selfy.L = (selfy.r_min * selfy.N) / selfy.Norm_par_number

        #changes
        #selfy.R_i = cfg.cylinder.R_i

        # variable parameters
        selfy.positions = np.zeros(selfy.N * 3 + 2) # 3N position + alpha + L

        # energy interactions
        selfy.interaction = ft.partial(misc.Harmonic, selfy=selfy)
        selfy.borders = ft.partial(misc.borders3D, selfy=selfy)
        

        # Force interactions
        selfy.interactionForce = ft.partial(misc.HarmonicForce, selfy=selfy)
        selfy.borderForce = ft.partial(misc.border3DForce, selfy=selfy)
        selfy.diffAlpha = misc.HarmonicDiffAlpha
        selfy.diffL = misc.HarmonicDiffL

    def RandomInitial(selfy):
        import numpy.random
        R = numpy.random.uniform(selfy.R_i, selfy.R_i + 1.5*selfy.r_min, selfy.N)
        th = numpy.random.uniform(0, 2 * np.pi, selfy.N)
        z = numpy.random.uniform(0, selfy.N * selfy.r_min, selfy.N)
        alpha = numpy.random.uniform(0, 2 * np.pi)
        #L = numpy.random.uniform(0.5 * selfy.r_min, selfy.N * selfy.r_min)
        L = selfy.L
        pos = np.array([misc.ToCartesianCo(R, th, z)])
        selfy.positions = np.append(pos.T.flatten(), np.array([alpha, L]))

    def ReadInitial(selfy, initial):
        R, th, z = np.loadtxt("data/" + initial + "Pos", unpack=True)
        furtherArgs = np.loadtxt("data/" + initial + "FurtherArgs", unpack=True)

        pos = np.array([misc.ToCartesianCo(R, th, z)])
        selfy.positions = np.append(pos.T.flatten(), furtherArgs)

    def GhostBoundary(selfy, x):
        xs = np.reshape(x[:selfy.size], (len(x[:selfy.size]) // 3, 3))
        alpha = x[selfy.size]
        L = selfy.L
        #L = x[selfy.size + 1]

        xs = np.apply_along_axis(misc.foldZ, 1, xs, L, alpha)
        xAbove = np.apply_along_axis(misc.shift, 1, xs, L, alpha)
        xBelow = np.apply_along_axis(misc.shift, 1, xs, -L, -alpha)
        return xs, np.concatenate((xs, xAbove, xBelow), axis=0)

    def Energy(selfy, x):
        #L = x[selfy.size + 1]
        L = selfy.L
        x, xGhost = selfy.GhostBoundary(x)

        # Energies between Ghost and Real spheres
        distances = ssd.cdist(x, xGhost, metric="euclidean")
        distances = np.delete(distances, [j + j * len(xGhost) for j in range(len(x))])
        energy = 0.5 * np.sum(selfy.interaction(distances))
#        print("interaction: ", energy / selfy.N)

        # Energies between opposing Ghost and Ghost spheres
        distancesGhost = ssd.cdist(xGhost[selfy.N:2 * selfy.N],
                              xGhost[2 * selfy.N:3 * selfy.N], metric="euclidean")

        energy += np.sum(selfy.interaction(distancesGhost))
#        print("Ghost interaction: ", np.sum(selfy.interaction(distancesGhost)) / selfy.N)

        # Energy from Border
        energy += selfy.borders(x)

#        print("border energy: ", sum(selfy.borders(x)) / selfy.N)

        # pV term 
        # ******not realy sure whats the best to do here********
        #energy += selfy.p * np.pi * L 


#        print("energy: ", energy)
        
        #print ("L is lenghteh ",L)
        return energy


    

    def HarmonicGradAlphaL(selfy, xs, alpha, L):
        xAboves = np.apply_along_axis(misc.shift, 1, xs, L, alpha)

        differenceVector = xs[:, None] - xAboves[None, :]
        r = np.linalg.norm(differenceVector, axis=2)
        scales = np.where((r > selfy.R_c) + (r == 0), 0, selfy.eps * (r - selfy.r_min) / r)

        alphaGrad = -np.sum(selfy.diffAlpha(xAboves, differenceVector, scales))
        LGrad = np.sum(selfy.diffL(differenceVector, scales)) + selfy.p * np.pi * selfy.R_z**2

        return np.array([alphaGrad, LGrad])

    def Gradient(selfy, x):
        xs, xGhost = selfy.GhostBoundary(x)
        xAbove = xGhost[selfy.N:2 * selfy.N]
        xBelow = xGhost[2 * selfy.N:]

        denergies = np.zeros((len(xs), 3))
        # real-real spheres
        xsDiffs = xs[:, None] - xs[None, :]
        denergies += np.sum(selfy.interactionForce(xsDiffs), axis=1)

        # real-top ghost spheres
        xsDiffs = xs[:, None] - xAbove[None, :]
        denergies += np.sum(selfy.interactionForce(xsDiffs), axis=1)

        # real-bottom ghost spheres
        xsDiffs = xs[:, None] - xBelow[None, :]
        denergies += np.sum(selfy.interactionForce(xsDiffs), axis=1)

        denergies += selfy.borderForce(xs)

        alpha = x[selfy.size]
        #L = x[selfy.size + 1]
        L = selfy.L
        denergies = np.append(denergies, selfy.HarmonicGradAlphaL(xs, alpha, L))

        return -denergies

    def GradientNUM(selfy, x, h):
        gradient = np.zeros(len(x))

        for i in range(len(gradient)):
            forwardpositions = np.concatenate((x[:i], np.array([x[i] + h]), x[i + 1:]))
            backwardpositions = np.concatenate((x[:i], np.array([x[i] - h]), x[i + 1:]))
            gradient[i] = (selfy.Energy(forwardpositions) - selfy.Energy(backwardpositions)) / (2 * h)
        return -gradient

    def pertube(selfy, perc):
        kick = np.random.uniform(-1, 1, 3 * selfy.N) * perc
        kickAlpha = np.random.uniform(0, 2 * np.pi * perc)
        kickL = np.random.uniform(0, selfy.positions[-1] * perc)
        kick = np.append(kick, np.array([kickAlpha, kickL]))

        selfy.positions = selfy.positions + kick

    def minimize(selfy, method):
        #not sure what to do here...
        bnds = [(-2 * selfy.r_min, 2* selfy.r_min ),
                (-2*selfy.r_min, 2*selfy.r_min ),
                (0, float("inf"))] * int((len(selfy.positions) + selfy.size) / 3)

        bnds.append((0, np.pi))
        bnds.append((selfy.r_min / 2., selfy.N * selfy.r_min))
       

        if (method=="b"):
            minimizer_kwargs = {"method": "L-BFGS-B", "jac": None, "bounds": bnds}
            ret = optimize.basinhopping(selfy.Energy, selfy.positions,
                                        minimizer_kwargs=minimizer_kwargs,
                                        niter=selfy.niter, stepsize=selfy.stepsize, T=selfy.T)
        elif (method == "CG"):
            print "using Cg"
            ret = optimize.minimize(selfy.Energy, selfy.positions, jac=None, method="CG")

        else:
            ret = optimize.minimize(selfy.Energy, selfy.positions, jac=None, method="L-BFGS-B", bounds=bnds)

        selfy.positions = ret.x
        return ret.fun

    def steepest_descent(selfy, stepsize, steps):
        convergence = np.zeros(steps)
        gradient = np.zeros(len(selfy.positions))
        for i in range(steps):
            gradient = selfy.Gradient(selfy.positions)
            gradnorm = np.linalg.norm(gradient)
            selfy.positions += stepsize * gradient

            convergence[i] = gradnorm

#            if (i % 1000 == 0):
#                print("convergence: ", i, convergence[i], flush=True)

            if (i > 1000 and gradnorm < 1e-11 and abs(gradnorm - convergence[i - 1000]) < 1e-18):
                break;

#        np.savetxt("data/TrajectorySteepest/TrajectoryTESTADILForward/N{}/convergencep{:.6f}D{:.4f}.txt".format(selfy.N, selfy.p, selfy.R_z), convergence)

        return selfy.Energy(selfy.positions)

    def steepest_descentNUM(selfy, stepsize, steps):
        gradnorm = np.linalg.norm(selfy.GradientNUM(selfy.positions, 1e-8))
        convergence = np.zeros(steps)
        for i in range(steps):
            convergence[i] = gradnorm
            gradient = selfy.GradientNUM(selfy.positions, 1e-8)
            selfy.positions += stepsize * gradient
            gradnorm = np.linalg.norm(gradient)

#            if (i % 5000 == 0):
#                print("convergence: ", i, gradnorm, flush=True)

#        np.savetxt("data/convergence.txt", convergence)
        return selfy.Energy(selfy.positions)

    def resize(selfy, repetition):
        xs = np.reshape(selfy.positions[:selfy.size], (len(selfy.positions[:selfy.size]) // 3, 3))
        alpha = selfy.positions[selfy.size]
        L = selfy.L
        xs = np.apply_along_axis(misc.foldZ, 1, xs, L, alpha)
        xs = xs[np.argsort(xs[:, 2])]
        xs = xs[:int(frac * selfy.N)]

        # repeat unit cell to repetition spheres
        Nrep = repetition * selfy.N
        xRep = np.zeros((Nrep, 3))
        for i in range(0, repetition):
            xRep[i * selfy.N: (i + 1) * selfy.N] = np.apply_along_axis(misc.shift, 1, xs, i * L, i * alpha)

        selfy.positions = np.append(xRep.flatten(), alpha * repetition)
        selfy.N = Nrep

    def plot3D(selfy, path):
        from mayavi import mlab

        pos, posGhost = selfy.GhostBoundary(selfy.positions)
        x, y, z = pos.T
        xGhost, yGhost, zGhost = posGhost.T
        #L = selfy.positions[selfy.size + 1]
        L = selfy.L

        zMin = 0
        zMax = zMin + L

        dphi, dz = np.pi / 250., np.pi / 250.
        [phi, zCylinder] = np.mgrid[0:2 * np.pi:dphi, zMin:zMax:dz]

        xCylinder = selfy.R_z * np.cos(phi)
        yCylinder = selfy.R_z * np.sin(phi)

        xInner = selfy.R_i * np.cos(phi)
        yInner = selfy.R_i * np.sin(phi)
        fig = mlab.figure(figure=1, bgcolor=(1, 1, 1), size=(500, 500))
        # graphing the outer cylinder
        #mlab.mesh(xCylinder, yCylinder, zCylinder, color=(0, 1, 0), opacity=0.3)
        # graphing the inner cyclinder 
        #mlab.mesh(xInner, yInner, zCylinder, color=(0, 1, 0), opacity=0.7)
        mlab.points3d(xGhost, yGhost, zGhost, selfy.r_min * np.ones(len(xGhost)), resolution=40, color=(1, 0, 0), scale_factor=1.)
        mlab.points3d(x, y, z, selfy.r_min * np.ones(len(x)), resolution=40, color=(0, 0, 1), scale_factor=1.)
        mlab.show()
#        mlab.savefig(path + "/" + "structurep{:.6f}D{:.4f}.png".format(selfy.p, selfy.R_z))
        #mlab.clf()

    def plot3DTube(selfy, path, repetition):
        from mayavi import mlab

        xs = np.reshape(selfy.positions[:selfy.size], (len(selfy.positions[:selfy.size]) // 3, 3))
        alpha = selfy.positions[selfy.size]
        #L = selfy.positions[selfy.size + 1]
        L = selfy.L

        xs = np.apply_along_axis(misc.foldZ, 1, xs, L, alpha)
        xAbove = np.zeros((repetition * selfy.N, 3))
        for i in range(0, repetition):
            xAbove[i * selfy.N: (i + 1) * selfy.N] = np.apply_along_axis(misc.shift, 1, xs, i * L, i * alpha)
        x, y, z = xAbove.T

        zMin = - selfy.r_min * 0.5
        zMax = max(z) + selfy.r_min * 0.5

        dphi, dz = np.pi / 250., np.pi / 250.
        [phi, zCylinder] = np.mgrid[0:2 * np.pi:dphi, zMin:zMax:dz]

        xCylinder = selfy.R_z * np.cos(phi)
        yCylinder = selfy.R_z * np.sin(phi)

        fig = mlab.figure(figure=1, bgcolor=(1, 1, 1), size=(500, 500))
        #mlab.mesh(xCylinder, yCylinder, zCylinder, color=(0, 1, 0), opacity=0.05)
        mlab.points3d(x, y, z, selfy.r_min * np.ones(len(x)), resolution=40, color=(0.3, 0.4, 0.7), scale_factor=1.)
        mlab.show()

    def plotSchematic(selfy, path, repetition, sdist_max):
        from mayavi import mlab

        xs = np.reshape(selfy.positions[:selfy.size], (len(selfy.positions[:selfy.size]) // 3, 3))
        alpha = selfy.positions[selfy.size]
        #L = selfy.positions[selfy.size + 1]
        L = selfy.L

        xs = np.apply_along_axis(misc.foldZ, 1, xs, L, alpha)
        xAbove = np.zeros((repetition * selfy.N, 3))
        for i in range(0, repetition):
            xAbove[i * selfy.N: (i + 1) * selfy.N] = np.apply_along_axis(misc.shift, 1, xs, i * L, i * alpha)
        x, y, z = xAbove.T
        R, phi, z = misc.ToCylinderCo(x, y, z)
        xSurf, ySurf, zSurf = misc.ToCartesianCo(R, phi, z)

        linksX, linksY, linksZ = [np.zeros(100)], [np.zeros(100)], [np.zeros(100)]
        counter = 0
        for i in range(len(x)):
            for j in range(len(x)):
                if (i != j):
                    ang_dist = np.abs(phi[i] - phi[j])
                    if (ang_dist <= 2 * np.pi):
                        sdx = x[i] - x[j]
                        sdy = y[i] - y[j]
                        sdz = z[i] - z[j]

                        sdist = np.sqrt(sdx**2 + sdy**2 + sdz**2)

                        if (sdist < sdist_max):
                            counter += 1
                            linksXi, linksYi, linksZi = np.linspace(x[i], x[j], 100), np.linspace(y[i], y[j], 100), np.linspace(z[i], z[j], 100)
                            Rlinks, linksPhi, linksZi = misc.ToCylinderCo(linksXi, linksYi, linksZi)
                            linksXi, linksYi, linksZi = misc.ToCartesianCo(np.mean(R) * np.ones(len(Rlinks)), linksPhi, linksZi)
                            linksX = np.append(linksX, np.reshape(linksXi, (1, len(linksXi))), axis=0)
                            linksY = np.append(linksY, np.reshape(linksYi, (1, len(linksYi))), axis=0)
                            linksZ = np.append(linksZ, np.reshape(linksZi, (1, len(linksZi))), axis=0)

        xtmp1, ytmp1, ztmp1 = xSurf[::2], ySurf[::2], zSurf[::2]
        #xtmp2, ytmp2, ztmp2 = xSurf[1::2], ySurf[1::2], zSurf[1::2]
        #xSpiral1, ySpiral1, zSpiral1 = np.concatenate((xtmp1[::2], xtmp2[::2])), np.concatenate((ytmp1[::2], ytmp2[::2])), np.concatenate((ztmp1[::2], ztmp2[::2]))
        xSpiral1, ySpiral1, zSpiral1 = xtmp1, ytmp1, ztmp1

        zMin = 0 #- selfy.r_min * 0.5
        zMax = max(z) + selfy.r_min * 0.5

        dphi, dz = np.pi / 250., np.pi / 250.
        [phi, zCylinder] = np.mgrid[0:2 * np.pi:dphi, zMin:zMax:dz]

        xCylinder = np.mean(R) * np.cos(phi)
        yCylinder = np.mean(R) * np.sin(phi)

        fig = mlab.figure(figure=1, bgcolor=(1, 1, 1), size=(500, 500))
        mlab.mesh(xCylinder, yCylinder, zCylinder, color=(0.9, 0.9, 0.9), opacity=1.)
        mlab.points3d(xSurf, ySurf, zSurf, np.ones(len(xSurf)), resolution=40, color=(1.0, 0., 0.), scale_factor=0.2)
 #       mlab.points3d(xSpiral1, ySpiral1, zSpiral1, np.ones(len(xSpiral1)), resolution=40, color=(0.4, 0.5, 0.7), scale_factor=0.2)
        for linksX, linksY, linksZ in zip(linksX[1:], linksY[1:], linksZ[1:]):
            mlab.plot3d(linksX, linksY, linksZ, color=(0, 0, 0))
        mlab.show()

    def setVariable(selfy, variable, value):
        exec("selfy.{} = {}".format(variable, value))

    def printToFile(selfy, final):
        x, y, z = selfy.GhostBoundary(selfy.positions)[0].T
        R, th, z = misc.ToCylinderCo(x, y, z)

        PosZylinder = np.array([R, th, z])
        furtherArgs = selfy.positions[selfy.size:]
        np.savetxt("data/" + final + "Final_N{}_RR{:.6f}_K{:.03f}_N_norm{:.4f}Pos".format(selfy.N, selfy.RR,selfy.eps, selfy.Norm_par_number), PosZylinder.T)
        np.savetxt("data/" + final + "Final_N{}_RR{:.6f}_K{:.03f}_N_norm{:.4f}FurtherArgs".format(selfy.N, selfy.RR,selfy.eps, selfy.Norm_par_number), furtherArgs.T)

        Pos, PosGhost = selfy.GhostBoundary(selfy.positions)
        distances = ssd.cdist(Pos, PosGhost, metric="euclidean")
        np.savetxt("data/" + final + "Final_N{}_RR{:.6f}_K{:.3f}_N_norm{:.4f}CGEmatrix".format(selfy.N, selfy.RR, selfy.eps, selfy.Norm_par_number), distances, fmt="%.5f")
