import numpy as np

def transition_parameter(p, D, directory):
    print("p: ", p, "D / d: ", D)
    xnods, ynods = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Unit.gz".format(directory, p, D), unpack=True)
    xedges, yedges, ledges  = np.genfromtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Edges.gz".format(directory, p, D), unpack=True)

    edgesPerNods = []
    for xnod, ynod in zip(xnods, ynods):
        edgesPerNod = []
        j = 0
        for i, (xedge, yedge) in enumerate(zip(xedges, yedges)):
            if (i % 2 == 1 and abs(xedge - xnod) < 1e-6 and abs(yedge - ynod) < 1e-6 and i < len(xedges) - 1):
                edgesPerNod.append([xedge, yedge])
                j += 1
                edgesPerNod.append([xedges[i + 1], yedges[i + 1]])
        edgesPerNods.append(edgesPerNod)

    angles = np.array([])
    for edgesPerNod in edgesPerNods:
        Es = np.zeros((len(edgesPerNod) // 2, 2)) # vector between linked nodes
        angleToX = np.zeros(len(edgesPerNod) // 2) # all angles around a node to x-axis
        j = 0
        for i in range(len(edgesPerNod)):
            if (i % 2 == 0):
                Es[j] = np.array(edgesPerNod[i + 1]) - np.array(edgesPerNod[i])
                angleToX[j] = np.arctan2(Es[j][1], Es[j][0])
                angleToX[j] = angleToX[j] + 2 * np.pi if (angleToX[j] < 0) else angleToX[j]
                j += 1

        angleToXsorted = np.sort(angleToX)

        # Triangle AREA parameter
        Essorted = Es[np.argsort(angleToX)]
        a1 = np.linalg.norm(Essorted[-2])
        b = np.linalg.norm(Essorted[-1])
        gamma1 = angleToXsorted[-1] - angleToXsorted[-2]
        a2 = np.linalg.norm(Essorted[0])
        gamma2 = 2 * np.pi - (angleToXsorted[-1] - angleToXsorted[0])
        gamma3 = angleToXsorted[1] - angleToXsorted[0]

        angle90 = 90 * 2 * np.pi / 360.
        angle50 = 63 * 2 * np.pi / 360.
        g90 = gamma1 > angle90 or gamma2 > angle90 or gamma3 > angle90
        s50 = np.sum(np.array([gamma1 < angle50, gamma2 < angle50, gamma3 < angle50]))
        s50 = True if (s50 > 1) else False
        if (g90 and not s50 and abs(angleToXsorted[0]) > 1e-3):
            # diamond case
            areaDiamond1 = a1 * b * np.sin(gamma1)
            areaDiamond2 = a2 * b * np.sin(gamma2)
            angles = np.append(angles, [areaDiamond1, areaDiamond2])
        else:
            # triangle case
            areaTriangle1 = 0.5 * a1 * b * np.sin(gamma1)
            areaTriangle2 = 0.5 * a2 * b * np.sin(gamma2)
            angles = np.append(angles, [areaTriangle1, areaTriangle2])

    return np.mean(angles)

ps = np.arange(0.0005, 0.021, 0.001)
Ds = np.arange(2.039, 2.44, 0.001)

for D in Ds:
    areas = np.zeros(len(ps))

    directoryRead = "Adil/UpperSweapPhyllo/Trajectory{:.4f}".format(D)

    for j, p in enumerate(ps):
        areas[j] = transition_parameter(p, D, directoryRead)
        print("areas: ", areas[j])

    np.savetxt("../../data/Adil/UpperSweapPhyllo/Areas{:.4f}.txt".format(D), np.array([ps, areas]).T)
