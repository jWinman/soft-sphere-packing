import sys
sys.path.append("../../../libraries")

import numpy as np
import classSpheres
import os
import subprocess

def ToCartesianCo(R, phi, z):
    x = R * np.cos(phi)
    y = R * np.sin(phi)
    return x, y, z

@np.vectorize
def phyllotaxis(p, D, directoryRead, directorySave):
    subprocess.call("mkdir -p ../../data/{}".format(directorySave), shell=True)
    NTBimages = 1
    NSIMAGE = 1
    sdist_max = 1.005

    # Read data
    R, th, z = np.loadtxt("../../data/{}/Finalp{:.6f}D{:.4f}Pos".format(directoryRead, p, D), unpack=True)
    alpha, L = np.loadtxt("../../data/{}/Finalp{:.6f}D{:.4f}FurtherArgs".format(directoryRead, p, D))
    alpha = -alpha
    alpha = -(2 * np.pi - alpha) if (alpha > np.pi) else alpha

    # sort data by z
    Rsorted = R[np.argsort(z)]
    thsorted = th[np.argsort(z)]
    zsorted = np.sort(z)

    # set origin at (thsorted[0], zsorted[0])
    thsorted = (thsorted - thsorted[0]) % (2 * np.pi)
    zsorted -= zsorted[0]

    Rnew = np.array([])
    thnew = np.array([])
    znew = np.array([])

    # calculate Nodes
    for i in range(-NTBimages, NTBimages + 1, 1):
        for j in range(-NSIMAGE, NSIMAGE + 1, 1):
            thcell = (thsorted + i * alpha) % (2 * np.pi) + j * 2 * np.pi
            zcell = zsorted + i * L
            Rnew = np.append(Rnew, Rsorted)
            thnew = np.append(thnew, thcell)
            znew = np.append(znew, zcell)

    Unitcell = np.array([thsorted * Rsorted, zsorted]).T
    Nodes = np.array([thnew * Rnew, znew]).T

    np.savetxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Unit.gz".format(directorySave, p, D), Unitcell)
    np.savetxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Nodes.gz".format(directorySave, p, D), Nodes)

    x, y, z = ToCartesianCo(Rnew, thnew, znew)
    links = np.array([[0, 0, 0]])

    counter = 0
    for i in range(len(Rnew)):
        for j in range(len(Rnew)):
            if (i != j):
                ang_dist = np.abs(thnew[i] - thnew[j])
                if (ang_dist <= np.pi or (ang_dist - np.pi) <= 0.001):
                    sdx = x[i] - x[j]
                    sdy = y[i] - y[j]
                    sdz = z[i] - z[j]

                    sdist = np.sqrt(sdx**2 + sdy**2 + sdz**2)

                    if (sdist < sdist_max):
                        counter += 1
                        links = np.append(links, [[Rnew[i] * thnew[i], znew[i], sdist]], axis=0)
                        links = np.append(links, [[Rnew[j] * thnew[j], znew[j], sdist]], axis=0)

    np.savetxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Edges.gz".format(directorySave, p, D), links)

# Trajectories

ps = np.arange(0.0005, 0.021, 0.001)
Ds = np.arange(2.039, 2.44, 0.001)
for D in Ds:
    directoryRead = "Adil/UpperSweap_CG/Trajectory{:.4f}".format(D)
    directorySave = "Adil/UpperSweap_CGPhyllo/Trajectory{:.4f}".format(D)

    for p in ps:
        print(p, D)
        phyllotaxis(p, D, directoryRead, directorySave)
