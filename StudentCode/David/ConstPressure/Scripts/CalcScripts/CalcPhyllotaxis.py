import sys
sys.path.append("../../../libraries")

import numpy as np
import classSpheres
import os
import subprocess

def ToCartesianCo(R, phi, z):
    x = R * np.cos(phi)
    y = R * np.sin(phi)
    return x, y, z

@np.vectorize
def phyllotaxis( NTraj, RR,k, N_Norm, directoryRead, directorySave):
    subprocess.call("mkdir -p ../../data/{}".format(directorySave), shell=True)
    NTBimages = 1 #maybe 2ish
    NSIMAGE = 3
    p = 0.005
    D_z = 1
    sdist_max = 1.003
    Ns = [1,2,3,4]


    if ("PhaseDiagram" in directoryRead):
        os.chdir("../..")

        E = np.zeros(len(Ns))
        directions = []
        directions3 = ["", "P", "2P"]
        for i, N in enumerate(Ns):
            if (N == 5 and D < 2.5):
                E1 = 1e6
                E2 = 2e6
                E3 = 3e6
            else:
                mySystem = classSpheres.SpheresCylinder("parameter.cfg")
                mySystem.setVariable("p", p)
                mySystem.setVariable("N", N)
                mySystem.setVariable("R_z", D / 2.)
                structure = "{}/N{}/Finalp{:.6f}D{:.4f}".format(directoryRead, N, p, D) #
                structure2 = "{}/NP{}/Finalp{:.6f}D{:.4f}".format(directoryRead, N, p, D) #
                structure3 = "{}/N2P{}/Finalp{:.6f}D{:.4f}".format(directoryRead, N, p, D) #

                mySystem.ReadInitial(structure)
                E1 = mySystem.Energy(mySystem.positions) / N
                mySystem.ReadInitial(structure2)
                E2 = mySystem.Energy(mySystem.positions) / N
                mySystem.ReadInitial(structure3)
                E3 = mySystem.Energy(mySystem.positions) / N

            E[i] = np.min([E1, E2, E3])
            direction = directions3[np.argmin([E1, E2, E3])]
            directions.append(direction)

        os.chdir("Scripts/CalcScripts")

        N = Ns[np.argmin(E)]
        direction = directions[np.argmin(E)]
    else:
        N = NTraj
        direction = ""


    # Read data
    R, th, z = np.loadtxt("data/HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_K{:.3f}_N_norm{:.4f}Pos".format(N, N, RR,k, N_Norm), unpack=True)
    alpha, L = np.loadtxt("data/HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_K{:.3f}_N_norm{:.4f}FurtherArgs".format(N, N, RR,k, N_Norm) )
    alpha = -alpha
    alpha = -(2 * np.pi - alpha) if (alpha > np.pi) else alpha

    # sort data by z
    Rsorted = R[np.argsort(z)]
    thsorted = th[np.argsort(z)]
    zsorted = np.sort(z)

    # set origin at (thsorted[0], zsorted[0])
    thsorted = (thsorted - thsorted[0]) % (2 * np.pi)
    zsorted -= zsorted[0]

    Rnew = np.array([])
    thnew = np.array([])
    znew = np.array([])

    # calculate Nodes
    for i in range(-NTBimages, NTBimages, 1):
        for j in range(-NSIMAGE, NSIMAGE, 1):
            thcell = (thsorted + i * alpha) % (2 * np.pi) + j * 2 * np.pi
            zcell = zsorted + i * L
            Rnew = np.append(Rnew, Rsorted)
            thnew = np.append(thnew, thcell)
            znew = np.append(znew, zcell)

    Unitcell = np.array([thsorted * Rsorted, zsorted]).T
    Nodes = np.array([thnew * Rnew, znew]).T

    np.savetxt("data/{}/PhyllotaxisN{}_RR{:.6f}_N_norm{:.4f}Unit.csv".format(directorySave,  N, RR, N_Norm), Unitcell)
    np.savetxt("data/{}/PhyllotaxisN{}_RR{:.6f}_N_norm{:.4f}Nodes.csv".format(directorySave,  N, RR, N_Norm), Nodes)

    x, y, z = ToCartesianCo(Rnew, thnew, znew)
    links = np.array([[0, 0, 0]])

    counter = 0
    for i in range(len(Rnew)):
        for j in range(len(Rnew)):
            if (i != j):
                ang_dist = np.abs(thnew[i] - thnew[j])
                if (ang_dist <= np.pi or (ang_dist - np.pi) <= 0.001):
                    sdx = x[i] - x[j]
                    sdy = y[i] - y[j]
                    sdz = z[i] - z[j]

                    sdist = np.sqrt(sdx**2 + sdy**2 + sdz**2)

                    if (sdist < sdist_max):
                        counter += 1
                        links = np.append(links, [[Rnew[i] * thnew[i], znew[i], sdist]], axis=0)
                        links = np.append(links, [[Rnew[j] * thnew[j], znew[j], sdist]], axis=0)

    np.savetxt("data/{}/PhyllotaxisN{}_RR{:.6f}_N_norm{:.4f}Edges.csv".format(directorySave,  N, RR, N_Norm), links)

# PhaseDiagram
#Np = 98
#ND_z = 60
#
#D_zs = np.arange(1.5, 2.7, 0.0025)
#ps = np.array([np.arange(0.0004, 0.02, 0.0002) for _ in range(ND_z)]).flatten()
#D_zs = np.array([D_zs for _ in range(Np)]).T.flatten()
#
#phyllotaxis(ps, D_zs, 4, "PhaseDiagram", "HysteresesN4")

# Trajectories
ps = np.arange(0.05, 0.0008, -0.0004)
D_zs = np.ones(len(ps)) * 1.85
#D_zs = np.arange(1.9525, 2.05, 0.005)
#ps = np.ones(len(D_zs)) * 0.005
number = "TrajExp"
best_N = 0
os.chdir("../..")


k=100
for RR in [1,10,20,30,40,50,60,70,80,90,100]:
    for N_Norm in np.arange(2.22, 3.64,.01):
        best_N = 24
        '''
        E = 1000000000
        for N in range (2, 5):
            initial = "HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_N_norm{:.4f}".format(N, N, RR, N_Norm)
            mySystem = classSpheres.SpheresCylinder("parameter.cfg")
            mySystem.setVariable("RR", RR)
            mySystem.setVariable("N", N)
            mySystem.setVariable("R_i", 1)
            mySystem.setVariable("L", (1 * N) / N_Norm)
            mySystem.setVariable("N_Norm", N_Norm)
            mySystem.ReadInitial(initial)

            if E > (mySystem.Energy(mySystem.positions) / N):
                #print("current E", E, "moveing to better E", ((mySystem.Energy(mySystem.positions) / N )))
                best_N = N
                E = (mySystem.Energy(mySystem.positions) / N)
        '''
        print("N_Norm " , N_Norm,"RR ", RR, "best_N", best_N)
        phyllotaxis( best_N, RR,k, N_Norm,"HardSpheres/compare_N{:.0f}".format(best_N), "TrajectorySteepest/Trajectory{}Forward".format(number))










