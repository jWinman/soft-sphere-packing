import numpy as np
import matplotlib.pyplot as plt
import subprocess
import os.path

D = 1
#ps = np.arange(0.05, 0.0008, -0.0004)
Ds = 1
p = 0.005
D_i = 1
N= 24
#number = "TrajExp"
#directory = "TrajectorySteepest/Trajectory{}Forward".format(number)
#directory = "PhaseDiagram"
directory = "TrajectorySteepest/TrajectoryTrajExpForward"


'''
best_N=24
for RR in np.arange(.1, 1, .1):
    for N_Norm in np.arange(.1, 4, .3):
            
            for N in range (2,5):
                if ( os.path.isfile("../../data/{}/PhyllotaxisN{}_RR{:.6f}_N_norm{:.4f}Nodes.csv".format(directory, N, RR, N_Norm)) == True):
                    best_N = N

            print("N_Norm " , N_Norm,"RR ", RR, "for N" , best_N)
            th, z = np.loadtxt("../../data/{}/PhyllotaxisN{}_RR{:.6f}_N_norm{:.4f}Nodes.csv".format(directory, best_N, RR, N_Norm), unpack=True)
            thunit, zunit = np.loadtxt("../../data/{}/PhyllotaxisN{}_RR{:.6f}_N_norm{:.4f}Unit.csv".format(directory, best_N, RR, N_Norm), unpack=True)
            links = np.loadtxt("../../data/{}/PhyllotaxisN{}_RR{:.6f}_N_norm{:.4f}Edges.csv".format(directory, best_N, RR, N_Norm))

            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1, aspect="equal")
            ax.set_title("N{}$, $RR = {:.6f}$, $N_Norm {:.4f}$ = ".format(best_N, RR, N_Norm))
            ax.plot(th, z, "b.")
            ax.plot(thunit, zunit, "rD")
            for i in range(1, len(links)):
                if (i % 2 == 1):
                    th1 = links[i][0]
                    z1 = links[i][1]
                    th2 = links[i + 1][0]
                    z2 = links[i + 1][1]
                    ax.plot([th1, th2], [z1, z2], "b-")
            ax.set_ylim(-5, 5)
            ax.set_xlim(-5, 5)
            ax.set_ylabel("$z$")
            ax.set_xlabel(r"$R_z \theta$")
            ax.grid()
            fig.savefig("../../Plots/{}/Phyllotaxis/phyllotaxisN{}_RR{:.6f}_N_norm{:.4f}.png".format(directory, best_N, RR, N_Norm))
            plt.close()
'''

best_N=24
RR = 1.
k=100
directory = "TrajectorySteepest/TrajectoryTrajExpForward"
for RR in [20]: #[1,10,20,30,40,50,60,70,80,90,100]:
    subprocess.call("mkdir -p ../../Plots/Phyllotaxis/Omega{:.2f}_K{:.2f}".format(RR,k), shell=True)
    for N_Norm in np.arange(2.22, 3.64, .01):
        print("N_Norm " , N_Norm,"RR ", RR, "for N" , best_N)
        th, z = np.loadtxt("../../data/{}/PhyllotaxisN{}_RR{:.6f}_N_norm{:.4f}Nodes.csv".format(directory, best_N, RR, N_Norm), unpack=True)
        thunit, zunit = np.loadtxt("../../data/{}/PhyllotaxisN{}_RR{:.6f}_N_norm{:.4f}Unit.csv".format(directory, best_N, RR, N_Norm), unpack=True)
        links = np.loadtxt("../../data/{}/PhyllotaxisN{}_RR{:.6f}_N_norm{:.4f}Edges.csv".format(directory, best_N, RR, N_Norm))
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1, aspect="equal")
        title="$N = {:.0f} \ \omega = {:.1f} \ k={:.1f} \ l={:.4f}$".format(best_N, RR,k, 1. / N_Norm)
        ax.set_title(title)
        #ax.set_title("N{:.0f} RR{:.3f} k{:.3f} N_Norm {:.4f} ".format(best_N, RR,k, N_Norm))
        ax.plot(th, z, "b.")
#        ax.plot(thunit, zunit, "rD")
        for i in range(1, len(links)):
            if (i % 2 == 1):
                th1 = links[i][0]
                z1 = links[i][1]
                th2 = links[i + 1][0]
                z2 = links[i + 1][1]
                ax.plot([th1, th2], [z1, z2], "b-")
        ax.set_ylim(-10, 10)
        ax.set_xlim(-10, 10)
        ax.set_ylabel("$z$")
        ax.set_xlabel(r"$R_z \theta$")
        ax.grid()
        print RR,N_Norm
        fig.savefig("../../Plots/Phyllotaxis/Omega{:.2f}_K{:.2f}/phyllotaxisN{}_RR{:.6f}_N_norm{:.4f}.png".format(RR,k, best_N, RR, 1. / N_Norm))
        plt.close()
        plt.close()

