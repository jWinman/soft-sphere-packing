import numpy as np
import matplotlib.pyplot as plt

directory = "PhaseDiagram3"
Volume = np.loadtxt("../../data/{}/VolumeMin.txt".format(directory))

x = lambda D: int(D / 0.0025) - 600
y = lambda p: int(p / 0.0002) - 2

D = 2.03
ps = np.arange(0.0004, 0.02, 0.0002)
PackFrac = np.zeros_like(ps)

for i, p in enumerate(ps):
    PackFrac[i] = 4. / 3. * np.pi * 0.5**3 / Volume[x(D) + 1, y(p)]

fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)

phi321lower = 0.44
phi321upper = 0.5

phi321LSlower = 0.485
phi321LSupper = 0.545

phi321LSMean = phi321LSlower + (phi321LSupper - phi321LSlower) / 2.
phi321Mean = phi321lower + (phi321upper - phi321lower) / 2.
print(phi321LSMean)

ax1.set_title("$D = {:.4f}$".format(D))
ax1.plot(ps, PackFrac, ".")
ax1.plot(ps, np.ones(len(ps)) * phi321LSMean, "k")
ax1.plot(ps, np.ones(len(ps)) * phi321Mean, "g")

ax1.fill_between(ps, phi321lower, phi321upper, color="g", alpha=0.5)
ax1.fill_between(ps, phi321LSlower, phi321LSupper, color="k", alpha=0.5)

ax1.set_ylabel("Packing Frac. $\phi$")
ax1.set_xlabel("pressure $p$")
ax1.set_xlim(0., 0.02)
ax1.grid()

fig.savefig("../../Plots/{}/PackingFrac.pdf".format(directory))

