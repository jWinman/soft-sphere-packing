import numpy as np
import matplotlib.pyplot as plt

directory = "PhaseDiagram"
fileName = "MeanArea"
param = np.loadtxt("../../data/{}/{}.txt".format(directory, fileName), unpack=True)

def FindContour(param, threshold_x, threshold_y):
    points = []
    for i, parami in enumerate(param):
        for j, param_ij in enumerate(parami):
           # if (j > 0):
           #     if (abs(param_ij - param[i, j - 1]) > threshold_y):
           #         points.append((j, i))
            if (i > 0):
                if (abs(param_ij - param[i - 1, j]) > threshold_x):
                    points.append((j, i))
    return np.array(points).T

def extractLines(ps, Ds, upperDs):
    plines = [np.array([])] * 11
    Dlines = [np.array([])] * 11
    for i, (p, D) in enumerate(zip(ps, Ds)):
        if (i == 0):
            plines[0] = np.append(plines[0], p)
            Dlines[0] = np.append(Dlines[0], D)
        else:
            for j, (line, upperD) in enumerate(zip(plines, upperDs)):
#                print(D, p, i, j)
                if (len(line) == 0 and p > ps[i - 1]):
                    plines[j] = np.append(plines[j], p)
                    Dlines[j] = np.append(Dlines[j], D)
                    break;
                elif (p <= line[-1] and D < upperD):
                    plines[j] = np.append(plines[j], p)
                    Dlines[j] = np.append(Dlines[j], D)
                    break;
    return plines, Dlines

def appendHardSpheres(plines, Dlines, hardSpheresDs):
    for i in range(len(plines)):
        plines[i] = np.append(plines[i], 0)
        Dlines[i] = np.append(Dlines[i], hardSpheresDs[i])
    return plines, Dlines

# LineSlip data
pointsLS = FindContour(param[:315,:].T, 0.15, 0.2)
D_zLS = 1.5 + pointsLS[0] * 0.0025
pLS = 0.0004 + pointsLS[1] * 0.0002

pLS = pLS[np.argsort(D_zLS)]
D_zLS = np.sort(D_zLS)
upperDs = np.array([1.855, 1.9, 1.98, 2.0, 2.05, 2.1, 2.15, 2.165, 2.25, 2.2525, 2.3])
hardSphereDs = np.array([1.866, 1.87, 1.99, 2.0, 2.038, 2.041, 2.15, 2.154, 2.2247, 2.23, 2.2905])

plines, Dlines = extractLines(pLS, D_zLS, upperDs)
plines, Dlines = appendHardSpheres(plines, Dlines, hardSphereDs)
pU7 = np.append(0.02, plines[0])
D_zU7 = np.append(1.55, Dlines[0])
pU6  = np.append(0.02, plines[1])
D_zU6 = np.append(1.675, Dlines[1])
pU5  = np.append(0.02, plines[2])
D_zU5 = np.append(1.682, Dlines[2])

pLS8   = plines[3]
D_zLS8 = Dlines[3]
pLS7   = plines[4]
D_zLS7 = Dlines[4]
pLS6   = plines[5]
D_zLS6 = Dlines[5]
pLS5   = plines[6]
D_zLS5 = Dlines[6]
pLS4   = plines[7]
D_zLS4 = Dlines[7]
pLS3   = plines[8]
D_zLS3 = Dlines[8]
pLS2   = plines[9]
D_zLS2 = Dlines[9]
pLS1   = plines[10]
D_zLS1 = Dlines[10]

# Uniform data
pointsU1 = FindContour(param[266:305, 23:].T, 0.009, 0.01)
D_zU1 = 1.5 + 266 * 0.0025 + pointsU1[0] * 0.0025
pU1 = 0.0004 + 23 * 0.0002 + pointsU1[1] * 0.0002

D_zU1Red = D_zU1[np.any(np.array([D_zU1 < 2.2, pU1 < 0.015]), axis=0)]
pU1Red = pU1[np.any(np.array([D_zU1 < 2.2, pU1 < 0.015]), axis=0)]
D_zU1Red = np.append(2.171, D_zU1Red[::-1])
pU1Red = np.append(0.02, pU1Red[::-1])

pointsU2 = FindContour(param[228:265, 33:].T, 0.009, 0.01)
D_zU2 = 1.5 + 228 * 0.0025 + pointsU2[0] * 0.0025
pU2 = 0.0004 + 33 * 0.0002 + pointsU2[1] * 0.0002

D_zU2 = np.append(2.08, D_zU2[::-1])
pU2 = np.append(0.02, pU2[::-1])

pointsU3 = FindContour(param[190:220, 69:].T, 0.009, 0.01)
D_zU3 = 1.5 + 190 * 0.0025 + pointsU3[0] * 0.0025
pU3 = 0.0004 + 69 * 0.0002 + pointsU3[1] * 0.0002

D_zU3 = np.append(1.983, D_zU3[::-1])
pU3 = np.append(0.02, pU3[::-1])

pointsU4 = FindContour(param[118:181, 22:].T, 0.009, 0.01)
D_zU4 = 1.5 + 118 * 0.0025 + pointsU4[0] * 0.0025
pU4 = 0.0004 + 22 * 0.0002 + pointsU4[1] * 0.0002

D_zU4Red = D_zU4[np.any(np.array([D_zU4 > 1.86, pU4 > 0.011]), axis=0)]
pU4Red = pU4[np.any(np.array([D_zU4 > 1.86, pU4 > 0.011]), axis=0)]
D_zU4Red = np.append(1.7925, D_zU4Red[::-1])
pU4Red = np.append(0.02, pU4Red[::-1])

D_zU4Red2 = D_zU4Red[np.any(np.array([D_zU4Red > 1.9, pU4Red > 0.007]), axis=0)]
pU4Red2 = pU4Red[np.any(np.array([D_zU4Red > 1.9, pU4Red > 0.007]), axis=0)]

# inner line slip data
pointsInnerLS1 = FindContour(param[263:280, :25].T, 0.03, 0.01)
D_zInnerLS1 = 1.5 + 263 * 0.0025 + pointsInnerLS1[0] * 0.0025
pInnerLS1 = 0.0004 + pointsInnerLS1[1] * 0.0002

D_zInnerLS1Red = np.concatenate((
    np.array([2.195]),
    D_zInnerLS1[np.all(np.array([D_zInnerLS1 > 2.175, pInnerLS1 < 0.002]), axis=0)],
    D_zInnerLS1[np.all(np.array([D_zInnerLS1 < 2.185, pInnerLS1 < 0.0034]), axis=0)],
    D_zInnerLS1[np.all(np.array([D_zInnerLS1 < 2.175, pInnerLS1 < 0.0042]), axis=0)],
    D_zInnerLS1[np.all(np.array([D_zInnerLS1 < 2.17, pInnerLS1 < 0.005]), axis=0)],
))
pInnerLS1Red = np.concatenate((
    np.array([0]),
    pInnerLS1[np.all(np.array([D_zInnerLS1 > 2.175, pInnerLS1 < 0.002]), axis=0)],
    pInnerLS1[np.all(np.array([D_zInnerLS1 < 2.185, pInnerLS1 < 0.0034]), axis=0)],
    pInnerLS1[np.all(np.array([D_zInnerLS1 < 2.175, pInnerLS1 < 0.0042]), axis=0)],
    pInnerLS1[np.all(np.array([D_zInnerLS1 < 2.17, pInnerLS1 < 0.005]), axis=0)],
))

pInnerLS1Red = np.delete(pInnerLS1Red, -3)
D_zInnerLS1Red = np.delete(D_zInnerLS1Red, -3)

pInnerLS2 = np.array([0, 0.0008, 0.0012, 0.0016, 0.0020])
D_zInnerLS2 = np.array([2.265, 2.2625, 2.26, 2.2575, 2.2555])

####################################################################

# Areas
AreaZigZag1Y = pU7
AreaZigZag1X = D_zU7

AreaZigZag2X = np.concatenate((D_zU6, D_zU7[::-1]))
AreaZigZag2Y = np.concatenate((pU6, pU7[::-1]))

AreaTwistZigZagX = np.concatenate((D_zU5, D_zU6[::-1]))
AreaTwistZigZagY = np.concatenate((pU5, pU6[::-1]))

Area220X = np.concatenate((D_zU4Red2, D_zLS8, D_zU5[::-1]))
Area220Y = np.concatenate((pU4Red2, pLS8, pU5[::-1]))

Area220LSX = np.concatenate((D_zLS8[::-1], D_zLS7))
Area220LSY = np.concatenate((pLS8[::-1], pLS7))

Area321X = np.concatenate((D_zU4Red2, D_zLS7, D_zLS6[::-1], D_zU3[::-1]))
Area321Y = np.concatenate((pU4Red2, pLS7, pLS6[::-1], pU3[::-1]))

Area321LSX = np.concatenate((D_zLS6[::-1], D_zLS5))
Area321LSY = np.concatenate((pLS6[::-1], pLS5))

Area330X = np.concatenate((D_zU3, D_zLS5, D_zLS4[::-1], D_zU2[::-1]))
Area330Y = np.concatenate((pU3, pLS5, pLS4[::-1], pU2[::-1]))

Area422X = np.concatenate((D_zU2, D_zLS3, D_zLS2[::-1], D_zU1Red[::-1]))
Area422Y = np.concatenate((pU2, pLS3, pLS2[::-1], pU1Red[::-1]))

Area431X = np.concatenate((D_zU1Red, D_zLS1, np.array([2.3, 2.3])))
Area431Y = np.concatenate((pU1Red, pLS1, np.array([0, 0.02])))

Area330LS2X = np.concatenate((D_zInnerLS2, D_zLS1[11:]))
Area330LS2Y = np.concatenate((pInnerLS2, pLS1[11:]))

Area422LSX = np.concatenate((D_zLS2[::-1], D_zLS1[1:11], D_zInnerLS2[::-1]))
Area422LSY = np.concatenate((pLS2[::-1], pLS1[1:11], pInnerLS2[::-1]))

Area321LS2X = np.concatenate((D_zInnerLS1Red, D_zLS3[10:]))
Area321LS2Y = np.concatenate((pInnerLS1Red, pLS3[10:]))

Area330LSX = np.concatenate((D_zLS4[::-1], D_zLS3[:10], D_zInnerLS1Red[::-1]))
Area330LSY = np.concatenate((pLS4[::-1], pLS3[:10], pInnerLS1Red[::-1]))

DTransitionLine1X = np.concatenate((D_zU1Red, D_zLS1[:11], D_zInnerLS2[::-1]))
DTransitionLine1Y = np.concatenate((pU1Red, pLS1[:11], pInnerLS2[::-1]))
DTransitionLine2X = np.concatenate((D_zU2, D_zLS3[:10], D_zInnerLS1Red[::-1]))
DTransitionLine2Y = np.concatenate((pU2, pLS3[:10], pInnerLS1Red[::-1]))
DTransitionLine3X = np.concatenate((D_zU3, D_zLS5))
DTransitionLine3Y = np.concatenate((pU3, pLS5))

#####################################################

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

Colors = plt.get_cmap("terrain")(np.linspace(0, 1, 40))
alpha = 0.7
ex = 1

ax.fill_betweenx(AreaZigZag1Y, AreaZigZag1X**ex, color=Colors[0], alpha=alpha)
ax.fill_betweenx(AreaZigZag2Y, AreaZigZag2X**ex, color=Colors[3], alpha=alpha)
ax.fill_betweenx(AreaTwistZigZagY, AreaTwistZigZagX**ex, color=Colors[5], alpha=alpha)
ax.fill_betweenx(Area220Y, Area220X**ex, color=Colors[8], alpha=alpha)
ax.fill_betweenx(Area220LSY, Area220LSX**ex, color=Colors[10], alpha=alpha)
ax.fill_betweenx(Area321Y, Area321X**ex, color=Colors[14], alpha=alpha)
ax.fill_betweenx(Area321LSY, Area321LSX**ex, color=Colors[16], alpha=alpha)
ax.fill_betweenx(Area330Y, Area330X**ex, color=Colors[20], alpha=alpha)
ax.fill_betweenx(Area330LSY, Area330LSX**ex, color=Colors[23], alpha=alpha)
ax.fill_betweenx(Area321LS2Y, Area321LS2X**ex, color=Colors[16], alpha=alpha)
ax.fill_betweenx(Area422Y, Area422X**ex, color=Colors[26], alpha=alpha)
ax.fill_betweenx(Area422LSY, Area422LSX**ex, color=Colors[29], alpha=alpha)
ax.fill_betweenx(Area330LS2Y, Area330LS2X**ex, color=Colors[23], alpha=alpha)
ax.fill_betweenx(Area431Y, Area431X**ex, color=Colors[35], alpha=alpha)

linewidth = 2.

ax.plot(DTransitionLine1X**ex, DTransitionLine1Y, "k-", linewidth=linewidth)
ax.plot(DTransitionLine2X**ex, DTransitionLine2Y, "k-", linewidth=linewidth)
ax.plot(DTransitionLine3X**ex, DTransitionLine3Y, "k-", linewidth=linewidth)

ax.plot(D_zU4Red2**ex, pU4Red2, "k-", linewidth=linewidth)
ax.plot(D_zU5[1::2]**ex, pU5[1::2], "k--", linewidth=linewidth)
ax.plot(D_zU6**ex, pU6, "k--", linewidth=linewidth)
ax.plot(D_zU7[::2]**ex, pU7[::2], "k--", linewidth=linewidth)

ax.plot(D_zLS1**ex, pLS1, "k--", linewidth=linewidth)
ax.plot(np.append(D_zLS2[::-1], D_zU1[0])**ex, np.append(pLS2[::-1], pU1[0]), "k--", linewidth=linewidth)
ax.plot(D_zLS3[10:]**ex, pLS3[10:], "k--", linewidth=linewidth)
ax.plot(D_zLS4**ex, pLS4, "k--", linewidth=linewidth)
ax.plot(D_zLS5**ex, pLS5, "k--", linewidth=linewidth)
ax.plot(D_zLS6**ex, pLS6, "k--", linewidth=linewidth)
ax.plot(D_zLS7**ex, pLS7, "k--", linewidth=linewidth)
ax.plot(D_zLS8**ex, pLS8, "k--", linewidth=linewidth)

ax.set_ylim(0, 0.02)
ax.set_xlim(2., 2.225)
ax.set_ylabel(r"pressure $p$")
#ax.yaxis.set_label_coords(-0.17, 0.6)
ax.set_xlabel("diameter ratio $D / d$")
ax.xaxis.set_label_coords(0.3, -0.08)

ax.plot(np.array([2.04, 2.152, 2.225]), np.array([0, 0, 0]), "rD", markersize=5, clip_on=False)
#ax.plot(2.04, 0.0054, "ro", fillstyle="none", markersize=13., mew=3.)

from matplotlib import patches
e1 = patches.Ellipse((2.04, 0.0054), 0.06, 0.003, angle=0, linewidth=2, fill=False, color="r")

ax.add_patch(e1)

fsize = 12
ax.text(2.043, 0.0022, r"$(3, \bm{2}, 1)$" + "\n" + "line slip", fontsize=fsize)
ax.text(2.055, 0.01, "$(3, 3, 0)$ \n uniform", fontsize=fsize)
ax.annotate("$(3, 2, 1)$ \n uniform", xy=(2.007, 0.0065), xytext=(1.93, 0.005), arrowprops=dict(facecolor='k', shrink=0.0001, width=0.75, headwidth=4.), fontsize=fsize, ha="center", va="center")
ax.text(2.15, 0.01, "$(4, 2, 2)$ \n uniform", fontsize=fsize)
ax.annotate("$(4, 3, 1)$ \n uniform", xy=(2.215, 0.015), xytext=(2.23, 0.01), arrowprops=dict(facecolor='r', color="k", shrink=0.0001, width=0.75, headwidth=4.), fontsize=fsize)
ax.annotate(r"$(3, \bm{3}, 0)$" + "\n" + "line slip", xy=(2.17, 0.0015), xytext=(2.23, 0.0045), arrowprops=dict(facecolor='black', shrink=0.0001, width=0.75, headwidth=4.), fontsize=fsize)
ax.annotate(r"$(3, \bm{2}, 1)$" + "\n" + "line slip", xy=(2.205, 0.0007), xytext=(2.23, 0.0015), arrowprops=dict(facecolor='black', shrink=0.0001, width=0.75, headwidth=4.), fontsize=fsize)
ax.annotate(r"$(2, \bm{2}, 0)$" + "\n" + "line slip", xy=(2.007, 0.0004), xytext=(1.93, 0.001), arrowprops=dict(facecolor='black', shrink=0.0001, width=0.75, headwidth=4.), fontsize=fsize, ha="center")
#ax.annotate(r"see note in caption$^*$", xy=(2.15, 0.), xytext=(2.15, -0.0025), arrowprops=dict(facecolor='red', color="red", shrink=0.0001, width=0.75, headwidth=4.), fontsize=12, va="center", zorder=0)


fig.savefig("../../Plots/{}/phasediagramContour.pdf".format(directory))
