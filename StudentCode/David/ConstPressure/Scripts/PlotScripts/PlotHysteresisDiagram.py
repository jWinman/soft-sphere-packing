import numpy as np
import matplotlib.pyplot as plt

N = 3
directory = "HysteresesN" + str(N)
fileName = "MeanArea"
barName = "\mu_{A" if "Mean" in fileName else "\sigma_{A"
barName += "_{Triangle}}" if ("Triangle" in fileName) else "}"

param = np.loadtxt("../../data/{}/{}.txt".format(directory, fileName), unpack=True).T

@np.vectorize
def roundMean(param, paramMean):
    if (param > paramMean):
        return 0
    else:
        return 1

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

if (N == 3):
    plt.imshow(param[:, :396], origin="lower", interpolation="nearest", cmap="terrain", aspect=3.)
    plt.xticks(np.arange(0, 396)[::50], np.arange(1.5, 2.49, 0.0025)[::50])

    ax.text(40, 10, r"zig-zag", fontsize=6, color="black")
    ax.text(143, 5, r"twisted" + "\n" + "zig-zag", fontsize=5, color="black")
    ax.text(148, 50, r"$(3, 2, 1)$" + "\n" + "uniform", fontsize=5, color="white")
    ax.text(215, 2, r"$(\bm{3}, 2, 1)$" + "\n" + "line slip", fontsize=3, color="white")
    ax.text(199, 80, r"$(3, 3, 0)$" + "\n" + "uniform", fontsize=5, color="white")
    ax.text(262, 2, r"$(3, \bm{3}, 0)$" + "\n" + "line slip", fontsize=4, color="white")
    ax.text(257, 80, r"$(4, 3, 1)$" + "\n" + "uniform", fontsize=5, color="white")
    ax.text(323, 5, r"$(4, \bm{3}, 1)$" + "\n" + "line slip", fontsize=5, color="white")
    ax.text(338, 80, r"$(5, 3, 2)$" + "\n" + "uniform", fontsize=5, color="white")

#    ax.annotate("", xy=(5, 20), xytext=(22, 21), arrowprops=dict(arrowstyle="->"))


if (N == 4):
    plt.imshow(param[:, :360], origin="lower", interpolation="nearest", cmap="terrain", aspect=3.)
    plt.xticks(np.arange(0, 360)[::50], np.arange(1.5, 2.49, 0.0025)[::50])

    ax.text(30, 15, r"zig-zag", fontsize=6, color="black")
    ax.text(140, 5, r"twisted" + "\n" + "zig-zag", fontsize=3.5, color="white")
    ax.text(118, 50, r"$(2, 2, 0)$" + "\n" + "uniform", fontsize=3.5, color="white")
    ax.text(160, 60, r"$(3, 2, 1)$" + "\n" + "uniform", fontsize=5, color="white")
    ax.text(222, 5, r"$(3, \bm{2}, 1)$" + "\n" + "line slip", fontsize=5, color="white")
    ax.text(237, 60, r"$(4, 2, 2)$" + "\n" + "uniform", fontsize=5, color="white")
    ax.text(290, 60, r"$(4, 3, 1)$" + "\n" + "uniform", fontsize=4, color="white", rotation=90)
    ax.text(317, 3, r"$(\bm{4}, 3, 1)$" + "\n" + "line slip", fontsize=4, color="white")
    ax.text(321, 60, r"$(4, 4, 0)$" + "\n" + "uniform", fontsize=4, color="white")

    ax.annotate("", xy=(160, -2), xytext=(160, 100), arrowprops=dict(arrowstyle="-", color="red"))

plt.yticks(np.arange(-2, 98)[::25], np.arange(0, 0.02, 0.0002)[::25])

plt.colorbar(label=r"${}$".format(barName), shrink=0.33)
plt.ylabel("pressure $p$")
plt.xlabel("diameter ratio $D / d$")

fig.savefig("../../Plots/{}/Phasediagram{}.pdf".format(directory, fileName), dpi=1000)
