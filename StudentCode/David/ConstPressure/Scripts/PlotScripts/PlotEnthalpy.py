import numpy as np
import matplotlib.pyplot as plt

directory = "PhaseDiagram"
Ds = np.arange(1.5, 2.7, 0.0025)
ps = np.arange(0., 0.02, 0.0002)

Enthalpy = np.loadtxt("../../data/{}/EnthalpyMin.txt".format(directory))
Energy = np.loadtxt("../../data/{}/EnergyMin.txt".format(directory))
Volume = np.loadtxt("../../data/{}/VolumeMin.txt".format(directory))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

plt.imshow(np.diff(Enthalpy, n=2, axis=0).T, origin="lower", interpolation="nearest", cmap="terrain", vmin=-5e-6, vmax=5e-6, aspect=3)
plt.colorbar(label=r"$\frac{\partial^2 H}{\partial D^2}$", shrink=0.33)
plt.yticks(np.arange(-2, 98)[::25], ps[::25])
plt.ylabel("pressure $p$")
plt.xticks(np.arange(0, 481)[::100], Ds[::100])
plt.xlabel("diameter ratio $D / d$")

fig.savefig("../../Plots/{}/Enthalpy.pdf".format(directory), dpi=1000)
