import numpy as np
import matplotlib.pyplot as plt

directory = "PhaseDiagram"
Enthalpy = np.loadtxt("../../data/{}/EnthalpyMin.txt".format(directory))
Energy = np.loadtxt("../../data/{}/EnergyMin.txt".format(directory))
Volume = np.loadtxt("../../data/{}/VolumeMin.txt".format(directory))

x = lambda D: int(D / 0.0025) - 600
y = lambda p: int(p / 0.0002) - 2

Ds = np.arange(2.0, 2.17, 0.0025)
p = 0.01

hardSphere = [x(2.035), x(2.045), x(2.145), x(2.22)]
Compression = np.zeros_like(Ds)

for i, D in enumerate(Ds):
    if (D <= 2.020):
        Compression[i] = Volume[x(D) + 1, y(p)] / Volume[hardSphere[0], 0]
    elif (D <= 2.04):
        Compression[i] = Volume[x(D) + 1, y(p)] / Volume[hardSphere[1], 0]
    elif (D <= 2.13):
        Compression[i] = Volume[x(D) + 1, y(p)] / Volume[hardSphere[2], 0]
    else:
        Compression[i] = Volume[x(D) + 1, y(p)] / Volume[hardSphere[3], 0]

fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)

ax1.plot(Ds, Enthalpy[x(Ds[0])-1:x(Ds[-1])+1, y(p)], ".")
ax1.set_ylabel("Enthalpy $h$")
ax1.set_xlim(2., 2.17)

ax2 = fig.add_subplot(2, 1, 2)

ax2.plot(Ds, 1 - Compression, ".")
ax2.set_ylabel("Compression ratio $C$")
ax2.set_xlabel("diameter ratio $D / d$")
ax2.set_ylim(0., 0.1)
ax2.set_xlim(2., 2.17)


Colors = plt.get_cmap("terrain")(np.linspace(0, 1, 40))
alpha = 0.7

ax1.axvspan(2., 2.025, color=Colors[14], alpha=alpha)
ax1.axvspan(2.025, 2.0425, color=Colors[16], alpha=alpha)
ax1.axvspan(2.0425, 2.1325, color=Colors[20], alpha=alpha)
ax1.axvspan(2.1325, 2.17, color=Colors[26], alpha=alpha)

ax1.annotate("$(3, 2, 1)$ \n uniform", xy=(2.015, 0.01015), xytext=(2.015, 0.0103), arrowprops=dict(facecolor="k", shrink=1e-5, width=0.75, headwidth=4.), fontsize=12, ha="right")
ax1.annotate(r"$(3, \bm{2}, 1)$" + "\n" + "line slip", xy=(2.035, 0.01015), xytext=(2.035, 0.0103), arrowprops=dict(facecolor="k", shrink=1e-5, width=0.75, headwidth=4.), fontsize=12, ha="left")
ax1.text(2.07, 0.00995, "$(3, 3, 0)$ \n uniform", fontsize=12)
ax1.annotate("$(4, 2, 2)$ \n uniform", xy=(2.15, 0.01), xytext=(2.175, 0.01), arrowprops=dict(facecolor="k", shrink=1e-5, width=0.75, headwidth=4.), fontsize=12, va="center")

ax1.text(2.173, 0.01020, "(a)")

ax2.axvspan(2., 2.0225, color=Colors[14], alpha=alpha)
ax2.axvspan(2.0225, 2.0425, color=Colors[16], alpha=alpha)
ax2.axvspan(2.0425, 2.1325, color=Colors[20], alpha=alpha)
ax2.axvspan(2.1325, 2.17, color=Colors[26], alpha=alpha)

ax2.text(2.173, 0.09, "(b)")


fig.savefig("../../Plots/{}/EnthalpyConstP.pdf".format(directory))
plt.close()

D = 2.10
ps = np.arange(0.0004, 0.02, 0.0002)
Compression = np.zeros_like(ps)
print(np.shape(Volume))

for i, p in enumerate(ps):
#    print(i, p, y(p))
    if (p <= 0.003):
        Compression[i] = Volume[x(D) + 1, y(p)] / Volume[x(D) + 1, 0]
    elif (p <= 0.0155):
        Compression[i] = Volume[x(D) + 1, y(p)] / Volume[hardSphere[2], 0]
    else:
        Compression[i] = Volume[x(D) + 1, y(p)] / Volume[hardSphere[3], 0]

fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)

ax1.plot(ps, Enthalpy[x(D), y(ps[0]):y(ps[-1]) + 1], ".")
ax1.set_ylabel("Enthalpy $h$")
ax1.set_xlim(0., 0.02)

ax2 = fig.add_subplot(2, 1, 2)

ax2.plot(ps, 1 - Compression, ".")
ax2.set_ylabel("Compression ratio $C$")
ax2.set_xlabel("pressure $p$")
ax2.set_xlim(0., 0.02)

ax1.axvspan(0., 0.003, color=Colors[16], alpha=alpha)
ax1.axvspan(0.003, 0.0155, color=Colors[20], alpha=alpha)
ax1.axvspan(0.0155, 0.02, color=Colors[26], alpha=alpha)

ax1.annotate(r"$(3, \bm{2}, 1)$" + "\n" + "line slip", xy=(0.001, 0.019), xytext=(0.001, 0.023), arrowprops=dict(facecolor="k", shrink=1e-5, width=0.75, headwidth=4.), fontsize=12, ha="center")
ax1.text(0.004, 0.01, "$(3, 3, 0)$ \n uniform", fontsize=12)
ax1.annotate("$(4, 2, 2)$ \n uniform", xy=(0.019, 0.01), xytext=(0.022, 0.01), arrowprops=dict(facecolor="k", shrink=1e-5, width=0.75, headwidth=4.), fontsize=15, va="center")

ax1.text(0.0205, 0.019, "(a)")

ax2.axvspan(0., 0.003, color=Colors[16], alpha=alpha)
ax2.axvspan(0.003, 0.0155, color=Colors[20], alpha=alpha)
ax2.axvspan(0.0155, 0.02, color=Colors[26], alpha=alpha)

ax2.text(0.0205, 0.11, "(b)")

fig.savefig("../../Plots/{}/EnthalpyConstD.pdf".format(directory))
