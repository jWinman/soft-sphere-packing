import numpy as np
import matplotlib
import matplotlib.pyplot as plt

areas = np.array([])
Ds = np.arange(2.039, 2.44, 0.001)
psScatt = np.array([])
DsScatt = np.array([])

for D in Ds:
    ps, area = np.genfromtxt("../../data/Adil/UpperSweap_CGPhyllo/Areas{:.4f}.txt".format(D), unpack=True)
    psScatt = np.append(psScatt, ps)
    DsScatt = np.append(DsScatt, np.ones(len(ps)) * D)
    areas = np.append(areas, area)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, aspect="auto")

sc = ax.scatter(DsScatt, psScatt, c=areas, marker="s", cmap="terrain", edgecolors="face", s=100.)

ax.set_ylabel("pressure $p$")
ax.set_xlabel("diameter ratio $D / d$")
ax.set_ylim(0, 0.021)
ax.set_xlim(2.039, 2.44)

ax.text(2.05, 0.002, r"$(3, \bm{2}, 1)$ ls")
ax.text(2.13, 0.01, r"$(4, 2, 2)$", color="white")
#ax.text(2.25, 0.005, r"$(4, \bm{2}, 2)$ ls")
ax.text(2.3, 0.015, r"$(4, 4, 0)$", color="white")
ax.annotate(r"$(4, \bm{4}, 0)$ ls", xy=(2.4, 0.02), xytext=(2.37, 0.022),
            arrowprops=dict(arrowstyle="->", linewidth=1.))

#ax.text(1.65, 0.05, "$(3, 2, 1)$", fontsize=10)
#ax.text(1.93, 0.08, "$(4, 2, 2)$", fontsize=10)
#ax.annotate(r"$(3, \bm{2}, 1)$ls", xy=(2.1, 0), xytext=(2.03, -0.11), fontsize=10,
#            arrowprops=dict(arrowstyle="->", linewidth=1.))
#ax.annotate(r"$(4, \bm{2}, 2)$ls", xy=(2.3, 0), xytext=(2.25, -0.11), fontsize=10,
#            arrowprops=dict(arrowstyle="->", linewidth=1.))
#ax.annotate(r"$(2, 2, 0)$", xy=(1.7, 0), xytext=(1.63, -0.11), fontsize=10,
#            arrowprops=dict(arrowstyle="->", linewidth=1.))
#ax.annotate(r"$(2, \bm{2}, 0)$ls", xy=(1.97, 0), xytext=(1.8, -0.11), fontsize=10,
#            arrowprops=dict(arrowstyle="->", linewidth=1.))
#ax.annotate(r"$(5, 3, 2)$ \newline \& $(6, 3, 3)$", xy=(2.03, 0.38), xytext=(2.25, 0.37), fontsize=10,
#            arrowprops=dict(arrowstyle="->", linewidth=1.))
#ax.annotate(r"", xy=(2.3, 0.15), xytext=(2.38, 0.37), fontsize=10, ha="center",
#            arrowprops=dict(arrowstyle="->", linewidth=1.))


fig.savefig("../../Plots/Adil/AreaUpperSweap_CG.pdf")
