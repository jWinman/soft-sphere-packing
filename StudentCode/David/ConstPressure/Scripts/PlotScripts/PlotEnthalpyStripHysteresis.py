import numpy as np
import matplotlib.pyplot as plt

N = 3
directory = "HysteresesN" + str(N)
Enthalpy = np.loadtxt("../../data/{}/EnthalpyMin.txt".format(directory))
Energy = np.loadtxt("../../data/{}/EnergyMin.txt".format(directory))
Volume = np.loadtxt("../../data/{}/VolumeMin.txt".format(directory))

x = lambda D: int(D / 0.0025) - 600
y = lambda p: int(p / 0.0002) - 2

Ds = np.arange(1.5, 2.7, 0.0025)
p = 0.001

fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)

ax1.plot(Ds, Enthalpy[:, y(p)], ".")
ax1.set_ylabel("Enthalpy $h$")
ax1.set_xlabel("Diameter ratio $D / d$")
ax1.grid()

ax1.set_ylim(0.0008, 0.00165)

if (N == 3):
    ax1.set_xlim(1.5, 2.54)
    ax1.annotate("zig-zag", xy=(1.75, Enthalpy[x(1.75), y(p)]), xytext=(1.9, 0.0016), arrowprops=dict(arrowstyle="->"))
    ax1.annotate("twisted" + "\n" + "zig-zag", xy=(1.93, Enthalpy[x(1.93), y(p)]), xytext=(2.05, 0.00135), arrowprops=dict(arrowstyle="->"))
    ax1.annotate("$(3, 2, 1)$" + "\n" + "uniform", xy=(2.04, Enthalpy[x(2.04), y(p)]), xytext=(1.95, 0.001), arrowprops=dict(arrowstyle="->"), ha="right", va="center")
    ax1.annotate("$(3, 3, 0)$" + "\n" + "uniform", xy=(2.15, Enthalpy[x(2.15), y(p)]), xytext=(2.13, 0.0009), arrowprops=dict(arrowstyle="->"), ha="right", va="center")
    ax1.annotate("$(4, 3, 1)$" + "\n" + "uniform", xy=(2.29, Enthalpy[x(2.29), y(p)]), xytext=(2.31, 0.00087), arrowprops=dict(arrowstyle="->"), ha="right", va="center")
    ax1.annotate("$(5, 3, 2)$" + "\n" + "uniform", xy=(2.49, Enthalpy[x(2.49), y(p)]), xytext=(2.51, 0.00085), arrowprops=dict(arrowstyle="->"), ha="right", va="center")

if (N == 4):
    ax1.set_xlim(1.5, 2.45)
    ax1.annotate("zig-zag", xy=(1.75, Enthalpy[x(1.75), y(p)]), xytext=(1.9, 0.0016), arrowprops=dict(arrowstyle="->"))
    ax1.annotate("twisted" + "\n" + "zig-zag", xy=(1.9, Enthalpy[x(1.9), y(p)]), xytext=(2.0, 0.0014), arrowprops=dict(arrowstyle="->"))
    ax1.annotate("$(2, 2, 0)$" + "\n" + "uniform", xy=(1.99, Enthalpy[x(1.99), y(p)]), xytext=(1.9, 0.0011), arrowprops=dict(arrowstyle="->"), ha="right", va="center")
    ax1.annotate("$(3, 2, 1)$" + "\n" + "uniform", xy=(2.04, Enthalpy[x(2.04), y(p)]), xytext=(1.97, 0.00096), arrowprops=dict(arrowstyle="->"), ha="right", va="center")
    ax1.annotate("$(4, 2, 2)$" + "\n" + "uniform", xy=(2.22, Enthalpy[x(2.22), y(p)]), xytext=(2.18, 0.00088), arrowprops=dict(arrowstyle="->"), ha="right", va="center")
    ax1.annotate("$(4, 3, 1)$" + "\n" + "uniform", xy=(2.29, Enthalpy[x(2.29), y(p)]), xytext=(2.43, 0.00087), arrowprops=dict(arrowstyle="->"), ha="right", va="center")
    ax1.annotate("$(4, 4, 0)$" + "\n" + "uniform", xy=(2.41, Enthalpy[x(2.41), y(p)]), xytext=(2.42, 0.0011), arrowprops=dict(arrowstyle="->", connectionstyle="arc3, rad=-0.45"), ha="right", va="center")
#    ax1.annotate("$(5, 3, 2)$" + "\n" + "uniform", xy=(2.49, Enthalpy[x(2.49), y(p)]), xytext=(2.51, 0.00085), arrowprops=dict(arrowstyle="->"), ha="right", va="center")


fig.savefig("../../Plots/{}/EnthalpyConstP.pdf".format(directory))
plt.close()
