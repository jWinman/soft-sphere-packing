import numpy as np
import matplotlib.pyplot as plt

directory = "PhaseDiagram"
fileName = "MeanArea"
barName = "\mu_{A" if "Mean" in fileName else "\sigma_{A"
barName += "_{Triangle}}" if ("Triangle" in fileName) else "}"

param = np.loadtxt("../../data/{}/{}.txt".format(directory, fileName), unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

plt.imshow(param.T, origin="lower", interpolation="nearest", cmap="terrain", aspect=3.)
plt.colorbar(label=r"${}$".format(barName), shrink=0.33)
plt.yticks(np.arange(-2, 98)[::25], np.arange(0, 0.02, 0.0002)[::25])
plt.ylabel("pressure $p$")
plt.xticks(np.arange(0, 481)[::50], np.arange(1.5, 2.7, 0.0025)[::50])
plt.xlabel("diameter ratio $D / d$")

fsizeUniform = 2.6
ax.text(20, 20, r"zigzag", fontsize=fsizeUniform)
ax.text(140, 5, r"twisted" + "\n" + "zigzag", fontsize=fsizeUniform)
ax.text(50, 70, r"straight" + "\n" + "zigzag", fontsize=fsizeUniform)
ax.text(107, 60, r"$(2, 2, 0)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
ax.text(217, 50, r"$(3, 3, 0)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
ax.text(160, 60, r"$(3, 2, 1)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
ax.text(248, 60, r"$(4, 2, 2)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
ax.text(275, 80, r"$(4, 3, 1)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
ax.text(320, 70, r"$(4, 4, 0)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
ax.text(353, 80, r"$(5, 3, 2)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
ax.text(390, 70, r"$(5, 4, 1)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
ax.text(440, 70, r"$(5, 5, 0)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")


fig.savefig("../../Plots/{}/Phasediagram{}.pdf".format(directory, fileName), dpi=1000)
