import numpy as np
import matplotlib.pyplot as plt
import subprocess

Ds = np.arange(1.5, 2.7, 0.01)
p = 0.001
directory = "PhaseDiagram"

x = lambda D: int(D / 0.0025) - 600
y = lambda p: int(p / 0.0002) - 2

subprocess.call("rm ../../Plots/{}/Animation/animation-*.pdf".format(directory), shell=True)
subprocess.call("mkdir -p ../../Plots/{}/Animation".format(directory), shell=True)

for j, D in enumerate(Ds):
    print(j, D)

    fig = plt.figure()

    # Upper plot -- Phyllotaxis
    axUpper = plt.subplot2grid( (2,2), [0,0], 1, 1, aspect="equal")
#    axUpper.set_title("$D / d = {:.4f}$, $p = {:.6f}$".format(D, p))

    th, z = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Nodes.csv".format(directory, p, D), unpack=True)
    thunit, zunit = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Unit.csv".format(directory, p, D), unpack=True)
    links = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Edges.csv".format(directory, p, D))

    axUpper.plot(th, z, "b.")
    axUpper.plot(thunit, zunit, "rD")
    for i in range(1, len(links)):
        if (i % 2 == 1):
            th1 = links[i][0]
            z1 = links[i][1]
            th2 = links[i + 1][0]
            z2 = links[i + 1][1]
            axUpper.plot([th1, th2], [z1, z2], "b-")
    axUpper.set_ylim(-5, 5)
    axUpper.set_xlim(-5, 5)
    axUpper.set_ylabel("$z$")
    axUpper.set_xlabel(r"$R_z \theta$")

    # Lower left plot -- PhaseDiagram
    axLeft = plt.subplot2grid( (2,2), [1,0], 1, 2 , aspect=3.5)

    param = np.loadtxt("../../data/{}/MeanArea.txt".format(directory), unpack=True)

    plt.imshow(param.T, origin="lower", interpolation="nearest", cmap="terrain", aspect=3)
    plt.yticks(np.arange(-2, 98)[::25], np.arange(0, 0.02, 0.0002)[::25])
    plt.ylabel("pressure $p$")
    plt.xticks(np.arange(0, 481)[::100], np.arange(1.5, 2.7, 0.0025)[::100])
    plt.xlabel("diameter ratio $D / d$")

    fsizeUniform = 3
    axLeft.text(30, 20, r"zigzag", fontsize=6)
    axLeft.text(145, 5, r"twisted" + "\n" + "zigzag", fontsize=fsizeUniform)
    axLeft.text(50, 70, r"straight" + "\n" + "zigzag", fontsize=fsizeUniform)
    axLeft.text(110, 60, r"$(2, 2, 0)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
    axLeft.text(160, 60, r"$(3, 2, 1)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
    axLeft.text(217, 50, r"$(3, 3, 0)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
    axLeft.text(248, 60, r"$(4, 2, 2)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
    axLeft.text(275, 80, r"$(4, 3, 1)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
    axLeft.text(320, 70, r"$(4, 4, 0)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
    axLeft.text(353, 80, r"$(5, 3, 2)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
    axLeft.text(390, 70, r"$(5, 4, 1)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")
    axLeft.text(440, 70, r"$(5, 5, 0)$" + "\n" + "uniform", fontsize=fsizeUniform, color="white")

    axLeft.plot(x(D), y(p), "ro")

    axRight = plt.subplot2grid( (2,2), [0,1], 1, 1 , aspect="auto")

    Enthalpy = np.loadtxt("../../data/{}/EnthalpyMin.txt".format(directory))
    D_zs = np.arange(1.5, 2.7, 0.0025)
    axRight.plot(D_zs, Enthalpy[:, y(p)], ".")
    axRight.plot(D, Enthalpy[x(D), y(p)], "ro")
    axRight.set_ylabel("Enthalpy $h$")
    axRight.set_xlabel("Diameter ratio $D / d$")

    fig.savefig("../../Plots/{}/Animation/animation-{:d}.png".format(directory, j), dpi=1000)
    plt.close()
