import sys
sys.path.append("../../libraries")

import classSpheres

import numpy as np
import matplotlib.pyplot as plt
import os

def EnergyFct(mySystem, p):
    mySystem.setVariable("p", 0)
    E = mySystem.Energy(mySystem.positions) / mySystem.N
    mySystem.setVariable("p", p)
    return E

def EnthalpyFct(mySystem):
    return mySystem.Energy(mySystem.positions) / mySystem.N

def VolumeFct(mySystem):
    R_z = mySystem.R_z
    L = mySystem.positions[-1]
    return R_z**2 * np.pi * L / mySystem.N

def Trajectory(N, ps, Ds, directory):
    os.chdir("..")
    Energy = np.zeros(len(Ds))
    Enthalpy = np.zeros(len(Ds))
    Volume = np.zeros(len(Ds))

    for i, (p, D) in enumerate(zip(ps, Ds)):
        mySystem = classSpheres.SpheresCylinder("parameter.cfg")
        initial = "{}/N{}/Finalp{:.6f}D{:.4f}".format(directory, N, p, D)
        mySystem.ReadInitial(initial)
        mySystem.setVariable("p", p)
        mySystem.setVariable("N", N)
        mySystem.setVariable("R_z", D / 2.)
        Enthalpy[i] = EnthalpyFct(mySystem)
        Energy[i] = EnergyFct(mySystem, p)
        Volume[i] = VolumeFct(mySystem)

    os.chdir("Scripts/")
    return Enthalpy, Energy, Volume

ps = np.arange(0.009, 0.03, 0.0002)
Ds = np.ones(len(ps)) * 2.05
#Ds = np.arange(1.91, 2.03, 0.005)
#ps = np.ones(len(Ds)) * 0.025
N = 12
number1 = "5Pertubed"
number2 = "5Pertubed"
directoryForward = "TrajectorySteepest/Trajectory{}Forward".format(number1)
EnthalpyForward, EnergyForward, VolumeForward = Trajectory(N, ps, Ds, directoryForward)
directoryBackward = "TrajectorySteepest/Trajectory{}Backward".format(number2)
EnthalpyBackward, EnergyBackward, VolumeBackward = Trajectory(N, ps, Ds, directoryBackward)

pForward, _, _, HForward = np.loadtxt("../data/Adil/ForwardTraj.txt", unpack=True)
pBackward, _, _, HBackward = np.loadtxt("../data/Adil/BackwardTraj.txt", unpack=True)
pBackward = pBackward[::-1]
HBackward = HBackward[::-1]

fig = plt.figure()
ax = fig.add_subplot(2, 1, 1)
ax.set_title("$D/d = {:.3f}$".format(Ds[0]))
ax.plot(ps / 1e-3, EnthalpyForward, "b.", label="Forward")
#ax.plot(ps / 1e-3, EnthalpyBackward, "b.", markersize=4., label="Reversed")
ax.plot(pForward / 1e-3, HForward, "b+", label="Adil Forward")
#ax.plot(pBackward / 1e-3, HBackward, "b+", label="Adil Backward")
ax.set_ylabel("$H = E + pV$")
ax.set_ylim(0.01, 0.02)
ax.set_xlim(10, 20)
ax.legend(loc="upper left")

#ax.axvline(1.98, color="b", linestyle="--")
#ax.axvline(1.995, color="g", linestyle="--")
#ax.text(1.92, 0.0248, r"$(3, 2, 1)$", fontsize=12)
#ax.text(2.02, 0.0248, r"$(4, 2, 2)$", fontsize=12)
#ax.axvline(7.6, color="b", linestyle="-.")
#ax.axvline(17.6, color="b", linestyle="--")
#ax.axvline(4.4, color="g", linestyle="-.")
#ax.axvline(15.6, color="g", linestyle="--")

ax2 = fig.add_subplot(2, 1, 2)
ax2.plot(ps[:-1] / 1e-3, np.diff(EnthalpyForward) / np.diff(ps), "b.")
#ax2.plot(ps[:-1] / 1e-3, np.diff(EnthalpyBackward) / np.diff(ps), "b.", markersize=4.)
ax2.plot(pForward[:-1] / 1e-3, np.diff(HForward) / np.diff(pForward), "b+")
#ax2.plot(pBackward[:-1] / 1e-3, np.diff(HBackward) / np.diff(pBackward), "b+")
ax2.set_ylabel("$\partial H / \partial p$")
ax2.set_xlabel("pressure $p / 10^{-3}$")
ax2.set_ylim(0.8, 1.)
ax2.set_xlim(10, 20)

#ax2.axvline(7.6, color="b", linestyle="-.")
#ax2.axvline(17.6, color="b", linestyle="--")
#ax2.axvline(4.4, color="g", linestyle="-.")
#ax2.axvline(15.6, color="g", linestyle="--")
#ax2.text(0.5, 0.00036, r"$(3, 2, 1)$", fontsize=8)
#ax2.text(5, 0.00036, r"$(3, \bm{2}, 1)$", fontsize=8)
#ax2.text(24, 0.00036, r"$(4, 2, 2)$", fontsize=8)

fig.savefig("../Plots/{}/Enthalpy{}N{}.pdf".format("TrajectorySteepest", number2, N))
