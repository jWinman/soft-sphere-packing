import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append("../libraries")
import classSpheres


E_list=[]
N_norm_list=[]
E_list_48 = []
N_norm_list_48=[]

# dont forget to fill theses in pleae
N = 24
k = 100
RR = 10.
print "RR=",RR
for N_norm in np.arange(3.66, 4.40, .01):
	#initial = "HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_K{:.3f}_N_norm{:.4f}".format(N, N, RR,k, N_norm)
	initial = "HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_K{:.3f}_N_norm{:.4f}".format(N, N, RR,k, N_norm)
	mySystem = classSpheres.SpheresCylinder("parameter.cfg")
	mySystem.setVariable("RR", RR)
	mySystem.setVariable("R_i", 1)
	mySystem.setVariable("eps",k)
	mySystem.setVariable("N_norm", N_norm)
	mySystem.setVariable("N", N)
	mySystem.setVariable("L", N / N_norm)
	mySystem.ReadInitial(initial)
	#print(mySystem.Energy(mySystem.positions) / N)
	#adding this energy for specidifc r_i to energy list
	N_norm_list.append(1/N_norm)
	E_list.append(mySystem.Energy(mySystem.positions) / N)

'''
for N_norm48 in np.arange(2.85, 3.17, .01):
	N=48
	initial = "HardSpheres/compare_N{:.0f}/constructed/Final_N{}_RR0.200000_K50.0_N_norm{:.4f}".format(N,N, N_norm48)
	mySystem = classSpheres.SpheresCylinder("parameter.cfg")
	mySystem.setVariable("RR", RR)
	mySystem.setVariable("R_i", 1)
	mySystem.setVariable("eps",k)
	mySystem.setVariable("N_norm", N_norm48)
	mySystem.setVariable("N", N)
	mySystem.setVariable("L", N / N_norm48)
	mySystem.ReadInitial(initial)
	print(mySystem.Energy(mySystem.positions) / N)
	#print(mySystem.Energy(mySystem.positions) / N)
	#adding this energy for specidifc r_i to energy list
	N_norm_list_48.append(1/N_norm48)
	E_list_48.append(mySystem.Energy(mySystem.positions) / N)
'''
l220, E220 = np.loadtxt("data/VaryOmega_k/Omega_{:.1f}k_{}_220Analytic.csv".format(RR,k),unpack=True)
l321, E321 = np.loadtxt("data/VaryOmega_k/Omega_{:.1f}k_{}_321.csv".format(RR,k),unpack=True)
l330, E330 = np.loadtxt("data/VaryOmega_k/Omega_{:.1f}k_{}_330Analytic.csv".format(RR,k),unpack=True)
lh, Eh = np.loadtxt("data/VaryOmega_k/HardSpheres_Omegas_0.2.txt",unpack=True)
l422, E422= np.loadtxt("data/VaryOmega_k/Omega_{:.1f}k_{}_422.csv".format(RR,k),unpack=True)
l440, E440= np.loadtxt("data/VaryOmega_k/Omega_{:.1f}k_{}_440Analytic.csv".format(RR,k),unpack=True)
#l431, E431 = np.loadtxt("data/VaryOmega_k/Omega_{:.1f}k_{}_431.csv".format(RR,k),unpack=True)
def const_tangent_ploter(leftE,leftL,rightE, rightL):
	####common tangent drawer######
	slopes1 = []
	slopes2 = []


	slopes1 = np.diff(rightE)/ np.diff(rightL)
	slopes2 = np.diff(leftE)/ np.diff(leftL)
	point1=[]
	point2=[]
	toggle = 0

	for i in np.arange(np.argmin(rightE),np.argmin(rightE)-50,-1):
		for j in np.arange(np.argmin(leftE),np.argmin(leftE)-50,-1):
			if (abs(slopes1[i]-slopes2[j])<=.005):
				b1 = rightE[i]- slopes1[i]*rightL[i]
				b2	= leftE[j] - slopes2[j]*leftL[j]
				if(abs(b1-b2)<= .005):
					x1 = rightL[i]
					y1 = rightE[i]
					x2 = leftL[j]
					y2 = leftE[j]
					toggle = 1
					break 

	if (toggle ==1):
		plt.plot([x1,x2],[y1,y2],"-", color="purple")
		print x1,y1,x2,y2
	else:
		print "there is none"

#E_list, R_i = np.loadtxt("data/HardSpheres/compare_N{:.0f}/E_list_and_R_i".format(N), delimiter=' ')
#print E_list

#plt.plot(N_norm_list[:-1], np.diff(E_list), ".")
#plt.plot(N_norm_list[:-1], np.diff(E_list), "-")




#plt.plot(N_norm_list_48,E_list_48,".", label="made mixed stucter")
plt.plot(lh,Eh,"-",label="HardSpheres")
plt.plot(N_norm_list, E_list, ".", label ="my simulation")
plt.plot(l220,E220,"-",label ="220")
plt.plot(l321,E321,"-", label ="321")
plt.plot(l330,E330,"-", label ="330")
plt.plot(l422,E422,"-",label ="422")
plt.plot(l440,E440,"-",label ="440")
#plt.plot(l431,E431,".",label ="431")
#plt.plot(l431,E431,"-",label ="431")
#plt.plot(l220[np.argmin(E220)],np.min(E220),"rx")
#plt.plot(l321[np.argmin(E321)],np.min(E321),"rx")
#plt.plot(l330[np.argmin(E330)],np.min(E330),"rx")
#plt.plot(0,0,"-", color="purple", label="Constant Tangent")


const_tangent_ploter(E321, l321, E220, l220)
const_tangent_ploter(E330, l330, E321, l321)



#plt.plot(x_line,y_line,"-",label = "const tangent from 220 to 321")
#plt.plot(N_norm_list, E_list, "-")

plt.title("Graph of E/N vs L/N ")
plt.xlabel("L/N")
plt.ylabel("Energys E / N")
plt.legend(loc="best")
plt.grid()
plt.show()