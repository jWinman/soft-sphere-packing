import numpy as np
import matplotlib.pyplot as plt

d = 1.
eps = 1.
## should one of the eps be diffrent
function = lambda r, z, eps, d: 0.5 * eps * r**2 \
        + 0.5 * eps * (d - 2*r * np.cos(np.pi / 6.))**2 \
        + eps * (d - np.sqrt(r**2 * np.cos(np.pi / 3.)**2 + r**2 * (np.sin(5. * np.pi / 6.) - 1)**2 + z**2))**2
#### check the 2 in (d - 2*r * np.cos(np.pi / 6.) term

bamboo = lambda r, z, eps, d: eps * (d - z)**2

twotwozero = lambda r, z, eps, d: 0.5 * eps * r**2 \
        + 0.5 * eps * (d - 2*r * np.sin(np.pi / 2))**2 \
        + eps * (d - np.sqrt(r**2 * np.cos(np.pi / 2)**2 + r**2 * (np.sin( np.pi / 2 + np.pi/2) - 1)**2 + z**2))**2

threethreezero = lambda r, z, eps, d: 0.5 * eps * r**2 \
        + 0.5 * eps * (d - 2*r * np.sin(np.pi / 3))**2 \
        + eps * (d - np.sqrt(r**2 * np.cos(np.pi / 3)**2 + r**2 * (np.sin( np.pi / 2 + np.pi/3) - 1)**2 + z**2))**2

fourfourzero = lambda r, z, eps, d: 0.5 * eps * r**2 \
        + 0.5 * eps * (d - 2*r * np.sin(np.pi / 4))**2 \
        + eps * (d - np.sqrt(r**2 * np.cos(np.pi / 4)**2 + r**2 * (np.sin( np.pi / 2 + np.pi/4) - 1)**2 + z**2))**2

fivefivezero = lambda r, z, eps, d: 0.5 * eps * r**2 \
        + 0.5 * eps * (d - 2*r * np.sin(np.pi / 5))**2 \
        + eps * (d - np.sqrt(r**2 * np.cos(np.pi / 5)**2 + r**2 * (np.sin( np.pi / 2 + np.pi/5) - 1)**2 + z**2))**2


def minimize(z, func):
    from scipy import optimize
    import functools as ft

    bnds = (0., 2 * d)
    #rint(func)
    pfunc = ft.partial(func, z=z, eps=eps, d=d)
    res = optimize.minimize(pfunc, 1., jac=None, method="L-BFGS-B")

    return res.x, res.fun


print function(1,1,.0005,1)
print threethreezero(1,1,.0005,1)

zs = np.arange(0.5 * d, 1.5 * d, 0.01)
rs1 = np.zeros(len(zs))
Es1 = np.zeros(len(zs))

for i, z in enumerate(zs):
    rs1[i], Es1[i] = minimize(z, bamboo)
##############################
zs = np.arange(0.5 * d, 1.5 * d, 0.01)
rs2 = np.zeros(len(zs))
Es2 = np.zeros(len(zs))

for i, z in enumerate(zs):
    rs2[i], Es2[i] = minimize(z, twotwozero)
#################
rs3 = np.zeros(len(zs))
Es3 = np.zeros(len(zs))

for i, z in enumerate(zs):
    rs3[i], Es3[i] = minimize(z, threethreezero)
##################
rs4 = np.zeros(len(zs))
Es4 = np.zeros(len(zs))

for i, z in enumerate(zs):
    rs4[i], Es4[i] = minimize(z, fourfourzero)
#######################
rs5 = np.zeros(len(zs))
Es5 = np.zeros(len(zs))

for i, z in enumerate(zs):
    rs5[i], Es5[i] = minimize(z, fivefivezero)











fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(zs, Es1, label="bamboo")
ax.plot(zs/2, Es2, label="2,2,0")
ax.plot(zs/3, Es3, label="3,3,0")
ax.plot(zs/4, Es4, label="4,4,0")
ax.plot(zs/5, Es5, label="5,5,0")

ax.set_xlabel("z/N")
ax.set_ylabel("$E$")
ax.legend()
ax.grid()

#fig.savefig("analyticPlot.pdf")
plt.show()
