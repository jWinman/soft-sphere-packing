import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres

import pylibconfig2 as pylib

fileName = "parameter.cfg"

with open(fileName, "r") as f:
    cfgStr = f.read()
        
cfg = pylib.Config(cfgStr)

#N = cfg.spheres.N
p = cfg.cylinder.p
D_i = cfg.cylinder.R_i * 2.
#N_norm = cfg.cylinder.Norm_par_num
#RR = cfg.cylinder.RR
#k = cfg.spheres.eps

#N, p, D_z = 2, 0.0002, 4.0
N=24
k=100
RR=20

####first option#####
mySystem = classSpheres.SpheresCylinder("parameter.cfg")
mySystem.setVariable("p", p)
mySystem.setVariable("N", N)
mySystem.setVariable("R_i", D_i / 2.)
#round(1/0.251255, 2)
for N_norm in np.arange(2.93,2.95,.01):
	initial = "HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_K{:.3f}_N_norm{:.4f}".format(N, N, RR,k, N_norm)
	mySystem.setVariable("L", N / N_norm)
	mySystem.ReadInitial(initial)
	print(mySystem.Energy(mySystem.positions) / N)
	print( 'N', N, 'RR ', RR, 'N_norm', 1 / N_norm)
	mySystem.plotSchematic(".", 2, 1.003)
	mySystem.plot3DTube(".",2)

print "hello"

'''
#####second option######
N_norm = 3.12
initial = "HardSpheres/compare_N{:.0f}/constructed/Final_N{}_RR0.200000_K50.0_N_norm{:.4f}".format(N,N, N_norm)
mySystem = classSpheres.SpheresCylinder("parameter.cfg")
mySystem.setVariable("p", p)
mySystem.setVariable("N", N)
mySystem.setVariable("R_i", D_i / 2.)
mySystem.setVariable("N_norm",N_norm)
mySystem.setVariable("L", N / N_norm)
mySystem.ReadInitial(initial)
print(mySystem.Energy(mySystem.positions) / N)
print( 'N', N, 'RR ', RR, 'N_norm', N_norm)
mySystem.plotSchematic(".", 1, 1.002)
mySystem.plot3DTube(".",1)

######third option######
initial = "HardSpheres/Finalp0.001000D1.9500"
mySystem = classSpheres.SpheresCylinder("parameter.cfg")
mySystem.setVariable("p", p)
mySystem.setVariable("N", 12)
mySystem.setVariable("R_i", D_i / 2.)
mySystem.ReadInitial(initial)
print(mySystem.Energy(mySystem.positions) / N)
print( 'N', N, 'RR ', RR, 'N_norm', N_norm)
mySystem.plotSchematic(".", 1, 1.002)
mySystem.plot3DTube(".",1)
'''







