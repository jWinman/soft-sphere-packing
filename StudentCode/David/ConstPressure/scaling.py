import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess
import time
from joblib import Parallel, delayed

def minimalE(D_z, p, N, initial, finalDir, niter):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_z", D_z / 2.)
    mySystem.setVariable("niter", niter)
    if (initial == "Random"):
        mySystem.RandomInitial()
    else:
        mySystem.ReadInitial(initial)
    print("pressure: ", p, "Diameter: ", D_z)
    E = mySystem.minimize("basinhopping") / N
    mySystem.printToFile(finalDir)
    print("Energy: ", E)
    return E, mySystem.positions[-1]

N = 4

# 1. create hard-sphere packing
D_z = 4.1
p = 0.0002
initial = "Random"
finalDir = "ScalingTest/k1d2/"
niter = 1000
print("1. Creating Hard-Sphere: ")
E0, L0 = minimalE(D_z, p, N, initial, finalDir, niter)

# 3. do the phasediagram with decreasing D_z
ps = np.arange(0.0004, 0.02, 0.0004)
initialSS = finalDir + "Finalp{:.6f}D{:.4f}".format(0.0002, D_z)
finalDirSS = "ScalingTest/k1d2/"
niterSS = 30
print("3. Creating Soft Spheres with decreasing D_z: ")
E, L = np.array([minimalE(D_z, p, N,
                          initialSS if (i == 0) else finalDirSS + "Finalp{:.6f}D{:.4f}".format(ps[i - 1], D_z),
                          finalDirSS, niterSS) for i, p in enumerate(ps)]).T


np.savetxt("data/ScalingTest/k1d2/E.txt", E)
np.savetxt("data/ScalingTest/k1d2/L.txt", L)
