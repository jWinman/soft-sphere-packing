import numpy as np
from joblib import Parallel, delayed
import time
import matplotlib.pyplot as plt

Omega=[1,10,20,30,40,50,60,70,80,90,100]

b321_right = [0.316,0.31845,0.3205,0.3215432,0.322577,0.32467,0.32467,0.325732,0.326796,0.326,0.327866]
b220_left = [0.353355,0.353,0.352,0.35085,0.3496,0.34965,0.3484319,0.348431,0.34722,0.346,0.346]
b220_right = [0.357,0.3703,0.38163,0.390624,0.39839,0.403223,0.40815,0.413229,0.4166,0.4184,0.4201]
b211_left = [.50,.4950,.49017,.4877,.483,.48075,.4785,.4739,.4716, .46728, .4651]
b211_right = [.5,.5025,.5025]
Omega_special = [1,50,100]


Omega_to_40=[1,10,20,30,40]
b321_left = [.31152, .30118, .29325, .2873, .282]
b330_right = [.275481, .27776, .27854, .277, .2747]





b220ls_220mixed =[1/3.16, 1/3.01, 1/2.95, 1/2.94, 1/2.89]
b220ls_220mixed_omega = [1,20,40,50,100]







print len(b220_left)
print len(b321_right)
plt.plot(b211_left,Omega)
plt.plot(b211_left,Omega,'b.')
plt.plot(b211_right,Omega_special)
plt.plot(b211_right,Omega_special,'b.')
plt.plot(b220_right,Omega)
plt.plot(b220_left,Omega)
plt.plot(b321_right,Omega)
plt.plot(b220_right,Omega,'b.')
plt.plot(b220_left,Omega,'b.')
plt.plot(b321_right,Omega,'b.')

plt.plot(b321_left,Omega_to_40,'b.')
plt.plot(b330_right,Omega_to_40,'b.')
plt.plot(b321_left,Omega_to_40)
plt.plot(b330_right,Omega_to_40)




plt.plot(b220ls_220mixed,b220ls_220mixed_omega,'b.')
plt.plot(b220ls_220mixed,b220ls_220mixed_omega)

plt.title("Phase Digram fixed k=100 ")
plt.xlabel("L/N")
plt.ylim(0,101)
plt.ylabel("$\omega^2$")
plt.legend(loc="best")
plt.grid()
plt.show()

