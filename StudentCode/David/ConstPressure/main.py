import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess
import time
import pylibconfig2 as pylib
from joblib import Parallel, delayed
import time

def minimalE( mySystem_passed, finalDir, niter, RR , k, N_Norm, L, N,R_i):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystem = mySystem_passed
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_i", R_i)
    mySystem.setVariable("RR", RR)
    mySystem.setVariable("eps", k)
    mySystem.setVariable("Norm_par_number", N_Norm)
    mySystem.setVariable("L", L)
    print "im now minmising the best one"
    #mySystem.setVariable("N", N)
    #N = mySystem.N
    mySystem.setVariable("niter", niter)

    print("RR:", RR,  "N_Norm:", N_Norm, "N:", N ,"k:",k)
    print("pre minimized energy",  mySystem.Energy(mySystem.positions) / N)

    E = mySystem.minimize("b") / N
    mySystem.printToFile(finalDir)
    #print("Energy: ", E)
    return E, mySystem.positions[-1] 

def minimalE_returns_class( initial, finalDir, niter, RR, k, N_Norm, L, N,R_i,i):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_i", R_i)
    mySystem.setVariable("RR", RR)
    mySystem.setVariable("eps", k)
    mySystem.setVariable("Norm_par_number", N_Norm)
    mySystem.setVariable("L", L)

    #N = mySystem.N
    mySystem.setVariable("niter", niter)

    if (initial == "Random"):
        mySystem.RandomInitial()
    else:
        mySystem.ReadInitial(initial)

    print("RR:", RR,  "N_Norm:", N_Norm, "N:", N)
    print("pre minimized energy",  mySystem.Energy(mySystem.positions) / N)

    E = mySystem.minimize("nb") / N
    print("Energy: ", E)
    return mySystem


#N = 2
Np = 98
ND_z = 41
#Np = 98
#ND_z = 481

#reades from config file
fileName = "parameter.cfg"
with open(fileName, "r") as f:
    cfgStr = f.read()

cfg = pylib.Config(cfgStr)
#N = cfg.spheres.N
niterHS = cfg.minimize.niter
p = cfg.cylinder.p
#RR = cfg.cylinder.RR
#N_Norm = cfg.cylinder.Norm_par_num
#eps = cfg.spheres.eps
#L =  (cfg.spheres.r_min * N) / N_Norm

#print niterHS

print("1. Creating One simulation: ")


niterHS = cfg.minimize.niter
      

initialHS = "Random"
print("1. Running in parrelle: ")


#initialHS = "HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_K50.0_N_norm{:.4f}".format(N, N, RR, 2.9)
eps = 100.
RR = 90.
N = 24
num_of_starts = 11
finalDirHS = "HardSpheres/compare_N{:.0f}/".format(N)


finalDirHS = "HardSpheres/compare_N{:.0f}/".format(N)
for N_Norm in np.arange(1.7, 2.3, .01):# the range of N_Norm we are going to run in
    
    #initialHS = "HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_K50.0_N_norm{:.4f}".format(N, N, RR, N_Norm-.01)
    E_s = np.zeros(num_of_starts)
    my_sytems = []#np.empty(4, dtype = object)
    print("")
    print("RR:", RR,"k:", eps, "N_Norm:", N_Norm, "N:", N, "eps aka K?:", eps)
    L =  (cfg.spheres.r_min * N) / N_Norm
    for i in range(num_of_starts):
        print i, "th seed" 
        my_sytems.append(minimalE_returns_class(initialHS, finalDirHS, 1, RR, eps,N_Norm, L , N, 1,i))

    min_E_arg = 0
    min_E =100000000
    for i in range(num_of_starts):
        E = my_sytems[i].Energy(my_sytems[i].positions)
        #print( E / 24.)
        if min_E > E:
            min_E = E
            min_E_arg = i
    # this is now the postions we will minimsise properly

    print"we are now useing the best reult from this"
    E, L = minimalE(my_sytems[min_E_arg],finalDirHS, 10, RR, eps, N_Norm, L , N, 1)
    print "final energy is" , E

'''
initialHS = "Random"
#initialHS = "HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_N_norm{:.4f}".format(N, N, RR, N_Norm)
E, L = minimalE(initialHS, finalDirHS, niterHS, RR, N_Norm, L , N, 1)

'''









