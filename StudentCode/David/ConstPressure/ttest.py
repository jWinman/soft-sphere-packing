import numpy as np
import matplotlib.pyplot as plt
import sys

def ret_index(elemnt,array):
	for i in range(len(array)):
		if (array[i] < elemnt + .0005 and array[i] > elemnt - .0005):
			return i

def inverser(array):
	for i in range(len(array)):
		array[i] = 1./array[i]
	return array

def energy_list_returner(start_index,end_index,array):
	print start_index
	print end_index
	x = np.zeros(int(end_index - start_index))
	
	for i in range(0, end_index-start_index):	
		print i
		x[i] = array[start_index+i]
	return x





sys.path.append("../libraries")

l220, E220 = np.loadtxt("data/VaryOmega_k/Omega_{:.1f}k_{:.1f}_220Analytic.csv".format(1,1),unpack=True)


E_pure_220_k100_w100 = energy_list_returner(ret_index(.350,l220),ret_index(.4319,l220),E220)
step_size= (.4319-.350)/float(len(E_pure_220_k100_w100))
print step_size

L_pure_220_k100_w100 = np.arange(.350,.4319,step_size)
L_mixed_220_321_w100_k100 = np.arange(.310,.350,step_size)
W_pure_220_k100_W100 = np.ones(len(L_pure_220_k100_w100))*100
W_mixed_220_k100_W100 = np.ones(len(L_mixed_220_321_w100_k100))*100


W_pure_220_k100_W90 = np.ones(len(L_pure_220_k100_w100))*90
W_pure_220_k100_W80 = np.ones(len(L_pure_220_k100_w100))*80



print len(E_pure_220_k100_w100)
print len(L_pure_220_k100_w100)

print"wooooo"
plt.plot(L_pure_220_k100_w100,W_pure_220_k100_W100, ".",ms=75)
plt.plot(L_mixed_220_321_w100_k100,W_mixed_220_k100_W100, ".",ms=75)
plt.plot(L_pure_220_k100_w100,W_pure_220_k100_W90, ".",ms=75)
plt.plot(L_pure_220_k100_w100,W_pure_220_k100_W80, ".",ms=75)
plt.plot(0,0)



plt.xlabel("L/N")
plt.ylabel("Energys E / N")

plt.show()