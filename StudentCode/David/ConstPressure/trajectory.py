import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess

def select_initial(initial1, initial2, N, p, D):
    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_z", D / 2.)

    mySystem.ReadInitial(initial1)
    E1 = mySystem.Energy(mySystem.positions)

    mySystem.ReadInitial(initial2)
    E2 = mySystem.Energy(mySystem.positions)

    return initial1 if (E1 < E2) else initial2


def minimalE(D_z, p, N, repetition, initial, finalDir):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_z", D_z / 2.)
    mySystem.ReadInitial(initial)
    if (repetition[0] != 1):
        mySystem.resize(repetition[0], repetition[1])
    mySystem.pertube(1e-3)

    print("pressure: ", p, "Diameter: ", D_z, mySystem.N, flush=True)

    E = mySystem.steepest_descent(0.01, 1000000)
    mySystem.printToFile(finalDir)

    print("Energy: ", E / N, flush=True)
    return E

def trajectory(angle, D_z0, rs, N, repetition, number):
    ps = np.round(rs * np.sin(angle) + rs[1], 6)
    D_zs = np.round(D_z0 + 5 * rs * np.cos(angle), 4)

    Nrep = int(N * repetition[0] * repetition[1])
    # create initial packing from phasediagram
    initialSSN = "PhaseDiagram/N{}/Finalp{:.6f}D{:.4f}".format(N, ps[0], D_zs[0])
    initialSSNP = "PhaseDiagram/NP{}/Finalp{:.6f}D{:.4f}".format(N, ps[0], D_zs[0])
    initialSS = select_initial(initialSSN, initialSSNP, N, ps[0], D_zs[0])
#    initialSS = "TrajectorySteepest/TrajectoryTrajExpForward/N{:d}/Finalp{:.6f}D{:.4f}".format(N, ps[0], D_zs[0])

    finalDirSS = "TrajectorySteepest/Trajectory{}Forward/N{:d}/".format(number, Nrep)

    # create forward trajectory
    EForward = np.zeros(len(ps))
    for i, (D_z, p) in enumerate(zip(D_zs, ps)):
        if (i == 0):
            EForward[i] = minimalE(D_z, p, N, repetition, initialSS, finalDirSS)
        else:
            EForward[i] = minimalE(D_z, p, Nrep, (1, 1.), finalDirSS + "Finalp{:.6f}D{:.4f}".format(ps[i - 1], D_zs[i - 1]), finalDirSS)

    # create backward trajectory
    ps, D_zs = ps[::-1], D_zs[::-1]
    initialSS = finalDirSS + "Finalp{:.6f}D{:.4f}".format(ps[0], D_zs[0])
    finalDirSS = "TrajectorySteepest/Trajectory{}Backward/N{:d}/".format(number, Nrep)
    EBackward = np.zeros(len(ps))

    for i, (D_z, p) in enumerate(zip(D_zs, ps)):
        if (i == 0):
            EBackward[i] = minimalE(D_z, p, Nrep, (1, 1.), initialSS, finalDirSS)
        else:
            EBackward[i] = minimalE(D_z, p, Nrep, (1, 1.), finalDirSS + "Finalp{:.6f}D{:.4f}".format(ps[i - 1], D_zs[i - 1]), finalDirSS)

    return EForward, EBackward

Nrep = 12
frac = 1.
N = 4
repetition = (int(Nrep / (N * frac)) , frac)
number = sys.argv[1]

# trajectory points in phasediagram
D_z0 = 2.03
angles = np.arange(0, 2 * np.pi, 0.06)
rs = np.arange(0., 0.04, 0.0004)

from joblib import Parallel, delayed
EForw, EBackw = np.array(Parallel(n_jobs=6)(delayed(trajectory)(
                                            angle, D_z0, rs, N, repetition, number) for angle in angles))
