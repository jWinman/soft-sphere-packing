#include <stdio.h>
#include <time.h>
#include <math.h>


#define NTBimages 5
#define NSIMAGE 2
#define pi 3.141592653589793
#define L 10.0

#define sdist_max 1.015

double modulo(double,double);

int main (void)
{
  int i, j, k;
  double Energy, Rlim, Clength, Angle, Pressure;
  double r[1000], th[1000], z[1000];
  double nr[1000], nth[1000], nz[1000];
  double ir, ith, iz, jrA[10000],jthA[10000], jzA[10000], jrB[10000],jthB[10000], jzB[10000], temp_th, temp_z;
  double bottom_radius, bottom_angle, bottom_height;
  double ang_dist;
  double sxi, sxj, syi, syj, szi, szj, sdx, sdy, sdz, sdist;
  int NUM_PAR, NUM_LAT;
  int LIM, RIM;
  int LN[100], RN[100];
  int jindxA[10000], jindxB[10000];
  FILE *fp, *falpha, *gp, *hp;
  char filename[100];	

  ///////////////////////////////////////////////////////////////////
  //READ IN DATA FILE
  ////////////////////////////////////////////////////////////////////
//  fp=fopen("FINAL_DATA", "r");
//  fscanf(fp,"%lf", &Energy);
//  //printf("Energy=%6.15f\n", Energy);
//  fscanf(fp,"%lf", &Pressure);
//  //printf("Pressure=%6.15f\n", Pressure);
//  fscanf(fp,"%d", &NUM_PAR);
//  //printf("NUM_PAR=%d\n", NUM_PAR);
//  fscanf(fp,"%lf", &Rlim);
//  //printf("Rlim=%6.15f\n", Rlim);
//  fscanf(fp,"%lf", &Clength);
//  //printf("Clength=%6.15f\n", Clength);

  NUM_PAR = 4;
  fp = fopen("../data/Tests/N4/Finalp0.0060D2.20Pos", "r");
  for(i=1;i<=NUM_PAR;i++){
    fscanf(fp,"%lf", &r[i]);
    fscanf(fp,"%lf", &th[i]);
    fscanf(fp,"%lf", &z[i]);
    printf("%6.15f %6.15f %6.15f\n", r[i], th[i], z[i]);
  }
  printf("hallo \n");
  falpha = fopen("../data/Tests/N4/Finalp0.0060D2.20FurtherArgs", "r");
  fscanf(falpha,"%lf", &Angle);
  fscanf(falpha,"%lf", &Clength);
  if(Angle>pi){
    Angle=-((2*pi)-Angle);
  }
  printf("Angle=%6.15f\n", Angle);
  printf("length=%6.15f\n", Clength);
  //printf("\n");
  fclose(falpha);
  fclose(fp);

  //End of reading data file
  ////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////
  //Rotate and Translate spheres
  /////////////////////////////////////////////////////////////////////////////////
  //in this section I translate and rotate the spheres, packed in the cylinder, so 
  //the that particle with the lowest height is located at
  //z=0 and th=0, also the rest of the particles are also sorted according to height

  //find the particle at the bottom of the cylinder
  //begin by printing the z and theta coordinates
  fp=fopen("z_and_theta_list_alt_1", "w");
  for(i=1;i<=NUM_PAR;i++){
    fprintf(fp,"%6.15f %6.15f %6.15f\n",r[i], th[i], z[i]);
  }
  fclose(fp);
  //now use a bash command to sort the data acoording to height
  sprintf(filename, "%s", "sort -n -k3 <z_and_theta_list_alt_1> z_and_theta_list_sorted_alt_1");
  fp=popen(filename,"w");
  fclose(fp);
  //read the first theta coordinate
  fp=fopen("z_and_theta_list_sorted_alt_1", "r");
  fscanf(fp,"%lf", &bottom_radius);
  fscanf(fp,"%lf", &bottom_angle);
  fscanf(fp,"%lf", &bottom_height);
  fclose(fp);
  //printf("bottom angle = %6.15f\n", bottom_angle);
  //////////////////////////////////

  fp=fopen("z_and_theta_coordinates", "w");
  for(i=1;i<=NUM_PAR;i++){
    fprintf(fp,"%6.15f  %6.15f %6.15f\n", r[i], modulo(th[i]-bottom_angle, 2*pi), z[i]-bottom_height);
  }  
  fclose(fp);

  //now use a bash command to sort the third coloumn
  sprintf(filename, "%s", "sort -n -k3 <z_and_theta_coordinates> temp");
  fp=popen(filename,"w");
  fclose(fp);
  sprintf(filename, "%s", "mv temp z_and_theta_coordinates");
  fp=popen(filename,"w");
  fclose(fp);
  //End of normalising coordinates

  sprintf(filename, "%s", "rm z_and_theta_list_alt_1 z_and_theta_list_sorted_alt_1");
  fp=popen(filename,"w");
  fclose(fp);


  /////////////////////////////////////////////////////////////////////////////////
  //Generate the Left and Right handed Phylotactic lattices.
  /////////////////////////////////////////////////////////////////////////////////
 

  fp=fopen("z_and_theta_coordinates", "r");
  for(i=1;i<=NUM_PAR;i++){
    fscanf(fp,"%lf", &nr[i]);
    fscanf(fp,"%lf", &nth[i]);
    fscanf(fp,"%lf", &nz[i]);
  }
  fclose(fp);


  /////////////////////////////////////////////////////////////////////////////////
  //First chirality...
  /////////////////////////////////////////////////////////////////////////////////
 
  NUM_LAT=0;
  fp=fopen("chirality_A", "w");
  for(j=-NTBimages;j<=NTBimages;j++){
    for (i = 1; i <= NUM_PAR; i++){
      //Vertical images 
      ir=nr[i];
      ith=modulo(nth[i] + (j*Angle), (2*pi));
      iz=(j*Clength)+ nz[i];
      //side images
      for(k=-NSIMAGE;k<=NSIMAGE;k++){
	NUM_LAT++;
        jrA[NUM_LAT]=ir;
	jthA[NUM_LAT]=(ith+(2*pi*(1.0*k)));
	jzA[NUM_LAT]= iz;
	jindxA[NUM_LAT]=(i-1)+(j*NUM_PAR);
	fprintf(fp,"%6.15f %6.15f\n",jrA[NUM_LAT]*jthA[NUM_LAT], jzA[NUM_LAT]);
      }  
    }
  }
  fclose(fp);

  fp=fopen("linksA", "w");
  gp=fopen("euclidean_distances", "w");
  for(i=1;i<=NUM_LAT;i++){
    for(j=1;j<=NUM_LAT;j++){
      
      ang_dist=sqrt( (jthA[i]-jthA[j]) * (jthA[i]-jthA[j]) );
      if(i!=j){
	if(ang_dist<=pi){
	  
	  sxi=jrA[i]*cos(jthA[i]);
	  sxj=jrA[j]*cos(jthA[j]);
	  
	  syi=jrA[i]*sin(jthA[i]); 
	  syj=jrA[i]*sin(jthA[j]);
	  
	  
	  szi=jzA[i];
	  szj=jzA[j];
	  
	  sdx=sxi-sxj;
	  sdy=syi-syj;
	  sdz=szi-szj;
	  
	  sdist=sqrt( (sdx*sdx) + (sdy*sdy) + (sdz*sdz));
     
	  if(sdist<=sdist_max){
	    fprintf(fp,"%6.6f %6.6f %6.6f\n", jrA[i]*jthA[i], jzA[i], sdist);
	    fprintf(fp,"%6.6f %6.6f %6.6f\n", jrA[j]*jthA[j], jzA[j], sdist);
	    fprintf(fp,"\n");
	    fprintf(gp,"%6.6f\n", sdist);
	  }
	}
      }
    }
  }
  fclose(fp);
  fclose(gp);

  fp=fopen("guide2_A", "w");
  fprintf(fp,"0.0 0.0\n");
  fprintf(fp,"%6.6f 0.0\n", (2*pi)*jrA[1]);
  fclose(fp);
  
  ////////////////////////////////////////////////////////////////////
  //Now for the other chirality 
  ////////////////////////////////////////////////////////////////////
  
  NUM_LAT=0;
  fp=fopen("chirality_B", "w");
  for(j=-NTBimages;j<=NTBimages;j++){
    for (i = 1; i <= NUM_PAR; i++){
      //Vertical images 
      ir=nr[i];
      ith=modulo(nth[i] + (j*Angle), (2*pi));
      iz=-((j*Clength)+ nz[i]);
      //side images
      for(k=-NSIMAGE;k<=NSIMAGE;k++){
	NUM_LAT++;
        jrB[NUM_LAT]=ir;
	jthB[NUM_LAT]=(ith+(2*pi*(1.0*k)));
	jzB[NUM_LAT]= iz;
	jindxB[NUM_LAT]=-((i-1)+(j*NUM_PAR));
	fprintf(fp,"%6.15f %6.15f\n",jrB[NUM_LAT]*jthB[NUM_LAT], jzB[NUM_LAT]);
      }  
    }
  }
  fclose(fp);

  fp=fopen("linksB", "w");
  for(i=1;i<=NUM_LAT;i++){
    for(j=1;j<=NUM_LAT;j++){
      
      ang_dist=sqrt( (jthB[i]-jthB[j]) * (jthB[i]-jthB[j]) );
      if(i!=j){
	if(ang_dist<=pi){
	  
	  sxi=jrB[i]*cos(jthB[i]);
	  sxj=jrB[j]*cos(jthB[j]);
	  
	  syi=jrB[i]*sin(jthB[i]); 
	  syj=jrB[i]*sin(jthB[j]);
	  
	  
	  szi=jzB[i];
	  szj=jzB[j];
	  
	  sdx=sxi-sxj;
	  sdy=syi-syj;
	  sdz=szi-szj;
	  
	  sdist=sqrt( (sdx*sdx) + (sdy*sdy) + (sdz*sdz));
	  
	  if(sdist<=sdist_max){
	    fprintf(fp,"%6.6f %6.6f\n", jrB[i]*jthB[i], jzB[i]);
	    fprintf(fp,"%6.6f %6.6f\n", jrB[j]*jthB[j], jzB[j]);
	    fprintf(fp,"\n");
	  }
	}
      }
    }
  }
  fclose(fp);


  fp=fopen("guide2_B", "w");
  fprintf(fp,"0.0 0.0\n");
  fprintf(fp,"%6.6f 0.0\n", (2*pi)*jrB[1]);
  fclose(fp);
}


double modulo(double num,double limit)
{
  return num-floor(num/limit)*limit;
}




/*

fp=fopen("chirality_A", "w");
  gp=fopen("labels_A", "w");
  for (i = 1; i <= NUM_PAR; i++){
    //for(j=-NTBimages;j<=NTBimages;j++){
    for(j=0;j<=1;j++){
      //Vertical images
      ith=modulo(nth[i] + (j*Angle), (2*pi));
      iz=(j*Clength)+ nz[i];
      //side images
      //for(k=-NSIMAGE;k<=NSIMAGE;k++){
      k=0;

	jth=ith+(2*pi*(1.0*k));
	jz= iz;
	fprintf(fp,"%6.15f %6.15f %d\n",jth , jz, (i-1)+j);
	fprintf(gp,"set label \"%d\" at %6.6f, %6.6f\n",(i-1)+j,jth,jz);
	//}  
    }
  }
  fclose(fp);
  fclose(gp);

*/
