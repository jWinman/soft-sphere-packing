import sys
sys.path.append("../libraries")
import numpy as np
import classSpheres


initial1 = "Final_N24_RR0.200000_K50.0_N_norm2.8200" 
initial2 = "Final_N24_RR0.200000_K50.0_N_norm3.1600"

R1, th1, z1 = np.loadtxt("data/HardSpheres/" + initial1 + "Pos", unpack=True)
R2, th2, z2 = np.loadtxt("data/HardSpheres/" + initial2 + "Pos", unpack=True)

for i in range(len(z2)):
	z2[i] = z2[i] + np.max(z1)
R3 = np.append(R1,R2)
th3 = np.append(th1,th2)
z3 = np.append(z1,z2)


PosZylinder = np.array([R3, th3, z3])
#furtherArgs = selfy.positions[selfy.size:]
np.savetxt("data/joined_N{}_L{:.4f}Pos".format(len(z3),np.max(z3) ), PosZylinder.T)
#np.savetxt("data/joined_N{}_L{:.4f}FurtherArgs".format(len(z3),np.max(z3) ))