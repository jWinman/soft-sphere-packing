import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess
import time
import pylibconfig2 as pylib
from joblib import Parallel, delayed
import time



def minimalE( initial, finalDir, niter, RR ,eps, N_Norm, L, N,R_i):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_i", R_i)
    mySystem.setVariable("RR", RR)
    mySystem.setVariable("Norm_par_number", N_Norm)
    mySystem.setVariable("L", L)
    mySystem.setVariable("eps", eps)

    #mySystem.setVariable("N", N)
    N = mySystem.N
    mySystem.setVariable("niter", niter)
    
    if (initial == "Random"):
        mySystem.RandomInitial()
    else:
        mySystem.ReadInitial(initial)

    print("RR:", RR,  "N_Norm:", N_Norm, "N:", N)
    print("pre minimized energy",  mySystem.Energy(mySystem.positions) / N)

    E = mySystem.minimize("nb") / N
    mySystem.printToFile(finalDir)
    print("Energy: ", E)
    return E, mySystem.positions[-1] 



#reades from config file
fileName = "parameter.cfg"
with open(fileName, "r") as f:
    cfgStr = f.read()

cfg = pylib.Config(cfgStr)
N = cfg.spheres.N
niterHS = cfg.minimize.niter
p = cfg.cylinder.p
RR = cfg.cylinder.RR
N_Norm = cfg.cylinder.Norm_par_num

#print niterHS


# bellow im going to attempt to parrisle the code
#initialHS = "Random"
initialHS = "joined_N48_L15.7516"
initialHS = "HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_K50.0_N_norm{:.4f}".format(N, N, RR, N_Norm-.01)




finalDirHS = "HardSpheres/compare_N{:.0f}/constructed/".format(N)

eps = 100.
RR = 100.
N = 24
num_of_starts = 1
finalDirHS = "HardSpheres/compare_N{:.0f}/".format(N)

print("1. Running in parrelle: ")
for N_Norm in np.arange(2.89, 3.03, .01):
    initialHS = "HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_K{:.3f}_N_norm{:.4f}".format(N, N, RR,eps, 2.92)

    L =  (cfg.spheres.r_min * N) / N_Norm
    E = minimalE(initialHS, finalDirHS, 1, RR,eps, N_Norm, L, N,1)
    #initialHS = "HardSpheres/compare_N{:.0f}/Final_N{}_RR{:.6f}_K{:.3f}_N_norm{:.4f}".format(N, N, RR,eps, N_Norm)











