import numpy as np

def common_tangent(leftL, leftE, rightL, rightE):
    import scipy.optimize as sco
    from functools import partial
    power = lambda x, a, b, c, d, e: a * x**4 + b * x**3 + c * x**2 + d * x + e
    derivative = lambda x, a, b, c, d: 4 * a * x**3 + 3 * b * x**2 + 2 * c * x + d
    (a1, b1, c1, d1, e1), _ = sco.curve_fit(power, leftL, leftE, p0=[1., 1., 1., 1., min(leftE)])
    (a2, b2, c2, d2, e2), _ = sco.curve_fit(power, rightL, rightE, p0=[1., 1., 1., 1., min(rightE)])

    tangent = lambda x, x0: derivative(x0, a2, b2, c2, d2) * x + power(x0, a2, b2, c2, d2, e2) - derivative(x0, a2, b2, c2, d2) * x0

    distance = lambda x, x0: np.sqrt((tangent(x[0], x0) - power(x[1], a1, b1, c1, d1, e1))**2 + (x[0] - x[1])**2)

    x = np.linspace(0.1, 0.4, 1000)

    deltaXold = 1e-4
    x0s = np.arange(rightL[np.argmin(rightE)] + deltaXold, 0.1, -deltaXold)
    deltaXs = np.logspace(-5, -11, 7)
    bnds = ([min(leftL), max(rightL)],
            [min(leftL), max(rightL)])

    for j, deltaX in enumerate(deltaXs):
        for i, x0 in enumerate(x0s):
            res = sco.minimize(partial(distance, x0=x0), [1., 1.], method="L-BFGS-B", bounds=bnds, options={"ftol": 1e-23, "gtol": 1e-23})
            if (res.fun < 1e-5):
                break;
        x0s = np.arange(x0 + deltaXold, x0, -deltaX)
        deltaXold = deltaX

    x1 = res.x[1]

    return x1, x0, power(x1, a1, b1, c1, d1, e1), power(x0, a2, b2, c2, d2, e2)

Ns = np.arange(2, 6)

omegas = np.arange(0.01, 0.62, 0.01)
k = 1

Ls211_1 = np.zeros(len(omegas))
Ls220_1 = np.zeros(len(omegas))
Ls321_1 = np.zeros(len(omegas))
Ls330_1 = np.zeros(len(omegas))
Ls422_1 = np.zeros(len(omegas))
Ls431_1 = np.zeros(len(omegas))
Ls440_1 = np.zeros(len(omegas))
Ls532_1 = np.zeros(len(omegas))
Ls541_1 = np.zeros(len(omegas))
Ls550_1 = np.zeros(len(omegas))

Ls110_2 = np.zeros(len(omegas))
Ls211_2 = np.zeros(len(omegas))
Ls220_2 = np.zeros(len(omegas))
Ls321_2 = np.zeros(len(omegas))
Ls330_2 = np.zeros(len(omegas))
Ls422_2 = np.zeros(len(omegas))
Ls431_2 = np.zeros(len(omegas))
Ls440_2 = np.zeros(len(omegas))
Ls532_2 = np.zeros(len(omegas))
Ls541_2 = np.zeros(len(omegas))

Ls321_3 = np.zeros(len(omegas))
Ls422_3 = np.zeros(len(omegas))
Ls431_3 = np.zeros(len(omegas))
Ls532_3 = np.zeros(len(omegas))

Es211_1 = np.zeros(len(omegas))
Es220_1 = np.zeros(len(omegas))
Es321_1 = np.zeros(len(omegas))
Es330_1 = np.zeros(len(omegas))
Es422_1 = np.zeros(len(omegas))
Es431_1 = np.zeros(len(omegas))
Es440_1 = np.zeros(len(omegas))
Es532_1 = np.zeros(len(omegas))
Es541_1 = np.zeros(len(omegas))
Es550_1 = np.zeros(len(omegas))

Es110_2 = np.zeros(len(omegas))
Es211_2 = np.zeros(len(omegas))
Es220_2 = np.zeros(len(omegas))
Es321_2 = np.zeros(len(omegas))
Es330_2 = np.zeros(len(omegas))
Es422_2 = np.zeros(len(omegas))
Es431_2 = np.zeros(len(omegas))
Es440_2 = np.zeros(len(omegas))
Es532_2 = np.zeros(len(omegas))
Es541_2 = np.zeros(len(omegas))

Es321_3 = np.zeros(len(omegas))
Es422_3 = np.zeros(len(omegas))
Es431_3 = np.zeros(len(omegas))
Es532_3 = np.zeros(len(omegas))

Ls = []
Es = []

for j, omega in enumerate(omegas):
    Ls = []
    Es = []

    print("Omega: ", omega)

    for i, N in enumerate(Ns):
        L, E = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_{}{}0.csv".format(omega, k, N, N), unpack=True)
        Ls.append(L)
        Es.append(E)

    Ls110, Es110 = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_110.csv".format(omega, k), unpack=True)
    Ls211, Es211 = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_211.csv".format(omega, k), unpack=True)
    Ls321, Es321 = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_321.csv".format(omega, k), unpack=True)
    Ls422, Es422 = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_422.csv".format(omega, k), unpack=True)
    Ls431, Es431 = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_431.csv".format(omega, k), unpack=True)
    Ls532, Es532 = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_532.csv".format(omega, k), unpack=True)
    Ls541, Es541 = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_541.csv".format(omega, k), unpack=True)

    Ls211_1[j], Ls110_2[j], Es211_1[j], Es110_2[j] = common_tangent(Ls211, Es211, Ls110, Es110)
    Ls220_1[j], Ls211_2[j], Es220_1[j], Es211_2[j] = common_tangent(Ls[0], Es[0], Ls211, Es211)
    Ls321_1[j], Ls220_2[j], Es321_1[j], Es220_2[j] = common_tangent(Ls321, Es321, Ls[0], Es[0])
    Ls330_1[j], Ls321_2[j], Es330_1[j], Es321_2[j] = common_tangent(Ls[1], Es[1], Ls321, Es321)
    Ls422_1[j], Ls330_2[j], Es422_1[j], Es330_2[j] = common_tangent(Ls422, Es422, Ls[1], Es[1])

    Ls422_3[j], Ls321_3[j], Es422_3[j], Es321_3[j] = common_tangent(Ls422, Es422, Ls321, Es321)

    Ls431_1[j], Ls422_2[j], Es431_1[j], Es422_2[j] = common_tangent(Ls431, Es431, Ls422, Es422)
    Ls440_1[j], Ls431_2[j], Es440_1[j], Es431_2[j] = common_tangent(Ls[2], Es[2], Ls431, Es431)
    Ls532_1[j], Ls440_2[j], Es532_1[j], Es440_2[j] = common_tangent(Ls532, Es532, Ls[2], Es[2])

    Ls532_3[j], Ls431_3[j], Es532_3[j], Es431_3[j] = common_tangent(Ls532, Es532, Ls431, Es431)

    Ls541_1[j], Ls532_2[j], Es541_1[j], Es532_2[j] = common_tangent(Ls541, Es541, Ls532, Es532)
    Ls550_1[j], Ls541_2[j], Es550_1[j], Es541_2[j] = common_tangent(Ls[3], Es[3], Ls541, Es541)

np.savetxt("../../data/PhaseDiagram/Ls211_1.csv", np.array([omegas, Ls211_1, Es211_1]).T)
np.savetxt("../../data/PhaseDiagram/Ls220_1.csv", np.array([omegas, Ls220_1, Es220_1]).T)
np.savetxt("../../data/PhaseDiagram/Ls321_1.csv", np.array([omegas, Ls321_1, Es321_1]).T)
np.savetxt("../../data/PhaseDiagram/Ls321_3.csv", np.array([omegas, Ls321_3, Es321_3]).T)
np.savetxt("../../data/PhaseDiagram/Ls330_1.csv", np.array([omegas, Ls330_1, Es330_1]).T)
np.savetxt("../../data/PhaseDiagram/Ls422_1.csv", np.array([omegas, Ls422_1, Es422_1]).T)
np.savetxt("../../data/PhaseDiagram/Ls422_3.csv", np.array([omegas, Ls422_3, Es422_3]).T)
np.savetxt("../../data/PhaseDiagram/Ls431_1.csv", np.array([omegas, Ls431_1, Es431_1]).T)
np.savetxt("../../data/PhaseDiagram/Ls431_3.csv", np.array([omegas, Ls431_3, Es431_3]).T)
np.savetxt("../../data/PhaseDiagram/Ls440_1.csv", np.array([omegas, Ls440_1, Es440_1]).T)
np.savetxt("../../data/PhaseDiagram/Ls532_1.csv", np.array([omegas, Ls532_1, Es532_1]).T)
np.savetxt("../../data/PhaseDiagram/Ls532_3.csv", np.array([omegas, Ls532_3, Es532_3]).T)
np.savetxt("../../data/PhaseDiagram/Ls541_1.csv", np.array([omegas, Ls541_1, Es541_1]).T)
np.savetxt("../../data/PhaseDiagram/Ls550_1.csv", np.array([omegas, Ls550_1, Es550_1]).T)

np.savetxt("../../data/PhaseDiagram/Ls110_2.csv", np.array([omegas, Ls110_2, Es110_2]).T)
np.savetxt("../../data/PhaseDiagram/Ls211_2.csv", np.array([omegas, Ls211_2, Es211_2]).T)
np.savetxt("../../data/PhaseDiagram/Ls220_2.csv", np.array([omegas, Ls220_2, Es220_2]).T)
np.savetxt("../../data/PhaseDiagram/Ls321_2.csv", np.array([omegas, Ls321_2, Es321_2]).T)
np.savetxt("../../data/PhaseDiagram/Ls330_2.csv", np.array([omegas, Ls330_2, Es330_2]).T)
np.savetxt("../../data/PhaseDiagram/Ls422_2.csv", np.array([omegas, Ls422_2, Es422_2]).T)
np.savetxt("../../data/PhaseDiagram/Ls431_2.csv", np.array([omegas, Ls431_2, Es431_2]).T)
np.savetxt("../../data/PhaseDiagram/Ls440_2.csv", np.array([omegas, Ls440_2, Es440_2]).T)
np.savetxt("../../data/PhaseDiagram/Ls532_2.csv", np.array([omegas, Ls532_2, Es532_2]).T)
np.savetxt("../../data/PhaseDiagram/Ls541_2.csv", np.array([omegas, Ls541_2, Es541_2]).T)
