import numpy as np
import matplotlib.pyplot as plt

LSim, ESim = np.loadtxt("../../data/EnergiesDavid.csv", unpack=True)

dESim = np.diff(ESim)
LSim = LSim[:-1]

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(LSim, dESim, ".")

ax.set_xlim(0.303, 0.38)
#ax.set_ylim(0.112, 0.135)

fig.savefig("../../Plots/DEnergies.pdf")
