import numpy as np
import matplotlib.pyplot as plt

D, phi = np.loadtxt("../../data/Input/VF_graph_xmgrace_file_2", unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(D, phi, label="Optimal packing fraction" + "\n" + "for hard spheres structures")

ax.axvline(1.866 , linestyle="--", color="k", linewidth=0.5)
ax.axvline(1.995 , linestyle="--", color="k", linewidth=0.5)
ax.axvline(2.0   , linestyle="--", color="k", linewidth=0.5)
ax.axvline(2.039 , linestyle="--", color="k", linewidth=0.5)
ax.axvline(2.1547, linestyle="--", color="k", linewidth=0.5)
ax.axvline(2.2247, linestyle="--", color="k", linewidth=0.5)
ax.axvline(2.2905, linestyle="--", color="k", linewidth=0.5)
ax.axvline(2.4142, linestyle="--", color="k", linewidth=0.5)
ax.axvline(2.4863, linestyle="--", color="k", linewidth=0.5)
ax.axvline(2.5712, linestyle="--", color="k", linewidth=0.5)
ax.axvline(2.7013, linestyle="--", color="k", linewidth=0.5)

ax.set_xlabel("Diameter ratio $D / d$")
ax.set_ylabel("Packing fraction $\phi$")
ax.legend()


fig.savefig("../../Plots/HardSpheres.pdf")
