import numpy as np
import matplotlib.pyplot as plt

Omega= np.array([0, 1,10,20,30,40,50,60,70,80,90,100]) / 100.

b321_right = np.array([0.316,0.316,0.31845,0.3205,0.3215432,0.322577,0.32467,0.32467,0.325732,0.326796,0.326,0.327866])
b220_left  = np.array([0.353355,0.353355,0.353,0.352,0.35085,0.3496,0.34965,0.3484319,0.348431,0.34722,0.346,0.346])
b220_right = np.array([0.354,0.357,0.3703,0.38163,0.390624,0.39839,0.403223,0.40815,0.413229,0.4166,0.4184,0.4201])
b211_left  = np.array([.50,.50,.4950,.49017,.4877,.483,.48075,.4785,.4739,.4716, .46728, .4651])
b211_right = np.array([.5,.5,.5025,.5025])

Omega_special = np.array([0, 1,50,100]) / 100.

Omega_to_40 = np.array([0, 1,10,20,30,40]) / 100.
b321_left   = np.array([.313,.31152, .30118, .29325, .2873, .282])
b330_right  = np.array([.275481,.275481, .27776, .27854, .277, .2747])

b220ls_220mixed = np.array([1/3.16,1/3.16, 1/3.01, 1/2.95, 1/2.94, 1/2.89])
b220ls_220mixed_omega = np.array([0,1,20,40,50,100]) / 100.

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

Colors = plt.get_cmap("rainbow")(np.arange(0, 1, 0.1))

ax.grid()
ax.set_axisbelow(True)
ax.yaxis.grid(color='gray', linestyle='dashed')
ax.xaxis.grid(color='gray', linestyle='dashed')

AreaX = np.concatenate((b211_left, b211_right[::-1]))
AreaY = np.concatenate((Omega, Omega_special[::-1]))

ax.annotate("$(2, 1, 1)$", xy=(0.48, 0.92), xytext=(0.48, 1.05), color=Colors[0],
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"), ha="center")
ax.fill_betweenx(AreaY, AreaX, color=Colors[0])

ax.annotate("$(2, 2, 0)$", xy=(0.41, 0.92), xytext=(0.43, 1.05), color="chocolate",
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"), ha="center")
ax.fill_betweenx(Omega, b220_left, b220_right, color="chocolate")

ax.annotate("$(3, 2, 1)$", xy=(0.306, 0.92), xytext=(0.30, 1.05), color=Colors[2],
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"), ha="right")
ax.fill_betweenx(Omega, b321_right, np.ones(len(Omega)) * 0.30, color=Colors[2])

ax.fill_betweenx(Omega_to_40, b321_left, np.ones(len(Omega_to_40)) * 0.30, color="w")

ax.plot(b220ls_220mixed,b220ls_220mixed_omega, color="r")


ax.annotate(r"$(3, 2, 1)-{}$line slip" + "\n" + "mixture", xy=(0.336, 0.92), xytext=(0.34, 1.05), color="k",
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"), ha="center")
ax.text(0.44, 0.28, r"$(2, 2, 0)-(2, 1, 1)$" + "\n" + "mixture", ha="center")
ax.text(0.338, 0.04, r"line slip", ha="center", fontsize=11)

ax.set_xlabel(r"inverse number density $(\rho d)^{-1}$")
ax.set_ylim(0, 1)
ax.set_xlim(0.3, 0.505)
ax.set_ylabel(r"$\omega^2 M / k$")

fig.savefig("../../Plots/SimulationPhaseDiagram.pdf")
