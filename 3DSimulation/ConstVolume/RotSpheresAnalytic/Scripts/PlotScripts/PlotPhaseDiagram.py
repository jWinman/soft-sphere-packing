import numpy as np
import matplotlib.pyplot as plt

omegas211_1, Ls211_1, _ = np.loadtxt("../../data/PhaseDiagram/Ls211_1.csv", unpack=True)
omegas220_1, Ls220_1, _ = np.loadtxt("../../data/PhaseDiagram/Ls220_1.csv", unpack=True)
omegas321_1, Ls321_1, _ = np.loadtxt("../../data/PhaseDiagram/Ls321_1.csv", unpack=True)
omegas330_1, Ls330_1, _ = np.loadtxt("../../data/PhaseDiagram/Ls330_1.csv", unpack=True)
omegas422_1, Ls422_1, _ = np.loadtxt("../../data/PhaseDiagram/Ls422_1.csv", unpack=True)
omegas431_1, Ls431_1, _ = np.loadtxt("../../data/PhaseDiagram/Ls431_1.csv", unpack=True)
omegas440_1, Ls440_1, _ = np.loadtxt("../../data/PhaseDiagram/Ls440_1.csv", unpack=True)
omegas532_1, Ls532_1, _ = np.loadtxt("../../data/PhaseDiagram/Ls532_1.csv", unpack=True)
omegas541_1, Ls541_1, _ = np.loadtxt("../../data/PhaseDiagram/Ls541_1.csv", unpack=True)
omegas550_1, Ls550_1, _ = np.loadtxt("../../data/PhaseDiagram/Ls550_1.csv", unpack=True)

omegas110_2, Ls110_2, _ = np.loadtxt("../../data/PhaseDiagram/Ls110_2.csv", unpack=True)
omegas211_2, Ls211_2, _ = np.loadtxt("../../data/PhaseDiagram/Ls211_2.csv", unpack=True)
omegas220_2, Ls220_2, _ = np.loadtxt("../../data/PhaseDiagram/Ls220_2.csv", unpack=True)
omegas321_2, Ls321_2, _ = np.loadtxt("../../data/PhaseDiagram/Ls321_2.csv", unpack=True)
omegas330_2, Ls330_2, _ = np.loadtxt("../../data/PhaseDiagram/Ls330_2.csv", unpack=True)
omegas422_2, Ls422_2, _ = np.loadtxt("../../data/PhaseDiagram/Ls422_2.csv", unpack=True)
omegas431_2, Ls431_2, _ = np.loadtxt("../../data/PhaseDiagram/Ls431_2.csv", unpack=True)
omegas440_2, Ls440_2, _ = np.loadtxt("../../data/PhaseDiagram/Ls440_2.csv", unpack=True)
omegas532_2, Ls532_2, _ = np.loadtxt("../../data/PhaseDiagram/Ls532_2.csv", unpack=True)
omegas541_2, Ls541_2, _ = np.loadtxt("../../data/PhaseDiagram/Ls541_2.csv", unpack=True)

omegas321_3, Ls321_3, _ = np.loadtxt("../../data/PhaseDiagram/Ls321_3.csv", unpack=True)
omegas422_3, Ls422_3, _ = np.loadtxt("../../data/PhaseDiagram/Ls422_3.csv", unpack=True)
omegas431_3, Ls431_3, _ = np.loadtxt("../../data/PhaseDiagram/Ls431_3.csv", unpack=True)
omegas532_3, Ls532_3, _ = np.loadtxt("../../data/PhaseDiagram/Ls532_3.csv", unpack=True)

# add hard spheres
L_transform = lambda D, phi: 2 / (3. * D**2 * phi)
Ds   = np.array([1.87, 2.0, 2.039, 2.1547, 2.2247, 2.2905, 2.4142, 2.4863, 2.5712, 2.7013])
phis = np.array([0.3776, 0.47140571, 0.50676915, 0.52027828, 0.53816036, 0.54127231, 0.54413811, 0.54372955, 0.54115259, 0.53701179])
Ls = L_transform(Ds, phis)

omegas211_1, Ls211_1 = np.append(0, omegas211_1), np.append(Ls211_1[0], Ls211_1)
omegas220_1, Ls220_1 = np.append(0, omegas220_1), np.append(Ls[1], Ls220_1)
omegas321_1, Ls321_1 = np.append(0, omegas321_1), np.append(Ls[2], Ls321_1)
omegas330_1, Ls330_1 = np.append(0, omegas330_1), np.append(Ls330_1[0], Ls330_1)
omegas422_1, Ls422_1 = np.append(0, omegas422_1), np.append(Ls[4], Ls422_1)
omegas431_1, Ls431_1 = np.append(0, omegas431_1), np.append(Ls[5], Ls431_1)
omegas440_1, Ls440_1 = np.append(0, omegas440_1), np.append(Ls440_1[0], Ls440_1)
omegas532_1, Ls532_1 = np.append(0, omegas532_1), np.append(Ls[7], Ls532_1)
omegas541_1, Ls541_1 = np.append(0, omegas541_1), np.append(Ls[8], Ls541_1)
omegas550_1, Ls550_1 = np.append(0, omegas550_1), np.append(Ls[9], Ls550_1)

omegas211_2, Ls211_2 = np.append(0, omegas211_2), np.append(Ls211_2[0], Ls211_2)
omegas220_2, Ls220_2 = np.append(0, omegas220_2), np.append(Ls[1], Ls220_2)
omegas321_2, Ls321_2 = np.append(0, omegas321_2), np.append(Ls[2], Ls321_2)
omegas330_2, Ls330_2 = np.append(0, omegas330_2), np.append(Ls330_2[0], Ls330_2)
omegas422_2, Ls422_2 = np.append(0, omegas422_2), np.append(Ls[4], Ls422_2)
omegas431_2, Ls431_2 = np.append(0, omegas431_2), np.append(Ls[5], Ls431_2)
omegas440_2, Ls440_2 = np.append(0, omegas440_2), np.append(Ls440_2[0], Ls440_2)
omegas532_2, Ls532_2 = np.append(0, omegas532_2), np.append(Ls[7], Ls532_2)
omegas541_2, Ls541_2 = np.append(0, omegas541_2), np.append(Ls[8], Ls541_2)

omegas330_1 = omegas330_1[Ls330_2 <= Ls330_1]
omegas330_2 = omegas330_2[Ls330_2 <= Ls330_1]
Ls330_2 = Ls330_2[Ls330_2 <= Ls330_1]
Ls330_1 = Ls330_1[:len(Ls330_2)]

omegas440_1 = omegas440_1[Ls440_2 <= Ls440_1]
omegas440_2 = omegas440_2[Ls440_2 <= Ls440_1]
Ls440_2 = Ls440_2[Ls440_2 <= Ls440_1]
Ls440_1 = Ls440_1[:len(Ls440_2)]

omegas321_3, Ls321_3 = omegas321_3[omegas321_3 >= omegas330_1[-1]], Ls321_3[omegas321_3 >= omegas330_1[-1]]
omegas422_3, Ls422_3 = omegas422_3[omegas422_3 >= omegas330_1[-1]], Ls422_3[omegas422_3 >= omegas330_1[-1]]
omegas431_3, Ls431_3 = omegas431_3[omegas431_3 >= omegas440_1[-1]], Ls431_3[omegas431_3 >= omegas440_1[-1]]
omegas532_3, Ls532_3 = omegas532_3[omegas532_3 >= omegas440_1[-1]], Ls532_3[omegas532_3 >= omegas440_1[-1]]

omegas321_2, Ls321_2 = omegas321_2[omegas321_2 < omegas330_1[-1]], Ls321_2[omegas321_2 < omegas330_1[-1]]
omegas422_1, Ls422_1 = omegas422_1[omegas422_1 < omegas330_1[-1]], Ls422_1[omegas422_1 < omegas330_1[-1]]
omegas431_2, Ls431_2 = omegas431_2[omegas431_2 < omegas440_2[-1]], Ls431_2[omegas431_2 < omegas440_1[-1]]
omegas532_1, Ls532_1 = omegas532_1[omegas532_1 < omegas440_1[-1]], Ls532_1[omegas532_1 < omegas440_1[-1]]

omegas321_2, Ls321_2 = np.append(omegas321_2, omegas321_3), np.append(Ls321_2, Ls321_3)
omegas422_1, Ls422_1 = np.append(omegas422_1, omegas422_3), np.append(Ls422_1, Ls422_3)
omegas431_2, Ls431_2 = np.append(omegas431_2, omegas431_3), np.append(Ls431_2, Ls431_3)
omegas532_1, Ls532_1 = np.append(omegas532_1, omegas532_3), np.append(Ls532_1, Ls532_3)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

Colors = plt.get_cmap("rainbow")(np.arange(0, 1, 0.1))


ax.grid()
ax.set_axisbelow(True)
ax.yaxis.grid(color='gray', linestyle='dashed')
ax.xaxis.grid(color='gray', linestyle='dashed')

ax.annotate("(2, 1, 1)", xy=(0.49, 0.55), xytext=(0.46, 0.64), color="k",
            arrowprops=dict(arrowstyle="-|>", color="k", relpos=(0.5, 0)))
ax.fill_betweenx(omegas211_2, Ls211_2, Ls211_1, color=Colors[0])

ax.annotate("$(2, 2, 0)$", xy=(0.380, 0.55), xytext=(0.38, 0.64), color="k",
            arrowprops=dict(arrowstyle="-|>", color="k"))
ax.fill_betweenx(omegas220_2, Ls220_2, Ls220_1, color=Colors[1])

ax.annotate("$(3, 2, 1)$", xy=(0.31, 0.55), xytext=(0.29, 0.64), color="k",
            arrowprops=dict(arrowstyle="-|>", color="k"))
ax.fill_betweenx(omegas321_2, Ls321_2, Ls321_1, color=Colors[2])

ax.annotate("$(3, 3, 0)$", xy=(0.26, 0.45), xytext=(0.280, 0.68), color="k", ha="center",
            arrowprops=dict(arrowstyle="-|>", connectionstyle="angle3,angleA=-40,angleB=70", color="k"))
ax.fill_betweenx(omegas330_1, Ls330_1, Ls330_2, color=Colors[3])

ax.plot(np.array([Ls321_3[-1], Ls422_1[-1]]), np.ones(2) * omegas330_1[-1], color=Colors[3])

ax.annotate("$(4, 2, 2)$", xy=(0.23, 0.55), xytext=(0.22, 0.64), color="k",
            arrowprops=dict(arrowstyle="-|>", color="k"))
ax.fill_betweenx(omegas422_1, Ls422_1, Ls422_2, color=Colors[4], zorder=3)

ax.plot(np.array([Ls431_3[-1], Ls532_1[-1]]), np.ones(2) * omegas440_1[-1], color=Colors[4])

ax.annotate("$(4, 3, 1)$", xy=(0.21, 0.55), xytext=(0.15, 0.64), color="k",
            arrowprops=dict(arrowstyle="-|>", color="k"))
ax.fill_betweenx(omegas431_1, Ls431_1, Ls431_2, color=Colors[5])

ax.annotate("$(4, 4, 0)$", xy=(0.195, 0.45), xytext=(0.030, 0.43), color="k",
            arrowprops=dict(arrowstyle="-|>", color="k"))
ax.fill_betweenx(omegas440_1, Ls440_1, Ls440_2, color=Colors[6])

ax.annotate("$(5, 3, 2)$", xy=(0.18, 0.55), xytext=(0.08, 0.63), color="k",
            arrowprops=dict(arrowstyle="-|>", color="k"))
ax.fill_betweenx(omegas532_1, Ls532_1, Ls532_2, color=Colors[7])

ax.annotate("$(5, 4, 1)$", xy=(0.162, 0.52), xytext=(0.030, 0.518), color="k",
            arrowprops=dict(arrowstyle="-|>", color="k"))
ax.fill_betweenx(omegas541_1, Ls541_1, Ls541_2, color=Colors[8])

ax.plot(Ls550_1, omegas550_1, color=Colors[9])
ax.annotate("$(5, 5, 0)$", xy=(0.15, 0.55), xytext=(0.030, 0.556), color="k",
            arrowprops=dict(arrowstyle="-|>", color="k"))

#ax.annotate(r"\bf peritectoid" + "\n" + r"\bf points", ha="center", xy=(0.260, 0.508), xytext=(0.30, 0.25), color="k", fontsize=16,
#            arrowprops=dict(arrowstyle="fancy", color="r", connectionstyle="angle3,angleA=100,angleB=0", relpos=(0.9,1.0)))
#ax.annotate(r"\bf peritectoid" + "\n" + r"\bf points", ha="center", xy=(0.192, 0.508), xytext=(0.30, 0.25), color="k", fontsize=16,
#            arrowprops=dict(arrowstyle="fancy", color="r", connectionstyle="angle3,angleA=0,angleB=100", relpos=(0.,0.5)))
#
#ax.annotate(r"\bf vanishing" + "\n" + r"\bf achiral structures", ha="center", xy=(0.263, 0.24), xytext=(0.23, 0.03), color="k",
#            arrowprops=dict(arrowstyle="fancy", color="k", connectionstyle="angle3,angleA=100,angleB=0", relpos=(1.0, 0.5)))
#ax.annotate(r"\bf vanishing" + "\n" + r"\bf achiral structures", ha="center", xy=(0.2005, 0.24), xytext=(0.23, 0.03), color="k",
#            arrowprops=dict(arrowstyle="fancy", color="k", connectionstyle="angle3,angleA=100,angleB=0", relpos=(0.,0.5)))

px = 0.5 / 123
diff = 55 * px

Exp211 = 2.0 + 35 * px
Exp220 = Exp211 + 3 * diff
Exp321 = Exp211 + 5 * diff
Exp330 = Exp211 + 7 * diff
Exp422 = Exp211 + 9 * diff

errorbar211 = abs(1 / (Exp211 - diff) - 1 / Exp211)
errorbar220 = abs(1 / (Exp220 - diff) - 1 / Exp220)
errorbar321 = abs(1 / (Exp321 - diff) - 1 / Exp321)
errorbar330 = abs(1 / (Exp330 - diff) - 1 / Exp330)
errorbar422 = abs(1 / (Exp422 - diff) - 1 / Exp422)

eb = ax.errorbar(1 / Exp211, 0, xerr=errorbar211, fmt="^", color=Colors[0],  clip_on=False)
eb[-1][0].set_linestyle(':')
eb[-1][0].set_linewidth(3)
eb[-1][0].set_clip_on(False)
eb = ax.errorbar(1 / Exp220, 0, xerr=errorbar220, fmt="s", color=Colors[1],  clip_on=False)
eb[-1][0].set_linestyle(':')
eb[-1][0].set_linewidth(3)
eb[-1][0].set_clip_on(False)
eb = ax.errorbar(1 / Exp321, 0, xerr=errorbar321, fmt="o", color=Colors[2],  clip_on=False)
eb[-1][0].set_linestyle(':')
eb[-1][0].set_linewidth(3)
eb[-1][0].set_clip_on(False)
eb = ax.errorbar(1 / Exp330, 0, xerr=errorbar330, fmt="h", color=Colors[3],  clip_on=False)
eb[-1][0].set_linestyle(':')
eb[-1][0].set_linewidth(3)
eb[-1][0].set_clip_on(False)
eb = ax.errorbar(1 / Exp422, 0, xerr=errorbar422, fmt="D", color=Colors[4],  clip_on=False)
eb[-1][0].set_linestyle(':')
eb[-1][0].set_linewidth(3)
eb[-1][0].set_clip_on(False)

ax.set_ylabel(r"$\omega^2 M / k$")
ax.set_xlabel(r"inverse number density $(\rho d)^{-1}$")
ax.set_ylim(0, 0.6)
ax.set_xlim(xmax=0.6)

ax.tick_params(axis='x', which='major', pad=3)


fig.savefig("../../Plots/PhaseDiagramLow.pdf")
