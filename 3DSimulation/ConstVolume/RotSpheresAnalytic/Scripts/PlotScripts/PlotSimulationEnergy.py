import numpy as np
import matplotlib.pyplot as plt

LSim, ESim = np.loadtxt("../../data/EnergiesDavid.csv", unpack=True)
print(1 / LSim)
L220, E220 = np.loadtxt("../../data/PhaseDiagram/Omega0.20_k1.00_220.csv", unpack=True)
L321, E321 = np.loadtxt("../../data/PhaseDiagram/Omega0.20_k1.00_321.csv", unpack=True)

_, Ls211_2, Es211_2 = np.loadtxt("../../data/PhaseDiagram/Ls211_2.csv", unpack=True)
_, Ls220_1, Es220_1 = np.loadtxt("../../data/PhaseDiagram/Ls220_1.csv", unpack=True)
_, Ls220_2, Es220_2 = np.loadtxt("../../data/PhaseDiagram/Ls220_2.csv", unpack=True)
_, Ls321_1, Es321_1 = np.loadtxt("../../data/PhaseDiagram/Ls321_1.csv", unpack=True)
_, Ls321_2, Es321_2 = np.loadtxt("../../data/PhaseDiagram/Ls321_2.csv", unpack=True)
_, Ls330_1, Es330_1 = np.loadtxt("../../data/PhaseDiagram/Ls330_1.csv", unpack=True)

omega = 0.2
index = int(omega / 0.01) - 1

width = 1.0
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

Colors = plt.get_cmap("rainbow")(np.arange(0, 1, 0.1))

ax.plot(LSim, ESim / 20, "r.", markersize=2*width, label="Simulation")
ax.plot(np.array([Ls220_1[index], Ls211_2[index]]), np.array([Es220_1[index], Es211_2[index]]), "k-", linewidth=width)
ax.plot(np.array([Ls321_1[index], Ls220_2[index]]), np.array([Es321_1[index], Es220_2[index]]), "k-", linewidth=width)
ax.plot(np.array([Ls330_1[index], Ls321_2[index]]), np.array([Es330_1[index], Es321_2[index]]), "k-", linewidth=width)
ax.plot(L220, E220, color="chocolate", linewidth=width, label="homogen. $(2, 2, 0)$")
ax.plot(L321, E321, color=Colors[2], linewidth=width, label="homogen. $(3, 2, 1)$")

ax.axvline(0.3333, color="r", linestyle="dashed", linewidth=width)
ax.axvline(0.3520, color="chocolate", linestyle="dashed", linewidth=width)
ax.axvline(0.3205, color=Colors[2], linestyle="dashed", linewidth=width)

ax.fill_betweenx(ESim / 20., 0.3, 0.3205, alpha=0.4, color=Colors[2])
#ax.fill_betweenx(ESim / 20., 0.205, 0.3333, alpha=0.2, color="r")
ax.fill_betweenx(ESim / 20., 0.3520, 0.38, alpha=0.2, color="chocolate")

ax.annotate("$(3, 2, 1)$", xy=(0.313, 0.136), xytext=(0.305, 0.139), color=Colors[2],
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"), ha="center")
ax.annotate("$(3, 2, 1)$ - line slip" + "\n" + "mixture", xy=(0.328, 0.136), xytext=(0.328, 0.139), color="k",
            arrowprops=dict(shrink=0.001, width=1., headwidth=5., color="k"), ha="center")
ax.text(0.337, 0.115, r"line slip", color="k")
ax.text(0.363, 0.123, r"$(2, 2, 0)$", color="chocolate")

ax.set_xlim(0.303, 0.38)
ax.set_ylim(0.112, 0.138)
ax.legend(loc="upper right", fontsize=12)

ax.text(0.305, 0.113, r"$\frac{\omega^2 M}{k} = 0.2$")

ax.set_ylabel(r"Energy $E / (\omega^2 M d^2)$")
ax.set_xlabel(r"inverse number density $(\rho d)^{-1}$")

fig.savefig("../../Plots/EnergiesSim.pdf")
