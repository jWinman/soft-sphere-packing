import numpy as np
import matplotlib.pyplot as plt

ks = np.array([1])
#omegas = np.array([0.01])
omegas = np.arange(0.01, 0.6, 0.1)
Ns = np.arange(2, 6)
width = 1.0

_, Ls211_1, Es211_1 = np.loadtxt("../../data/PhaseDiagram/Ls211_1.csv", unpack=True)
_, Ls220_1, Es220_1 = np.loadtxt("../../data/PhaseDiagram/Ls220_1.csv", unpack=True)
_, Ls321_1, Es321_1 = np.loadtxt("../../data/PhaseDiagram/Ls321_1.csv", unpack=True)
_, Ls330_1, Es330_1 = np.loadtxt("../../data/PhaseDiagram/Ls330_1.csv", unpack=True)
_, Ls422_1, Es422_1 = np.loadtxt("../../data/PhaseDiagram/Ls422_1.csv", unpack=True)
_, Ls431_1, Es431_1 = np.loadtxt("../../data/PhaseDiagram/Ls431_1.csv", unpack=True)
_, Ls440_1, Es440_1 = np.loadtxt("../../data/PhaseDiagram/Ls440_1.csv", unpack=True)
_, Ls532_1, Es532_1 = np.loadtxt("../../data/PhaseDiagram/Ls532_1.csv", unpack=True)
_, Ls541_1, Es541_1 = np.loadtxt("../../data/PhaseDiagram/Ls541_1.csv", unpack=True)
_, Ls550_1, Es550_1 = np.loadtxt("../../data/PhaseDiagram/Ls550_1.csv", unpack=True)

_, Ls110_2, Es110_2 = np.loadtxt("../../data/PhaseDiagram/Ls110_2.csv", unpack=True)
_, Ls211_2, Es211_2 = np.loadtxt("../../data/PhaseDiagram/Ls211_2.csv", unpack=True)
_, Ls220_2, Es220_2 = np.loadtxt("../../data/PhaseDiagram/Ls220_2.csv", unpack=True)
_, Ls321_2, Es321_2 = np.loadtxt("../../data/PhaseDiagram/Ls321_2.csv", unpack=True)
_, Ls330_2, Es330_2 = np.loadtxt("../../data/PhaseDiagram/Ls330_2.csv", unpack=True)
_, Ls422_2, Es422_2 = np.loadtxt("../../data/PhaseDiagram/Ls422_2.csv", unpack=True)
_, Ls431_2, Es431_2 = np.loadtxt("../../data/PhaseDiagram/Ls431_2.csv", unpack=True)
_, Ls440_2, Es440_2 = np.loadtxt("../../data/PhaseDiagram/Ls440_2.csv", unpack=True)
_, Ls532_2, Es532_2 = np.loadtxt("../../data/PhaseDiagram/Ls532_2.csv", unpack=True)
_, Ls541_2, Es541_2 = np.loadtxt("../../data/PhaseDiagram/Ls541_2.csv", unpack=True)

for index, omega in enumerate(omegas):
    print("Omega: ", omega)
    for k in ks:
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)

        Ls110, Es110 = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_110.csv".format(omega, k), unpack=True)
        ax.plot(Ls110, Es110, "g-", label="$(1, 1, 0)$", linewidth=width)

        Ls211, Es211 = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_211.csv".format(omega, k), unpack=True)
        Ls211contacts, Es211contacts = np.loadtxt("../../data/PhaseDiagram/contacts{:.2f}_k{:.2f}_211.csv".format(omega, k), unpack=True)
        ax.plot(Ls211, Es211, "g-", label="$(2, 1, 1)$", linewidth=width)
        ax.plot(Ls211contacts, Es211contacts, "k--", linewidth=width)

        # plot NN0 structures
        for i, N in enumerate(Ns):
            L, E = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_{:d}{:d}0.csv".format(omega, k, int(N), int(N)), unpack=True)
            ax.plot(L, E, "-", color=plt.cm.Paired(i), label="$({}, {}, 0)$".format(N, N), linewidth=width)

        # plot 4, 2, 2 structure
        Ls422, Es422 = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_422.csv".format(omega, k), unpack=True)
        ax.plot(Ls422, Es422, "-", color="brown", label="$(4, 2, 2)$", linewidth=width)

       # plot achiral PQR structures
        PQRs = np.array([(3, 2, 1), (4, 3, 1), (5, 3, 2), (5, 4, 1)])
        for j, (P, Q, R) in enumerate(PQRs):
            L, E = np.loadtxt("../../data/PhaseDiagram/Omega{:.2f}_k{:.2f}_{:d}{:d}{:d}.csv".format(omega, k, int(P), int(Q), int(R)), unpack=True)

            if (P == 4 and Q == 3 and R == 1):
                L, E = L[-700:], E[-700:]
            if (P == 5 and Q == 3 and R == 2):
                L, E = L[-600:], E[-600:]
            if (P == 5 and Q == 4 and R == 1):
                L, E = L[-500:], E[-500:]

            ax.plot(L, E, "-", color=plt.cm.Paired((i + j + 1)), label="$({:d}, {:d}, {:d})$".format(int(P), int(Q), int(R)), linewidth=width)

        ax.plot(np.array([Ls110_2[index], Ls211_1[index]]), np.array([Es110_2[index], Es211_1[index]]), "k", linewidth=0.8*width)
        ax.plot(np.array([Ls211_2[index], Ls220_1[index]]), np.array([Es211_2[index], Es220_1[index]]), "k", linewidth=0.8*width)
        ax.plot(np.array([Ls220_2[index], Ls321_1[index]]), np.array([Es220_2[index], Es321_1[index]]), "k", linewidth=0.8*width)
        ax.plot(np.array([Ls321_2[index], Ls330_1[index]]), np.array([Es321_2[index], Es330_1[index]]), "k", linewidth=0.8*width)
        ax.plot(np.array([Ls330_2[index], Ls422_1[index]]), np.array([Es330_2[index], Es422_1[index]]), "k", linewidth=0.8*width)
        ax.plot(np.array([Ls422_2[index], Ls431_1[index]]), np.array([Es422_2[index], Es431_1[index]]), "k", linewidth=0.8*width)
        ax.plot(np.array([Ls431_2[index], Ls440_1[index]]), np.array([Es431_2[index], Es440_1[index]]), "k", linewidth=0.8*width)
        ax.plot(np.array([Ls440_2[index], Ls532_1[index]]), np.array([Es440_2[index], Es532_1[index]]), "k", linewidth=0.8*width)
        ax.plot(np.array([Ls532_2[index], Ls541_1[index]]), np.array([Es532_2[index], Es541_1[index]]), "k", linewidth=0.8*width)
        ax.plot(np.array([Ls541_2[index], Ls550_1[index]]), np.array([Es541_2[index], Es550_1[index]]), "k", linewidth=0.8*width)

        ax.set_title("$\omega = {:.2f}, k = {}$".format(omega, k))
        ax.set_xlabel("Length $L / (Nd)$")
        ax.set_ylabel("Energy $E$")
        ax.legend(ncol=2, fontsize=10)

        fig.savefig("../../Plots/PhaseDiagram/Omega{:.2f}_k{:.2f}.pdf".format(omega, k))
