import numpy as np
import matplotlib.pyplot as plt

directory ="../../data/PhaseDiagram"

_, Ls211_1, Es211_1 = np.loadtxt("{}/Ls211_1.csv".format(directory), unpack=True)
_, Ls220_1, Es220_1 = np.loadtxt("{}/Ls220_1.csv".format(directory), unpack=True)
_, Ls321_1, Es321_1 = np.loadtxt("{}/Ls321_1.csv".format(directory), unpack=True)
_, Ls330_1, Es330_1 = np.loadtxt("{}/Ls330_1.csv".format(directory), unpack=True)
_, Ls422_1, Es422_1 = np.loadtxt("{}/Ls422_1.csv".format(directory), unpack=True)
_, Ls431_1, Es431_1 = np.loadtxt("{}/Ls431_1.csv".format(directory), unpack=True)
_, Ls440_1, Es440_1 = np.loadtxt("{}/Ls440_1.csv".format(directory), unpack=True)
_, Ls532_1, Es532_1 = np.loadtxt("{}/Ls532_1.csv".format(directory), unpack=True)
_, Ls541_1, Es541_1 = np.loadtxt("{}/Ls541_1.csv".format(directory), unpack=True)
_, Ls550_1, Es550_1 = np.loadtxt("{}/Ls550_1.csv".format(directory), unpack=True)

_, Ls110_2, Es110_2 = np.loadtxt("{}/Ls110_2.csv".format(directory), unpack=True)
_, Ls211_2, Es211_2 = np.loadtxt("{}/Ls211_2.csv".format(directory), unpack=True)
_, Ls220_2, Es220_2 = np.loadtxt("{}/Ls220_2.csv".format(directory), unpack=True)
_, Ls321_2, Es321_2 = np.loadtxt("{}/Ls321_2.csv".format(directory), unpack=True)
_, Ls330_2, Es330_2 = np.loadtxt("{}/Ls330_2.csv".format(directory), unpack=True)
_, Ls422_2, Es422_2 = np.loadtxt("{}/Ls422_2.csv".format(directory), unpack=True)
_, Ls431_2, Es431_2 = np.loadtxt("{}/Ls431_2.csv".format(directory), unpack=True)
_, Ls440_2, Es440_2 = np.loadtxt("{}/Ls440_2.csv".format(directory), unpack=True)
_, Ls532_2, Es532_2 = np.loadtxt("{}/Ls532_2.csv".format(directory), unpack=True)
_, Ls541_2, Es541_2 = np.loadtxt("{}/Ls541_2.csv".format(directory), unpack=True)

width = 0.8

k = 1
omega = 0.55
index = int(omega / 0.01) - 1

Colors = plt.get_cmap("rainbow")(np.arange(0, 1, 0.1))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

Ls211, Es211 = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_211.csv".format(directory, omega, k), unpack=True)
ax.plot(Ls211[600:-150], Es211[600:-150], "-", color=Colors[0], label="$(2, 1, 1)$", linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}0.csv".format(directory, omega, k, 2, 2), unpack=True)
ax.plot(L[400:-500], E[400:-500], "-", color=Colors[1], label="$({}, {}, 0)$".format(2, 2), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}{:d}.csv".format(directory, omega, k, 3, 2, 1), unpack=True)
ax.plot(L[0:-500], E[0:-500], "-", color=Colors[2], label="$({:d}, {:d}, {:d})$".format(3, 2, 1), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}0.csv".format(directory, omega, k, 3, 3), unpack=True)
ax.plot(L[50:-500], E[50:-500], "-", color=Colors[3], label="$({}, {}, 0)$".format(3, 3), linewidth=width)

Ls422, Es422 = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_422.csv".format(directory, omega, k), unpack=True)
ax.plot(Ls422[0:-500], Es422[0:-500], "-", color=Colors[4], label="$(4, 2, 2)$", linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}{:d}.csv".format(directory, omega, k, 4, 3, 1), unpack=True)
ax.plot(L[300:-180], E[300:-180], "-", color=Colors[5], label="$({:d}, {:d}, {:d})$".format(4, 3, 1), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}0.csv".format(directory, omega, k, 4, 4), unpack=True)
ax.plot(L[50:-700], E[50:-700], "-", color=Colors[6], label="$({}, {}, 0)$".format(4, 4), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}{:d}.csv".format(directory, omega, k, 5, 3, 2), unpack=True)
ax.plot(L[20:-100], E[20:-100], "-", color=Colors[7], label="$({:d}, {:d}, {:d})$".format(5, 3, 2), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}{:d}.csv".format(directory, omega, k, 5, 4, 1), unpack=True)
ax.plot(L[100:-50], E[100:-50], "-", color=Colors[8], label="$({:d}, {:d}, {:d})$".format(5, 4, 1), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}0.csv".format(directory, omega, k, 5, 5), unpack=True)
ax.plot(L, E, "-", color=Colors[9], label="$({}, {}, 0)$".format(5, 5), linewidth=width)

msize = 1.5
ax.plot(np.array([Ls110_2[index], Ls211_1[index]]), np.array([Es110_2[index], Es211_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls110_2[index], Ls211_1[index]]), np.array([Es110_2[index], Es211_1[index]]), "ko", markersize=msize)
ax.plot(np.array([Ls211_2[index], Ls220_1[index]]), np.array([Es211_2[index], Es220_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls211_2[index], Ls220_1[index]]), np.array([Es211_2[index], Es220_1[index]]), "ko", markersize=msize)
ax.plot(np.array([Ls220_2[index], Ls321_1[index]]), np.array([Es220_2[index], Es321_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls220_2[index], Ls321_1[index]]), np.array([Es220_2[index], Es321_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls321_2[index], Ls422_1[index]]), np.array([Es321_2[index], Es422_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls321_2[index], Ls422_1[index]]), np.array([Es321_2[index], Es422_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls422_2[index], Ls431_1[index]]), np.array([Es422_2[index], Es431_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls422_2[index], Ls431_1[index]]), np.array([Es422_2[index], Es431_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls431_2[index], Ls532_1[index]]), np.array([Es431_2[index], Es532_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls431_2[index], Ls532_1[index]]), np.array([Es431_2[index], Es532_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls532_2[index], Ls541_1[index]]), np.array([Es532_2[index], Es541_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls532_2[index], Ls541_1[index]]), np.array([Es532_2[index], Es541_1[index]]), "ko", markersize=msize)
ax.plot(np.array([Ls541_2[index], Ls550_1[index]]), np.array([Es541_2[index], Es550_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls541_2[index], Ls550_1[index]]), np.array([Es541_2[index], Es550_1[index]]), "ko", markersize=msize)

E = np.linspace(0.0, 0.4, 100)
alpha = 0.16
ax.fill_betweenx(E, Ls211_1[index], Ls211_2[index], color=Colors[0], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls220_1[index], Ls220_2[index], color=Colors[1], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls321_1[index], Ls321_2[index], color=Colors[2], alpha=alpha, linewidth=0)

ax.fill_betweenx(E, Ls422_1[index], Ls422_2[index], color=Colors[4], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls431_1[index], Ls431_2[index], color=Colors[5], alpha=alpha, linewidth=0)

ax.fill_betweenx(E, Ls532_1[index], Ls532_2[index], color=Colors[7], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls541_1[index], Ls541_2[index], color=Colors[8], alpha=alpha, linewidth=0)

ax.text(0.15, 0.09, r"$\frac{{\omega^2 M}}{{k}} = {:.2f}$".format(omega))
ax.set_xlim(0.13, 0.55)
ax.set_ylim(0.07, 0.40)
ax.set_ylabel(r"Energy $E_{\text{rot}}^S / (\omega^2 M d^2)$")
ax.set_xlabel(r"inverse number density $(\rho d)^{-1}$")

ax.legend(ncol=4, fontsize=9.58, bbox_to_anchor=(-0.03, 1.245))

omega = 0.1
index = int(omega / 0.01) - 1

ax = fig.add_axes([0.374, 0.46, 0.57, 0.456])

Ls211, Es211 = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_211.csv".format(directory, omega, k), unpack=True)
ax.plot(Ls211[600:-250], Es211[600:-250], "-", color=Colors[0], label="$(2, 1, 1)$", linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}0.csv".format(directory, omega, k, 2, 2), unpack=True)
ax.plot(L[600:-500], E[600:-500], "-", color=Colors[1], label="$({}, {}, 0)$".format(2, 2), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}{:d}.csv".format(directory, omega, k, 3, 2, 1), unpack=True)
ax.plot(L[0:-500], E[0:-500], "-", color=Colors[2], label="$({:d}, {:d}, {:d})$".format(3, 2, 1), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}0.csv".format(directory, omega, k, 3, 3), unpack=True)
ax.plot(L[170:-500], E[170:-500], "-", color=Colors[3], label="$({}, {}, 0)$".format(3, 3), linewidth=width)

Ls422, Es422 = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_422.csv".format(directory, omega, k), unpack=True)
ax.plot(Ls422[50:-500], Es422[50:-500], "-", color=Colors[4], label="$(4, 2, 2)$", linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}{:d}.csv".format(directory, omega, k, 4, 3, 1), unpack=True)
ax.plot(L[370:-180], E[370:-180], "-", color=Colors[5], label="$({:d}, {:d}, {:d})$".format(4, 3, 1), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}0.csv".format(directory, omega, k, 4, 4), unpack=True)
ax.plot(L[100:-700], E[100:-700], "-", color=Colors[6], label="$({}, {}, 0)$".format(4, 4), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}{:d}.csv".format(directory, omega, k, 5, 3, 2), unpack=True)
ax.plot(L[70:-100], E[70:-100], "-", color=Colors[7], label="$({:d}, {:d}, {:d})$".format(5, 3, 2), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}{:d}.csv".format(directory, omega, k, 5, 4, 1), unpack=True)
ax.plot(L[150:-10], E[150:-10], "-", color=Colors[8], label="$({:d}, {:d}, {:d})$".format(5, 4, 1), linewidth=width)

L, E = np.loadtxt("{}/Omega{:.2f}_k{:.2f}_{:d}{:d}0.csv".format(directory, omega, k, 5, 5), unpack=True)
ax.plot(L[:-800], E[:-800], "-", color=Colors[9], label="$({}, {}, 0)$".format(5, 5), linewidth=width)

msize = 1.2
ax.plot(np.array([Ls110_2[index], Ls211_1[index]]), np.array([Es110_2[index], Es211_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls110_2[index], Ls211_1[index]]), np.array([Es110_2[index], Es211_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls211_2[index], Ls220_1[index]]), np.array([Es211_2[index], Es220_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls211_2[index], Ls220_1[index]]), np.array([Es211_2[index], Es220_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls220_2[index], Ls321_1[index]]), np.array([Es220_2[index], Es321_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls220_2[index], Ls321_1[index]]), np.array([Es220_2[index], Es321_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls321_2[index], Ls330_1[index]]), np.array([Es321_2[index], Es330_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls321_2[index], Ls330_1[index]]), np.array([Es321_2[index], Es330_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls330_2[index], Ls422_1[index]]), np.array([Es330_2[index], Es422_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls330_2[index], Ls422_1[index]]), np.array([Es330_2[index], Es422_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls422_2[index], Ls431_1[index]]), np.array([Es422_2[index], Es431_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls422_2[index], Ls431_1[index]]), np.array([Es422_2[index], Es431_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls431_2[index], Ls440_1[index]]), np.array([Es431_2[index], Es440_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls431_2[index], Ls440_1[index]]), np.array([Es431_2[index], Es440_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls440_2[index], Ls532_1[index]]), np.array([Es440_2[index], Es532_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls440_2[index], Ls532_1[index]]), np.array([Es440_2[index], Es532_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls532_2[index], Ls541_1[index]]), np.array([Es532_2[index], Es541_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls532_2[index], Ls541_1[index]]), np.array([Es532_2[index], Es541_1[index]]), "ko", markersize=msize)

ax.plot(np.array([Ls541_2[index], Ls550_1[index]]), np.array([Es541_2[index], Es550_1[index]]), "k", linewidth=0.8*width)
ax.plot(np.array([Ls541_2[index], Ls550_1[index]]), np.array([Es541_2[index], Es550_1[index]]), "ko", markersize=msize)

E = np.linspace(0.0, 0.4, 100)
alpha = 0.5
ax.fill_betweenx(E, Ls211_1[index], Ls211_2[index], color=Colors[0], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls220_1[index], Ls220_2[index], color=Colors[1], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls321_1[index], Ls321_2[index], color=Colors[2], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls330_1[index], Ls330_2[index], color=Colors[3], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls422_1[index], Ls422_2[index], color=Colors[4], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls431_1[index], Ls431_2[index], color=Colors[5], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls440_1[index], Ls440_2[index], color=Colors[6], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls532_1[index], Ls532_2[index], color=Colors[7], alpha=alpha, linewidth=0)
ax.fill_betweenx(E, Ls541_1[index], Ls541_2[index], color=Colors[8], alpha=alpha, linewidth=0)

ax.text(0.15, 0.09, r"$\frac{{\omega^2 M}}{{k}} = {:.2f}$".format(omega))
ax.set_xlim(0.14, 0.55)
ax.set_ylim(0.07, 0.4)

fig.savefig("../../Plots/Energies.pdf")
