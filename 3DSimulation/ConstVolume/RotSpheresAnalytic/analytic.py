import sys
sys.path.append("../../libraries")

import numpy as np
import classRotSpheresAnalytic as lib

def Min_Energy_Curves(RotSpheres, rInitials, thInitials):
    ## Do the 110 structure ###
    Ls110 = np.arange(1.2 * RotSpheres.d, 0.7 * RotSpheres.d, -0.0002)
    Es110 =   np.zeros(len(Ls110))

    for i, L in enumerate(Ls110):
        RotSpheres.setVariable("L", L)
        Es110[i] = RotSpheres.E_Bamboo()

    np.savetxt("data/PhaseDiagram/Omega{:.2f}_k{:.2f}_110.csv".format(RotSpheres.omega, RotSpheres.k),
               np.array([Ls110, Es110]).T)

    ## Do the 211 structure ###
    Ls211 = np.arange(0.7 * RotSpheres.d, 0.3 * RotSpheres.d, -0.0002)
    rs211 =   np.zeros(len(Ls211))
    phis211 = np.zeros(len(Ls211))
    Es211 =   np.zeros(len(Ls211))
    contacts211 = np.empty(len(Ls211), dtype=bool)

    rInitial = rInitials[0]
    for i, L in enumerate(Ls211):
        RotSpheres.setVariable("L", L)
        (rs211[i], phis211[i]), Es211[i] = RotSpheres.minimize(RotSpheres.Energy_ZigZag, [rInitial, np.pi], [(0.1, 1.86 / 2.), (0, 1.5 * np.pi)])
        contacts211[i] = RotSpheres.contact_ZigZag([rs211[i], phis211[i]])
        rInitial = rs211[i]

    rInitials[0] = rInitial

    np.savetxt("data/PhaseDiagram/Omega{:.2f}_k{:.2f}_211.csv".format(RotSpheres.omega, RotSpheres.k),
               np.array([Ls211, Es211]).T)
    np.savetxt("data/PhaseDiagram/Contacts{:.2f}_k{:.2f}_211.csv".format(RotSpheres.omega, RotSpheres.k),
               np.array([Ls211[contacts211], Es211[contacts211]]).T)

    ## Do the NN0 structures first ##
    NN0 = np.arange(2, 6)
    LsNN0 = np.arange(1.0 * RotSpheres.d, 0.45 * RotSpheres.d, -0.0002)
    rsNN0 =  np.zeros(len(LsNN0))
    EsNN0 =  np.zeros(len(LsNN0))
    contacts = np.empty(len(LsNN0), dtype=bool)
    rInitial = rInitials[1:len(NN0) + 1]

    for j, N in enumerate(NN0):
        RotSpheres.setVariable("Nplane", N)
        rInitial_j = rInitial[j]
        for i, L in enumerate(LsNN0):
            RotSpheres.setVariable("L", L)
            rsNN0[i], EsNN0[i] = RotSpheres.minimize(RotSpheres.E_NN0, rInitial_j, [(0.1, 2.5 / 2.)])
            rInitial_j = rsNN0[i]
        rInitial[j] = rInitial_j

        np.savetxt("data/PhaseDiagram/Omega{:.2f}_k{:.2f}_{:d}{:d}0.csv".format(RotSpheres.omega, RotSpheres.k, int(N), int(N)),
                   np.array([LsNN0 / N, EsNN0]).T)

    rInitials[1:len(NN0) + 1] = rInitial

    ## Do the 422 structure ###
    Ls422 = np.arange(0.6 * RotSpheres.d, 0.3 * RotSpheres.d, -0.0002)
    rs422 = np.zeros(len(Ls422))
    Es422 = np.zeros(len(Ls422))
    contacts422 = np.empty(len(Ls422), dtype=bool)

    rInitial = rInitials[len(NN0) + 1]
    for i, L in enumerate(Ls422):
        RotSpheres.setVariable("L", L)
        rs422[i], Es422[i] = RotSpheres.minimize(RotSpheres.Energy_422, rInitial, [(0.5, 2.3 / 2.)])
        rsInitial = rs422[i]

    rInitials[len(NN0) + 1] = rInitial

    np.savetxt("data/PhaseDiagram/Omega{:.2f}_k{:.2f}_422.csv".format(RotSpheres.omega, RotSpheres.k), np.array([Ls422 / 2., Es422]).T)

    ## Do the achiral PQR structures ##
    LsPQRs = [np.arange(0.35 * RotSpheres.d, 0.15 * RotSpheres.d, -0.0002),
             np.arange(0.35 * RotSpheres.d, 0.15 * RotSpheres.d, -0.0002),
             np.arange(0.25 * RotSpheres.d, 0.13 * RotSpheres.d, -0.0002),
             np.arange(0.25 * RotSpheres.d, 0.13 * RotSpheres.d, -0.0002)]

    PQRs = np.array([(3, 2, 1), (4, 3, 1), (5, 3, 2), (5, 4, 1)])
    rInitial = rInitials[len(NN0) + 1:]

    for j, (P, Q, R) in enumerate(PQRs):
        RotSpheres.setVariable("structure", "np.array([{}, {}, {}])".format(P, Q, R))
        LsPQR = LsPQRs[j]

        rsPQR =   np.zeros(len(LsPQR))
        phisPQR = np.zeros(len(LsPQR))
        EsPQR =   np.zeros(len(LsPQR))
        contactsPQR = np.empty(len(LsPQR), dtype=bool)

        for i, L in enumerate(LsPQR):
            RotSpheres.setVariable("L", L)
            (rsPQR[i], phisPQR[i]), EsPQR[i] = RotSpheres.minimize(RotSpheres.Energy_PQR, (rInitials[j], phiInitials[j]), [(0.2, 2.51), (0., np.pi)])
            rInitial[j] = rsPQR[i]
            phiInitials[j] = phisPQR[i]

        np.savetxt("data/PhaseDiagram/Omega{:.2f}_k{:.2f}_{:d}{:d}{:d}.csv".format(RotSpheres.omega, RotSpheres.k, int(P), int(Q), int(R)), np.array([LsPQR, EsPQR]).T)

    rInitials[len(NN0) + 1:] = rInitial
    phiInitials[len(NN0) + 1:] = phisPQR[np.argmin(EsPQR)]


import subprocess
subprocess.call("mkdir data/PhaseDiagram", shell=True)

RotSpheres = lib.RotatingSpheres("parameter.cfg")

omegas = np.arange(0.01, 0.62, 0.01)
ks = np.array([1])
rInitials = np.array([1.86, # (2,1,1)
                     2., 2.1547, 2.4142, 2.7013, # (N,N,0)
                     2.25, # (4,2,2)
                     2.03, 2.29, 2.48, 2.57 # (3,2,1), (4,3,1), (5,3,2), (5,4,1)
                    ]) / 2.
phiInitials = np.array([np.arccos(-2 / 3.), # (3,2,1)
                      1.70, # (4,3,1)
                      2.51, # (5,3,2)
                      1.35, # (5,4,1)
                     ])

for omega in omegas:
    RotSpheres.setVariable("omega", omega)
    for k in ks:
        print("Omega: ", omega, "k: ", k)
        RotSpheres.setVariable("k", k)
        Min_Energy_Curves(RotSpheres, rInitials, phiInitials)
