import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess
import time
from joblib import Parallel, delayed

def minimalE(D_z, p, N, initial, finalDir, niter):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_z", D_z / 2.)
    mySystem.setVariable("niter", niter)
    if (initial == "Random"):
        mySystem.RandomInitial()
    else:
        mySystem.ReadInitial(initial)
    print("pressure: ", p, "Diameter: ", D_z)
    E = mySystem.minimize("basinhopping") / N
    mySystem.printToFile(finalDir)
    print("Energy: ", E)
    return E, mySystem.positions[-1]

N = int(sys.argv[1])
Np = 98
ND_z = 200
#Np = 98
#ND_z = 481

# 1. create hard-sphere packing
D_zs = np.arange(1., 1.5, 0.0025)
p = 0.0002
initialHS = "Random"
finalDirHS = "HardSpheres/N{}/".format(N)
niterHS = 100
print("1. Creating Hard-Sphere: ")
E, L = np.array(Parallel(n_jobs=3)(delayed(minimalE)(D_z, p, N, initialHS, finalDirHS, niterHS) for D_z in D_zs)).T

# 2. do the phasediagram with increasing pressure
ps = np.array([np.arange(0.0004, 0.02, 0.0002) for _ in range(ND_z)]).T.flatten()
D_zs = np.array([D_zs for _ in range(Np)]).flatten()
initialSS2 = finalDirHS + "Finalp{:.6f}D".format(p)
finalDirSS2 = "PhaseDiagram/N{}/".format(N)
niterSS = 5
print("2. Creating Soft Spheres with increasing pressure: ")
E, L = np.array(Parallel(n_jobs=3)(delayed(minimalE)(D_z, p, N,
                                                     initialSS2 + "{:.4f}".format(D_z) if (i < ND_z) else finalDirSS2 + "Finalp{:.6f}D{:.4f}".format(p - 0.0002, D_z),
                                   finalDirSS2, niterSS) for i, (D_z, p) in enumerate(zip(D_zs, ps)))).T

# 3. do the phasediagram with decreasing D_z
D_zs = np.arange(1., 1.5, 0.0025)[::-1]
D_z = D_zs[1]
D_zs = np.array([D_zs for _ in range(Np)]).T.flatten()
ps = np.array([np.arange(0.0004, 0.02, 0.0002) for _ in range(ND_z)]).flatten()
initialSS3 = finalDirSS2 + "FinalpD{:.4f}".format(D_z)
finalDirSS3 = "PhaseDiagram/NP{}/".format(N)
niterSS = 5
print("3. Creating Soft Spheres with decreasing D_z: ")
E, L = np.array(Parallel(n_jobs=3)(delayed(minimalE)(D_z, p, N,
                                                     initialSS3[:-7] + "{:.6f}".format(p) + initialSS3[-7:] if (i < Np) else finalDirSS3 + "Finalp{:.6f}D{:.4f}".format(p, D_z + 0.0025),
                                                     finalDirSS3, niterSS) for i, (D_z, p) in enumerate(zip(D_zs, ps)))).T

# 4. do the phasediagram with increasing D_z
D_zs = np.arange(1., 1.5, 0.0025)
D_z = D_zs[1]
D_zs = np.array([D_zs for _ in range(Np)]).T.flatten()
ps = np.array([np.arange(0.0004, 0.02, 0.0002) for _ in range(ND_z)]).flatten()
initialSS4 = finalDirSS2 + "FinalpD{:.4f}".format(D_z)
finalDirSS4 = "PhaseDiagram/N2P{}/".format(N)
niterSS = 5
print("3. Creating Soft Spheres with increasing D_z: ")
E, L = np.array(Parallel(n_jobs=3)(delayed(minimalE)(D_z, p, N,
                                                     initialSS4[:-7] + "{:.6f}".format(p) + initialSS4[-7:] if (i < Np) else finalDirSS4 + "Finalp{:.6f}D{:.4f}".format(p, D_z - 0.0025),
                                                     finalDirSS4, niterSS) for i, (D_z, p) in enumerate(zip(D_zs, ps)))).T
