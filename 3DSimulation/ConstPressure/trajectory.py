import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess

def select_initial(initial1, initial2, initial3, N, p, D):
    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_z", D / 2.)

    mySystem.ReadInitial(initial1)
    E1 = mySystem.Energy(mySystem.positions)

    mySystem.ReadInitial(initial2)
    E2 = mySystem.Energy(mySystem.positions)

    mySystem.ReadInitial(initial3)
    E3 = mySystem.Energy(mySystem.positions)

    E = np.array([E1, E2, E3])
    initial = np.array([initial1, initial2, initial3])
    return initial[np.argmin(E)]


def minimalE(D_z, p, N, repetition, initial, finalDir):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_z", D_z / 2.)
    mySystem.ReadInitial(initial)
    if (repetition[0] != 1):
        mySystem.resize(repetition[0], repetition[1])

    mySystem.perturb(1e-3)

    print("pressure: ", p, "Diameter: ", D_z, mySystem.N, flush=True)

    E = mySystem.minimize("L-BFGS-B")
    mySystem.printToFile(finalDir, prec=(10, 8))

    print("Energy: ", E / mySystem.N, flush=True)
    return E

def trajectory(D_zs, ps, N, repetition, number):

    Nrep = int(N * repetition[0] * repetition[1])
    # create initial packing from phasediagram
    initialSSN = "PhaseDiagram/N{}/Finalp{:.6f}D{:.4f}".format(N, 0.0004, D_zs[0])
    initialSSNP = "PhaseDiagram/NP{}/Finalp{:.6f}D{:.4f}".format(N, 0.0004, D_zs[0])
    initialSSN2P = "PhaseDiagram/N2P{}/Finalp{:.6f}D{:.4f}".format(N, 0.0004, D_zs[0])
    initialSS = select_initial(initialSSN, initialSSNP, initialSSN2P, N, 0.0004, D_zs[0])
#    initialSS = "Adil/422LineSlip/211LineSlip"

    finalDirSS = "Hysteresis/Traj{}Forward/N{:d}/".format(number, Nrep)

    # create forward trajectory
    EForward = np.zeros(len(ps))
    for i, (D_z, p) in enumerate(zip(D_zs, ps)):
        if (i == 0):
            EForward[i] = minimalE(D_z, p, N, repetition, initialSS, finalDirSS)
        else:
            EForward[i] = minimalE(D_z, p, Nrep, (1, 1.), finalDirSS + "Finalp{:.10f}D{:.8f}".format(ps[i - 1], D_zs[i - 1]), finalDirSS)

    # create backward trajectory
    ps, D_zs = ps[::-1], D_zs[::-1]
    initialSS = finalDirSS + "Finalp{:.6f}D{:.4f}".format(ps[0], D_zs[0])
#    initialSS = "Hysteresis/Traj{}Backward/N{:d}/Finalp{:.6f}D{:.4f}".format(number, Nrep, ps[0], D_zs[0])
    finalDirSS = "Hysteresis/Traj{}Backward/N{:d}/".format(number, Nrep)
    EBackward = np.zeros(len(ps))

    for i, (D_z, p) in enumerate(zip(D_zs, ps)):
        if (i == 0):
            EBackward[i] = minimalE(D_z, p, Nrep, (1, 1.), initialSS, finalDirSS)
        else:
            EBackward[i] = minimalE(D_z, p, Nrep, (1, 1.), finalDirSS + "Finalp{:.10f}D{:.8f}".format(ps[i - 1], D_zs[i - 1]), finalDirSS)

    return EForward, EBackward

Nrep = 12
frac = 1.
N = 3
repetition = (int(Nrep / (N * frac)) , frac)
number = sys.argv[1]

# trajectory points in phasediagram
D_zs = np.arange(2.15, 2.03, -0.0005)
ps = np.ones(len(D_zs)) * 1e-8

trajectory(D_zs, ps, N, repetition, number)
