#!/bin/bash

cd ..;
nohup python3 hysteresis.py $1 > nohup.txt;
cd Scripts/CalcScripts/;
nohup python3 CalcHysteresisPhyllo.py > CalcPhyllo.txt;
nohup python3 CalcHysteresisArea.py > CalcAreas.txt;

cd ../../data/Hysteresis;
tar czf $1.tar $1*;
