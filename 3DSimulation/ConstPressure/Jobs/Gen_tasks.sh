#!/bin/bash

count=0
theta=2
for j in {1..1};
do
    sed s/VARIABLE/$j/g < taskfarming.pbs > taskfarming$j.pbs;
    for i in {1..45};
    do
	x=$(cat task_raw)
	printf "$x $count $theta\n" >> tasks$j
	let count=count+1
	let theta=theta+4
    done
done
