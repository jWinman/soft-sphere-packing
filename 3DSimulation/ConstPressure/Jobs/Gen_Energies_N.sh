#!/bin/bash

declare -a array
array=(2 3 4);

for i in ${array[@]};
do
	sed s/VARIABLE/$i/g < Energies_N_raw.sh > Energies_$i.sh;
	#qsub -vVAR="$1" Energies_$i.sh;
done
