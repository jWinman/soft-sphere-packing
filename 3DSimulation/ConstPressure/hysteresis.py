import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess
from joblib import Parallel, delayed

def select_initial(initial1, initial2, initial3, N, p, D):
    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_z", D / 2.)

    mySystem.ReadInitial(initial1)
    E1 = mySystem.Energy(mySystem.positions)

    mySystem.ReadInitial(initial2)
    E2 = mySystem.Energy(mySystem.positions)

    mySystem.ReadInitial(initial3)
    E3 = mySystem.Energy(mySystem.positions)

    E = np.array([E1, E2, E3])
    initial = np.array([initial1, initial2, initial3])
    return initial[np.argmin(E)]

def minimalE(D_z, p, N, repetition, initial, finalDir):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_z", D_z / 2.)
    mySystem.ReadInitial(initial)
    if (repetition[0] != 1):
        mySystem.resize(repetition[0], repetition[1])

    if ("Forward" in finalDir):
        jiggle = 0.005
    else:
        jiggle = 0.005

    print("pressure: ", p, "Diameter: ", D_z, "N: ", mySystem.N, "jiggle: ", jiggle, flush=True)

    mySystem.perturb(jiggle)
    E = mySystem.minimize("L-BFGS-B")
    mySystem.printToFile(finalDir)

    print("Energy: ", E / mySystem.N, flush=True)
    return E

def const_D(D_zs, p, N, repetition, initialSS, number):
    Nrep = int(N * repetition[0] * repetition[1])
    finalDirSS = "Hysteresis/{}Forward/N{:d}/".format(number, Nrep)

    # create forward trajectory
    EForward = np.zeros(len(Ds))
    for i, D_z in enumerate(D_zs):
        if (i == 0):
            EForward[i] = minimalE(D_z, p, N, repetition, initialSS, finalDirSS)
        else:
            EForward[i] = minimalE(D_z, p, Nrep, (1, 1.), finalDirSS + "Finalp{:.6f}D{:.4f}".format(p, D_zs[i - 1]), finalDirSS)

    # create backward trajectory
    D_zs = D_zs[::-1]
    initialSS = finalDirSS + "Finalp{:.6f}D{:.4f}".format(p, D_zs[0])
    finalDirSS = "Hysteresis/{}Backward/N{:d}/".format(number, Nrep)
    EBackward = np.zeros(len(Ds))

    for i, D_z in enumerate(D_zs):
        if (i == 0):
            EBackward[i] = minimalE(D_z, p, Nrep, (1, 1.), initialSS, finalDirSS)
        else:
            EBackward[i] = minimalE(D_z, p, Nrep, (1, 1.), finalDirSS + "Finalp{:.6f}D{:.4f}".format(p, D_zs[i - 1]), finalDirSS)

    return EForward, EBackward

def const_p(Ds, ps, structure, number):
    initialSSN = "PhaseDiagram/N{}/Finalp{:.6f}D{:.4f}".format(structure[0], structure[1], structure[2])
    initialSSNP = "PhaseDiagram/NP{}/Finalp{:.6f}D{:.4f}".format(structure[0], structure[1], structure[2])
    initialSSN2P = "PhaseDiagram/N2P{}/Finalp{:.6f}D{:.4f}".format(structure[0], structure[1], structure[2])
    initialSS = select_initial(initialSSN, initialSSNP, initialSSN2P, structure[0], structure[1], structure[2])

    Nrep = 12
    frac = 1.
    repetition = (int(Nrep / (structure[0] * frac)), frac)

    EForward, EBackward = np.array(Parallel(n_jobs=8)(delayed(const_D)(Ds, p, structure[0], repetition, initialSS, number) for p in ps))[0]
    #EForward, EBackward = np.array([const_D(Ds, p, structure[0], repetition, initialSS, number) for p in ps])[0]

    return EForward, EBackward

Ds = np.arange(1.945, 2.04, 0.0005)
ps = np.arange(0.0001, 0.0055, 0.0001)
structure = (4, 14e-4, 1.95)

F, EB = const_p(Ds, ps, structure, sys.argv[1])
