import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess
import time
from joblib import Parallel, delayed

def minimalE(D_z, p, N, initial, finalDir, niter):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystemNew = classSpheres.SpheresCylinder("parameter.cfg")
    mySystemNew.setVariable("p", p)
    mySystemNew.setVariable("N", N)
    mySystemNew.setVariable("R_z", D_z / 2.)
    mySystemNew.setVariable("niter", niter)
    mySystemNew.ReadInitial(initial)
    print("pressure: ", p, "Diameter: ", D_z, flush=True)
    Enew = mySystemNew.minimize("basinhopping") / N

    initialOld = finalDir + "Finalp{:.6f}D{:.4f}".format(p, D_z)
    mySystemOld = classSpheres.SpheresCylinder("parameter.cfg")
    mySystemOld.setVariable("p", p)
    mySystemOld.setVariable("N", N)
    mySystemOld.setVariable("R_z", D_z / 2.)
    mySystemOld.ReadInitial(initialOld)
    Eold = mySystemOld.minimize("BFGS") / N

    print("Energies: ", Enew, Eold)
    if (Enew < Eold):
        mySystemNew.printToFile(finalDir)
        return Enew, mySystemNew.positions[-1]
    else:
        mySystemOld.printToFile(finalDir)
        return Eold, mySystemOld.positions[-1]

N = int(sys.argv[1])
Np = 98
ND_z = 33

D_zs = np.arange(2.62, 2.7, 0.0025)
ps = np.array([np.arange(0.0004, 0.02, 0.0002) for _ in range(ND_z)]).T.flatten()
D_zs = np.array([D_zs for _ in range(Np)]).flatten()

p = 0.015
D = 2.65

# 2. do the phasediagram with increasing pressure
finalDirSS = "PhaseDiagram/N{}/".format(N)
initialSS = finalDirSS + "Finalp{:.6f}D{:.4f}".format(p, D)
niterSS = 5
print("2. Creating Soft Spheres with increasing pressure: ")
E, L = np.array(Parallel(n_jobs=8)(delayed(minimalE)(D_z, p, N,
                                   initialSS,
                                   finalDirSS, niterSS) for i, (D_z, p) in enumerate(zip(D_zs, ps)))).T

# 3. do the phasediagram with decreasing D_z
finalDirSS = "PhaseDiagram/NP{}/".format(N)
initialSS = finalDirSS + "Finalp{:.6f}D{:.4f}".format(p, D)
print("2. Creating Soft Spheres with decreasing D_z: ")
E, L = np.array(Parallel(n_jobs=8)(delayed(minimalE)(D_z, p, N,
                                   initialSS,
                                   finalDirSS, niterSS) for i, (D_z, p) in enumerate(zip(D_zs, ps)))).T

finalDirSS = "PhaseDiagram/N2P{}/".format(N)
initialSS = finalDirSS + "Finalp{:.6f}D{:.4f}".format(p, D)
print("2. Creating Soft Spheres with increasing pressure: ")
E, L = np.array(Parallel(n_jobs=8)(delayed(minimalE)(D_z, p, N,
                                   initialSS,
                                   finalDirSS, niterSS) for i, (D_z, p) in enumerate(zip(D_zs, ps)))).T
