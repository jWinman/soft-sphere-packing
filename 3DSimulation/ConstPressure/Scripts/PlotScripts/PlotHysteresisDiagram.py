import numpy as np
import matplotlib.pyplot as plt

##############################################################
directory = "321to422Forward"

param = np.loadtxt("../../data/Hysteresis/{}/Areas.txt".format(directory), unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

plt.imshow(param, origin="lower", interpolation="nearest", cmap="terrain", aspect="auto", extent=[0, 56, 1, 40])
plt.xticks(np.arange(0, 56)[::10], np.arange(1.95, 2.225, 0.005)[::10])
plt.yticks(np.arange(0, 39)[::5], np.arange(0., 0.04, 0.001)[::5])
ax.set_ylim(0, 40)

symmetric = np.array([2.04, 2.22])
symmetric_pix = (symmetric - 1.95) / 0.005
ax.plot(symmetric_pix, np.zeros(len(symmetric)), "D", clip_on=False)

plt.ylabel("pressure $p$")
plt.xlabel("diameter ratio $D / d$")

size = 12
ax.text(0, 10, r"$(3, 2, 1)$", fontsize=size)
ax.text(20, 5, r"$(3, \bm{2}, 1)$ ls", fontsize=size)
ax.text(40, 20, r"$(4, 2, 2)$", fontsize=size)
ax.grid()

fig.savefig("../../Plots/Hysteresis/Phasediagram{}.pdf".format(directory), dpi=1000)
##############################################################

directory = "422to532Forward"

param = np.loadtxt("../../data/Hysteresis/{}/Areas.txt".format(directory), unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

plt.imshow(param, origin="lower", interpolation="nearest", cmap="terrain", aspect="auto", extent=[0, 140, 1, 79])
plt.xticks(np.arange(0, 140)[::5], np.arange(2.20, 2.48, 0.002)[::5])
plt.yticks(np.arange(0, 79)[::5], np.arange(0., 0.04, 0.0005)[::5])
ax.set_ylim(0, 79)

symmetric = np.array([2.29, 2.48])
symmetric_pix = (symmetric - 2.2) / 0.002
ax.plot(symmetric_pix, np.zeros(len(symmetric)), "D", clip_on=False)

plt.ylabel("pressure $p$")
plt.xlabel("diameter ratio $D / d$")

size = 12
#ax.text(0, 10, r"$(4, 2, 2)$", fontsize=size)
#ax.text(20, 5, r"$(4, \bm{2}, 2)$ ls", fontsize=size)
ax.text(90, 40, r"$(5, 3, 2)$", fontsize=size)
ax.grid()

fig.savefig("../../Plots/Hysteresis/Phasediagram{}.pdf".format(directory), dpi=1000)
##############################################################

directory = "431to532Forward"

param = np.loadtxt("../../data/Hysteresis/{}/Areas.txt".format(directory), unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

plt.imshow(param, origin="lower", interpolation="nearest", cmap="terrain", aspect="auto", extent=[0, 52, 1, 40])
plt.xticks(np.arange(0, 52)[::5], np.arange(2.22, 2.48, 0.005)[::5])
plt.yticks(np.arange(0, 39)[::5], np.arange(0., 0.04, 0.001)[::5])
ax.set_ylim(0, 40)

symmetric = np.array([2.29, 2.48])
symmetric_pix = (symmetric - 2.22) / 0.005
ax.plot(symmetric_pix, np.zeros(len(symmetric)), "D", clip_on=False)

plt.ylabel("pressure $p$")
plt.xlabel("diameter ratio $D / d$")

size = 12
ax.text(0, 10, r"$(4, 3, 1)$", fontsize=size)
ax.text(20, 5, r"$(4, \bm{3}, 1)$ ls", fontsize=size)
ax.text(35, 20, r"$(5, 3, 2)$", fontsize=size)
ax.grid()

fig.savefig("../../Plots/Hysteresis/Phasediagram{}.pdf".format(directory), dpi=1000)
##############################################################

directory = "440to541Forward"

param = np.loadtxt("../../data/Hysteresis/{}/Areas.txt".format(directory), unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

plt.imshow(param, origin="lower", interpolation="nearest", cmap="terrain", aspect="auto", extent=[0, 42, 1, 70])
plt.xticks(np.arange(0, 37)[::5], np.arange(2.35, 2.56, 0.005)[::5])
plt.yticks(np.arange(0, 70)[::5], np.arange(0., 0.07, 0.001)[::5])
ax.set_ylim(0, 70)

symmetric = np.array([2.41, 2.56])
symmetric_pix = (symmetric - 2.35) / 0.005
ax.plot(symmetric_pix, np.zeros(len(symmetric)), "D", clip_on=False)

plt.ylabel("pressure $p$")
plt.xlabel("diameter ratio $D / d$")

size = 12
ax.text(0, 10, r"$(4, 4, 0)$", fontsize=size)
ax.text(20, 5, r"$(4, \bm{4}, 0)$ ls", fontsize=size)
ax.text(35, 20, r"$(5, 4, 1)$", fontsize=size)
ax.grid()

fig.savefig("../../Plots/Hysteresis/Phasediagram{}.pdf".format(directory), dpi=1000)
##############################################################

directory = "532to633Forward"

param = np.loadtxt("../../data/Hysteresis/{}/Areas.txt".format(directory), unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

plt.imshow(param, origin="lower", interpolation="nearest", cmap="terrain", aspect="auto", extent=[0, 60, 1, 40])
plt.xticks(np.arange(0, 60)[::10], np.arange(2.45, 2.75, 0.005)[::10])
plt.yticks(np.arange(0, 39)[::5], np.arange(0.0, 0.04, 0.001)[::5])
ax.set_ylim(0, 40)

symmetric = np.array([2.48, 2.72])
symmetric_pix = (symmetric - 2.45) / 0.005
ax.plot(symmetric_pix, np.zeros(len(symmetric)), "D", clip_on=False)

size=12
ax.text(0, 10, r"$(5, 3, 2)$", fontsize=size)
ax.text(20, 5, r"$(5, \bm{3}, 2)$ ls", fontsize=size)
ax.text(40, 20, r"$(6, 3, 3)$ ls", fontsize=size)
ax.grid()

plt.ylabel("pressure $p$")
plt.xlabel("diameter ratio $D / d$")

fig.savefig("../../Plots/Hysteresis/Phasediagram{}.pdf".format(directory), dpi=1000)
