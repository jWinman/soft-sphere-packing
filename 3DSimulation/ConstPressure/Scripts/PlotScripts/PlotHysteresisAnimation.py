import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from matplotlib import gridspec
import subprocess
from scipy.misc import imread

import os
import sys
sys.path.append("../../../libraries")
import classSpheres

def stability_map(p, D, directory, ax):
    ForwardX, ForwardY = np.loadtxt("../../data/{}Forward/Transitions.txt".format(directory), unpack=True)
    BackwardX, BackwardY = np.loadtxt("../../data/{}Backward/Transitions.txt".format(directory), unpack=True)

    ax.plot(ForwardX, ForwardY, "b+", label="Forward trajectory", markersize=4.)
    ax.plot(BackwardX, BackwardY, "ro", label="Reversed trajectory", markersize=4., mfc="none", mec="r")

    ax.plot(D, p, "ko", markersize=4.)

    # triple points
    ax2 = ax.twinx()

    if ("321to422" in directory):
        ax2.plot(1.986, 0.0248,  "kD",  markersize=4.)
        ax2.plot(np.linspace(1.986, 2.225, 100), np.ones(100) * 0.0248, "k--", linewidth=0.6)
        ax2.plot(1.984, 0.0274,  "kD",  markersize=4.)
        ax2.plot(np.linspace(1.984, 2.225, 100), np.ones(100) * 0.0274, "k--", linewidth=0.6)
        ax2.plot(2.0699, 0.0147, "kD", markersize=4.)
        ax2.plot(np.linspace(2.0699, 2.225, 100), np.ones(100) * 0.0147, "k--", linewidth=0.6)
        ax2.set_ylim(0., 0.04)
        ax2.set_yticks([0.0248, 0.0274, 0.0147])
        ax2.set_yticklabels(["$p_2$", "$p_1$", "$p_3$"])
        ax2.set_xlim(1.95, 2.22)
    else:
        ax.plot(2.235, 0.0289,  "kD",  markersize=4.)
        ax.plot(np.linspace(2.235, 2.48, 100), np.ones(100) * 0.0289, "k--", linewidth=0.6)
        ax.plot(2.2645, 0.0230,  "kD",  markersize=4.)
        ax.plot(np.linspace(2.2645, 2.48, 100), np.ones(100) * 0.0230, "k--", linewidth=0.6)
        ax.plot(2.3208, 0.01618,  "kD",  markersize=4.)
        ax.plot(np.linspace(2.3208, 2.48, 100), np.ones(100) * 0.01618, "k--", linewidth=0.6)
        ax.set_xlim(2.2, 2.48)
        ax.set_ylim(0, 0.035)

        ax2.set_ylim(0., 0.035)
        ax2.set_yticks([0.0289, 0.0230, 0.01618])
        ax2.set_yticklabels(["$p_1$", "$p_2$", "$p_3$"])

    ax.set_ylabel("dimensionless pressure $p$", fontsize=16)
    ax.set_xlabel("Diameter ratio $D / d$", fontsize=16)
    ax.grid()


def plot3DStructure(N, p, D, directory, direction, ax):
    os.chdir("../..")
    initial_N = "{}/N{}/Finalp{:.6f}D{:.4f}".format(directory + direction, N, p, D) #

    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_z", D / 2.)

    mySystem.ReadInitial(initial_N)

    mySystem.plot3DTube("Plots/" + directory + "/", 2)
    img = imread("Plots/{}/3DStructure.png".format(directory))

    ax.imshow(img)
    ax.set_axis_off()
    os.chdir("Scripts/PlotScripts/")

N = 12
Ds = np.arange(2.00, 2.175, 0.002)
p = 0.007
directory = "Hysteresis/321to422"

subprocess.call("rm ../../Plots/{}/*-*.png".format(directory), shell=True)
subprocess.call("mkdir -p ../../Plots/{}".format(directory), shell=True)

for j, D in enumerate(Ds):
    print(j, D)

    fig = plt.figure(figsize=(6.6, 4.2))
    gs = gridspec.GridSpec(1, 3, hspace=0.0, left=0.0, right=1.7, width_ratios=[1.2, 1.3, 0.9])

    # Upper plot -- Phyllotaxis
    axLeft = plt.subplot(gs[0], aspect="equal")
#    axUpper.set_title("$D / d = {:.4f}$, $p = {:.6f}$".format(D, p))

    th, z = np.loadtxt("../../data/{}Forward/Phyllotaxisp{:.6f}D{:.4f}Nodes.gz".format(directory, p, D), unpack=True)
    thunit, zunit = np.loadtxt("../../data/{}Forward/Phyllotaxisp{:.6f}D{:.4f}Unit.gz".format(directory, p, D), unpack=True)
    links = np.loadtxt("../../data/{}Forward/Phyllotaxisp{:.6f}D{:.4f}Edges.gz".format(directory, p, D))

    axLeft.plot(th, z, "bo", markersize=5.)
    for i in range(1, len(links)):
        if (i % 2 == 1):
            th1 = links[i][0]
            z1 = links[i][1]
            th2 = links[i + 1][0]
            z2 = links[i + 1][1]
            axLeft.plot([th1, th2], [z1, z2], "b-")
#    axLeft.plot(thunit, zunit, "rD")
    axLeft.set_ylim(-0.5, 5)
    axLeft.set_xlim(-0.5, 5)
    axLeft.set_ylabel("$z / d$", fontsize=16)
    axLeft.set_xlabel(r"$D /d \cdot \theta$", fontsize=16)
    axLeft.set_xticks([])
    axLeft.set_yticks([])

    axRight = plt.subplot(gs[1], aspect="auto")
    stability_map(p, D, directory, axRight)

    axCentre = plt.subplot(gs[2])
    plot3DStructure(N, p, D, directory, "Forward", axCentre)

    fig.savefig("../../Plots/{}/Forward-{:d}.png".format(directory, j), dpi=1000)
    plt.close()

for j, D in enumerate(Ds[::-1]):
    print(j, D)

    fig = plt.figure(figsize=(6.6, 4.2))
    gs = gridspec.GridSpec(1, 3, hspace=0.0, left=0.0, right=1.7, width_ratios=[1.2, 1.3, 0.9])

    # Upper plot -- Phyllotaxis
    axLeft = plt.subplot(gs[0], aspect="equal")
#    axUpper.set_title("$D / d = {:.4f}$, $p = {:.6f}$".format(D, p))

    th, z = np.loadtxt("../../data/{}Backward/Phyllotaxisp{:.6f}D{:.4f}Nodes.gz".format(directory, p, D), unpack=True)
    thunit, zunit = np.loadtxt("../../data/{}Backward/Phyllotaxisp{:.6f}D{:.4f}Unit.gz".format(directory, p, D), unpack=True)
    links = np.loadtxt("../../data/{}Backward/Phyllotaxisp{:.6f}D{:.4f}Edges.gz".format(directory, p, D))

    axLeft.plot(th, z, "bo", markersize=5.)
    for i in range(1, len(links)):
        if (i % 2 == 1):
            th1 = links[i][0]
            z1 = links[i][1]
            th2 = links[i + 1][0]
            z2 = links[i + 1][1]
            axLeft.plot([th1, th2], [z1, z2], "b-")
#    axLeft.plot(thunit, zunit, "rD")
    axLeft.set_ylim(-0.5, 5)
    axLeft.set_xlim(-0.5, 5)
    axLeft.set_ylabel("$z / d$", fontsize=16)
    axLeft.set_xlabel(r"$D / d \cdot \theta$", fontsize=16)
    axLeft.set_xticks([])
    axLeft.set_yticks([])

    # Lower left plot -- stability map
    axRight = plt.subplot(gs[1], aspect="auto")
    stability_map(p, D, directory, axRight)

    axCentre = plt.subplot(gs[2])
    plot3DStructure(N, p, D, directory, "Backward", axCentre)

    fig.savefig("../../Plots/{}/Backward-{:d}.png".format(directory, j), dpi=1000)
    plt.close()

