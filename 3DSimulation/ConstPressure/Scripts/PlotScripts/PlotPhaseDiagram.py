import numpy as np
import matplotlib.pyplot as plt

directory = "PhaseDiagram"
fileName = "MeanArea"
barName = "\mu_{A" if "Mean" in fileName else "\sigma_{A"
barName += "_{Triangle}}" if ("Triangle" in fileName) else "}"

conv_x = lambda x: (x - 1.5) / (2.7 - 1.5) * 481
conv_y = lambda y: y / 0.02 * 98

param = np.loadtxt("../../data/{}/{}.txt".format(directory, fileName), unpack=True)
pmax = max(param.flatten())
pmin = min(param.flatten())

#### Full PhaseDiagram ##########################
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

plt.imshow(param.T, vmin=pmin, vmax=pmax, origin="lower", interpolation="nearest", cmap="terrain", aspect="auto")
#plt.colorbar(label=r"${}$".format(barName), shrink=0.33)
plt.yticks(np.arange(-2, 98)[::25], np.round(np.arange(0, 0.02, 0.0002)[::25], 3))
plt.ylabel("pressure $p$")
plt.xticks(np.arange(0, 481)[::50], np.round(np.arange(1.5, 2.7, 0.0025)[::50], 3))
ax.set_xlim(0., 481)
plt.xlabel("diameter ratio $D / d$")

#### Hard Sphere Symmetric Structures ##########################
hardSpheres_D = np.array([2.0, 2.04, 2.15, 2.22, 2.29, 2.41, 2.49, 2.57, 2.7])
hardSpheres_pixel = conv_x(hardSpheres_D)
ax.plot(hardSpheres_pixel, np.zeros(len(hardSpheres_pixel)), "rD", clip_on=False, markersize=4.)

#### Labels ##########################
size_u = 4
size_d = 6
ax.text(20, 20, r"zigzag", fontsize=size_d)
ax.text(140, 5, r"twisted" + "\n" + "zigzag", fontsize=size_d)
ax.text(50, 70, r"straight" + "\n" + "zigzag", fontsize=size_d)
ax.text(107, 60, r"$(2, 2, 0)$" + "\n" + "uniform", fontsize=size_d, color="white")
ax.text(conv_x(1.95), conv_y(0.007), r"$(3, 2, 1)$" + "\n" + "uniform", fontsize=size_u, color="k")
ax.text(conv_x(2.02), conv_y(0.015), r"$(3, 3, 0)$" + "\n" + "uniform", fontsize=size_u, color="white")
ax.text(conv_x(2.115), conv_y(0.015), r"$(4, 2, 2)$" + "\n" + "uniform", fontsize=size_u, color="white")
ax.text(conv_x(2.20), conv_y(0.015), r"$(4, 3, 1)$" + "\n" + "uniform", fontsize=size_u, color="white")
ax.text(conv_x(2.31), conv_y(0.015), r"$(4, 4, 0)$" + "\n" + "uniform", fontsize=size_u, color="white")
ax.text(conv_x(2.4), conv_y(0.015), r"$(5, 3, 2)$" + "\n" + "uniform", fontsize=size_u, color="white")
ax.text(conv_x(2.48), conv_y(0.015), r"$(5, 4, 1)$" + "\n" + "uniform", fontsize=size_u, color="white")
ax.text(conv_x(2.60), conv_y(0.015), r"$(5, 5, 0)$" + "\n" + "uniform", fontsize=size_u, color="white")

ax.text(conv_x(2.04), conv_y(0.001), r"$(3, \bm{2}, 1)$" + "\n" + "line slip", fontsize=size_d, color="green")
ax.text(conv_x(2.289), conv_y(0.001), r"$(4, \bm{3}, 1)$" + "\n" + "line slip", fontsize=size_d, color="k")
ax.text(conv_x(2.568), conv_y(0.001), r"$(5, \bm{4}, 1)$" + "\n" + "line slip", fontsize=size_d, color="k")

fig.savefig("../../Plots/{}/Phasediagram{}.pdf".format(directory, fileName), dpi=1000)
