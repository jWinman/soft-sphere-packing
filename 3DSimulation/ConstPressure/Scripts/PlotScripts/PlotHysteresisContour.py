import numpy as np
import matplotlib
import matplotlib.pyplot as plt

number = "321to422"
ForwardX, ForwardY = np.loadtxt("../../data/Hysteresis/{}Forward/Transitions.txt".format(number), unpack=True)
BackwardX, BackwardY = np.loadtxt("../../data/Hysteresis/{}Backward/Transitions.txt".format(number), unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(ForwardX, ForwardY, "b+", label="Forward trajectory", markersize=4.)
ax.plot(BackwardX, BackwardY, "ro", label="Reversed trajectory", markersize=4., mfc="none")

# triple points
ax2 = ax.twinx()

ax2.plot(1.986, 0.0248,  "kD",  markersize=4.)
ax2.plot(np.linspace(1.986, 2.225, 100), np.ones(100) * 0.0248, "k--", linewidth=0.6)
ax2.plot(1.984, 0.0274,  "kD",  markersize=4.)
ax2.plot(np.linspace(1.984, 2.225, 100), np.ones(100) * 0.0274, "k--", linewidth=0.6)
ax2.plot(2.0699, 0.0147, "kD", markersize=4.)
ax2.plot(np.linspace(2.0699, 2.225, 100), np.ones(100) * 0.0147, "k--", linewidth=0.6)
ax2.set_ylim(0., 0.04)
ax2.set_yticks([0.0248, 0.0274, 0.0147])
ax2.set_yticklabels(["$p_2$", "$p_1$", "$p_3$"])

ax.set_ylabel("pressure $p$")
ax.set_xlabel("diameter ratio $D/d$")
ax.set_ylim(0, 0.04)
ax.set_xlim(1.95, 2.225)

size = 14
ax.text(1.9505, 0.012, "$(3, 2, 1)$ \n uniform", fontsize=size)
ax.text(2.04, 0.0051, r"$(3, \bm{2}, 1)$" + "\n line slip", fontsize=size)
ax.text(2.12, 0.018, "$(4, 2, 2)$ \n uniform", fontsize=size)
#ax.legend(loc="best", fontsize=size)
ax.grid()

fig.savefig("../../Plots/Hysteresis/{}Contour.pdf".format(number))
###############################################################

number = "422to532"
ForwardX, ForwardY = np.loadtxt("../../data/Hysteresis/{}Forward/Transitions.txt".format(number), unpack=True)
BackwardX, BackwardY = np.loadtxt("../../data/Hysteresis/{}Backward/Transitions.txt".format(number), unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(ForwardX, ForwardY, "b+", label="Forward trajectory", markersize=4.)
ax.plot(BackwardX, BackwardY, "ro", label="Reversed trajectory", markersize=4., mfc="none")
ax.plot(2.235, 0.0289,  "kD",  markersize=4.)
ax.plot(np.linspace(2.235, 2.48, 100), np.ones(100) * 0.0289, "k--", linewidth=0.6)
ax.plot(2.2645, 0.0230,  "kD",  markersize=4.)
ax.plot(np.linspace(2.2645, 2.48, 100), np.ones(100) * 0.0230, "k--", linewidth=0.6)
ax.plot(2.3208, 0.01618,  "kD",  markersize=4.)
ax.plot(np.linspace(2.3208, 2.48, 100), np.ones(100) * 0.01618, "k--", linewidth=0.6)
ax.set_ylabel("pressure $p$")
ax.set_xlabel("diameter ratio $D/d$")

# triple points
ax2 = ax.twinx()

ax2.set_ylim(0., 0.035)
ax2.set_yticks([0.0289, 0.0230, 0.01618])
ax2.set_yticklabels(["$p_1$", "$p_2$", "$p_3$"])

size = 12
ax.text(2.176, 0.02, r"$(4, 2, 2)$" + "\n" + "uniform", fontsize=size, color="b")
ax.text(2.215, 0.013, r"$(4, 3, 1)$" + "\n" + "uniform", fontsize=size, color="r")
ax.text(2.232, 0.005, r"$(4, \bm{2}, 2)$" + "\n" + "line slip", fontsize=size, color="b")
ax.text(2.31, 0.005, r"$(4, \bm{3}, 1)$" + "\n" + "line slip", fontsize=size, color="r")
ax.text(2.40, 0.017, r"$(5, 3, 2)$" + "\n" + "uniform", fontsize=size)
ax.grid()
ax.legend(loc="upper right", fontsize=size)
ax.set_ylim(0., 0.035)
ax.set_xlim(2.17, 2.48)
fig.savefig("../../Plots/Hysteresis/{}Contour.pdf".format(number))
