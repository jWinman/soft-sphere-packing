import numpy as np
import matplotlib.pyplot as plt
import subprocess

Ds = np.arange(2.57, 2.7, 0.0025)
ps = np.arange(0.0008, 0.007, 0.0008)

directory = "PhaseDiagram"

for i, p in enumerate(ps):
    subprocess.call("mkdir -p ../../Plots/{}/Phyllotaxis5".format(directory, p), shell=True)

    for j, D in enumerate(Ds):
        th, z = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Nodes.gz".format(directory, p, D), unpack=True)
        thunit, zunit = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Unit.gz".format(directory, p, D), unpack=True)
        links = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Edges.gz".format(directory, p, D))

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1, aspect="equal")
        ax.set_title("$D / d = {:.4f}$, $p = {:.6f}$".format(D, p))

#        ax.plot(thunit, zunit, "rD")
        for i in range(1, len(links)):
            if (i % 2 == 1):
                th1 = links[i][0]
                z1 = links[i][1]
                th2 = links[i + 1][0]
                z2 = links[i + 1][1]
                ax.plot([th1, th2], [z1, z2], "k-")
        ax.plot(th[1::2], z[1::2], "ro")
        ax.plot(th[::2], z[::2], "bo")
        ax.set_ylim(-5, 5)
        ax.set_xlim(-5, 5)
        ax.set_ylabel("$z / d$")
        ax.set_xlabel(r"$D / d \cdot \theta$")

        fig.savefig("../../Plots/{}/Phyllotaxis5/phyllotaxisD{:.4f}p{:.6f}.png".format(directory, p, D, p))
        plt.close()

