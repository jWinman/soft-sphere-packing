import numpy as np
import matplotlib.pyplot as plt

directory = "PhaseDiagram"
Volume = np.loadtxt("../../data/{}/VolumeMin.txt".format(directory))

conv_x = lambda x: (x - 2.) / (2.7 - 1.5) * 481
conv_y = lambda y: y / 0.02 * 98

PackFrac = 4. / 3. * np.pi * 0.5**3 / Volume

fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)

plt.imshow(PackFrac[200:, :].T, origin="lower", interpolation="nearest", cmap="terrain", aspect="auto")
plt.colorbar(label=r"Packing fraction $\phi$", shrink=1.)

ax1.set_ylabel("pressure $p$")
plt.yticks(np.arange(-2, 98)[::25], np.arange(0, 0.02, 0.0002)[::25])
ax1.set_xlabel("diameter ratio")
plt.xticks(np.arange(0, 281)[::50], np.arange(2.0, 2.7, 0.0025)[::50])

hardSpheres_D = np.array([2.0, 2.04, 2.15, 2.22, 2.29, 2.41, 2.49, 2.57, 2.7])
hardSpheres_pixel = conv_x(hardSpheres_D)
ax1.plot(hardSpheres_pixel, np.zeros(len(hardSpheres_pixel)), "rD", clip_on=False, markersize=4.)

D_z = np.arange(1.5, 2.7, 0.0025)
packFraction = PackFrac[:, 0]
print(len(D_z), len(packFraction))
AdilD_z, AdilPhi = np.loadtxt("../../data/Adil/VF_graph_xmgrace_file_2", unpack=True)

ax1 = fig.add_subplot(2, 1, 2)

ax1.plot(D_z, packFraction, ".", label="soft disks $p = 0.0005$")
ax1.plot(AdilD_z, AdilPhi, "-", linewidth=2., label="hard spheres")
ax1.set_ylabel("Packing fraction $\phi$")
ax1.set_xlabel("Diameter ratio $D / d$")
ax1.set_xlim(1.5, 2.7)
ax1.grid()
ax1.legend(loc="best")

fig.savefig("../../Plots/{}/PackingFrac.pdf".format(directory))

