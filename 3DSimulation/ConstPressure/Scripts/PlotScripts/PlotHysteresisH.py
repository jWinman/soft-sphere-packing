import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

name = "321to422"
directoryForward = "Hysteresis/{}Forward".format(name)
EnthalpyForward = np.loadtxt("../../data/{}/Enthalpy.txt".format(directoryForward))
EnergyForward = np.loadtxt("../../data/{}/Energy.txt".format(directoryForward))
VolumeForward = np.loadtxt("../../data/{}/Volume.txt".format(directoryForward))

directoryBackward = "Hysteresis/321to422Backward"
EnthalpyBackward = np.loadtxt("../../data/{}/Enthalpy.txt".format(directoryBackward))
EnergyBackward = np.loadtxt("../../data/{}/Energy.txt".format(directoryBackward))
VolumeBackward = np.loadtxt("../../data/{}/Volume.txt".format(directoryBackward))

D0_321to422 = 1.95
delta_D = 0.002
p0 = 0.0005
delta_p = 0.0005
widthForward = 6.5
widthBackward = 5.
widthVline = 0.8

y = lambda p: int((p - p0) / delta_p)

Ds = np.arange(1.95, 2.22, 0.002)

###################################################################################
p3 = 0.007
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)

ax1.plot(Ds, EnthalpyForward[:,y(p3)], "k+", markersize=widthForward, label="Forward trajectory")
ax1.set_ylabel("Enthalpy $h$")
ax1.set_xlabel("diameter ratio $D / d$")
start, end = ax1.get_ylim()
ax1.yaxis.set_ticks(np.arange(round(start, 4), round(end, 4), 0.00025))

ax1.set_xlim(1.97, 2.17)

ax1.axvline(2.024, linestyle="--", linewidth=widthVline, color="k")
ax1.axvline(2.144, linestyle="--", linewidth=widthVline, color="k")

ax1.text(1.976, 0.0073, r"$(3, 2, 1)$" + "\n" + "uniform", color="k")
ax1.text(2.075, 0.0073, r"$(3, \bm{2}, 1)$" + "\n" + "line slip", color="k")
ax1.annotate(r"$(4, 2, 2)$" + "\n" + "uniform", xy=(2.154, 0.0074), xytext=(2.145, 0.0075), color="k", arrowprops=dict(arrowstyle="->"))

#ax1.legend(loc="best")

fig.savefig("../../Plots/Hysteresis/{}Enthalpy_p{:.3f}.pdf".format(name, p3))
###################################################################################

p2 = 0.02
fig = plt.figure()
ax2 = fig.add_subplot(1, 1, 1)

ax2.plot(Ds, EnthalpyForward[:,y(p2)] , "b+", markersize=widthForward, label="Forward trajectory")
ax2.plot(Ds, EnthalpyBackward[:,y(p2)], "ro", markersize=widthBackward, label="Backward trajectory", mfc="none")
ax2.set_ylabel("Enthalpy $h$")
ax2.set_xlabel("diameter ratio $D / d$")
start, end = ax2.get_ylim()
ax2.yaxis.set_ticks(np.arange(round(start, 4), round(end, 4), 0.0004))

ax2.set_xlim(1.97, 2.17)

ax2.axvline(1.990, linestyle="--", linewidth=widthVline, color="k")
#ax2.axvline(1.9906, linestyle="--", linewidth=widthVline, color="r")
ax2.axvline(2.03, linestyle="-", linewidth=widthVline, color="b")
ax2.axvline(2.022, linestyle="-", linewidth=2 * widthVline, color="r")

ax2.annotate(r"$(3, 2, 1)$" + "\n" + "uniform", xy=(1.975, 0.0198), xytext=(1.94, 0.02005), color="k", arrowprops=dict(arrowstyle="->", relpos=(0.8, 0.)))
ax2.annotate(r"$(3, \bm{2}, 1)$" + "\n" + "line slip", xy=(2.005, 0.01985), xytext=(1.99, 0.02005), color="k", arrowprops=dict(arrowstyle="->"))
ax2.text(2.12, 0.0189, r"$(4, 2, 2)$" + "\n" + "uniform", color="k")

#axins2 = inset_axes(ax2, width="40%", height="40%", loc=1)
#axins2.plot(Ds, EnthalpyForward[:,y(p2)] , "b+", markersize=widthForward, label="Forward trajectory")
#axins2.plot(Ds, EnthalpyBackward[:,y(p2)], "ro", markersize=widthBackward, label="Backward trajectory")
#axins2.set_xlim(2.015, 2.035) # apply the x-limits
#axins2.set_ylim(0.01965, 0.0199) # apply the y-limits
#axins2.axvline(2.029, linestyle="-", linewidth=widthVline, color="b")
#axins2.axvline(2.023, linestyle="-", linewidth=widthVline, color="r")
#axins2.xaxis.set_tick_params(labelsize=10)
#axins2.yaxis.set_tick_params(labelsize=10)

#plt.yticks(visible=False)
#plt.xticks(visible=False)

#mark_inset(ax2, axins2, loc1=2, loc2=4, fc="none", ec="0.5")

fig.savefig("../../Plots/Hysteresis/{}Enthalpy_p{:.3f}.pdf".format(name, p2))
###################################################################################

fig = plt.figure()
ax2 = fig.add_subplot(1, 1, 1)

ax2.plot(Ds[:-1], np.diff(EnthalpyForward[:, y(p2)]), "b+", markersize=widthForward, label="Forward trajectory")
ax2.plot(Ds[:-1], np.diff(EnthalpyBackward[:, y(p2)]), "ro", markersize=widthBackward, label="Backward trajectory")
ax2.axvline(1.990, linestyle="--", linewidth=widthVline, color="b")
ax2.axvline(1.9906, linestyle="--", linewidth=widthVline, color="r")
ax2.axvline(2.03, linestyle="-", linewidth=widthVline, color="b")
ax2.axvline(2.022, linestyle="-", linewidth=widthVline, color="r")


fig.savefig("../../Plots/Hysteresis/{}DerivEnthalpy_p{:.3f}.pdf".format(name, p2))
###################################################################################
p1 = 0.026
fig = plt.figure()
ax3 = fig.add_subplot(1, 1, 1)

ax3.plot(Ds, EnthalpyForward[:,y(p1)] , "b+", markersize=widthForward, label="Forward trajectory")
ax3.plot(Ds, EnthalpyBackward[:,y(p1)], "ro", markersize=widthBackward, label="Backward trajectory", mfc="none")
ax3.set_ylabel("Enthalpy $h$")
ax3.set_xlabel("diameter ratio $D / d$")
start, end = ax3.get_ylim()
ax3.yaxis.set_ticks(np.arange(round(start, 4), round(end, 4), 0.0005))

ax3.set_xlim(1.96, 2.17)
ax3.set_ylim(0.0233, 0.0253)

ax3.axvline(1.981, linestyle="-", linewidth=2 * widthVline, color="r")
ax3.axvline(1.989, linestyle="-", linewidth=widthVline, color="b")
ax3.axvline(1.995, linestyle="-", linewidth=widthVline, color="b")

ax3.annotate(r"$(3, 2, 1)$" + "\n" + "uniform", xy=(1.975, 0.0252), xytext=(1.940, 0.0254), color="k", arrowprops=dict(arrowstyle="->", relpos=(0.5, 0.)))
ax3.annotate(r"$(3, \bm{2}, 1)$" + "\n" + "line slip", xy=(1.990, 0.0252), xytext=(2.0, 0.0254), color="k", arrowprops=dict(arrowstyle="->"))
ax3.text(2.10, 0.0239, r"$(4, 2, 2)$" + "\n" + "uniform", color="k")

axins3 = inset_axes(ax3, width="40%", height="40%", loc=1)
axins3.plot(Ds, EnthalpyForward[:,y(p1)] , "b+", markersize=widthForward, label="Forward trajectory")
axins3.plot(Ds, EnthalpyBackward[:,y(p1)], "ro", markersize=widthBackward, label="Backward trajectory", mfc="none")
axins3.set_xlim(1.976, 2.00) # apply the x-limits
axins3.set_ylim(0.02475, 0.0252) # apply the y-limits
axins3.axvline(1.981, linestyle="-", linewidth=2 * widthVline, color="r")
axins3.axvline(1.989, linestyle="-", linewidth=widthVline, color="b")
axins3.axvline(1.995, linestyle="-", linewidth=widthVline, color="b")
axins3.xaxis.set_tick_params(labelsize=10)
axins3.yaxis.set_tick_params(labelsize=10)

fig.savefig("../../Plots/Hysteresis/{}Enthalpy_p{:.3f}.pdf".format(name,p1))
