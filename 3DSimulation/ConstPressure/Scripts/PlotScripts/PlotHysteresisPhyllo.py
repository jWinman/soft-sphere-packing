import numpy as np
import matplotlib.pyplot as plt
import subprocess

Ds = np.arange(1.95, 2.225, 0.002)
ps = np.ones(len(Ds)) * 0.007

directory = "Hysteresis/321to422Forward"
subprocess.call("mkdir -p ../../Plots/{}/".format(directory), shell=True)

for j, (p, D) in enumerate(zip(ps, Ds)):
    th, z = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Nodes.gz".format(directory, p, D), unpack=True)
    thunit, zunit = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Unit.gz".format(directory, p, D), unpack=True)
    links = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Edges.gz".format(directory, p, D))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, aspect="equal")
    ax.set_title("$D / d = {:.4f}$, $p = {:.6f}$".format(D, p))

    ax.plot(th, z, "b.")
    ax.plot(thunit, zunit, "rD")
    for i in range(1, len(links)):
        if (i % 2 == 1):
            th1 = links[i][0]
            z1 = links[i][1]
            th2 = links[i + 1][0]
            z2 = links[i + 1][1]
            ax.plot([th1, th2], [z1, z2], "b-")
    ax.set_ylim(-4, 10)
    ax.set_xlim(-4, 10)
    ax.set_ylabel("$z$")
    ax.set_xlabel(r"$R_z \theta$")

    fig.savefig("../../Plots/{}/phyllotaxisD{:.4f}p{:.6f}.png".format(directory, D, p))
    plt.close()

