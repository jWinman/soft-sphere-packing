import numpy as np
import matplotlib.pyplot as plt

directory = "PhaseDiagram"
Enthalpy = np.loadtxt("../../data/{}/EnthalpyMin.txt".format(directory))
Energy = np.loadtxt("../../data/{}/EnergyMin.txt".format(directory))
Volume = np.loadtxt("../../data/{}/VolumeMin.txt".format(directory))

x = lambda D: int((D - 1.5) / 0.0025)
y = lambda p: int(p / 0.0002) - 2

Ds = np.arange(2., 2.2, 0.0025)
p = 0.01

fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)

ax1.plot(Ds, Enthalpy[x(Ds[0]) - 1:x(Ds[-1]) + 1:, y(p)], ".")
ax1.set_ylabel("Enthalpy $h$")
ax1.set_xlabel("Diameter ratio $D / d$")
ax1.set_xlim(2., 2.17)

ax1.set_title("$p = {:.3f}$".format(p))

Colors = plt.get_cmap("terrain")(np.linspace(0, 1, 40))
alpha = 0.7

#ax1.axvspan(1.5, 1.85, color=Colors[0], alpha=alpha)
#ax1.axvspan(1.85, 1.97, color=Colors[3], alpha=alpha)
#ax1.axvspan(1.97, 2., color=Colors[7], alpha=alpha)
ax1.axvspan(2., 2.02, color=Colors[14], alpha=alpha)
ax1.axvspan(2.02, 2.045, color=Colors[16], alpha=alpha)
ax1.axvspan(2.045, 2.135, color=Colors[20], alpha=alpha)
ax1.axvspan(2.135, 2.2, color=Colors[26], alpha=alpha)

ax1.grid()

ax1.annotate("$(3, 2, 1)$ \n uniform", xy=(2.015, 0.01015), xytext=(2.015, 0.0103), arrowprops=dict(facecolor="k", shrink=1e-5, width=0.75, headwidth=4.), fontsize=12, ha="right")
ax1.annotate(r"$(3, \bm{2}, 1)$" + "\n" + "line slip", xy=(2.035, 0.01015), xytext=(2.035, 0.0103), arrowprops=dict(facecolor="k", shrink=1e-5, width=0.75, headwidth=4.), fontsize=12, ha="left")
ax1.text(2.07, 0.00995, "$(3, 3, 0)$ \n uniform", fontsize=12)
ax1.annotate("$(4, 2, 2)$ \n uniform", xy=(2.15, 0.01), xytext=(2.175, 0.01), arrowprops=dict(facecolor="k", shrink=1e-5, width=0.75, headwidth=4.), fontsize=12, va="center")

fig.savefig("../../Plots/{}/EnthalpyConstP.pdf".format(directory))
plt.close()
