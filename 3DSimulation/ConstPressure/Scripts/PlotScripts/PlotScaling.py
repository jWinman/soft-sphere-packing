import numpy as np
import matplotlib.pyplot as plt

N = 4
Ek1d1 = np.loadtxt("../data/ScalingTest/k1d1/E.txt", unpack=True)
Ek2d1 = np.loadtxt("../data/ScalingTest/k2d1/E.txt", unpack=True)
Ek1d2 = np.loadtxt("../data/ScalingTest/k1d2/E.txt", unpack=True)
Ek2d2 = np.loadtxt("../data/ScalingTest/k2d2/E.txt", unpack=True)
ps = np.arange(0.0004, 0.02, 0.0004)

fig = plt.figure()
ax = fig.add_subplot(2, 1, 1)

ax.plot(ps, Ek1d1, "b-", linewidth=2, label="$k = 1, d = 1$")
ax.plot(ps[::2], Ek2d1[::2], "mx", label="$k = 2, d = 1$", markersize=3.)
ax.plot(ps, Ek1d2, ".", color="darkred", label="$k = 1, d = 2$")
ax.plot(ps, Ek2d2, "g--", label="$k = 2, d = 2$")
ax.set_ylabel("Enthalpy H")
ax.set_xlabel("pressure $P$")
ax.legend(loc="best")
ax.set_xlim(0., 0.02)
ax.set_ylim(0., 0.035)

ax2 = fig.add_subplot(2, 1, 2)

ax2.plot(ps, Ek1d1, "b-", linewidth=2, label="$k = 1, d = 1$")
ax2.plot(ps[::2] / 2, Ek2d1[::2] / 2, "mx", markersize=3, label="$k = 2, d = 1$")
ax2.plot(ps * 2, Ek1d2 / 4., ".", color="darkred", label="$k = 1, d = 2$")
ax2.plot(ps, Ek2d2 / 8, "g--", label="$k = 2, d = 2$")
ax2.set_ylabel("Enthalpy $H / (kd^2)$")
ax2.legend(loc="best")
ax2.set_xlim(0., 0.02)
ax2.set_ylim(0., 0.02)

ax2.set_xlabel("pressure $P / (k / d)$")
fig.savefig("../Plots/ScalingTest/Energy.pdf")
