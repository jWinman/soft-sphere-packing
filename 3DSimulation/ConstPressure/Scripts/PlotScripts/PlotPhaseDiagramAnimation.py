import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
from matplotlib import gridspec
import subprocess
from scipy.misc import imread

import os
import sys
sys.path.append("../../../libraries")
import classSpheres

def phase_diagram(p, D, ax):
    directory = "PhaseDiagram"

    hardSpheres_D = np.array([1.86, 2.0, 2.04, 2.15, 2.22, 2.29, 2.405, 2.49, 2.57, 2.7])
    uniforms = np.array([1.795, 1.985, 2.0825, 2.1725, 2.27, 2.37, 2.455, 2.555, 2.561])

    # Load in all extraced lines
    linesX, linesY = [], []
    lineSlipsX, lineSlipsY = [], []
    for i in range(20):
        lineX, lineY = np.loadtxt("../../data/{}/Lines/lineD{}.txt".format(directory, i), unpack=True)
        linesX.append(lineX)
        linesY.append(lineY)

        # hand generated line slip data
        if (i < 5):
            lineSlipX, lineSlipY = np.loadtxt("../../data/{}/Lines/lineSlipD{}.txt".format(directory, i), unpack=True)
            lineSlipsX.append(lineSlipX - 0.003)
            lineSlipsY.append(lineSlipY)

    linesX[0] = np.append(np.append(1.55, linesX[0]), 1.86)
    linesY[0] = np.append(np.append(0.02, linesY[0]), 0.)
    linesX[1] = np.append(np.append(1.86, linesX[1]), 1.67)
    linesY[1] = np.append(np.append(0., linesY[1]), 0.02)
    linesX[2] = np.append(1.68, linesX[2])
    linesY[2] = np.append(0.02, linesY[2])

    # add data for p = 0.02
    uniform_linesX, uniform_linesY = [], []
    uniform_linesX.append([linesX[4][0], uniforms[0]])
    uniform_linesX.append([linesX[6][0], uniforms[1]])
    uniform_linesX.append([linesX[8][0], uniforms[2]])
    uniform_linesX.append([linesX[11][0], uniforms[3]])
    uniform_linesX.append([linesX[13][0], uniforms[4]])
    uniform_linesX.append([linesX[15][0], uniforms[5]])
    uniform_linesX.append([linesX[17][0], uniforms[6]])
    uniform_linesX.append([linesX[18][-1], uniforms[7]])
    uniform_linesX.append([linesX[19][0], uniforms[8]])

    uniform_linesY.append([linesY[4][0],  0.02])
    uniform_linesY.append([linesY[6][0],  0.02])
    uniform_linesY.append([linesY[8][0],  0.02])
    uniform_linesY.append([linesY[11][0], 0.02])
    uniform_linesY.append([linesY[13][0], 0.02])
    uniform_linesY.append([linesY[15][0], 0.02])
    uniform_linesY.append([linesY[17][0], 0.02])
    uniform_linesY.append([linesY[18][-1], 0.02])
    uniform_linesY.append([linesY[19][0], 0.02])

    # devide the line slips
    linesX_2, linesY_2 = [], []
    linesX_2.append(linesX[11][linesX[11] < lineSlipsX[0][-1]])
    linesY_2.append(linesY[11][linesX[11] < lineSlipsX[0][-1]])
    linesX[11], linesY[11] = linesX[11][linesX[11] > lineSlipsX[0][-1]], linesY[11][linesX[11] > lineSlipsX[0][-1]]

    linesX_2.append(linesX[13][linesX[13] < lineSlipsX[1][-2]])
    linesY_2.append(linesY[13][linesX[13] < lineSlipsX[1][-2]])
    linesX[13], linesY[13] = linesX[13][linesX[13] > lineSlipsX[1][-2]], linesY[13][linesX[13] > lineSlipsX[1][-2]]

    linesX_2.append(linesX[15][linesX[15] < lineSlipsX[2][-1]])
    linesY_2.append(linesY[15][linesX[15] < lineSlipsX[2][-1]])
    linesX[15], linesY[15] = linesX[15][linesX[15] > lineSlipsX[2][-1]], linesY[15][linesX[15] > lineSlipsX[2][-1]]

    linesX_2.append(linesX[17][linesY[17] > lineSlipsY[3][-1]])
    linesY_2.append(linesY[17][linesY[17] > lineSlipsY[3][-1]])
    linesX[17], linesY[17] = linesX[17][linesY[17] < lineSlipsY[3][-1]], linesY[17][linesY[17] < lineSlipsY[3][-1]]

    linesX_2.append(linesX[19][linesY[19] > lineSlipsY[4][-1]])
    linesY_2.append(linesY[19][linesY[19] > lineSlipsY[4][-1]])
    linesX[19], linesY[19] = linesX[19][linesY[19] < lineSlipsY[4][-1]], linesY[19][linesY[19] < lineSlipsY[4][-1]]

    # Create the uniform (m, l, k) areas
    AreaZigZag_X = linesX[0][::-1]
    AreaZigZag_Y = linesY[0][::-1]

    Area211_X = np.concatenate((linesX[1], linesX[0]))
    Area211_Y = np.concatenate((linesY[1], linesY[0]))
    AreaTwistedZigZag_X = np.concatenate((linesX[1][::-1], linesX[2][::-1]))
    AreaTwistedZigZag_Y = np.concatenate((linesY[1][::-1], linesY[2][::-1]))
    Area220_X = np.concatenate((linesX[2], linesX[3], uniform_linesX[0]))
    Area220_Y = np.concatenate((linesY[2], linesY[3], uniform_linesY[0]))
    Area321_X = np.concatenate((uniform_linesX[0][::-1], linesX[4], linesX[5], uniform_linesX[1]))
    Area321_Y = np.concatenate((uniform_linesY[0][::-1], linesY[4], linesY[5], uniform_linesY[1]))
    Area330_X = np.concatenate((uniform_linesX[1][::-1], linesX[6], linesX[7], uniform_linesX[2]))
    Area330_Y = np.concatenate((uniform_linesY[1][::-1], linesY[6], linesY[7], uniform_linesY[2]))
    Area422_X = np.concatenate((uniform_linesX[2][::-1], linesX[8][linesX[8] < linesX[9][0]], linesX[9], linesX[10], uniform_linesX[3]))
    Area422_Y = np.concatenate((uniform_linesY[2][::-1], linesY[8][linesX[8] < linesX[9][0]], linesY[9], linesY[10], uniform_linesY[3]))
    Area431_X = np.concatenate((uniform_linesX[3][::-1], linesX_2[0], linesX[11], linesX[12], uniform_linesX[4]))
    Area431_Y = np.concatenate((uniform_linesY[3][::-1], linesY_2[0], linesY[11], linesY[12], uniform_linesY[4]))
    Area440_X = np.concatenate((uniform_linesX[4][::-1], linesX_2[1], linesX[13], linesX[14], uniform_linesX[5]))
    Area440_Y = np.concatenate((uniform_linesY[4][::-1], linesY_2[1], linesY[13], linesY[14], uniform_linesY[5]))
    Area532_X = np.concatenate((uniform_linesX[5][::-1], linesX_2[2], linesX[15], linesX[16], uniform_linesX[6]))
    Area532_Y = np.concatenate((uniform_linesY[5][::-1], linesY_2[2], linesY[15], linesY[16], uniform_linesY[6]))
    Area541_X = np.concatenate((uniform_linesX[6][::-1], linesX_2[3], linesX[17], linesX[18], uniform_linesX[7]))
    Area541_Y = np.concatenate((uniform_linesY[6][::-1], linesY_2[3], linesY[17], linesY[18], uniform_linesY[7]))
    Area550_X = np.concatenate((uniform_linesX[8][::-1], linesX_2[4], linesX[19], np.ones(10) * hardSpheres_D[-1]))
    Area550_Y = np.concatenate((uniform_linesY[8][::-1], linesY_2[4], linesY[19], np.linspace(0, 0.02, 10)))

    # Create line slip areas
    Area220ls_X = np.concatenate((linesX[3], linesX[4]))
    Area220ls_Y = np.concatenate((linesY[3], linesY[4]))
    Area321ls_X = np.concatenate((linesX[5], linesX[6]))
    Area321ls_Y = np.concatenate((linesY[5], linesY[6]))
    Area330ls_X = np.concatenate((linesX[7], linesX[8]))
    Area330ls_Y = np.concatenate((linesY[7], linesY[8]))
    Area321ls_2X = np.concatenate((linesX[8][linesX[8] > linesX[9][0]][::-1], linesX[9]))
    Area321ls_2Y = np.concatenate((linesY[8][linesX[8] > linesX[9][0]][::-1], linesY[9]))
    Area422ls_X = np.concatenate((linesX[10], linesX_2[0], lineSlipsX[0][::-1]))
    Area422ls_Y = np.concatenate((linesY[10], linesY_2[0], lineSlipsY[0][::-1]))
    Area330ls_2X = np.concatenate((lineSlipsX[0], linesX[11]))
    Area330ls_2Y = np.concatenate((lineSlipsY[0], linesY[11]))
    Area431ls_X = np.concatenate((linesX[12], linesX_2[1], lineSlipsX[1][::-1]))
    Area431ls_Y = np.concatenate((linesY[12], linesY_2[1], lineSlipsY[1][::-1]))
    Area431ls_2X = np.concatenate((lineSlipsX[1], linesX[13]))
    Area431ls_2Y = np.concatenate((lineSlipsY[1], linesY[13]))
    Area440ls_X = np.concatenate((linesX[14], linesX_2[2], lineSlipsX[2][::-1]))
    Area440ls_Y = np.concatenate((linesY[14], linesY_2[2], lineSlipsY[2][::-1]))
    Area431ls_3X = np.concatenate((lineSlipsX[2], linesX[15]))
    Area431ls_3Y = np.concatenate((lineSlipsY[2], linesY[15]))
    Area532ls_X = np.concatenate((linesX[16], linesX_2[3], lineSlipsX[3][::-1]))
    Area532ls_Y = np.concatenate((linesY[16], linesY_2[3], lineSlipsY[3][::-1]))
    Area440ls_2X = np.concatenate((lineSlipsX[3], linesX[17]))
    Area440ls_2Y = np.concatenate((lineSlipsY[3], linesY[17]))
    Area541ls_X = np.concatenate((linesX[18], uniform_linesX[-2], uniform_linesX[-1][::-1], linesX_2[4], lineSlipsX[4][::-1]))
    Area541ls_Y = np.concatenate((linesY[18], uniform_linesY[-2], uniform_linesY[-1][::-1], linesY_2[4], lineSlipsY[4][::-1]))
    Area541ls_2X = np.concatenate((lineSlipsX[4], linesX[19]))
    Area541ls_2Y = np.concatenate((lineSlipsY[4], linesY[19]))

    alpha = 0.7
    Colors = plt.get_cmap("terrain")(np.linspace(0, 1, 100))

    # plot uniform areas
    ax.fill_betweenx(AreaZigZag_Y, AreaZigZag_X, color=Colors[0], alpha=alpha)
    ax.fill_betweenx(Area211_Y,    Area211_X,    color=Colors[3], alpha=alpha)
    ax.fill_betweenx(AreaTwistedZigZag_Y, AreaTwistedZigZag_X, color=Colors[5], alpha=alpha)
    ax.fill_betweenx(Area220_Y, Area220_X, color=Colors[10], alpha=alpha)
    ax.fill_betweenx(Area321_Y, Area321_X, color=Colors[11], alpha=alpha)
    ax.fill_betweenx(Area330_Y, Area330_X, color=Colors[12], alpha=alpha)
    ax.fill_betweenx(Area422_Y, Area422_X, color=Colors[13], alpha=alpha)
    ax.fill_betweenx(Area431_Y, Area431_X, color=Colors[14], alpha=alpha)
    ax.fill_betweenx(Area440_Y, Area440_X, color=Colors[15], alpha=alpha)
    ax.fill_betweenx(Area532_Y, Area532_X, color=Colors[16], alpha=alpha)
    ax.fill_betweenx(Area541_Y, Area541_X, color=Colors[17], alpha=alpha)
    ax.fill_betweenx(Area550_Y, Area550_X, color=Colors[18], alpha=alpha)

    # plot line slip areas
    ax.fill_betweenx(Area220ls_Y, Area220ls_X, color=Colors[27], alpha=alpha)
    ax.fill_betweenx(Area321ls_Y, Area321ls_X, color=Colors[34], alpha=alpha)
    ax.fill_betweenx(Area330ls_Y, Area330ls_X, color=Colors[95], alpha=alpha)
    ax.fill_betweenx(Area321ls_2Y, Area321ls_2X, color=Colors[34], alpha=alpha)
    ax.fill_betweenx(Area422ls_Y, Area422ls_X, color=Colors[43], alpha=alpha)
    ax.fill_betweenx(Area330ls_2Y, Area330ls_2X, color=Colors[95], alpha=alpha)
    ax.fill_betweenx(Area431ls_Y, Area431ls_X, color=Colors[83], alpha=alpha)
    ax.fill_betweenx(Area431ls_2Y, Area431ls_2X, color=Colors[50], alpha=alpha)
    ax.fill_betweenx(Area440ls_Y, Area440ls_X, color=Colors[59], alpha=alpha)
    ax.fill_betweenx(Area431ls_3Y, Area431ls_3X, color=Colors[83], alpha=alpha)
    ax.fill_betweenx(Area532ls_Y, Area532ls_X, color=Colors[74], alpha=alpha)
    ax.fill_betweenx(Area440ls_2Y, Area440ls_2X, color=Colors[59], alpha=alpha)
    ax.fill_betweenx(Area541ls_Y, Area541ls_X, color=Colors[65], alpha=alpha)
    ax.fill_betweenx(Area541ls_2Y, Area541ls_2X, color=Colors[70], alpha=alpha)

    for i, (lineX, lineY) in enumerate(zip(linesX, linesY)):
        if (i == 8 or i == 6):
            ax.plot(lineX, lineY, "k-", linewidth=0.7)
        else:
            ax.plot(lineX, lineY, "k--", linewidth=0.7)

    for lineX, lineY in zip(uniform_linesX, uniform_linesY):
        ax.plot(lineX, lineY, "k-", linewidth=0.7)

    for i, (lineSlipX, lineSlipY, lineX_2, lineY_2) in enumerate(zip(lineSlipsX, lineSlipsY, linesX_2, linesY_2)):
        ax.plot(np.concatenate((lineX_2, lineSlipX[::-1])), np.concatenate((lineY_2, lineSlipY[::-1])), "k-", linewidth=0.7)

    ax.plot(hardSpheres_D, np.zeros(len(hardSpheres_D)), "rD", clip_on=False, markersize=5.)

    ax.plot(D, p, "ko", clip_on=False)

    ax.set_ylabel("dimensionless pressure $p$", fontsize=14)
    ax.set_xlabel("diameter ratio $D / d$", labelpad=70, fontsize=14)
    ax.set_xlim(1.5, hardSpheres_D[-1])
    ax.set_ylim(0, 0.02)

    # plotting parameters
    size = 11
    shrink = 1.

    ax.annotate(r"zigzag", xy=(1.52, 0.019), xytext=(1.4, 0.0245), fontsize=size,
               arrowprops=dict(arrowstyle="->", relpos=(0.8, 0)))
    ax.annotate(r"(2, 1, 1)" + "\n" + "uniform", xy=(1.62, 0.019), xytext=(1.54, 0.021), fontsize=size,
               arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))
    ax.annotate(r"$(2, 2, 0)$" + "\n" + "uniform", xy=(1.75, 0.019), xytext=(1.66, 0.0235), fontsize=size,
               arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))
    ax.annotate(r"$(3, 2, 1)$" + "\n" + "uniform", xy=(1.9, 0.019), xytext=(1.82, 0.021), fontsize=size,
               arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))
    ax.annotate(r"$(3, 3, 0)$" + "\n" + "uniform", xy=(2.03,  0.019), xytext=(1.95, 0.0235), fontsize=size,
               arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))
    ax.annotate(r"$(4, 2, 2)$" + "\n" + "uniform", xy=(2.12,  0.019), xytext=(2.05, 0.021),  fontsize=size,
               arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))
    ax.annotate(r"$(4, 3, 1)$" + "\n" + "uniform", xy=(2.22, 0.019), xytext=(2.16, 0.0235),   fontsize=size,
               arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))
    ax.annotate(r"$(4, 4, 0)$" + "\n" + "uniform", xy=(2.33, 0.019), xytext=(2.25, 0.021), fontsize=size,
               arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))
    ax.annotate(r"$(5, 3, 2)$" + "\n" + "uniform", xy=(2.42, 0.019), xytext=(2.35, 0.0235), fontsize=size,
               arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))
    ax.annotate(r"$(5, 4, 1)$" + "\n" + "uniform", xy=(2.5, 0.019), xytext=(2.46, 0.021), fontsize=size,
               arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))
    ax.annotate(r"$(5, 5, 0)$" + "\n" + "uniform", xy=(2.64, 0.019), xytext=(2.60, 0.0235), fontsize=size,
               arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))

    ax.annotate(r"twisted" + "\n" + "zigzag", xy=(1.90, 0.001), xytext=(1.6, -0.004), fontsize=size,
                arrowprops=dict(arrowstyle="->", relpos=(0., 0)))
    ax.annotate(r"$(2, \bm{2}, 0)$" + "\n" + "line slip", xy=(2.01, 0.001), xytext=(1.77, -0.004), fontsize=size,
                arrowprops=dict(arrowstyle="->", relpos=(0.2, 0)))
    ax.annotate(r"$(3, \bm{2}, 1)$" + "\n" + "line slip", xy=(2.08,  0.001), xytext=(1.94, -0.004), fontsize=size,
                arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))
    ax.annotate(r"$(3, \bm{3}, 0)$" + "\n" + "line slip", xy=(2.17, 0.001), xytext=(2.12, -0.004), fontsize=size,
                arrowprops=dict(arrowstyle="->", relpos=(0.5, 0)))
    ax.annotate(r"$(3, \bm{2}, 1)$" + "\n" + "line slip", xy=(2.21, 0.0007), xytext=(1.94, -0.004), fontsize=size,
                arrowprops=dict(arrowstyle="->", alpha = 0.7, relpos=(0., 0.)))
    ax.annotate(r"$(3, \bm{3}, 0)$" + "\n" + "line slip", xy=(2.275, 0.0007), xytext=(2.12, -0.004), fontsize=size,
                arrowprops=dict(arrowstyle="->", alpha=0.7, relpos=(0.5, 0)))
    ax.annotate(r"$(4, \bm{2}, 2)$" + "\n" + "line slip", xy=(2.24, 0.001), xytext=(2.17, -0.007), fontsize=size,
                arrowprops=dict(arrowstyle="->", relpos=(0.9, 0.)))
    ax.annotate(r"$(4, \bm{3}, 1)$" + "\n" + "line slip", xy=(2.33, 0.001), xytext=(2.32, -0.004), fontsize=size,
                arrowprops=dict(arrowstyle="->", relpos=(0.5, 0.)))
    ax.annotate(r"$(\bm{4}, 3, 1)$" + "\n" + "line slip", xy=(2.38, 0.0008), xytext=(2.35, -0.007), fontsize=size,
                arrowprops=dict(arrowstyle="->", alpha=0.7, relpos=(0.4, 0.)))
    ax.annotate(r"$(4, \bm{4}, 0)$" + "\n" + "line slip", xy=(2.42, 0.0008), xytext=(2.53, -0.007), fontsize=size,
                arrowprops=dict(arrowstyle="->", relpos=(0.3, 0)))
    ax.annotate(r"$(4, \bm{3}, 1)$" + "\n" + "line slip", xy=(2.465, 0.001), xytext=(2.32, -0.004), fontsize=size,
                arrowprops=dict(arrowstyle="->", alpha= 0.7, relpos=(0.6, 0)))
    ax.annotate(r"$(5, \bm{3}, 2)$" + "\n" + "line slip", xy=(2.50, 0.001), xytext=(2.52, -0.004), fontsize=size,
                arrowprops=dict(arrowstyle="->", relpos=(0.8, 0)))
    ax.annotate(r"$(4, \bm{4}, 0)$" + "\n" + "line slip", xy=(2.54, 0.0005), xytext=(2.75, -0.007), fontsize=size,
                arrowprops=dict(arrowstyle="->", relpos=(0.23, 1.0)))
    ax.annotate(r"$(5, \bm{4}, 1)$" + "\n" + "line slip", xy=(2.61, 0.001), xytext=(2.75, -0.004), fontsize=size,
                arrowprops=dict(arrowstyle="->"))
    ax.annotate(r"$(\bm{5}, 4, 1)$" + "\n" + "line slip", xy=(2.65, 0.001), xytext=(2.75, -0.001), fontsize=size,
                arrowprops=dict(arrowstyle="->", relpos=(0., 0.5)))


def plot3DStructure(N, p, D, ax):
    os.chdir("../..")
    initial_N = "PhaseDiagram/N{}/Finalp{:.6f}D{:.4f}".format(N, p, D) #
    initial_NP = "PhaseDiagram/NP{}/Finalp{:.6f}D{:.4f}".format(N, p, D) #
    initial_N2P = "PhaseDiagram/NP{}/Finalp{:.6f}D{:.4f}".format(N, p, D) #

    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_z", D / 2.)

    mySystem.ReadInitial(initial_N)
    E_N = mySystem.Energy(mySystem.positions)
    mySystem.ReadInitial(initial_NP)
    E_NP = mySystem.Energy(mySystem.positions)
    mySystem.ReadInitial(initial_N2P)
    E_N2P = mySystem.Energy(mySystem.positions)
    E = np.argmin([E_N, E_NP, E_N2P])

    if (E == 0):
        mySystem.ReadInitial(initial_N)
    elif (E == 1):
        mySystem.ReadInitial(initial_NP)
    elif (E == 2):
        mySystem.ReadInitial(initial_N2P)

    mySystem.plot3DTube("./Plots/PhaseDiagram/Animation/", 12)
    img = imread("Plots/PhaseDiagram/Animation/3DStructure.png")

    ax.imshow(img)
    ax.set_axis_off()
    os.chdir("Scripts/PlotScripts/")

Ds = np.arange(2.7, 1.5, -0.005)
p = 0.001
directory = "PhaseDiagram"

subprocess.call("rm ../../Plots/{}/Animation/animation-*.png".format(directory), shell=True)
subprocess.call("mkdir -p ../../Plots/{}/Animation".format(directory), shell=True)

for j, D in enumerate(Ds):
    print(j, D)

    fig = plt.figure(figsize=(6.9, 3.8))
    gs = gridspec.GridSpec(1, 3, hspace=0.0, left=0., right=1.87, width_ratios=[1.4, 1.8, 0.9])

    # Upper plot -- Phyllotaxis
    axLeft = plt.subplot(gs[0], aspect="equal")
#    axLeft = plt.subplot2grid( (1,12), [0,0], aspect="equal", colspan=4)
#    axUpper.set_title("$D / d = {:.4f}$, $p = {:.6f}$".format(D, p))

    th, z = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Nodes.gz".format(directory, p, D), unpack=True)
    thunit, zunit = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Unit.gz".format(directory, p, D), unpack=True)
    links = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Edges.gz".format(directory, p, D))

    axLeft.plot(th, z, "bo", markersize=5.)
    for i in range(1, len(links)):
        if (i % 2 == 1):
            th1 = links[i][0]
            z1 = links[i][1]
            th2 = links[i + 1][0]
            z2 = links[i + 1][1]
            axLeft.plot([th1, th2], [z1, z2], "b-")
#    axLeft.plot(thunit, zunit, "rD")
    axLeft.set_ylim(-2, 3)
    axLeft.set_xlim(-1, 5)
    axLeft.set_ylabel("$z / d$", fontsize=16)
    axLeft.set_xlabel(r"$D / d \cdot \theta$", fontsize=16)
    axLeft.set_xticks([])
    axLeft.set_yticks([])

    N = len(thunit)

    axCentre = plt.subplot(gs[1], aspect=40.)
#    axCentre = plt.subplot2grid( (1,12), [0, 4], aspect=50., colspan=5)

    phase_diagram(p, D, axCentre)
    axCentre.set_xticklabels(axCentre.get_xticks(), fontsize=10)
    axCentre.set_yticklabels(axCentre.get_yticks(), fontsize=10)

    axRight = plt.subplot(gs[2])
    plot3DStructure(N, p, D, axRight)

    # Lower left plot -- PhaseDiagram


    fig.savefig("../../Plots/{}/Animation/animation-{:d}.png".format(directory, j), dpi=1000)
    plt.close()
