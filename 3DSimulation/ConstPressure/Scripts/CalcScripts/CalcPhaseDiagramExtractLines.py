import numpy as np
from skimage import measure
import pandas as pd

directory = "PhaseDiagram"
fileName = "MeanArea"

# fontsizes
size_u = 10
size_d = 10

conv_x = lambda x: (x - 1.5) / (2.7 - 1.5) * 481
conv_inv_x = lambda pixel: pixel / 481 * (2.7 - 1.5) + 1.5
conv_y = lambda y: y / 0.02 * 98
conv_inv_y = lambda pixel: pixel / 98 * 0.02

param = np.loadtxt("../../data/{}/{}.txt".format(directory, fileName), unpack=True)
pmax = max(param.flatten())
pmin = min(param.flatten())

hardSpheres_D = np.array([1.5, 1.86, 2.0, 2.04, 2.15, 2.22, 2.29, 2.405, 2.49, 2.57, 2.69])
hardSpheres_pixel = conv_x(hardSpheres_D).astype(int)

def sort_dots(metrics, contours, start):
    import scipy.spatial.distance as ssd
    dist_m = ssd.squareform(ssd.pdist(contours, metrics))

    total_points = contours.shape[0]
    points_index = set(range(total_points))
    sorted_index = []
    target    = start

    points_index.discard(target)
    while len(points_index)>0:
        candidate = list(points_index)
        nneigbour = candidate[dist_m[target, candidate].argmin()]
        points_index.discard(nneigbour)
        points_index.discard(target)
#        print(points_index, target, nneigbour)
        sorted_index.append(target)
        target    = nneigbour
    sorted_index.append(target)

    return contours[sorted_index, 1], contours[sorted_index, 0]


def ExtractLines(hardSpheres, threshold1, threshold2, half):
    paramDiff = abs(np.diff(param, axis=0, n=1))

    for i in range(len(paramDiff)):
        for j in range(len(paramDiff[0])):
            paramDiff[i, j] = 0 if paramDiff[i, j] < threshold1 else 1

#    plt.imshow(paramDiff.T, origin="lower", interpolation="nearest", cmap="terrain", aspect="auto")

    contours = measure.find_contours(paramDiff[hardSpheres[0]:hardSpheres[-1], :].T, threshold2)
    contours = np.unique(np.concatenate(contours), axis=0)

    if (half == 0):
        start = np.argmin(contours[:, 1])
    else:
        start = np.argmax(contours[:, 1])

    return sort_dots("euclidean", contours, start)

contoursX, contoursY = ExtractLines(hardSpheres_pixel[:-3], 0.13, 0.9, 0)

line0_x, line0_y = contoursX[:387], contoursY[:387]
line1_x, line1_y = contoursX[386:727], contoursY[386:727]
line2_x, line2_y = contoursX[727:1107], contoursY[727:1107]
line3_x, line3_y = contoursX[1109:1165], contoursY[1109:1165]
line4_x, line4_y = contoursX[1166:1244], contoursY[1166:1244]
line5_x, line5_y = contoursX[1245:1400], contoursY[1245:1400]
line6_x, line6_y = contoursX[1410:1633], contoursY[1410:1633]
line7_x, line7_y = contoursX[1645:1721], contoursY[1645:1721]
line8_x, line8_y = contoursX[1722:1828], contoursY[1722:1828]
line9_x, line9_y = contoursX[1830:1855], contoursY[1830:1855]
line10_x, line10_y = contoursX[1856:1913], contoursY[1856:1913]
line11_x, line11_y = contoursX[1914:2002], contoursY[1914:2002]
line12_x, line12_y = contoursX[2002:2203], contoursY[2002:2203]
line13_x, line13_y = contoursX[2223:2499], contoursY[2223:2499]

line9B_x, line9B_y = contoursX[2502:2560], contoursY[2502:2560]
line7B_x, line7B_y = contoursX[2561:2568], contoursY[2561:2568]
line3B_x, line3B_y = contoursX[2569:2579], contoursY[2569:2579]

line3_x, line3_y = np.concatenate((line3B_x, line3_x)), np.concatenate((line3B_y, line3_y))
line7_x, line7_y = np.concatenate((line7B_x[::-1], line7_x)), np.concatenate((line7B_y[::-1], line7_y))
line9_x, line9_y = np.concatenate((line9B_x[::-1], line9_x)), np.concatenate((line9B_y[::-1], line9_y))

line3_x  += 0.9
line4_x  += 1.2
line5_x  += 0.9
line6_x  += 1.40
line7_x  += 1.30
line8_x  += 1.40
line9_x  += 1.30
line10_x += 0.80
line11_x += 1.5
line12_x += 1.5
line13_x += 1.65
contoursX = np.concatenate((line0_x,
                            line1_x,
                            line2_x,
                            line3_x,
                            line4_x,
                            line5_x,
                            line6_x,
                            line7_x,
                            line8_x,
                            line9_x,
                            line10_x,
                            line11_x,
                            line12_x,
                            line13_x,
                           ))

contoursY = np.concatenate((line0_y,
                            line1_y,
                            line2_y,
                            line3_y,
                            line4_y,
                            line5_y,
                            line6_y,
                            line7_y,
                            line8_y,
                            line9_y,
                            line10_y,
                            line11_y,
                            line12_y,
                            line13_y,
                           ))

##################################################################

hardSpheres_D[-4] = 2.40
hardSpheres_pixel[-4] = conv_x(2.40)

contoursX, contoursY = ExtractLines(hardSpheres_pixel[-4:], 0.1, 0.9, 1)
contoursX += conv_x(2.40)

line19_x, line19_y = contoursX[:282], contoursY[:282]
line18_x, line18_y = contoursX[284:485], contoursY[284:485]
line17_x, line17_y = contoursX[486:652], contoursY[486:652]
line16_x, line16_y = contoursX[653:763], contoursY[653:763]
line15_x, line15_y = contoursX[764:780], contoursY[764:780]
line15B_x, line15B_y = contoursX[795:929], contoursY[795:929]
line14_x, line14_y = contoursX[930:1040], contoursY[930:1040]

line15_x, line15_y = np.concatenate((line15_x, line15B_x)), np.concatenate((line15_y, line15B_y))

contoursX = np.concatenate((line19_x,
                            line18_x,
                            line17_x,
                            line16_x,
                            line15_x,
                            line14_x,
                           ))

contoursY = np.concatenate((line19_y,
                            line18_y,
                            line17_y,
                            line16_y,
                            line15_y,
                            line14_y,
                           ))

for i in range(0, 20):
    if (i <= 13):
        lineD_x, lineD_y = eval("conv_inv_x(line{0}_x), conv_inv_y(line{0}_y)".format(i))
        np.savetxt("../../data/PhaseDiagram/Lines/line{}.txt".format(i), np.array(eval("line{0}_x, line{0}_y".format(i))).T)
        np.savetxt("../../data/PhaseDiagram/Lines/lineD{}.txt".format(i), np.array([lineD_x, lineD_y]).T)
    else:
        lineD_x, lineD_y = eval("conv_inv_x(line{0}_x[::-1]), conv_inv_y(line{0}_y[::-1])".format(i))
        np.savetxt("../../data/PhaseDiagram/Lines/line{}.txt".format(i), np.array(eval("line{0}_x[::-1], line{0}_y[::-1]".format(i))).T)
        np.savetxt("../../data/PhaseDiagram/Lines/lineD{}.txt".format(i), np.array([lineD_x, lineD_y]).T)
