import sys
sys.path.append("../../../libraries")

import classSpheres

import numpy as np
import os

def EnergyFct(mySystem, p):
    mySystem.setVariable("p", 0)
    E = mySystem.Energy(mySystem.positions) / mySystem.N
    mySystem.setVariable("p", p)
    return E

def EnthalpyFct(mySystem):
    return mySystem.Energy(mySystem.positions) / mySystem.N

def VolumeFct(mySystem):
    R_z = mySystem.R_z
    L = mySystem.positions[-1]
    return R_z**2 * np.pi * L / mySystem.N

def CutThroughPD(ps, Ds, Ns, const, directory):
    Energy = np.zeros((len(Ds), len(ps), len(Ns)))
    Enthalpy = np.zeros((len(Ds), len(ps), len(Ns)))
    Volume = np.zeros((len(Ds), len(ps), len(Ns)))

    os.chdir("../..")

    for i, D in enumerate(Ds):
        for l, p in enumerate(ps):
            for j, N in enumerate(Ns):
                if (N == 5 and D < 2.5):
                    Enthalpy1 = Enthalpy2 = Enthalpy3 = 1e6
                    Volume1 = Volume2 = Volume3 = 0
                    Energy1 = Energy2 = Energy3 = 0

                else:
                    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
                    initial = "{}/N{}/Finalp{:.6f}D{:.4f}".format(directory, N, p, D)
                    mySystem.ReadInitial(initial)
                    mySystem.setVariable("p", p)
                    mySystem.setVariable("N", N)
                    mySystem.setVariable("R_z", D / 2.)
                    Enthalpy1 = EnthalpyFct(mySystem)
                    Energy1 = EnergyFct(mySystem, p)
                    Volume1 = VolumeFct(mySystem)

                    initial = "{}/NP{}/Finalp{:.6f}D{:.4f}".format(directory, N, p, D)
                    mySystem.ReadInitial(initial)
                    Enthalpy2 = EnthalpyFct(mySystem)
                    Energy2 = EnergyFct(mySystem, p)
                    Volume2 = VolumeFct(mySystem)

                    initial = "{}/N2P{}/Finalp{:.6f}D{:.4f}".format(directory, N, p, D)
                    mySystem.ReadInitial(initial)
                    Enthalpy3 = EnthalpyFct(mySystem)
                    Energy3 = EnergyFct(mySystem, p)
                    Volume3 = VolumeFct(mySystem)

                EnthalpyArray = np.array([Enthalpy1, Enthalpy2, Enthalpy3])
                EnergyArray = np.array([Energy1, Energy2, Energy3])
                VolumeArray = np.array([Volume1, Volume2, Volume3])

                Enthalpy[i][l][j] = np.min(EnthalpyArray)
                Energy[i][l][j] = EnergyArray[np.argmin(EnthalpyArray)]
                Volume[i][l][j] = VolumeArray[np.argmin(EnthalpyArray)]

    os.chdir("Scripts/CalcScripts")

    EnthalpyMin = np.min(Enthalpy, axis=2)
    EnthalpyArgmin = np.argmin(Enthalpy, axis=2)
    EnergyMin = np.zeros(np.shape(EnthalpyMin))
    VolumeMin = np.zeros(np.shape(EnthalpyMin))

    for i in range(len(Ds)):
        for j in range(len(ps)):
            EnergyMin[i][j] = Energy[i][j][EnthalpyArgmin[i][j]]
            VolumeMin[i][j] = Volume[i][j][EnthalpyArgmin[i][j]]

    return EnergyMin, EnthalpyMin, VolumeMin

Ds = np.arange(1.5, 2.7, 0.0025)
ps = np.arange(0.0004, 0.02, 0.0002)
Ns = np.array([3])
directoryREAD = "PhaseDiagram"
directoryWRITE = "HysteresesN3"

EnergyMin, EnthalpyMin, VolumeMin = CutThroughPD(ps, Ds, Ns, "p", directoryREAD)

np.savetxt("../../data/{}/EnergyMin.txt".format(directoryWRITE), EnergyMin)
np.savetxt("../../data/{}/EnthalpyMin.txt".format(directoryWRITE), EnthalpyMin)
np.savetxt("../../data/{}/VolumeMin.txt".format(directoryWRITE), VolumeMin)
