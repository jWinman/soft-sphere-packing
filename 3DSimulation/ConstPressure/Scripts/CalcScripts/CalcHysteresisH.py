import sys
sys.path.append("../../../libraries")

import classSpheres

import numpy as np
import os

def EnergyFct(mySystem, p):
    mySystem.setVariable("p", 0)
    E = mySystem.Energy(mySystem.positions) / mySystem.N
    mySystem.setVariable("p", p)
    return E

def EnthalpyFct(mySystem):
    return mySystem.Energy(mySystem.positions) / mySystem.N

def VolumeFct(mySystem):
    R_z = mySystem.R_z
    L = mySystem.positions[-1]
    return R_z**2 * np.pi * L / mySystem.N

def CutThroughPD(ps, Ds, N, const, directory):
    Energy = np.zeros((len(Ds), len(ps)))
    Enthalpy = np.zeros((len(Ds), len(ps)))
    Volume = np.zeros((len(Ds), len(ps)))

    os.chdir("../..")

    for i, D in enumerate(Ds):
        for l, p in enumerate(ps):
            mySystem = classSpheres.SpheresCylinder("parameter.cfg")
            initial = "{}/N{}/Finalp{:.6f}D{:.4f}".format(directory, N, p, D)
            mySystem.ReadInitial(initial)
            mySystem.setVariable("p", p)
            mySystem.setVariable("N", N)
            mySystem.setVariable("R_z", D / 2.)
            Enthalpy[i][l] = EnthalpyFct(mySystem)
            Energy[i][l] = EnergyFct(mySystem, p)
            Volume[i][l] = VolumeFct(mySystem)

    os.chdir("Scripts/CalcScripts")
    return Energy, Enthalpy, Volume

Ds = np.arange(1.95, 2.22, 0.002)
ps = np.arange(0.0005, 0.04, 0.0005)
N = 12
directoryREAD = "Hysteresis/321to422Backward"
directoryWRITE = "Hysteresis/321to422Backward"

EnergyMin, EnthalpyMin, VolumeMin = CutThroughPD(ps, Ds, N, "p", directoryREAD)

np.savetxt("../../data/{}/Energy.txt".format(directoryWRITE), EnergyMin)
np.savetxt("../../data/{}/Enthalpy.txt".format(directoryWRITE), EnthalpyMin)
np.savetxt("../../data/{}/Volume.txt".format(directoryWRITE), VolumeMin)

