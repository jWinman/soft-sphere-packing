import numpy as np
from joblib import Parallel, delayed

def transition_parameter(p, D, directory):
    print("p: ", p, "D / d: ", D)
    xnods, ynods = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Unit.gz".format(directory, p, D), unpack=True)
    xedges, yedges, ledges  = np.genfromtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Edges.gz".format(directory, p, D), unpack=True)

    edgesPerNods = []
    for xnod, ynod in zip(xnods, ynods):
        edgesPerNod = []
        j = 0
        for i, (xedge, yedge) in enumerate(zip(xedges, yedges)):
            if (i % 2 == 1 and abs(xedge - xnod) < 1e-6 and abs(yedge - ynod) < 1e-6 and i < len(xedges) - 1):
                edgesPerNod.append([xedge, yedge])
                j += 1
                edgesPerNod.append([xedges[i + 1], yedges[i + 1]])
        edgesPerNods.append(edgesPerNod)

    angles = np.array([])
    for edgesPerNod in edgesPerNods:
        Es = np.zeros((len(edgesPerNod) // 2, 2)) # vector between linked nodes
        angleToX = np.zeros(len(edgesPerNod) // 2) # all angles around a node to x-axis
        j = 0
        for i in range(len(edgesPerNod)):
            if (i % 2 == 0):
                Es[j] = np.array(edgesPerNod[i + 1]) - np.array(edgesPerNod[i])
                angleToX[j] = np.arctan2(Es[j][1], Es[j][0])
                angleToX[j] = angleToX[j] + 2 * np.pi if (angleToX[j] < 0) else angleToX[j]
                j += 1

        angleToXsorted = np.sort(angleToX)

        # Triangle AREA parameter
        Essorted = Es[np.argsort(angleToX)]
        a1 = np.linalg.norm(Essorted[-2])
        b = np.linalg.norm(Essorted[-1])
        gamma1 = angleToXsorted[-1] - angleToXsorted[-2]
        a2 = np.linalg.norm(Essorted[0])
        gamma2 = 2 * np.pi - (angleToXsorted[-1] - angleToXsorted[0])
        gamma3 = angleToXsorted[1] - angleToXsorted[0]

        angle90 = 90 * 2 * np.pi / 360.
        angle50 = 63 * 2 * np.pi / 360.
        g90 = gamma1 > angle90 or gamma2 > angle90 or gamma3 > angle90
        s50 = np.sum(np.array([gamma1 < angle50, gamma2 < angle50, gamma3 < angle50]))
        s50 = True if (s50 > 1) else False
        if (g90 and not s50 and abs(angleToXsorted[0]) > 1e-3):
            # diamond case
            areaDiamond1 = a1 * b * np.sin(gamma1)
            areaDiamond2 = a2 * b * np.sin(gamma2)
            angles = np.append(angles, [areaDiamond1, areaDiamond2])
        else:
            # triangle case
            areaTriangle1 = 0.5 * a1 * b * np.sin(gamma1)
            areaTriangle2 = 0.5 * a2 * b * np.sin(gamma2)
            angles = np.append(angles, [areaTriangle1, areaTriangle2])

    return np.mean(angles)

def const_p(p, Ds, directoryRead):
    areas_p = np.zeros(len(Ds))

    for i, D in enumerate(Ds):
        print(p, D)
        areas_p[i] = transition_parameter(p, D, directoryRead)
    return areas_p

######## 220 to 321 #############
ps = np.arange(0.0001, 0.0055, 0.0001)
Ds = np.arange(1.945, 2.04, 0.0005)

directoryRead = "Hysteresis/220to321Backward"
areas = np.array(Parallel(n_jobs=4)(delayed(const_p)(p, Ds, directoryRead) for p in ps))
print(np.shape(areas))
np.savetxt("../../data/{}/Areas.txt".format(directoryRead), areas.T)

directoryRead = "Hysteresis/220to321Forward"
areas = np.array(Parallel(n_jobs=4)(delayed(const_p)(p, Ds, directoryRead) for p in ps))
np.savetxt("../../data/{}/Areas.txt".format(directoryRead), areas.T)

######## 321 to 422 #############
#ps = np.arange(0.0005, 0.04, 0.0005)
#Ds = np.arange(1.95, 2.225, 0.002)
#
#areas = np.zeros((len(Ds), len(ps)))
#
#for i, D in enumerate(Ds):
#    directoryRead = "Hysteresis/321to422Backward"
#
#    for j, p in enumerate(ps):
#        areas[i, j] = transition_parameter(p, D, directoryRead)
#        print("areas: 321-422Back", areas[i, j])
#
#np.savetxt("../../data/Hysteresis/321to422Backward/Areas.txt", areas)
#
#for i, D in enumerate(Ds):
#    directoryRead = "Hysteresis/321to422Forward"
#
#    for j, p in enumerate(ps):
#        areas[i, j] = transition_parameter(p, D, directoryRead)
#        print("areas 321-422For: ", areas[i, j])
#
#np.savetxt("../../data/Hysteresis/321to422Forward/Areas.txt", areas)

######## 422 Line Slip #############
#ps = np.arange(0.0001, 0.01, 0.0002)
#Ds = np.arange(2.2005, 2.30, 0.0005)
#number = "422LS"
#
#areas = np.zeros((len(Ds), len(ps)))
#directoryRead = "Hysteresis/{}Forward".format(number)
#
#for i, D in enumerate(Ds):
#    for j, p in enumerate(ps):
#        areas[i, j] = transition_parameter(p, D, directoryRead)
#        print("areas" + number + " For: ", areas[i, j])
#
#np.savetxt("../../data/{}/Areas.txt".format(directoryRead), areas)

######### 431 to 532 #############
#ps = np.arange(0.0005, 0.04, 0.0005)
#Ds = np.arange(2.22, 2.48, 0.002)
#
#areas = np.zeros((len(Ds), len(ps)))
#
#for i, D in enumerate(Ds):
#    directoryRead = "Hysteresis/431to532Backward"
#
#    for j, p in enumerate(ps):
#        areas[i, j] = transition_parameter(p, D, directoryRead)
#        print("areas 431-532Back: ", areas[i, j])
#
#np.savetxt("../../data/Hysteresis/431to532Backward/Areas.txt", areas)

#areas = np.zeros((len(Ds), len(ps)))
#
#for i, D in enumerate(Ds):
#    directoryRead = "Hysteresis/431to532Forward"
#
#    for j, p in enumerate(ps):
#        areas[i, j] = transition_parameter(p, D, directoryRead)
#        print("areas 431-532For: ", areas[i, j])
#
#np.savetxt("../../data/Hysteresis/431to532Forward/Areas.txt", areas)

######### 422 to 532 ################
#ps = np.arange(0.0005, 0.05, 0.0005)
#Ds = np.arange(2.20, 2.48, 0.002)
#
#areas = np.zeros((len(Ds), len(ps)))
#
#for i, D in enumerate(Ds):
#    directoryRead = "Hysteresis/422to532Backward"
#
#    for j, p in enumerate(ps):
#        areas[i, j] = transition_parameter(p, D, directoryRead)
#        print("areas 422-532Back: ", areas[i, j])
#
#np.savetxt("../../data/Hysteresis/422to532Backward/Areas.txt", areas)

#areas = np.zeros((len(Ds), len(ps)))
#
#for i, D in enumerate(Ds):
#    directoryRead = "Hysteresis/422to532Forward"
#
#    for j, p in enumerate(ps):
#        areas[i, j] = transition_parameter(p, D, directoryRead)
#        print("areas 422-532For: ", areas[i, j])
#
#np.savetxt("../../data/Hysteresis/422to532Forward/Areas.txt", areas)

########## 440 to 541 ################
#ps = np.arange(0.001, 0.07, 0.001)
#Ds = np.arange(2.35, 2.56, 0.005)
#
#areas = np.zeros((len(Ds), len(ps)))
#
#for i, D in enumerate(Ds):
#    directoryRead = "Hysteresis/440to541Backward"
#
#    for j, p in enumerate(ps):
#        areas[i, j] = transition_parameter(p, D, directoryRead)
#        print("areas 440-541Back: ", areas[i, j])
#
#np.savetxt("../../data/Hysteresis/440to541Backward/Areas.txt", areas)

#areas = np.zeros((len(Ds), len(ps)))
#
#for i, D in enumerate(Ds):
#    directoryRead = "Hysteresis/440to541Forward"
#
#    for j, p in enumerate(ps):
#        areas[i, j] = transition_parameter(p, D, directoryRead)
#        print("areas 440-541For: ", areas[i, j])
#
#np.savetxt("../../data/Hysteresis/440to541Forward/Areas.txt", areas)
