import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def ExtractLines(param, threshold1, threshold2):
    from skimage import measure
    paramDiff = abs(np.diff(param, axis=1, n=1))

    for i in range(len(paramDiff)):
        for j in range(len(paramDiff[0])):
            paramDiff[i, j] = 0 if paramDiff[i, j] < threshold1 else 1

    contours = measure.find_contours(paramDiff, threshold2)
    contours = np.unique(np.concatenate(contours), axis=0)

    return contours.T

conv_x = lambda pixel, l, r, tot: pixel / tot * (r - l) + l
conv_y = lambda y, u, tot: y / tot * u + 0.0005

###############################################################

number = "321to422"
Forward = np.loadtxt("../../data/Hysteresis/{}Forward/Areas.txt".format(number), unpack=True)
Backward = np.loadtxt("../../data/Hysteresis/{}Backward/Areas.txt".format(number), unpack=True)

contoursY, contoursX = ExtractLines(Forward, 0.04, 0.99999999999)
ForwardX, ForwardY = conv_x(contoursX, 1.95, 2.225, 138) , conv_y(contoursY, 0.04, 79)

contoursY, contoursX = ExtractLines(Backward, 0.04, 0.999999999)
BackwardX, BackwardY = conv_x(contoursX, 1.95, 2.225, 138) , conv_y(contoursY, 0.04, 79)

ForwardX, ForwardY = ForwardX[np.any([ForwardY > 0.005, ForwardX > 2.03], axis=0)], ForwardY[np.any([ForwardY > 0.005, ForwardX > 2.03], axis=0)]
BackwardX, BackwardY = BackwardX[np.any([BackwardY > 0.005, BackwardX > 2.03], axis=0)], BackwardY[np.any([BackwardY > 0.005, BackwardX > 2.03], axis=0)]

np.savetxt("../../data/Hysteresis/{}Forward/Transitions.txt".format(number), np.array([ForwardX, ForwardY]).T)
np.savetxt("../../data/Hysteresis/{}Backward/Transitions.txt".format(number), np.array([BackwardX, BackwardY]).T)

###############################################################
number = "422to532"
Forward = np.loadtxt("../../data/Hysteresis/{}Forward/Areas.txt".format(number), unpack=True)
Backward = np.loadtxt("../../data/Hysteresis/{}Backward/Areas.txt".format(number), unpack=True)

contoursY, contoursX = ExtractLines(Forward, 0.01, 0.99999999)
ForwardX, ForwardY = conv_x(contoursX, 2.2, 2.48, 140) , conv_y(contoursY, 0.04, 79)

ForwardXtmp = ForwardX[np.any([ForwardX < 2.45, ForwardY < 0.02], axis=0)]
ForwardY = ForwardY[np.any([ForwardX < 2.45, ForwardY < 0.02], axis=0)]
ForwardX = ForwardXtmp

ForwardXtmp = ForwardX[np.any([ForwardX < 2.47], axis=0)]
ForwardY = ForwardY[np.any([ForwardX < 2.47], axis=0)]
ForwardX = ForwardXtmp

ForwardXtmp = ForwardX[np.any([ForwardX > 2.21], axis=0)]
ForwardY = ForwardY[np.any([ForwardX > 2.21], axis=0)]
ForwardX = ForwardXtmp

contoursY, contoursX = ExtractLines(Backward, 0.02, 0.999999)
BackwardX, BackwardY = conv_x(contoursX, 2.2, 2.48, 140) , conv_y(contoursY, 0.04, 79)

BackwardXtmp = BackwardX[np.any([BackwardX > 2.285, BackwardY > 0.0051], axis=0)]
BackwardY = BackwardY[np.any([BackwardX > 2.285, BackwardY > 0.0051], axis=0)]
BackwardX = BackwardXtmp

BackwardXtmp = BackwardX[np.any([BackwardX < 2.45, BackwardY < 0.02], axis=0)]
BackwardY = BackwardY[np.any([BackwardX < 2.45, BackwardY < 0.02], axis=0)]
BackwardX = BackwardXtmp

BackwardXtmp = BackwardX[np.any([BackwardX > 2.38, BackwardX < 2.34, BackwardY > 0.005], axis=0)]
BackwardY = BackwardY[np.any([BackwardX > 2.38, BackwardX < 2.34, BackwardY > 0.005], axis=0)]
BackwardX = BackwardXtmp

np.savetxt("../../data/Hysteresis/{}Forward/Transitions.txt".format(number), np.array([ForwardX, ForwardY]).T)
np.savetxt("../../data/Hysteresis/{}Backward/Transitions.txt".format(number), np.array([BackwardX, BackwardY]).T)
