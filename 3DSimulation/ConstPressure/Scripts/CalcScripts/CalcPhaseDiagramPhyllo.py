import sys
sys.path.append("../../../libraries")

import numpy as np
import classSpheres
import os
import subprocess

def ToCartesianCo(R, phi, z):
    x = R * np.cos(phi)
    y = R * np.sin(phi)
    return x, y, z

@np.vectorize
def phyllotaxis(p, D, NTraj, directoryRead, directorySave):
    subprocess.call("mkdir -p ../../data/{}".format(directorySave), shell=True)
    NTBimages = 5
    NSIMAGE = 5
    sdist_max = 1.005
    Ns = [3, 4, 5]

    if ("PhaseDiagram" in directoryRead):
        os.chdir("../..")

        E = np.zeros(len(Ns))
        directions = []
        directions3 = ["", "P", "2P"]
        for i, N in enumerate(Ns):
            if (N == 5 and D < 2.5):
                E1 = 1e6
                E2 = 2e6
                E3 = 3e6
            else:
                mySystem = classSpheres.SpheresCylinder("parameter.cfg")
                mySystem.setVariable("p", p)
                mySystem.setVariable("N", N)
                mySystem.setVariable("R_z", D / 2.)

                structure = "{}/N{}/Finalp{:.6f}D{:.4f}".format(directoryRead, N, p, D) #
                structure2 = "{}/NP{}/Finalp{:.6f}D{:.4f}".format(directoryRead, N, p, D) #
                structure3 = "{}/N2P{}/Finalp{:.6f}D{:.4f}".format(directoryRead, N, p, D) #

                mySystem.ReadInitial(structure)
                E1 = mySystem.Energy(mySystem.positions) / N
                mySystem.ReadInitial(structure2)
                E2 = mySystem.Energy(mySystem.positions) / N
                mySystem.ReadInitial(structure3)
                E3 = mySystem.Energy(mySystem.positions) / N

            E[i] = np.min([E1, E2, E3])
            direction = directions3[np.argmin([E1, E2, E3])]
            directions.append(direction)

        os.chdir("Scripts/CalcScripts")

        N = Ns[np.argmin(E)]
        direction = directions[np.argmin(E)]
    else:
        N = NTraj
        direction = ""

    # Read data
    R, th, z = np.loadtxt("../../data/{}/N{}{}/Finalp{:.6f}D{:.4f}Pos".format(directoryRead, direction, N, p, D), unpack=True)
    alpha, L = np.loadtxt("../../data/{}/N{}{}/Finalp{:.6f}D{:.4f}FurtherArgs".format(directoryRead, direction, N, p, D))
    alpha = -alpha
    alpha = -(2 * np.pi - alpha) if (alpha > np.pi) else alpha

    # sort data by z
    Rsorted = R[np.argsort(z)]
    thsorted = th[np.argsort(z)]
    zsorted = np.sort(z)

    # set origin at (thsorted[0], zsorted[0])
    thsorted = (thsorted - thsorted[0]) % (2 * np.pi)
    zsorted -= zsorted[0]

    Rnew = np.array([])
    thnew = np.array([])
    znew = np.array([])

    # calculate Nodes
    for i in range(-NTBimages, NTBimages, 1):
        for j in range(-NSIMAGE, NSIMAGE, 1):
            thcell = (thsorted + i * alpha) % (2 * np.pi) + j * 2 * np.pi
            zcell = zsorted + i * L
            Rnew = np.append(Rnew, Rsorted)
            thnew = np.append(thnew, thcell)
            znew = np.append(znew, zcell)

    Unitcell = np.array([thsorted * Rsorted, zsorted]).T
    Nodes = np.array([thnew * Rnew, znew]).T

    np.savetxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Unit.gz".format(directorySave, p, D), Unitcell)
    np.savetxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Nodes.gz".format(directorySave, p, D), Nodes)

    x, y, z = ToCartesianCo(Rnew, thnew, znew)
    links = np.array([[0, 0, 0]])

    counter = 0
    for i in range(len(Rnew)):
        for j in range(len(Rnew)):
            if (i != j):
                ang_dist = np.abs(thnew[i] - thnew[j])
                if (ang_dist <= np.pi or (ang_dist - np.pi) <= 0.001):
                    sdx = x[i] - x[j]
                    sdy = y[i] - y[j]
                    sdz = z[i] - z[j]

                    sdist = np.sqrt(sdx**2 + sdy**2 + sdz**2)

                    if (sdist < sdist_max):
                        counter += 1
                        links = np.append(links, [[Rnew[i] * thnew[i], znew[i], sdist]], axis=0)
                        links = np.append(links, [[Rnew[j] * thnew[j], znew[j], sdist]], axis=0)

    np.savetxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Edges.gz".format(directorySave, p, D), links)

# PhaseDiagram
Np = 1
ND_z = 481

D_zs = np.arange(1.5, 2.7, 0.0025)
ps = np.array([np.array([0.001]) for _ in range(ND_z)]).flatten()
#ps = np.array([np.arange(0.0004, 0.02, 0.0002) for _ in range(ND_z)]).flatten()
D_zs = np.array([D_zs for _ in range(Np)]).T.flatten()

phyllotaxis(ps, D_zs, 4, "PhaseDiagram", "PhaseDiagram")
