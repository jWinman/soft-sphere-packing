import sys
sys.path.append("../../../libraries")

import numpy as np
import classSpheres
import os
import subprocess
from joblib import Parallel, delayed


def ToCartesianCo(R, phi, z):
    x = R * np.cos(phi)
    y = R * np.sin(phi)
    return x, y, z

def phyllotaxis(p, D, directoryRead, directorySave):
    subprocess.call("mkdir -p ../../data/{}".format(directorySave), shell=True)
    NTBimages = 2
    NSIMAGE = 2
    sdist_max = 1.004

    # Read data
    R, th, z = np.loadtxt("../../data/{}/Finalp{:.6f}D{:.4f}Pos".format(directoryRead, p, D), unpack=True)
    alpha, L = np.loadtxt("../../data/{}/Finalp{:.6f}D{:.4f}FurtherArgs".format(directoryRead, p, D))
    alpha = -alpha
    alpha = -(2 * np.pi - alpha) if (alpha > np.pi) else alpha

    # sort data by z
    Rsorted = R[np.argsort(z)]
    thsorted = th[np.argsort(z)]
    zsorted = np.sort(z)

    # set origin at (thsorted[0], zsorted[0])
#    thsorted = (thsorted - thsorted[0]) % (2 * np.pi)
#    zsorted -= zsorted[0]

    Rnew = np.array([])
    thnew = np.array([])
    znew = np.array([])

    # calculate Nodes
    for i in range(-NTBimages, NTBimages + 1, 1):
        for j in range(-NSIMAGE, NSIMAGE + 1, 1):
            thcell = (thsorted + i * alpha) % (2 * np.pi) + j * 2 * np.pi
            zcell = zsorted + i * L
            Rnew = np.append(Rnew, Rsorted)
            thnew = np.append(thnew, thcell)
            znew = np.append(znew, zcell)

    Unitcell = np.array([thsorted * Rsorted, zsorted]).T
    Nodes = np.array([thnew * Rnew, znew]).T

    np.savetxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Unit.gz".format(directorySave, p, D), Unitcell)
    np.savetxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Nodes.gz".format(directorySave, p, D), Nodes)

    x, y, z = ToCartesianCo(Rnew, thnew, znew)
    links = np.array([[0, 0, 0]])

    counter = 0
    for i in range(len(Rnew)):
        for j in range(len(Rnew)):
            if (i != j):
                ang_dist = np.abs(thnew[i] - thnew[j])
                if (ang_dist <= np.pi or (ang_dist - np.pi) <= 0.001):
                    sdx = x[i] - x[j]
                    sdy = y[i] - y[j]
                    sdz = z[i] - z[j]

                    sdist = np.sqrt(sdx**2 + sdy**2 + sdz**2)

                    if (sdist < sdist_max):
                        counter += 1
                        links = np.append(links, [[Rnew[i] * thnew[i], znew[i], sdist]], axis=0)
                        links = np.append(links, [[Rnew[j] * thnew[j], znew[j], sdist]], axis=0)

    np.savetxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Edges.gz".format(directorySave, p, D), links)

def const_p(p, Ds, number, N):
    directoryRead = "Hysteresis/{}/N{}".format(number, N)
    directorySave = "Hysteresis/{}".format(number)

    for D in Ds:
        print(p, D)
        phyllotaxis(p, D, directoryRead, directorySave)


####### 220 to 321 #################
ps = np.arange(0.0001, 0.0055, 0.0001)
Ds = np.arange(1.945, 2.04, 0.0005)
N = 12
number = "220to321"

numberForward = number + "Forward"
Parallel(n_jobs=4)(delayed(const_p)(p, Ds, numberForward, N) for p in ps)
numberBackward = number + "Backward"
Parallel(n_jobs=4)(delayed(const_p)(p, Ds, numberBackward, N) for p in ps)


########### Trajectories ##################
#ps = np.array([5e-6])
#Ds = np.arange(2.27, 3.1, 0.0004)
#N = 12
#number = "Traj422LS"
#
#numberForward = number + "Forward"
#Parallel(n_jobs=4)(delayed(const_p)(p, Ds, numberForward, N) for p in ps)
#numberBackward = number + "Backward"
#Parallel(n_jobs=4)(delayed(const_p)(p, Ds, numberBackward, N) for p in ps)


####### 321 to 422 #################
#ps = np.arange(0.0005, 0.04, 0.0005)
#Ds = np.arange(1.95, 2.225, 0.002)
#N = 12
#number = "321to422"
#
#numberForward = number + "Forward"
#Parallel(n_jobs=4)(delayed(const_p)(p, Ds, numberForward, N) for p in ps)
#numberBackward = number + "Backward"
#Parallel(n_jobs=4)(delayed(const_p)(p, Ds, numberBackward, N) for p in ps)


######## 422 Line Slip #################
#ps = np.arange(0.0001, 0.01, 0.0002)
#Ds = np.arange(2.2005, 2.30, 0.0005)
#N = 12
#number = "422LS"

#numberForward = number + "Forward"
#Parallel(n_jobs=4)(delayed(const_p)(p, Ds, numberForward, N) for p in ps)
#numberBackward = number + "Backward"
#Parallel(n_jobs=4)(delayed(const_p)(p, Ds, numberBackward, N) for p in ps)

######### 422 to 532 #################
#ps = np.arange(0.0005, 0.05, 0.0005)
#Ds = np.arange(2.20, 2.48, 0.002)
#N = 12
#number = "422to532"
#
#numberForward = number + "Forward"
#Parallel(n_jobs=4)(delayed(const_p)(p, Ds, numberForward, N) for p in ps)
#numberBackward = number + "Backward"
#Parallel(n_jobs=4)(delayed(const_p)(p, Ds, numberBackward, N) for p in ps)
