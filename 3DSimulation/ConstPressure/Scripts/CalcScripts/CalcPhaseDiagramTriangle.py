import numpy as np

def transition_parameter(p, D, directory):
    print("p: ", p, "D / d: ", D)
    xnods, ynods = np.loadtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Nodes.gz".format(directory, p, D), unpack=True)
    xedges, yedges, ledges  = np.genfromtxt("../../data/{}/Phyllotaxisp{:.6f}D{:.4f}Edges.gz".format(directory, p, D), unpack=True)

    xnodsTmp = xnods[np.all([abs(xnods) < 2, abs(ynods) < 0.5], axis=0)]
    ynods = ynods[np.all([abs(xnods) < 2, abs(ynods) < 0.5], axis=0)]
    xnods = xnodsTmp

    edgesPerNods = []
    for xnod, ynod in zip(xnods, ynods):
        edgesPerNod = []
        j = 0
        for i, (xedge, yedge) in enumerate(zip(xedges, yedges)):
            if (i % 2 == 1 and abs(xedge - xnod) < 1e-6 and abs(yedge - ynod) < 1e-6 and i < len(xedges) - 1):
                edgesPerNod.append([xedge, yedge])
                j += 1
                edgesPerNod.append([xedges[i + 1], yedges[i + 1]])
        edgesPerNods.append(edgesPerNod)

    angles = np.array([])
    for edgesPerNod in edgesPerNods:
        Es = np.zeros((len(edgesPerNod) // 2, 2)) # vector between linked nodes
        angleToX = np.zeros(len(edgesPerNod) // 2) # all angles around a node to x-axis
        j = 0
        for i in range(len(edgesPerNod)):
            if (i % 2 == 0):
                Es[j] = np.array(edgesPerNod[i + 1]) - np.array(edgesPerNod[i])
                angleToX[j] = np.arctan2(Es[j][1], Es[j][0])
                angleToX[j] = angleToX[j] + 2 * np.pi if (angleToX[j] < 0) else angleToX[j]
                j += 1

        angleToXsorted = np.sort(angleToX)

# Triangle AREA parameter
        Essorted = Es[np.argsort(angleToX)]
        a1 = np.linalg.norm(Essorted[-2])
        b = np.linalg.norm(Essorted[-1])
        gamma1 = angleToXsorted[-1] - angleToXsorted[-2]
        a2 = np.linalg.norm(Essorted[0])
        gamma2 = 2 * np.pi - (angleToXsorted[-1] - angleToXsorted[0])
        gamma3 = angleToXsorted[1] - angleToXsorted[0]

        factor = 0.5 if ("Triangle" in fileName) else 1.

        angle90 = 90 * 2 * np.pi / 360.
        angle50 = 63 * 2 * np.pi / 360.
        g90 = gamma1 > angle90 or gamma2 > angle90 or gamma3 > angle90
        s50 = np.sum(np.array([gamma1 < angle50, gamma2 < angle50, gamma3 < angle50]))
        s50 = True if (s50 > 1) else False
        if (g90 and not s50 and abs(angleToXsorted[0]) > 1e-3):
            # diamond case
            areaDiamond1 = factor * a1 * b * np.sin(gamma1)
            areaDiamond2 = factor * a2 * b * np.sin(gamma2)
            angles = np.append(angles, [areaDiamond1, areaDiamond2])
        else:
            # triangle case
            areaTriangle1 = 0.5 * a1 * b * np.sin(gamma1)
            areaTriangle2 = 0.5 * a2 * b * np.sin(gamma2)
            angles = np.append(angles, [areaTriangle1, areaTriangle2])

    if ("Mean" in fileName):
        return np.mean(angles)
    elif ("STD" in fileName):
        return np.std(angles)
    else:
        print("ERROR: Something went wrong!!")
        return


directory = "PhaseDiagram"
fileName = "MeanArea"

D_zs =np.arange(1.5, 2.7, 0.0025)
ps = np.arange(0.0004, 0.02, 0.0002)
#D_zs =np.array([2.12])
#ps = np.array([0.001])
parameter = np.zeros((len(ps), len(D_zs)))

for i, p in enumerate(ps):
    for j, D_z in enumerate(D_zs):
        parameter[i][j] = transition_parameter(p, D_z, directory)

np.savetxt("../../data/{}/{}.txt".format(directory, fileName), parameter)
