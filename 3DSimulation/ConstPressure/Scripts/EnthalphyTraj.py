import sys
sys.path.append("../../libraries")

import classSpheres

import numpy as np
import matplotlib.pyplot as plt
import os

def EnergyFct(mySystem, p):
    mySystem.setVariable("p", 0)
    E = mySystem.Energy(mySystem.positions) / mySystem.N
    mySystem.setVariable("p", p)
    return E

def EnthalpyFct(mySystem):
    return mySystem.Energy(mySystem.positions) / mySystem.N

def VolumeFct(mySystem):
    R_z = mySystem.R_z
    L = mySystem.positions[-1]
    return R_z**2 * np.pi * L / mySystem.N

def Trajectory(N, ps, Ds, directory):
    os.chdir("..")
    Energy = np.zeros(len(Ds))
    Enthalpy = np.zeros(len(Ds))
    Volume = np.zeros(len(Ds))

    for i, (p, D) in enumerate(zip(ps, Ds)):
        mySystem = classSpheres.SpheresCylinder("parameter.cfg")
        initial = "{}/N{}/Finalp{:.6f}D{:.4f}".format(directory, N, p, D)
        mySystem.ReadInitial(initial)
        mySystem.setVariable("p", p)
        mySystem.setVariable("N", N)
        mySystem.setVariable("R_z", D / 2.)
        Enthalpy[i] = EnthalpyFct(mySystem)
        Energy[i] = EnergyFct(mySystem, p)
        Volume[i] = VolumeFct(mySystem)

    os.chdir("Scripts/")
    return Enthalpy, Energy, Volume

Ds = np.arange(1.95, 2.22, 0.002)
ps = np.ones(len(Ds)) * 0.026
N = 12
number1 = "321to422"
number2 = "321to422"
directoryForward = "Hysteresis/{}Forward".format(number1)
directoryBackward = "Hysteresis/{}Backward".format(number1)
EnthalpyForward, EnergyForward, VolumeForward = Trajectory(N, ps, Ds, directoryForward)
EnthalpyBackward, EnergyBackward, VolumeBackward = Trajectory(N, ps, Ds, directoryBackward)

fig = plt.figure()
ax = fig.add_subplot(2, 1, 1)
ax.set_title("$p = {:.4f}$".format(ps[0]))
ax.plot(Ds, EnthalpyForward, "b.", markersize=4., label="$(3, 2, 1) \Rightarrow (4, 2, 2)$")
ax.plot(Ds, EnthalpyBackward, "rx", markersize=2., label="$(4, 2, 2) \Rightarrow (3, 2, 1)$")
ax.set_ylabel("Enthalpy $h$")
ax.legend(loc="upper right")
ax.grid()
ax.set_xlim(min(Ds), max(Ds))
#ax.set_ylim(0.0094, 0.0105)

#ax.text(2.214, 0.019, "$(4, 3, 1)$", fontsize=18, color="k")
#ax.text(2.267, 0.0186, r"$(4, \bf{3}, 1)$ ls", fontsize=18, color="k")
#ax.text(2.4, 0.019, r"$(5, 3, 2)$", fontsize=18, color="k")
#ax.axvline(2.265, linestyle="--", color="k")
#ax.axvline(2.29, linestyle="--", color="g")
#ax.axvline(2.325, linestyle="--", color="b")


#ax.text(2.195, 0.0296, "$(4, 2, 2)$", fontsize=18, color="b")
#ax.text(2.26,  0.0296, "$(4, 2, 2)$", fontsize=18, color="g")
#ax.text(2.235, 0.02925, r"$(4, 2, 2)$ ls", fontsize=18, color="b")
#ax.text(2.25,  0.02860, r"$(4, 3, 1)$ ls", fontsize=18, color="g")
#ax.text(2.33,  0.0285, "$(5, 3, 2)$", fontsize=18, color="b")
#ax.axvli(2.205, linestyle="--", color="b")
#ax.axvline(2.235, linestyle="--", color="b")
#ax.axvline(2.290, linestyle="--", color="b")
#ax.axvline(2.470, linestyle="--", color="b")

#ax.axvline(2.265, linestyle="--", color="g")
#ax.axvline(2.215, linestyle="--", color="g")

#ax.text(1.955, 0.01445, "$(3, 2, 1)$ \n uniform", fontsize=11)
#ax.text(2.011, 0.01445, r"$(3, \bm{2}, 1)$" + "\n line slip", fontsize=11)
#ax.text(2.14, 0.01425, "$(4, 2, 2)$ \n uniform", fontsize=11)
#
#ax.axvline(2.008, linestyle="--", color="k")
#ax.axvline(2.064, linestyle="--", color="k")
#
ax2 = fig.add_subplot(2, 1, 2)
ax2.plot(Ds[:-1], -np.diff(EnthalpyForward) / np.diff(Ds), "b.", markersize=4.)
ax2.plot(Ds[:-1], np.diff(EnthalpyBackward) / np.diff(Ds[::-1]), "rx", markersize=2.)
ax2.set_ylabel("$\partial h / \partial D$")
ax2.set_xlabel("diameter ratio $D/d$")
ax2.set_ylim(min(-np.diff(EnthalpyForward) / np.diff(Ds)), max(-np.diff(EnthalpyForward) / np.diff(Ds)) + 0.002)
ax2.grid()
ax2.set_xlim(min(Ds), max(Ds))
#ax2.set_ylim(-0.01, 0.011)

#ax2.axvline(2.008, linestyle="--", color="k")
#ax2.axvline(2.064, linestyle="--", color="k")

fig.savefig("../Plots/Hysteresis/Enthalpy{}p{:.4f}.pdf".format(number2, ps[0]))
