import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres


N, p, D_z = 4, 0.005, 1.85
initial = "PhaseDiagram/N{}/Finalp{:.6f}D{:.4f}".format(N, p, D_z) #
#directory = "321to422Backward"
#initial = "Hysteresis/{}/N{}/Finalp{:.6f}D{:.4f}".format(directory, N, p, D_z)

mySystem = classSpheres.SpheresCylinder("parameter.cfg")
mySystem.setVariable("p", p)
mySystem.setVariable("N", N)
mySystem.setVariable("R_z", D_z / 2.)
mySystem.ReadInitial(initial)

print(mySystem.Energy(mySystem.positions) / N)

#mySystem.plotSchematic(".", 6, 1.004)
mySystem.plot3DTube(".", 3)
