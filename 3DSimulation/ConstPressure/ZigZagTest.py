import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess
import matplotlib.pyplot as plt

def minimalE(D_z, p, N, repetition, initial, finalDir):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("N", N)
    mySystem.setVariable("R_z", D_z / 2.)
    if (initial == "Random"):
        mySystem.ReadInitial("ZigZagTest/N3/RandomFinalp{:.6f}D{:.4f}".format(p, D_z))
        mySystem.resize(repetition)
    else:
        mySystem.ReadInitial(initial)
    print("pressure: ", p, "Diameter: ", D_z, mySystem.N)

    E = mySystem.steepest_descent(0.05, 10000)

    mySystem.printToFile(finalDir)
    print("Energy: ", E)
    return E / N

def AnalyticEnergy(args, p, D_z):
    k, d = 1, 1
    L, h = args[0], args[1]
    E = 0.5 * k * (d - np.sqrt(L**2 + h**2))**2 if (d - np.sqrt(L**2 + h**2) > 0) else 0
    E += 0.5 * k * (h + d - D_z)**2 if (h + d - D_z > 0) else 0
    E += p * np.pi * L * D_z**2 / 4.
    return E

def minEAnalytic(D_z, p, N):
    import scipy.optimize as sco
    import functools as ft
    pos0 = np.random.uniform(0, 1, 2)
    bnds = [(0, 1), (0, D_z)]
    ret = sco.minimize(ft.partial(AnalyticEnergy, p=p, D_z=D_z), pos0, jac=None, method="L-BFGS-B", bounds=bnds)
    return ret.fun, ret.x

ps = np.arange(0.0004, 1.5, 0.0004)
D_zs = np.ones(len(ps)) * 1.
N = 3
repetition = 3

finalDirSS = "ZigZagTest/N{}/".format(N * repetition)

# create forward trajectory
ESim = np.zeros(len(ps))
EAnalytic = np.zeros(len(ps))
x = np.zeros((len(ps), 2))

for i, (D_z, p) in enumerate(zip(D_zs, ps)):
#    if (i == 0):
#        ESim[i] = minimalE(D_z, p, N, repetition, "Random", finalDirSS)
#    else:
#        ESim[i] = minimalE(D_z, p, N * repetition, 1, finalDirSS + "Finalp{:.6f}D{:.4f}".format(ps[i - 1], D_zs[i - 1]), finalDirSS)
    EAnalytic[i], x[i] = minEAnalytic(D_z, p, N)

for xi in x:
    print(xi)

EBamboo = lambda p, D_z: np.pi * D_z**2 / 4. * p - 1 / 32. * np.pi**2 * D_z**4 * p**2

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

#ax.plot(ps, ESim, "b.")
ax.plot(ps, EAnalytic, "kx")
ax.plot(ps, EBamboo(ps, D_zs), "c-")

plt.show()
