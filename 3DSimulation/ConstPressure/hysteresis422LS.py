import sys
sys.path.append("../libraries")

import numpy as np
import classSpheres
import subprocess
#from joblib import Parallel, delayed

def minimalE(D_z, p, N, initial, finalDir):
    subprocess.call("mkdir -p data/" + finalDir, shell=True)

    mySystem = classSpheres.SpheresCylinder("parameter.cfg")
    mySystem.setVariable("p", p)
    mySystem.setVariable("R_z", D_z / 2.)
    mySystem.ReadInitial(initial)
    mySystem.setVariable("N", N)

    print("pressure: ", p, "Diameter: ", D_z, "N: ", mySystem.N, flush=True)


    E = mySystem.minimize("L-BFGS-B")
    mySystem.printToFile(finalDir)

    print("Energy: ", E / mySystem.N, flush=True)
    return E

def const_D(D_zs, p, number):
    N = 12
    initialSS = "Hysteresis/{}Forward/N12/Finalp{:.6f}D{:.4f}".format(number, p - 0.0002, D_zs[0])
#    initialSS = "Adil/422LineSlip/532LineSlip"
    finalDirSS = "Hysteresis/{}Forward/N{:d}/".format(number, N)

    # create forward trajectory
    EForward = np.zeros(len(Ds))
    for i, D_z in enumerate(D_zs):
        if (i == 0):
            EForward[i] = minimalE(D_z, p, N, initialSS, finalDirSS)
        else:
            EForward[i] = minimalE(D_z, p, N, finalDirSS + "Finalp{:.6f}D{:.4f}".format(p, D_zs[i - 1]), finalDirSS)

    return EForward

def const_p(Ds, ps, number):
    EForward = np.array(Parallel(n_jobs=8)(delayed(const_D)(Ds, p, number) for p in ps))[0]
#    EForward = np.array([const_D(Ds, p, number) for p in ps])[0]

    return EForward

Ds = np.arange(2.49, 2.45, -0.0005)
ps = np.arange(0.0003, 0.01, 0.0002)
const_p(Ds, ps, sys.argv[1])

Ds = np.arange(2.49, 2.58, 0.0005)
const_p(Ds, ps, sys.argv[1])
