import numpy as np

def ToCylinderCo(x, y, z):
    R = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return R, phi, z

def ToCartesianCo(R, phi, z):
    x = R * np.cos(phi)
    y = R * np.sin(phi)
    return x, y, z

################### Period. Boundary functions ###################
def foldZ(x, L, alpha):
    angle = -alpha * np.floor(x[-1] / L)
    cosA = np.cos(angle)
    sinA = np.sin(angle)

    rotation = np.array([[ cosA, sinA, 0],
                         [-sinA, cosA, 0],
                         [   0,     0, 1]])

    xs = np.dot(rotation, x)
    xs[-1] = x[-1] - L * np.floor(x[-1] / L)

    return xs

def shift(x, L, alpha):
    cosA = np.cos(alpha)
    sinA = np.sin(alpha)

    rotation = np.array([[ cosA, sinA, 0],
                         [-sinA, cosA, 0],
                         [   0,     0, 1]])

    xalpha = np.dot(rotation, x)
    xalpha[-1] = x[-1] + L

    return xalpha

################### Energy functions ###################
def Harmonic(distances, selfy):
    eps = selfy.eps
    R_c = selfy.R_c
    r_min = selfy.r_min
    harmonicLocal= lambda r: 0.5 * eps * (r - r_min)**2
    return np.where(distances > R_c, 0, harmonicLocal(distances))

def HarmonicForce(differenceVector, selfy):
    R_c = selfy.R_c
    r_min = selfy.r_min
    eps = selfy.eps
    r = np.linalg.norm(differenceVector, axis=2)
    scales = np.where((r > R_c) + (r == 0), 0, eps * (r - r_min) / r)
    return scales[:, :, None] * differenceVector

def HarmonicDiffAlpha(xAbove, differenceVector, scales):
    vector = (differenceVector[:, :, 0] * xAbove[None, :, 1] - differenceVector[:, :, 1] * xAbove[None, :, 0])
    return scales * vector

def HarmonicDiffL(differenceVector, scales):
    vector = differenceVector[:, :, 2]
    return -scales * vector

################### Borders in 3D ###################
def borders3D(r, selfy):
    """
    Takes Arguments:
        r: nDarray, position of spheres
        R_z: cylinder radius
        r_min: minimum of LJ, sphere radius
    Returns:
        border energy
    """
    R_z = selfy.R_z
    r_min = selfy.r_min
    eps = selfy.eps
    borderLocal = lambda r: np.exp(10 * r) - 1
    harmonicLocal = lambda r: 0.5 * eps * r**2
    x, y, z = r.T
    R, _, _ = ToCylinderCo(x, y, z)

    energy = np.sum(np.where(R > R_z + 0.5 * r_min, borderLocal(R + 0.5 * r_min - R_z), 0))
    energy += np.sum(np.where(R > R_z - 0.5 * r_min, harmonicLocal(R + 0.5 * r_min - R_z), 0))
    return energy

def border3DForce(r, selfy):
    eps = selfy.eps
    r_min = selfy.r_min
    R_z = selfy.R_z
    borderXforce = lambda x: 10 * np.exp(10 * x)
    harmonicXForce = lambda x: eps * x
    x, y, z = r.T
    R, _, _ = ToCylinderCo(x, y, z)

    denergies = np.zeros(np.shape(r.T))
    denergies[0] = np.where(R > R_z + 0.5 * r_min, borderXforce(R + 0.5 * r_min - R_z) * x / R, 0)
    denergies[1] = np.where(R > R_z + 0.5 * r_min, borderXforce(R + 0.5 * r_min - R_z) * y / R, 0)
    denergies[0] += np.where(R > R_z - 0.5 * r_min, harmonicXForce(R + 0.5 * r_min - R_z) * x / R, 0)
    denergies[1] += np.where(R > R_z - 0.5 * r_min, harmonicXForce(R + 0.5 * r_min - R_z) * y / R, 0)
    return denergies.T

def callback(x, f, accept):
    if accept:
        file_handle = open("data/Tests/EnergyRun.txt", "ab")
        print(f / 4)
        np.savetxt(file_handle, np.array([f / 4]))
        file_handle.close()


