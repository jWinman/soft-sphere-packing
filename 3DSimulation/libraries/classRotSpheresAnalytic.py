import numpy as np
import matplotlib.pyplot as plt

class RotatingSpheres:
    def __init__(selfy, fileName):
        import pylibconfig2 as pylib

        with open(fileName, "r") as f:
            cfgStr = f.read()

        cfg = pylib.Config(cfgStr)

        # parameters
        selfy.omega = cfg.cylinder.omega
        selfy.L = cfg.cylinder.L
        selfy.k = cfg.spheres.k
        selfy.d = cfg.spheres.d
        selfy.M = cfg.spheres.M
        selfy.Nplane = cfg.spheres.Nplane

        selfy.structure = cfg.cylinder.structure

        # variable(s)
        selfy.r = 1.

    def minimize(selfy, func, initial, bnds):
        from scipy import optimize
        import functools as ft

        res = optimize.minimize(func, initial, jac=None, method="L-BFGS-B", bounds=bnds)

        return res.x, res.fun

    def setVariable(selfy, variable, value):
        exec("selfy.{} = {}".format(variable, value))

    def E_Bamboo(selfy):
        interactionTop = 0.5 * selfy.k / (selfy.omega * selfy.M)  * ((selfy.d - selfy.L) / selfy.d)**2
        return interactionTop

    def E_NN0(selfy, r):
        rotation = 0.5 * (r / selfy.d)**2
        interactionPlane = 0.5 * selfy.k / (selfy.omega * selfy.M) * ((selfy.d - 2 * r * np.sin(np.pi / selfy.Nplane)) / selfy.d)**2
        interactionTop = selfy.k / (selfy.omega * selfy.M) * ((selfy.d - np.sqrt(r**2 * (np.cos(np.pi / selfy.Nplane) - 1)**2 + r**2 * np.sin(np.pi / selfy.Nplane)**2 + selfy.L**2)) / selfy.d)**2

        if (selfy.Nplane == 2):
            interactionPlane *= 0.5

        return rotation + interactionPlane + interactionTop

    def contact_NN0(selfy, r):
        d_plane = (2 * r * np.sin(np.pi / selfy.Nplane) - selfy.d) / selfy.d
        d_top = (np.sqrt(r**2 * (np.cos(np.pi / selfy.Nplane) - 1)**2 + r**2 * np.sin(np.pi / selfy.Nplane)**2 + selfy.L**2) - selfy.d) / selfy.d
        return d_plane < 0 and d_top < 0

    def Energy_PQR(selfy, args):
        R, Twist = args
        x0, y0, z0 = R * np.cos(0), R * np.sin(0), 0

        thetas = selfy.structure * Twist
        zs = selfy.structure * selfy.L

        xs, ys = R * np.cos(thetas), R * np.sin(thetas)

        overlap = np.sqrt((xs - x0)**2 + (ys - y0)**2 + (zs - z0)**2) - selfy.d

        return 0.5 * (R / selfy.d)**2 + 0.5 * selfy.k / (selfy.omega * selfy.M) * np.sum((overlap / selfy.d)**2)

    def contact_PQR(selfy, args):
        R, Twist = args
        x0, y0, z0 = R * np.cos(0), R * np.sin(0), 0

        thetas = selfy.structure * Twist
        zs = selfy.structure * selfy.L

        xs, ys = R * np.cos(thetas), R * np.sin(thetas)

        overlap = (np.sqrt((xs - x0)**2 + (ys - y0)**2 + (zs - z0)**2) - selfy.d) / selfy.d

        return np.all(overlap < 0)

    def Energy_422(selfy, r):
        xUnit, yUnit, zUnit = r * np.cos(0), r * np.sin(0), 0
        xTop, yTop, zTop = r * np.cos(0), r * np.sin(0), 2 * selfy.L
        xSide, ySide, zSide = r * np.cos(np.pi / 2.), r * np.sin(np.pi / 2.), selfy.L

        overlapTop = np.sqrt((xUnit - xTop)**2 + (yUnit - yTop)**2 + (zUnit - zTop)**2) - selfy.d
        overlapSide = np.sqrt((xUnit - xSide)**2 + (yUnit - ySide)**2 + (zUnit - zSide)**2) - selfy.d

        return 0.5 * (r / selfy.d)**2 + 0.5 * selfy.k / (selfy.omega * selfy.M) * ((overlapTop / selfy.d)**2 + 2 * (overlapSide / selfy.d)**2)

    def contact_422(selfy, r):
        xUnit, yUnit, zUnit = r * np.cos(0), r * np.sin(0), 0
        xTop, yTop, zTop = r * np.cos(0), r * np.sin(0), selfy.L
        xSide, ySide, zSide = r * np.cos(np.pi / 2.), r * np.sin(np.pi / 2.), 0.5 * selfy.L

        overlapTop = (np.sqrt((xUnit - xTop)**2 + (yUnit - yTop)**2 + (zUnit - zTop)**2) - selfy.d) / selfy.d
        overlapSide = (np.sqrt((xUnit - xSide)**2 + (yUnit - ySide)**2 + (zUnit - zSide)**2) - selfy.d) / selfy.d

        return overlapTop < 0 and overlapSide < 0

    def Energy_ZigZag(selfy, args):
        r, twist = args

        xUnit, yUnit, zUnit = r * np.cos(0), r * np.sin(0), 0
        xTwist, yTwist, zTwist = r * np.cos(twist), r * np.sin(twist), selfy.L
        xTop, yTop, zTop = r * np.cos(2 * twist), r * np.sin(2 * twist), 2 * selfy.L

        overlapTop = (np.sqrt((xUnit - xTop)**2 + (yUnit - yTop)**2 + (zUnit - zTop)**2) - selfy.d) / selfy.d
        overlapTwist = (np.sqrt((xUnit - xTwist)**2 + (yUnit - yTwist)**2 + (zUnit - zTwist)**2) - selfy.d) / selfy.d

        return 0.5 * (r / selfy.d)**2 + 0.5 * selfy.k / (selfy.omega * selfy.M) * ((overlapTwist / selfy.d)**2 + (overlapTop / selfy.d)**2)

    def contact_ZigZag(selfy, args):
        r, twist = args

        xUnit, yUnit, zUnit = r * np.cos(0), r * np.sin(0), 0
        xTwist, yTwist, zTwist = r * np.cos(twist), r * np.sin(twist), selfy.L
        xTop, yTop, zTop = r * np.cos(2 * twist), r * np.sin(2 * twist), 2 * selfy.L

        overlapTop = (np.sqrt((xUnit - xTop)**2 + (yUnit - yTop)**2 + (zUnit - zTop)**2) - selfy.d) / selfy.d
        overlapTwist = (np.sqrt((xUnit - xTwist)**2 + (yUnit - yTwist)**2 + (zUnit - zTwist)**2) - selfy.d) / selfy.d

        return overlapTop < 0 and overlapTwist < 0

    def Energy_HardSpheres(selfy, D, phi):
        R = (D / selfy.d - 1) / 2.
        L = 2 / 3. * (selfy.d / D)**2 / phi
        Energy = 0.5 * R**2

        return L, Energy

