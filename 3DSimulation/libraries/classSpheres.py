import numpy as np
import functools as ft
import scipy.spatial.distance as ssd
from scipy import optimize
import miscellaneous as misc

class SpheresCylinder:
    def __init__(selfy, fileName, interaction="softsphere", borders="softborder"):
        import pylibconfig2 as pylib

        with open(fileName, "r") as f:
            cfgStr = f.read()

        cfg = pylib.Config(cfgStr)

        # static parameters
        selfy.R_z = cfg.cylinder.R_z
        selfy.p = cfg.cylinder.p
        selfy.N = cfg.spheres.N
        selfy.R_c = cfg.spheres.R_c
        selfy.eps, selfy.r_min = cfg.spheres.eps, cfg.spheres.r_min
        selfy.niter = cfg.minimize.niter
        selfy.stepsize = cfg.minimize.stepsize
        selfy.T = cfg.minimize.T
        selfy.size = -2 # number of additional variable parameters

        # variable parameters
        selfy.positions = np.zeros(selfy.N * 3 + 2) # 3N position + alpha + L

        # energy interactions
        selfy.interaction = ft.partial(misc.Harmonic, selfy=selfy)
        selfy.borders = ft.partial(misc.borders3D, selfy=selfy)

        # Force interactions
        selfy.interactionForce = ft.partial(misc.HarmonicForce, selfy=selfy)
        selfy.borderForce = ft.partial(misc.border3DForce, selfy=selfy)
        selfy.diffAlpha = misc.HarmonicDiffAlpha
        selfy.diffL = misc.HarmonicDiffL

        #Hessian
        selfy.Hess = np.zeros((selfy.N, selfy.N))

    def RandomInitial(selfy):
        import numpy.random
        R = numpy.random.uniform(0, selfy.R_z, selfy.N)
        th = numpy.random.uniform(0, 2 * np.pi, selfy.N)
        z = numpy.random.uniform(0, selfy.N * selfy.r_min, selfy.N)
        alpha = numpy.random.uniform(0, 2 * np.pi)
        L = numpy.random.uniform(0.5 * selfy.r_min, selfy.N * selfy.r_min)

        pos = np.array([misc.ToCartesianCo(R, th, z)])
        selfy.positions = np.append(pos.T.flatten(), np.array([alpha, L]))

    def ReadInitial(selfy, initial):
        R, th, z = np.loadtxt("data/" + initial + "Pos", unpack=True)
        furtherArgs = np.loadtxt("data/" + initial + "FurtherArgs", unpack=True)

        pos = np.array([misc.ToCartesianCo(R, th, z)])
        selfy.positions = np.append(pos.T.flatten(), furtherArgs)

    def GhostBoundary(selfy, x):
        xs = np.reshape(x[:selfy.size], (len(x[:selfy.size]) // 3, 3))
        alpha = x[selfy.size]
        L = x[selfy.size + 1]

        xs = np.apply_along_axis(misc.foldZ, 1, xs, L, alpha)
        xAbove = np.apply_along_axis(misc.shift, 1, xs, L, alpha)
        xBelow = np.apply_along_axis(misc.shift, 1, xs, -L, -alpha)
        return xs, np.concatenate((xs, xAbove, xBelow), axis=0)

    def Energy(selfy, x):
        L = x[selfy.size + 1]
        x, xGhost = selfy.GhostBoundary(x)

        # Energies between Ghost and Real spheres
        distances = ssd.cdist(x, xGhost, metric="euclidean")
        distances = np.delete(distances, [j + j * len(xGhost) for j in range(len(x))])
        energy = 0.5 * np.sum(selfy.interaction(distances))
#        print("interaction: ", energy / selfy.N)

        # Energies between opposing Ghost and Ghost spheres
        distancesGhost = ssd.cdist(xGhost[selfy.N:2 * selfy.N],
                              xGhost[2 * selfy.N:3 * selfy.N], metric="euclidean")

        energy += np.sum(selfy.interaction(distancesGhost))
#        print("Ghost interaction: ", np.sum(selfy.interaction(distancesGhost)) / selfy.N)

        # Energy from Border
        energy += selfy.borders(x)
#        print("border energy: ", selfy.borders(x) / selfy.N)

        # pV term
        energy += selfy.p * np.pi * selfy.R_z**2 * L
#        print("pV: ", selfy.p * np.pi * selfy.R_z**2 * L / selfy.N)

#        print("energy: ", energy)

        return energy

    def HarmonicGradAlphaL(selfy, xs, alpha, L):
        xAboves = np.apply_along_axis(misc.shift, 1, xs, L, alpha)

        differenceVector = xs[:, None] - xAboves[None, :]
        r = np.linalg.norm(differenceVector, axis=2)
        scales = np.where((r > selfy.R_c) + (r == 0), 0, selfy.eps * (r - selfy.r_min) / r)

        alphaGrad = -np.sum(selfy.diffAlpha(xAboves, differenceVector, scales))
        LGrad = np.sum(selfy.diffL(differenceVector, scales)) + selfy.p * np.pi * selfy.R_z**2

        return np.array([alphaGrad, LGrad])

    def Gradient(selfy, x):
        xs, xGhost = selfy.GhostBoundary(x)
        xAbove = xGhost[selfy.N:2 * selfy.N]
        xBelow = xGhost[2 * selfy.N:]

        denergies = np.zeros((len(xs), 3))
        # real-real spheres
        xsDiffs = xs[:, None] - xs[None, :]
        denergies += np.sum(selfy.interactionForce(xsDiffs), axis=1)

        # real-top ghost spheres
        xsDiffs = xs[:, None] - xAbove[None, :]
        denergies += np.sum(selfy.interactionForce(xsDiffs), axis=1)

        # real-bottom ghost spheres
        xsDiffs = xs[:, None] - xBelow[None, :]
        denergies += np.sum(selfy.interactionForce(xsDiffs), axis=1)

        denergies += selfy.borderForce(xs)

        alpha = x[selfy.size]
        L = x[selfy.size + 1]

        denergies = np.append(denergies, selfy.HarmonicGradAlphaL(xs, alpha, L))

        return denergies

    def Hessian(selfy):
        import numdifftools as nd

        Hessian = nd.Hessian(selfy.Energy)
        selfy.Hess = Hessian(selfy.positions)

    def definite_test(selfy):
        eig = np.linalg.eigvalsh(selfy.Hess)

        return (np.all(eig > 0), not np.any(abs(eig) < 1e-8))

    def perturb_eig(selfy, perc):
        eigval, eigvec  = np.linalg.eigh(selfy.Hess)

        Es = np.zeros((len(eigval), len(perc)))
        for j, mv in enumerate(eigvec):
            pos = np.zeros(len(selfy.positions))
            for i, p in enumerate(perc):
                pos = selfy.positions + p / np.linalg.norm(mv) * mv
                Es[j][i] = selfy.Energy(pos)

        return Es

    def line_search_eig(selfy):
        eigval, eigvec  = np.linalg.eigh(selfy.Hess)

        if (np.min(eigval) < 0):
            mv = eigvec[np.argmin(eigval)]
#            selfy.positions += 0.01 / np.linalg.norm(mv) * mv

            alpha, fc, gc, E_new, old_fval, new_slope = optimize.line_search(selfy.Energy, selfy.Gradient, selfy.positions, mv)
            print(alpha, fc, gc, new_slope)
            selfy.positions += alpha / np.linalg.norm(mv) * mv


    def perturb(selfy, perc):
        kick = np.random.uniform(-perc, perc, 3 * selfy.N)
        kickAlpha = np.random.uniform(0, 2 * np.pi * perc)
        kickL = 0
        kick = np.append(kick, np.array([kickAlpha, kickL]))

        selfy.positions += kick

    def perturbOneBubble(selfy, perc):
        kick = np.zeros(3 * selfy.N)
        sphere = np.random.randint(selfy.N)
        kick[3 * sphere: 3 * sphere + 3] = np.random.uniform(-perc, perc, 3)
        kickAlpha = np.random.uniform(0, 2 * np.pi * perc)
        kick = np.append(kick, np.array([kickAlpha, 0]))

        selfy.positions += kick

    def minimize(selfy, method):
        bnds = [(-selfy.R_z, selfy.R_z),
                (-selfy.R_z, selfy.R_z),
                (0, float("inf"))] * int((len(selfy.positions) + selfy.size) / 3)
        bnds.append((0, 2 * np.pi))
        bnds.append((selfy.r_min / 2., selfy.N * selfy.r_min))

        if (method=="basinhopping"):
            minimizer_kwargs = {"method": "L-BFGS-B", "jac": selfy.Gradient, "bounds": bnds}
            ret = optimize.basinhopping(selfy.Energy, selfy.positions,
                                        minimizer_kwargs=minimizer_kwargs,
                                        niter=selfy.niter, stepsize=selfy.stepsize, T=selfy.T)
            E = ret.fun
            selfy.positions = ret.x

        elif (method == "SD"):
            E = selfy.Gradient_descent(0.001, int(1e5))

        else:
            ret = optimize.minimize(selfy.Energy, selfy.positions, jac=selfy.Gradient, method=method, bounds=bnds,
                                    options={"ftol": 1e-23, "gtol": 1e-23})
            E = ret.fun
            selfy.positions = ret.x

        return E

    def minimize_eig(selfy, method, perc):
        E = selfy.minimize(method)
        selfy.Hessian()
        definite = selfy.definite_test()

        for i in range(5):
            if definite != (True, True):
                break;
            selfy.perturb_eig(perc)
            E = selfy.minimize(method)
            selfy.Hessian()
            definite = selfy.definite_test()

        return E

    def Gradient_descent(selfy, stepsize, steps):
        convergence = np.zeros(steps)
        Energy = np.zeros(steps)
        gradient_c = selfy.Gradient(selfy.positions)

        for i in range(steps):
            selfy.positions -= stepsize * gradient_c

            gradient_c = selfy.Gradient(selfy.positions)
            gradnorm = np.linalg.norm(gradient_c)

            convergence[i] = gradnorm
            Energy[i] = selfy.Energy(selfy.positions)

            if (2 * np.fabs(Energy[i] - Energy[i - 1]) <= 1e-8 * (np.fabs(Energy[i]) - np.fabs(Energy[i - 1]) + 1e-10)):
                break;

        return selfy.Energy(selfy.positions) / selfy.N

    def DG_adjust(selfy, stepsize, steps):
        convergence = np.zeros(steps)
        Energy = np.zeros(steps)
        gradient_c = selfy.Gradient(selfy.positions)
        gradient_p = np.zeros(len(selfy.positions))
        positions_p = np.zeros(len(selfy.positions))
        stepsize = stepsize

        for i in range(steps):
            positions_p = np.copy(selfy.positions)

            selfy.positions -= stepsize * gradient_c

            gradient_p = np.copy(gradient_c)
            gradient_c = selfy.Gradient(selfy.positions)
            gradnorm = np.linalg.norm(gradient_c)

            convergence[i] = gradnorm
            Energy[i] = selfy.Energy(selfy.positions)

            print(i, convergence[i], Energy[i], 2 * np.fabs(Energy[i] - Energy[i - 1]), 1e-8 * (np.fabs(Energy[i]) - np.fabs(Energy[i - 1]) + 1e-10))

            stepsize, _, _, _, _, _ = optimize.line_search(selfy.Energy, selfy.Gradient, selfy.positions, gradient_c)
            if (stepsize == None):
                stepsize = np.inner((selfy.positions - positions_p), gradient_c - gradient_p) / np.linalg.norm(gradient_c - gradient_p)**2
            if (2 * np.fabs(Energy[i] - Energy[i - 1]) <= 1e-8 * (np.fabs(Energy[i]) - np.fabs(Energy[i - 1]) + 1e-10)):
                break;

        return selfy.Energy(selfy.positions), len(convergence[np.nonzero(convergence)])

    def Nonlinear_CG(selfy, steps):
        convergence = np.zeros(steps)
        Energy = np.zeros(steps)

        beta = 0

        # First step is gradient descent
        gradient_c = -selfy.Gradient(selfy.positions)
        conj_grad = gradient_c
        alpha, _, _, _, _, _ = optimize.line_search(selfy.Energy, selfy.Gradient, selfy.positions, conj_grad)
        selfy.positions += alpha * conj_grad

        for i in range(steps):
            gradient_p, gradient_c = np.copy(gradient_c), -selfy.Gradient(selfy.positions)
            beta = np.dot(gradient_c, gradient_c - gradient_p) / np.dot(gradient_p, gradient_p)
            conj_grad = gradient_c - beta * conj_grad
            alpha, _, _, _, _, _ = optimize.line_search(selfy.Energy, selfy.Gradient, selfy.positions, conj_grad)
            if (alpha == None):
                alpha = 0.001
            selfy.positions += alpha * conj_grad

            convergence[i] = np.linalg.norm(gradient_c)
            Energy[i] = selfy.Energy(selfy.positions)
            print("i: ", i, "conv: ", convergence[i], "E: ", Energy[i], 2 * np.fabs(Energy[i] - Energy[i - 1]), 1e-8 * (np.fabs(Energy[i]) - np.fabs(Energy[i - 1]) + 1e-10))
            if (2 * np.fabs(Energy[i] - Energy[i - 1]) <= 1e-8 * (np.fabs(Energy[i]) - np.fabs(Energy[i - 1]) + 1e-10)):
                break;

        return selfy.Energy(selfy.positions), len(convergence[np.nonzero(convergence)])

    def resize(selfy, repetition, frac=1.):
        xs = np.reshape(selfy.positions[:selfy.size], (len(selfy.positions[:selfy.size]) // 3, 3))
        alpha = selfy.positions[selfy.size]
        L = selfy.positions[selfy.size + 1]
        xs = np.apply_along_axis(misc.foldZ, 1, xs, L, alpha)
        xsSort = xs[np.argsort(xs[:, 2])]
        xsUnit = xsSort[:int(frac * selfy.N)]

        # reduce unit cell to frac spheres
        if (frac < 1.):
            _, phi1, z1 = misc.ToCylinderCo(xsSort[0, 0], xsSort[0, 1], xsSort[0, 2])
            _, phi2, z2 = misc.ToCylinderCo(xsSort[int(frac * selfy.N), 0], xsSort[int(frac * selfy.N), 1], xsSort[int(frac * selfy.N), 2])
            alphaunit = (phi1 - phi2) % (2 * np.pi)
            Lunit = z2 - z1
            selfy.N = int(frac * selfy.N)
        else:
            alphaunit = alpha
            Lunit = L

#        # repeat unit cell to repetition spheres
        Nrep = repetition * selfy.N
        xRep = np.zeros((Nrep, 3))
        for i in range(0, repetition):
            xRep[i * selfy.N: (i + 1) * selfy.N] = np.apply_along_axis(misc.shift, 1, xsUnit, i * Lunit, i * alphaunit)

        furtherArgs = np.array([(repetition * alphaunit) % (2 * np.pi), repetition * Lunit])
        selfy.positions = np.append(xRep.flatten(), furtherArgs)
        selfy.N = (len(selfy.positions) + selfy.size) // 3

    def plot3D(selfy, path):
        from mayavi import mlab

        pos, posGhost = selfy.GhostBoundary(selfy.positions)
        x, y, z = pos.T
        xGhost, yGhost, zGhost = posGhost.T
        L = selfy.positions[selfy.size + 1]

        Rs, phis, zs = misc.ToCylinderCo(x, y, z)
        phis = phis - phis[0]
        x, y, z = misc.ToCartesianCo(Rs, phis, zs)
        pos = np.array([x, y, z]).T
        selfy.positions = np.append(pos.flatten(), selfy.positions[-2:])

        pos, posGhost = selfy.GhostBoundary(selfy.positions)
        x, y, z = pos.T
        xGhost, yGhost, zGhost = posGhost.T
        L = selfy.positions[selfy.size + 1]

        zMin = 0
        zMax = zMin + L

        dphi, dz = np.pi / 250., np.pi / 250.
        [phi, zCylinder] = np.mgrid[0:2 * np.pi:dphi, zMin:zMax:dz]

        xCylinder = selfy.R_z * np.cos(phi)
        yCylinder = selfy.R_z * np.sin(phi)

        fig = mlab.figure(figure=1, bgcolor=(1, 1, 1), size=(500, 500))
        mlab.mesh(xCylinder, yCylinder, zCylinder, color=(0, 1, 0), opacity=0.1)
        mlab.points3d(xGhost, yGhost, zGhost, selfy.r_min * np.ones(len(xGhost)), resolution=40, color=(1, 0, 0), scale_factor=1.)
        mlab.points3d(x, y, z, selfy.r_min * np.ones(len(x)), resolution=40, color=(0, 0, 1), scale_factor=1.)
        mlab.show()
#        mlab.savefig(path + "/" + "structurep{:.6f}D{:.4f}.png".format(selfy.p, selfy.R_z))
        #mlab.clf()

    def plot3DTube(selfy, path, repetition):
        from mayavi import mlab

        xs = np.reshape(selfy.positions[:selfy.size], (len(selfy.positions[:selfy.size]) // 3, 3))
        alpha = selfy.positions[selfy.size]
        L = selfy.positions[selfy.size + 1]

        x, y, z = np.apply_along_axis(misc.foldZ, 1, xs, L, alpha).T
        Rs, phis, zs = misc.ToCylinderCo(x, y, z)
        phis = phis - phis[0]
        zs = zs - zs[0]
        x, y, z = misc.ToCartesianCo(Rs, phis, zs)
        xs = np.array([x, y, z]).T

        xAbove1 = np.zeros(((np.ceil(repetition / 2.)) * selfy.N, 3))
        xAbove2 = np.zeros((int(repetition / 2.) * selfy.N, 3))

        i = 0
        for j in range(repetition):
            if (j % 2 == 0):
                xAbove1[i * selfy.N: (i + 1) * selfy.N] = np.apply_along_axis(misc.shift, 1, xs, j * L, j * alpha)
            else:
                xAbove2[i * selfy.N: (i + 1) * selfy.N] = np.apply_along_axis(misc.shift, 1, xs, j * L, j * alpha)
                i += 1

        x1, y1, z1 = xAbove1.T
        x2, y2, z2 = xAbove2.T

        zMin = min(np.concatenate((z1, z2))) -  0.5 * selfy.r_min
        zMax = max(np.concatenate((z1, z2))) +  0.5 * selfy.r_min

        dphi, dz = np.pi / 250., np.pi / 250.
        [phi, zCylinder] = np.mgrid[-0.001:2 * np.pi:dphi, zMin:zMax:dz]

        xCylinder = selfy.R_z * np.cos(phi)
        yCylinder = selfy.R_z * np.sin(phi)

        fig = mlab.figure(bgcolor=(1, 1, 1), size=(300, 600))
        mlab.mesh(xCylinder, yCylinder, zCylinder, color=(0, 1., 0), opacity=0.14)
        mlab.points3d(x1, y1, z1, selfy.r_min * np.ones(len(x1)), resolution=40, color=(16. / 255, 115. / 255, 186. / 255), scale_factor=1.)
        mlab.points3d(x2, y2, z2, selfy.r_min * np.ones(len(x2)), resolution=40, color=(1, 0, 0), scale_factor=1.)

        mlab.view(azimuth=30, elevation=90, distance=14, focalpoint=(0, 0, (repetition - 1) * L / 2.))
        mlab.show()
#        mlab.savefig(path + "3DStructure.png")
#        mlab.close()

    def plotSchematic(selfy, path, repetition, sdist_max):
        from mayavi import mlab

        xs = np.reshape(selfy.positions[:selfy.size], (len(selfy.positions[:selfy.size]) // 3, 3))
        alpha = selfy.positions[selfy.size]
        L = selfy.positions[selfy.size + 1]

        x, y, z = np.apply_along_axis(misc.foldZ, 1, xs, L, alpha).T
        Rs, phis, zs = misc.ToCylinderCo(x, y, z)
        phis = phis - phis[0]
        zs = zs - zs[0]
        x, y, z = misc.ToCartesianCo(Rs, phis, zs)
        xs = np.array([x, y, z]).T

        xAbove = np.zeros((repetition * selfy.N, 3))
        for i in range(0, repetition):
            xAbove[i * selfy.N: (i + 1) * selfy.N] = np.apply_along_axis(misc.shift, 1, xs, i * L, i * alpha)
        x, y, z = xAbove.T
        R, phi, z = misc.ToCylinderCo(x, y, z)
        xSurf, ySurf, zSurf = misc.ToCartesianCo(R, phi, z)

        linksX, linksY, linksZ = [np.zeros(100)], [np.zeros(100)], [np.zeros(100)]
        counter = 0
        for i in range(len(x)):
            for j in range(len(x)):
                if (i != j):
                    ang_dist = np.abs(phi[i] - phi[j])
                    if (ang_dist <= 2 * np.pi):
                        sdx = x[i] - x[j]
                        sdy = y[i] - y[j]
                        sdz = z[i] - z[j]

                        sdist = np.sqrt(sdx**2 + sdy**2 + sdz**2)

                        if (sdist < sdist_max):
                            counter += 1
                            linksXi, linksYi, linksZi = np.linspace(x[i], x[j], 100), np.linspace(y[i], y[j], 100), np.linspace(z[i], z[j], 100)
                            Rlinks, linksPhi, linksZi = misc.ToCylinderCo(linksXi, linksYi, linksZi)
                            linksXi, linksYi, linksZi = misc.ToCartesianCo(np.mean(R) * np.ones(len(Rlinks)), linksPhi, linksZi)
                            linksX = np.append(linksX, np.reshape(linksXi, (1, len(linksXi))), axis=0)
                            linksY = np.append(linksY, np.reshape(linksYi, (1, len(linksYi))), axis=0)
                            linksZ = np.append(linksZ, np.reshape(linksZi, (1, len(linksZi))), axis=0)

        forcesX, forcesY, forcesZ = selfy.Gradient(selfy.positions)[:selfy.size].reshape(len(selfy.positions[:selfy.size]) // 3, 3).T
        xUnit, yUnit, zUnit = selfy.positions[:selfy.size].reshape(len(selfy.positions[:selfy.size]) // 3, 3).T

        xtmp1, ytmp1, ztmp1 = xSurf[::2], ySurf[::2], zSurf[::2]
        #xtmp2, ytmp2, ztmp2 = xSurf[1::2], ySurf[1::2], zSurf[1::2]
        #xSpiral1, ySpiral1, zSpiral1 = np.concatenate((xtmp1[::2], xtmp2[::2])), np.concatenate((ytmp1[::2], ytmp2[::2])), np.concatenate((ztmp1[::2], ztmp2[::2]))

        zMin = - selfy.r_min * 0.5
        zMax = max(z) + selfy.r_min * 0.5

        dphi, dz = np.pi / 250., np.pi / 250.
        [phi, zCylinder] = np.mgrid[0:2 * np.pi:dphi, zMin:zMax:dz]

        xCylinder = np.mean(R) * np.cos(phi)
        yCylinder = np.mean(R) * np.sin(phi)

        fig = mlab.figure(figure=1, bgcolor=(1, 1, 1), size=(200, 600))
        mlab.mesh(xCylinder, yCylinder, zCylinder, color=(0.9, 0.9, 0.9), opacity=1.)
        mlab.points3d(xSurf, ySurf, zSurf, np.ones(len(xSurf)), resolution=40, color=(16. / 255, 115. / 255, 186. / 255), scale_factor=0.4)
        for linksX, linksY, linksZ in zip(linksX[1:], linksY[1:], linksZ[1:]):
            mlab.plot3d(linksX, linksY, linksZ, color=(0, 0, 0))

        mlab.view(azimuth=30, elevation=90, distance=15.5, focalpoint=(0, 0, repetition * L / 2.))
#        mlab.savefig(path + "3DNetwork.png")
#        mlab.close()
        mlab.show()

    def setVariable(selfy, variable, value):
        exec("selfy.{} = {}".format(variable, value))

    def printToFile(selfy, final, prec=(6, 4)):
        x, y, z = selfy.GhostBoundary(selfy.positions)[0].T
        R, th, z = misc.ToCylinderCo(x, y, z)

        PosZylinder = np.array([R, th, z])
        furtherArgs = selfy.positions[selfy.size:]
        np.savetxt("data/" + final + "Finalp{p:.{pprec}f}D{D:.{Dprec}f}Pos".format(p=selfy.p, pprec=prec[0], D=2. * selfy.R_z, Dprec=prec[1]), PosZylinder.T)
        np.savetxt("data/" + final + "Finalp{p:.{pprec}f}D{D:.{Dprec}f}FurtherArgs".format(p=selfy.p, pprec=prec[0], D=2. * selfy.R_z, Dprec=prec[1]), furtherArgs.T)

        Pos, PosGhost = selfy.GhostBoundary(selfy.positions)
        distances = ssd.cdist(Pos, PosGhost, metric="euclidean")
        np.savetxt("data/" + final + "Finalp{p:.{pprec}f}D{D:.{Dprec}f}CGEmatrix".format(p=selfy.p, pprec=prec[0], D=2 * selfy.R_z, Dprec=prec[1]), distances, fmt="%.5f")
