#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>


#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

float ran2(long *idum)
{
  int j; 
  long k;
  static long idum2=123456789;
  static long iy=0;
  static long iv[NTAB];
  float temp;

  if (*idum <= 0) {
    if (-(*idum) < 1) *idum=1;
    else *idum = -(*idum);
    idum2=(*idum);
    for (j=NTAB+7;j>=0;j--) {
      k=(*idum)/IQ1;
      *idum=IA1*(*idum-k*IQ1)-k*IR1;
      if (*idum < 0) *idum += IM1;
      if (j < NTAB) iv[j] = *idum;
    }
    iy=iv[0];
  }
  k=(*idum)/IQ1;
  *idum=IA1*(*idum-k*IQ1)-k*IR1;
  if (*idum < 0) *idum += IM1;
  k=idum2/IQ2;
  idum2=IA2*(idum2-k*IQ2)-k*IR2;
  if (idum2 < 0) idum2 += IM2;
  j=iy/NDIV;
  iy=iv[j]-idum2;
  iv[j] = *idum;
  if (iy < 1) iy += IMM1;
  if ((temp=AM*iy) > RNMX) return RNMX;
  else return temp;
}
#undef IM1
#undef IM2
#undef AM
#undef IMM1
#undef IA1
#undef IA2
#undef IQ1
#undef IQ2
#undef IR1
#undef IR2
#undef NTAB
#undef NDIV
#undef EPS
#undef RNMX
/* (C) Copr. 1986-92 Numerical Recipes Software 5.){2p491&].#RsL. */


int main (int argc, char *argv[])
{
  long seed;
  int i;
  int NUM_PAR;
  double pos;
  double Pold, Rlim_old, initial_Clength, Clength;
  double rnum, rnum2;
  double r[5000], th[5000], z[5000];
  double A;
  double jiggle;
  FILE *fpt;
  
  sscanf(argv[1],"%lf",&jiggle);
  seed=-time(NULL);
  
  fpt=fopen("cylinder_seed", "r");
  fscanf(fpt, "%lf", &Pold); //This is the current pressure 
  fscanf(fpt, "%d", &NUM_PAR); //Number of particles in systems
  fscanf(fpt, "%lf", &Rlim_old); //Effectively the previous tube diameter - expressesd in terms of Rlim
  fscanf(fpt, "%lf", &initial_Clength); //The current cylinder length
  rnum=(double)ran2(&seed); 
  rnum2=2.0*(rnum-0.5);
  Clength=initial_Clength+(jiggle*rnum2);  //set Clength to the current cylinder length with some perturbation
  for(i = 1; i <= NUM_PAR; i++){ //read in the coordinates and give them a bit of a kick
    fscanf(fpt, "%lf", &pos);
    rnum=(double)ran2(&seed); 
    rnum2=2.0*(rnum-0.5);
    r[i]=pos+(jiggle*rnum2);
    
    fscanf(fpt, "%lf", &pos);
    rnum=(double)ran2(&seed); 
    rnum2=2.0*(rnum-0.5);
    th[i]=pos+(jiggle*rnum2);
    
    fscanf(fpt, "%lf", &pos);
    rnum=(double)ran2(&seed); 
    rnum2=2.0*(rnum-0.5);
    z[i]=pos+(jiggle*rnum2);
  }
  fscanf(fpt, "%lf", &A);
  rnum=(double)ran2(&seed); 
  rnum2=2.0*(rnum-0.5);
  A=A+(jiggle*rnum2);
  fclose(fpt);

  
  fpt=fopen("cylinder_seed", "w");
  fprintf(fpt, "%6.15f\n",Pold);
  fprintf(fpt, "%d\n", NUM_PAR);
  fprintf(fpt, "%6.15f\n", Rlim_old);
  fprintf(fpt, "%6.15f\n", Clength);
  for(i = 1; i <= NUM_PAR; i++){
    fprintf(fpt, "%6.15f %6.15f %6.15f\n",r[i], th[i], z[i]);
  }
  fprintf(fpt, "%6.15f\n", A);
  fclose(fpt);


  
}
