#include <stdio.h>
#include <time.h>
#include <math.h>
#include "nrutil.h"
#include "nr.h"

#define pi 3.14159265359

#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

float ran2(long *idum)
{
  int j;
  long k;
  static long idum2=123456789;
  static long iy=0;
  static long iv[NTAB];
  float temp;

  if (*idum <= 0) {
    if (-(*idum) < 1) *idum=1;
    else *idum = -(*idum);
    idum2=(*idum);
    for (j=NTAB+7;j>=0;j--) {
      k=(*idum)/IQ1;
      *idum=IA1*(*idum-k*IQ1)-k*IR1;
      if (*idum < 0) *idum += IM1;
      if (j < NTAB) iv[j] = *idum;
    }
    iy=iv[0];
  }
  k=(*idum)/IQ1;
  *idum=IA1*(*idum-k*IQ1)-k*IR1;
  if (*idum < 0) *idum += IM1;
  k=idum2/IQ2;
  idum2=IA2*(idum2-k*IQ2)-k*IR2;
  if (idum2 < 0) idum2 += IM2;
  j=iy/NDIV;
  iy=iv[j]-idum2;
  iv[j] = *idum;
  if (iy < 1) iy += IMM1;
  if ((temp=AM*iy) > RNMX) return RNMX;
  else return temp;
}
#undef IM1
#undef IM2
#undef AM
#undef IMM1
#undef IA1
#undef IA2
#undef IQ1
#undef IQ2
#undef IR1
#undef IR2
#undef NTAB
#undef NDIV
#undef EPS
#undef RNMX
/* (C) Copr. 1986-92 Numerical Recipes Software 5.){2p491&].#RsL. */

main (int argc, char *argv[])
{
  
  long seed;
  int i, j;
  int p, plim;
  int N;
  FILE *fpt;
  double x;
  double Rlim, max_Clength, Clength;
  double A;
  double pressure;
  double r[500000], th[50000], z[50000];
  char filename[100];  

  sscanf(argv[1],"%d",&N);
  sscanf(argv[2],"%lf",&Rlim);
  sscanf(argv[3],"%lf",&max_Clength);
  sscanf(argv[4],"%d",&plim);  
  sscanf(argv[5],"%lf",&pressure);  

  for(p=1;p<=plim;p++){
    
    fpt=fopen("timeseed", "r");  
    fscanf(fpt, "%ld", &seed);
    fclose(fpt);
    
    fpt=fopen("timeseed", "w");
    fprintf(fpt,"%ld\n",seed-1);
    fclose(fpt);
    
    x=(double)ran2(&seed);
    Clength=max_Clength*x;

    for(i=1; i<=N; i++){
      x=(double)ran2(&seed);
      r[i]=(Rlim-0.1)*sqrt(x);
      x=(double)ran2(&seed);
      th[i]=2*pi*x;
      x=(double)ran2(&seed);
      z[i]=Clength*x;
    }
    x=(double)ran2(&seed);
    A=2*pi*x;

   
    
    sprintf(filename,"%s%d","seed",p);
    fpt=fopen(filename, "w");  
    fprintf(fpt,"%6.15f\n",pressure);
    fprintf(fpt,"%d\n",N);
    fprintf(fpt,"%6.15f\n",Rlim);
    fprintf(fpt,"%6.15f\n",Clength);
    for(i=1;i<=N;i++){
      fprintf(fpt,"%6.10f %6.10f %6.10f\n",r[i],th[i],z[i]);
    }
    fprintf(fpt,"%6.10f\n",A);
    fclose(fpt);
    
  }
  
  
}



