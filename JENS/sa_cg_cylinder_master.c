#include <stdio.h>
#include <time.h>
#include <math.h>

#define MYPATH "/Users/jenswinkelmann/Desktop/TrinityCollege/Year_1/SoftSpherePacking/JENS/"


#define pi 3.14159265359
#define REP 4

int main (int argc, char *argv[])
{
  int i;
  int NUM_PAR, Nstart, NLIM;
  int best_result;
  double D, max_Clength;
  double Rlim;  
  double pressure;
  double lowest_energy;
  double sa_energy, gd_energy;
  char filename[2000];
  FILE *fp, *gp, *hp;

  //sscanf(argv[1],"%lf",&D); 
  //printf("D=%6.15f\n", D);

  //D=1.0;

  sscanf(argv[1],"%lf",&max_Clength);
  printf("initial Clength=%6.15f\n", max_Clength);   
  sscanf(argv[2],"%lf",&pressure); 
  printf("pressure=%6.15f\n", pressure); 
  sscanf(argv[3],"%d",&Nstart); 
  printf("Nstart=%d\n", Nstart); 
  sscanf(argv[4],"%d",&NLIM); 
  printf("NLIM=%d\n", NLIM); 
  sscanf(argv[5],"%lf",&D); 
  printf("D=%6.15f\n", D); 

  //fflush (stdin);
  //(void) getchar ();

  Rlim=(D+1.0)/2.0; 
  printf("\n\nRlim=%6.15f\n\n", Rlim); 

  //sprintf(filename,"%s%s", "cp ", "/home/adil/Dropbox/soft_spheres_in_a_cylinder/CODE/run_stats ./");
  sprintf(filename,"%s%s%s", "cp ", MYPATH, "run_stats ./");
  fp = popen(filename, "w");
  pclose(fp);

  //fflush (stdin);
  //(void) getchar ();

  sprintf(filename,"%s%s%s", "cp ", MYPATH, "cooling_schedule ./");
  fp = popen(filename, "w");
  pclose(fp);

  sprintf(filename,"%s%s", MYPATH, "generate_random_seed");
  fp = popen(filename, "w");
  pclose(fp);

  for(NUM_PAR=Nstart; NUM_PAR<=NLIM; NUM_PAR++){

    sprintf(filename,"%s%s %d %6.15f %6.15f %d %6.15f", MYPATH,"generate_starting_configurations",NUM_PAR, Rlim, max_Clength, REP, pressure);
    fp = popen(filename, "r");
    pclose(fp);

    for(i=1;i<=REP;i++){      
      sprintf(filename,"%s%d%s","cp seed",i," seed");
      fp = popen(filename, "r");
      pclose(fp);

      sprintf(filename,"%s%s", MYPATH, "simulated_annealing_in_a_cylinder_with_look_up_table");
      fp = popen(filename, "w");
      pclose(fp);

      sprintf(filename,"%s","cp sa_out cylinder_seed");
      fp = popen(filename, "r");
      pclose(fp);

      sprintf(filename,"%s%d","mv sa_out sa_out_",i);
      fp = popen(filename, "r");
      pclose(fp);

      sprintf(filename,"%s%d","mv temp_vs_step_length temp_vs_step_length_",i);
      fp = popen(filename, "r");
      pclose(fp);

      sprintf(filename,"%s%d","mv SAEmatrix SAEmatrix_",i);
      fp = popen(filename, "r");
      pclose(fp);

     
      sprintf(filename,"%s%d%s","mv sa_gpic gpic_", i, "0001");
      fp = popen(filename, "r");
      pclose(fp);

      sprintf(filename,"%s%s%6.15f%s%6.15f", MYPATH, "gradient_fixed_metric 0.001 5000000 ", pressure, " ", D);
      fp = popen(filename, "w");
      pclose(fp);
    
      sprintf(filename,"%s%d","mv DATA data",i);
      fp = popen(filename, "w");
      pclose(fp);

      sprintf(filename,"%s%d%s","cp cg_gpic gpic_", i, "0002");
      fp = popen(filename, "r");
      pclose(fp);

      sprintf(filename,"%s%d","cp cg_gpic gpic_", i);
      fp = popen(filename, "r");
      pclose(fp);
      
      sprintf(filename,"%s%d","mv energy energy_",i);
      fp = popen(filename, "w");
      pclose(fp);

      fp=fopen("simulated_annealing_energy", "r");
      fscanf(fp, "%lf", &sa_energy);
      fclose(fp);
      
      fp=fopen("gradient_descent_energy", "r");
      fscanf(fp, "%lf", &gd_energy);
      fclose(fp);
      
      //sprintf(filename,"%s","rm simulated_annealing_energy gradient_descent_energy");
      //fp = popen(filename, "r");
      //pclose(fp);
      
      fp=fopen("sa_gd_energy_list", "a");
      fprintf(fp, "%6.15f %6.15f\n", sa_energy, gd_energy);
      fclose(fp);
      
      
      //printf("\n...waiting...\n");
      //fflush (stdin);
      //(void) getchar ();

    }

    sprintf(filename,"%s%s", MYPATH, "energy_sort.sh");
    fp = popen(filename, "r");
    pclose(fp);
    
    fp=fopen("sorted_energylist", "r");
    fscanf(fp, "%lf", &lowest_energy);
    fscanf(fp, "%d", &best_result);
    fclose(fp);
    
    sprintf(filename,"%s%d%s%d","cp data",best_result," final_data_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);
    
    sprintf(filename,"%s%d%s%d","mv gpic_",best_result," final_gpic_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);
 
    //fp = popen("rm data* gpic* energylist seed state.vmd cylinder_seed", "r");
    //pclose(fp);
   
    gp=fopen("N_vs_energy", "a");
    fprintf(gp,"%d %6.15f\n", NUM_PAR, lowest_energy);
    fclose(gp);

    sprintf(filename,"%s%d","mkdir NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);
    
    sprintf(filename,"%s%d","mv sorted_energylist NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);

    sprintf(filename,"%s%d","mv energylist* NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);
    
    sprintf(filename,"%s%d","mv final_data* NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);

    sprintf(filename,"%s%d","mv data* NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);

    sprintf(filename,"%s%d","mv SAEmatrix* NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);

    sprintf(filename,"%s%d","mv gpic* NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);
        
    sprintf(filename,"%s%d","mv final_gpic* NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);

    sprintf(filename,"%s%d","mv  sa_out* NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);
    
    sprintf(filename,"%s%d","mv  seed* NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);

    sprintf(filename,"%s%d","mv energy_* NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);

    sprintf(filename,"%s%d","mv temp_vs_step_length_* NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);

    sprintf(filename,"%s%d","mv  sa_gd_energy_list NUM_PAR_", NUM_PAR);
    fp = popen(filename, "r");
    pclose(fp);

    sprintf(filename,"%s","rm simulated_annealing_energy gradient_descent_energy cg_gpic cylinder_seed enthalpy*");
    fp = popen(filename, "r");
    pclose(fp);
      
   
    //sprintf(filename,"%s%d","mv simulated_annealing_energies NUM_PAR_", NUM_PAR);
    //fp = popen(filename, "r");
    //pclose(fp);

  }
  


}
  







