mkdir -p data
mkdir -p data/test_SA
mkdir -p data/test_sweep

home="/Users/jenswinkelmann/Desktop/TrinityCollege/Year_1/SoftSpherePacking/JENS"

for((i=1;i<=2;i++))#start at pressure p=0.015 and finish at pressure P=0.010
    do

    vv=$(printf "%03d" $i)

    #make all necessary directories
    cd ${home}/data/test_SA/ # Simulated Annealing
    mkdir P_0_$vv
    cd ${home}/data/test_sweep/ # Gradient descent
    mkdir P_0_$vv

    #### FOR SIMULATED ANNEALING ####
    cd ${home}/data/test_SA/P_0_$vv

    # Run program
    ${home}/sa_cg_cylinder_master 10.0 0.$vv 6 6 2.45 #run simulated annealing (10.0-maximum cylinder length - leave this alone), pressure, search for the minimum enthalpy starting with 6 particles and going up to 6 particles - in other words only do a search with N=6, at D/d=2.45 
    cp NUM_PAR_6/final_data_6  ../../test_sweep/P_0_$vv/cylinder_seed_original

    #### For Gradient descent ####
    cd ${home}/data/test_sweep/P_0_$vv

    ${home}/extend_unit_cell
    mv cylinder_seed_extended cylinder_seed_original
    rm cg_gpic_original #remove useles file

    ${home}/upward_sweep_CG_and_SD 0.$vv 2.45 0.0005 600 0.0 1 #run the gradient descent code, pressure, step size for D/d, number of steps, magnitude of jiggle - currently set to zero, how often to print results - currently every time step.

done


