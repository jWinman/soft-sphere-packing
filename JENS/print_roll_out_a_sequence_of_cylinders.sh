echo set size ratio -1 >> animation_index
echo unset key >> animation_index
echo set xrange[-3:10.0] >> animation_index
echo set yrange[0:10] >> animation_index

echo set terminal png >> animation_index

#echo set terminal png size 1000, 10000 >> animation_index
#for file in `\ls -v -r DATA_*`
for file in `\ls -v DATA_*`
do

filename=`echo $file|awk -FDATA_ '{print $2}'`
echo $filename
cp DATA_$filename FINAL_DATA
#Need to run this code TWICE:
/home/adil/Desktop/JENS/phyllotaxis
/home/adil/Desktop/JENS/phyllotaxis

mv chirality_A chirality_$filename 
mv linksA links_$filename
mv guide2_A guide_$filename
rm chirality_B euclidean_distances FINAL_DATA guide2_B linksB z_and_theta_coordinates


echo set title \"$filename\" >> animation_index
echo set output \'$filename.png\' >> animation_index
echo plot \"chirality_$filename\" pt 7 pointsize 3, \"links_$filename\" w lines lw 2 lc -1, \"guide_$filename\" w lines lw 3 >> animation_index

done

gnuplot animation_index

mkdir movie
cp *.png movie/
cd movie
ls | cat -n | while read n f; do mv "$f" "$n.png"; done 
rename 's/\d+/sprintf("%05d",$&)/e' *.png
avconv -f image2 -i %05d.png animation.avi
mv animation.avi ../
cd ..
rm -r movie/

mkdir roll_out
mkdir GEOMVIEW
mkdir SIM
mkdir FRAMES
mkdir GNUPLOT_FILES

mv cg_* GEOMVIEW
mv energy_* SIM/
mv *.png FRAMES/
mv links_*  chirality_*  guide_* animation_index GNUPLOT_FILES/
mv DATA_* SIM/

tar cvzf SIM.tar.gz SIM/
rm -r SIM/
tar cvzf GNUPLOT_FILES.tar.gz GNUPLOT_FILES
rm -r GNUPLOT_FILES

mv FRAMES GNUPLOT_FILES.tar.gz roll_out/
mv animation.avi roll_out/
