#include <stdio.h>
#include <time.h>
#include <math.h>

#define pi 3.141592653589793
#define radius 0.5
#define L 1.0
#define hlim 50.0
double modulo(double,double);

int main (void)
{
  int i,j,k;
  int cindx;
  int col;
  int NUM_PAR, NT_images, NB_images;
  int NTBimages;
  int Bcol[5000], Tcol[5000];
  double Energy, Pressure, Rlim, Clength, Angle;
  double opacity_func;
  double r[5000], th[5000], x[5000], y[5000], z[5000];
  double nr[5000], nth[5000], nx[5000], ny[5000], nz[5000];
  double Br[5000], Bth[5000], Bz[5000], Bx[5000], By[5000];
  double Tr[5000], Tth[5000], Tz[5000], Tx[5000], Ty[5000];
  double xout, yout, zout;
  double xo, yo, zo;
  double bottom_angle, bottom_height;
  double topz, botz, topz_temp, botz_temp;
  FILE *fp,*gp,*hp, *ip;
  char filename[100];		
  
  NTBimages=1;

  ///////////////////////////////////////////////////////////////////
  //READ IN DATA FILE
  ////////////////////////////////////////////////////////////////////
  fp=fopen("cylinder_seed_original", "r");
  fscanf(fp,"%lf", &Pressure);
  printf("Pressure=%6.15f\n", Pressure);
  fscanf(fp,"%d", &NUM_PAR);
  printf("NUM_PAR=%d\n", NUM_PAR);
  fscanf(fp,"%lf", &Rlim);
  printf("Rlim=%6.15f\n", Rlim);
  fscanf(fp,"%lf", &Clength);
  printf("Clength=%6.15f\n", Clength);

  for(i=1;i<=NUM_PAR;i++){
    fscanf(fp,"%lf", &r[i]);
    fscanf(fp,"%lf", &th[i]);
    fscanf(fp,"%lf", &z[i]);
    printf("%6.15f %6.15f %6.15f\n", r[i], th[i], z[i]);
    x[i]=r[i]*cos(th[i]);
    y[i]=r[i]*sin(th[i]);
  }
  fscanf(fp,"%lf", &Angle);
  printf("Angle=%6.15f\n", Angle);
  printf("\n");
  fclose(fp);
  //End of reading data file
  ////////////////////////////////////////////////////////////////////
  topz=0.0;botz=0.0;

  //if(NUM_PAR==1){
  //NTBimages=3
  //}

  col=0;
  //Generate bottom images
  k=0;
  topz=0.0;
  topz_temp=topz;
  for(j=0;j<=NTBimages;j++){

 
    for (i = 1; i <= NUM_PAR; i++){
      if( ((j*Clength)+z[i]) <= hlim){
	col++;
	k++;
	Bz[k]=(j*Clength)+z[i];
	topz_temp=Bz[k];
	Bth[k]=modulo(th[i] + (j*Angle), 2*pi);
	Br[k]=r[i];
	Bx[k]=Br[k]*cos(Bth[k]);
	By[k]=Br[k]*sin(Bth[k]);
	Bcol[k]=col;

	if(topz_temp>topz){topz=topz_temp;}
      }	
    }
  }
  NB_images=k;

  
  //Generate top images
  k=0;
  botz=0.0;
  botz_temp=botz;
  for(j=0;j<=NTBimages;j++){



    for (i = 1; i <= NUM_PAR; i++){
      if( (-((j*Clength)-z[i])) >= -hlim){
	col++;
	k++;
	Tz[k]=-((j*Clength)-z[i]);
	botz_temp=Tz[k];
	Tth[k]=modulo(th[i] - (j*Angle), 2*pi);
	Tr[k]=r[i];
	Tx[k]=Tr[k]*cos(Tth[k]);
	Ty[k]=Tr[k]*sin(Tth[k]);
	Tcol[k]=col;

	if(botz_temp<botz){botz=botz_temp;}
      }	
    }
  }
  NT_images=k;
  
  fp=fopen("cylinder_seed_extended", "w");
  fprintf(fp,"%6.15f\n", Pressure);
  fprintf(fp,"%d\n", NUM_PAR+(NTBimages*NUM_PAR));
  fprintf(fp,"%6.15f\n", Rlim);
  fprintf(fp,"%6.15f\n", Clength+(Clength*NTBimages));
  //for(i=1;i<=NUM_PAR;i++){
  //fprintf(fp,"%6.15f %6.15f %6.15f\n", r[i], th[i], z[i]);
  //}
  for(i=1;i<=NB_images;i++){
    fprintf(fp,"%6.15f %6.15f %6.15f\n", Br[i], Bth[i], Bz[i]);
  }
  fprintf(fp,"%6.15f\n", Angle+(Angle*NTBimages));
  fclose(fp);
 
  
  fp=fopen("cg_gpic_original", "w");  
  fprintf(fp, "LIST\n");
  for(i = 1; i <= NB_images; i++){
    xo=Br[i]*cos(Bth[i]);
    yo=Br[i]*sin(Bth[i]);
    zo=Bz[i];
    fprintf(fp, "{\n");
    fprintf(fp, "SPHERE\n");
    fprintf(fp,"%6.15f\n", L/2.0);
    fprintf(fp,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fp,"}\n");
  }
  for(i = 1; i <= NB_images; i++){
    xo=Br[i]*cos(Bth[i] +  Angle+(Angle*NTBimages) );
    yo=Br[i]*sin(Bth[i] +  Angle+(Angle*NTBimages) );
    zo=Bz[i]+Clength+(Clength*NTBimages);
    fprintf(fp, "{\n");
    fprintf(fp, "SPHERE\n");
    fprintf(fp,"%6.15f\n", L/2.0);
    fprintf(fp,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fp,"}\n");
  }
  for(i = 1; i <= NB_images; i++){
    xo=Br[i]*cos(Bth[i]- Angle-(Angle*NTBimages));
    yo=Br[i]*sin(Bth[i]- Angle-(Angle*NTBimages));
    zo=Bz[i]-Clength-(Clength*NTBimages);
    fprintf(fp, "{\n");
    fprintf(fp, "SPHERE\n");
    fprintf(fp,"%6.15f\n", L/2.0);
    fprintf(fp,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fp,"}\n");
  }
  fclose(fp);
  

}



double modulo(double num,double limit)
{
  return num-floor(num/limit)*limit;
}



/*
fp=fopen("mathematica_output", "w");
  Gp=fopen("hue_list", "w");
  hp=fopen("opacity_list", "w");
  ip=fopen("unit_cell", "w");
  for(i=1;i<=NUM_PAR;i++){
    xout=r[i]*cos(th[i]);
    yout=r[i]*sin(th[i]);
    zout=z[i];
    fprintf(fp,"%6.15f %6.15f %6.15f\n",xout, yout, zout);
    if(sqrt((xout*xout)+(yout*yout))< 0.05){
      printf("%6.6f %6.6f %6.6f\n", xout, yout, zout);
    }  
    fprintf(ip,"%6.15f %6.15f %6.15f\n",xout, yout, zout);  
    fprintf(gp,"0.2\n"); 
    fprintf(hp,"1.0\n");   
  }
  for(i=1;i<=NB_images;i++){
    xout=Br[i]*cos(Bth[i]);
    yout=Br[i]*sin(Bth[i]);
    zout=Bz[i];
    fprintf(fp,"%6.15f %6.15f %6.15f\n",xout, yout, zout);
    if(sqrt((xout*xout)+(yout*yout))< 0.5){
      printf("%6.6f %6.6f %6.6f\n", xout, yout, zout);
    }    
    fprintf(gp,"0.4\n");  
    opacity_func=sqrt( ((zout-Clength)/(NTBimages*Clength))*((zout-Clength)/(NTBimages*Clength)) ) +0.2;
    fprintf(hp,"%6.6f\n", opacity_func);     
  }
  for(i=1;i<=NT_images;i++){
    xout=Tr[i]*cos(Tth[i]);
    yout=Tr[i]*sin(Tth[i]);
    zout=Tz[i];
    fprintf(fp,"%6.15f %6.15f %6.15f\n",xout, yout, zout);
    if(sqrt((xout*xout)+(yout*yout))< 0.05){
      printf("%6.6f %6.6f %6.6f\n", xout, yout, zout);
    }   
    fprintf(gp,"0.4\n"); 
    opacity_func=sqrt( (zout/(NTBimages*Clength))*(zout/(NTBimages*Clength)) ) +0.2;
    fprintf(hp,"%6.6f\n",opacity_func);   
  }
  fclose(fp);
  fclose(gp);
  fclose(hp);
  fclose(ip);
*/


/*

 fp=fopen("picture.vmd", "w");
  fprintf(fp,"mol new\n");
  fprintf(fp,"draw material Opaque\n");
  for(i=1;i<=NUM_PAR;i++){
    fprintf(fp,"draw color     136\n" );
    fprintf(fp,"draw sphere {");
    fprintf(fp,"        %6.15f",x[i]); 
    fprintf(fp,"        %6.15f",z[i]); 
    fprintf(fp,"        %6.15f}",y[i]); 
    fprintf(fp," radius        %6.6f", radius);
    fprintf(fp," resolution  20\n");
  }
  
 
  for(i=1;i<=NB_images;i++){
    fprintf(fp,"draw color     %d\n", Bcol[i]);
    fprintf(fp,"draw sphere {");
    fprintf(fp,"        %6.15f",Bx[i]); 
    fprintf(fp,"        %6.15f",Bz[i]); 
    fprintf(fp,"        %6.15f}",By[i]); 
    fprintf(fp," radius        %6.6f", radius);
    fprintf(fp," resolution  20\n");
  }

  for(i=1;i<=NT_images;i++){  
    fprintf(fp,"draw color     %d\n", Tcol[i]);
    fprintf(fp,"draw sphere {");
    fprintf(fp,"        %6.15f",Tx[i]); 
    fprintf(fp,"        %6.15f",Tz[i]); 
    fprintf(fp,"        %6.15f}",Ty[i]); 
    fprintf(fp," radius        %6.6f", radius);
    fprintf(fp," resolution  20\n");
  }

  fprintf(fp,"mol new\n");
  fprintf(fp,"draw material Transparent\n");
  fprintf(fp,"draw color  800\n");
  fprintf(fp,"draw cylinder {   0.0  %6.6f   0.0} {   0.0   %6.6f   0.0} radius  %6.6f resolution 20 filled no\n", botz, topz, (Rlim-0.5)+0.02);
  
  fprintf(fp,"display resetview\n");
  fprintf(fp,"light 0 on\n");
  fprintf(fp,"light 1 on\n");
  fprintf(fp,"light 2 off\n");
  fprintf(fp,"light 3 off\n");
  fprintf(fp,"axes location off\n");
  fprintf(fp,"display projection orthographic\n");
  

  fclose(fp);

  


*/

