#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#define pi 3.141592653589793238462643383279502884
#define BS 1.0
#define Htol 0.0000000000001
#define Emax 100
#define L 1.0

double modulo(double num,double limit)
{
  return num-floor(num/limit)*limit;
}

int main (int argc, char *argv[])	
{
  int i, j, k;
  int t;
  int NUM_PAR, NT_images, NB_images;
  int counter, ecounter;
  int acc;

  double pos;
  double P, Pold, A, Rlim, Rlim_old, initial_Clength, Clength;
  double r[5000], th[5000], z[5000];
  double Tr[5000], Tth[5000], Tz[5000];
  double Br[5000], Bth[5000], Bz[5000];
  double energy, enthalpy, fr[5000], fth[5000], fz[5000], fl, fA;
  double enthalpy_old, denthalpy, absdenthalpy;
  double dr, dth, dz, dl, dA;
  double xo, yo, zo;
  double part1, part2, part3, Etemp;
  double dist, pre_dist;
  double cylinder, image, boundary, pressure, D;
  double volume;
  double distance_from_boundary;

  double xi, yi, zi;
  double xj, yj, zj;

  double ziprime, zjprime;
  double Bziprime, Bzjprime;
  double Tziprime, Tzjprime;
  double dlpart1, dlpart2;

  double GDtime;

  int TLIM;
  double lambda;

  char filename[100];
  FILE *fpt;
  

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////    INITIALISE     ///////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //Read in command line options
  sscanf(argv[1],"%lf",&lambda); //Step size
  sscanf(argv[2],"%d",&TLIM); //Number of GD steps
  sscanf(argv[3],"%lf",&P); // Target pressure
  sscanf(argv[4],"%lf",&D); //Target diameter

  ecounter=Emax-1;//Initialise counter, set to print out the energy of the system every Emax number of time steps 
  GDtime=0.0;//Set GD time to zero

  fpt=fopen("cylinder_seed", "r");
  fscanf(fpt, "%lf", &Pold); //This is the current pressure 
  fscanf(fpt, "%d", &NUM_PAR); //Number of particles in systems
  fscanf(fpt, "%lf", &Rlim_old); //Effectively the previous tube diameter - expressesd in terms of Rlim
  fscanf(fpt, "%lf", &initial_Clength); //The current cylinder length
  Clength=initial_Clength;  //set Clength to the current cylinder
  for(i = 1; i <= NUM_PAR; i++){ //read in the coordinates 
    fscanf(fpt, "%lf", &pos);
    r[i]=pos;
    fscanf(fpt, "%lf", &pos);
    th[i]=pos;
    fscanf(fpt, "%lf", &pos);
    z[i]=pos;
  }
  fscanf(fpt, "%lf", &A);
  fclose(fpt); 
  Rlim=(D+1.0)/2.0; //Compute Rlim from the target diameter
  enthalpy_old=0.0;
 
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////    START GRADIENT DESCENT SIMULATION     ////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  for(t=0; t<=TLIM;t++){ 

    GDtime=lambda*t; //current GD time
    ecounter++; //increament counter

    
    ////////////////////////////////////////////////////////////////////////////
    //Apply boundary conditions
    ////////////////////////////////////////////////////////////////////////////
    Clength=sqrt(Clength*Clength); //Don't let Clength become negative
    for(i=1;i<=NUM_PAR;i++){//ensure that no particles can escape the system from the top of the cylinder
      if(z[i]>Clength){
	z[i]=Clength;
	fz[i]=0.0;
      }
      if(z[i]<0.0){//ensure that no particles can escape the system from the top of the cylinder
	z[i]=0.0;
	fz[i]=0.0;
      }
    }
    for(i=1;i<=NUM_PAR;i++){//prevent particles moving beyond a radius Rlim
      r[i]=sqrt(r[i]*r[i]);    
      if(r[i] > Rlim){
	r[i]=Rlim;
      }
    }
    A=modulo(A, 2*pi);
    ////////////////////////////////////////////////////////////////////////////



    

    ////////////////////////////////////////////////////////////////////////////
    //set energy and all forces to zero.
    ////////////////////////////////////////////////////////////////////////////
    energy=0.0;
    enthalpy=0.0;
    for(i=1;i<=NUM_PAR;i++){
      fr[i]=0.0; fth[i]=0.0; fz[i]=0.0;
    }
    fl=0.0;
    fA=0.0;
    ////////////////////////////////////////////////////////////////////////////





    

    ////////////////////////////////////////////////////////////////////////////
    //generate image particles
    ///////////////////////////////////////////////////////////////////////////
    //find all the particles at the bottom of the cylinder
    k=0;
    for (i = 1; i <= NUM_PAR; i++){//From these generate image particles at the TOP of the simulation cell
      if(z[i] < L){
	k++;
	Bz[k]=Clength+z[i];
	Bth[k]=modulo(th[i] + A, 2*pi);
	Br[k]=r[i];
      }
    }
    NB_images=k; //Number of B[] image particles
    
    //find all the particles at the top of the cylinder
    k=0;
    for (i = 1; i <= NUM_PAR; i++){//From these generate image particles at the BOTTOM of the simulation cell
      if(z[i] > Clength - L){
	k++;
	Tz[k]=-(Clength - z[i]);
	Tth[k]=modulo(th[i] - A, 2*pi);
	Tr[k]=r[i];
      }
    }
    NT_images=k; //Number of T[] image particles
    //////////////////////////////////////////////////////////////////////////    






    
    //////////////////////////////////////////////////////////////////////////
    //compute the energy of the system
    //////////////////////////////////////////////////////////////////////////
    energy=0.0; //The energy due to interactions between particle E.
    enthalpy=0.0;// H=E+pV    

    ////Energy due to interaction between particles in the simulation cell///
    cylinder=0.0; 
    for (i = 1; i <= NUM_PAR; i++){
      for (j = 1; j <= NUM_PAR; j++){
	if(i != j ){
	  part1= (r[i]*r[i]) + (r[j]*r[j]) - ( 2*r[i]*r[j]*cos(th[i]-th[j]) );
	  part2= ( (z[i]*z[i]) + (z[j]*z[j]) - (2*z[i]*z[j]) );
	  pre_dist=part1+part2;
	  dist=sqrt(pre_dist);
	  if(dist<=L){
	    Etemp = 0.5*(0.5*(dist-L)*(dist-L)); //NOTE: the first 0.5 is due to double counting, the second is from Hooke's Law, i.e. if E=(1/2)Kx^2 then F=-dE/dx=-Kx. So spring constant K=1 in our simulations.
	    cylinder+=Etemp;
	  }
	}
      }
    }
    /////////////////////////////////////////////////////////////////////////




    
    
    //// Energy due to interaction with the image particles///
    //// Note the energy in both cases here is defined as:
    //// 0.5*(0.5*(dist-L)*(dist-L));
    
    //// One of the factors of 0.5 is again just due to Hooke's Law.

    //// Now lets discuss the other factor of 0.5:
    //// Consider two particles i and j in the simulation cell.
    //// These particles are not close enough inside the cell to interact directly,
    //// but the do interact via their images. There also exist the images of i - that is Ti and Bi.
    //// And the images of j - that is Tj and Bj. Thus if we consider the interaction i---Tj
    //// and j---Bi these two have the same energy. Actuall, they are the same interaction... counted twice
    //// and to prevent double counting we multiply by 0.5.
   
    image=0.0;
    for(i=1;i<=NUM_PAR;i++){
      for(j=1;j<=NB_images;j++){
	part1= (r[i]*r[i]) + (Br[j]*Br[j]) - ( 2*r[i]*Br[j]*cos(th[i]-Bth[j]) );
	part2= (z[i]*z[i]) + (Bz[j]*Bz[j]) - (2*z[i]*Bz[j]);
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
	if(dist<=L){
	  Etemp = 0.5*(0.5*(dist-L)*(dist-L)); 
	  image+=Etemp;
	}
      }
    }
    
    for(i=1;i<=NUM_PAR;i++){
      for(j=1;j<=NT_images;j++){
	part1= (r[i]*r[i]) + (Tr[j]*Tr[j]) - ( 2*r[i]*Tr[j]*cos(th[i]-Tth[j]) );
	part2= (z[i]*z[i]) + (Tz[j]*Tz[j]) - (2*z[i]*Tz[j]);
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
	if(dist<=L){
	  Etemp = 0.5*(0.5*(dist-L)*(dist-L));
	  image+=Etemp;
	}
      }
    }
    //////////////////////////////////////////////////////////



    
    
    ////Energy due to interaction between particles and the boundary///
    boundary=0.0;
    for(i=1; i<=NUM_PAR; i++){
      dist=r[i];
      distance_from_boundary=Rlim-dist;
      if(distance_from_boundary<=L){
	Etemp=BS*0.5*(distance_from_boundary-L)*(distance_from_boundary-L);
	boundary+=Etemp;
      }
    }
    ///////////////////////////////////////////////////////////////////



    

    ////Energy due to pressure term////////////////////////////////////
    pressure=0.0;
    volume=pi*((D/2.0)*(D/2.0))*Clength;
    pressure=P*volume;
    ///////////////////////////////////////////////////////////////////



    

    //////// Final energy //////////////////////////////////////
    energy=cylinder+image+boundary;
    enthalpy = energy+pressure;
    ////////////////////////////////////////////////////////////




    
   
    //////////////////////////////////////////////////////////////////////////
    //comupte the force on each particle
    //////////////////////////////////////////////////////////////////////////

    // We compute the force on the ith particle in each case.
    // If we consider the energy of interaction between the ith and jth particles, the energy is Eij=1/2 K (ri-rj)^2
    // There is only ONE term for each pair of particles, differentiating this term with respect to i gives the ith derivative
    // from which we have the force on the ith particle
    for(i=1;i<= NUM_PAR;i++){
      for(j=1;j<= NUM_PAR;j++){
	if(i != j){
	  	  
	  part1= (r[i]*r[i]) + (r[j]*r[j]) - ( 2*r[i]*r[j]*cos(th[i]-th[j]) );	  
	  part2=( (z[i]*z[i]) + (z[j]*z[j]) - (2*z[i]*z[j]) );
	  pre_dist=part1+part2;
	  dist=sqrt(pre_dist);
	  
	  if(dist<=L){// Derivative with respect to the coordinates of the ith particle, converted into a force
	    part3=((dist-L)/dist);
	    dr=( r[i] - (  r[j]*(cos(th[i]-th[j]))  ) )*part3;
	    fr[i]-=dr; 
	    dth=(r[i]*r[j])*sin(th[i]-th[j])*part3;
	    fth[i]-=dth; 
	    dz=(z[i]-z[j])*part3;
	    fz[i]-=dz; 
	  }
	}     
      }
    }

    // We compute the force on the ith particle in each case.
    // Again there is a single term for the interaction between the ith particle and the image of the jth particle
    for(i=1;i<=NUM_PAR;i++){
      for(j=1;j<=NB_images;j++){
	
	part1= (r[i]*r[i]) + (Br[j]*Br[j]) - ( 2*r[i]*Br[j]*cos(th[i]-Bth[j]) );
	part2= ( (z[i]*z[i]) + (Bz[j]*Bz[j]) - (2*z[i]*Bz[j]) );
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
	
	if(dist<=L){
	  part3=( (dist-L)/dist );
	  dr = ( r[i]  -   (  Br[j]*(cos(th[i]-Bth[j]))  ) )*part3;
	  fr[i] -= dr; 
	  dth=(r[i]*Br[j])*sin(th[i]-Bth[j])*part3;
	  fth[i] -= dth; 
	  dz=(z[i]-Bz[j])*part3;
	  fz[i] -=dz;
	  
	  //Force twisting and pushing away the image particles.
	  //Note, the factor of 0.5 since we count the interaction here
	  //and again in the next loop over the image particles.
	  dA=-0.5*dth; //Equal and opposite -dth
	  fA-= dA;
	  dl=-0.5*dz; //Equal and opposite -dz 
	  fl -= dl;
	  
	}
      }
    }    
    for(i=1;i<=NUM_PAR;i++){
      for(j=1;j<=NT_images;j++){
		
	part1= (r[i]*r[i]) + (Tr[j]*Tr[j]) - ( 2*r[i]*Tr[j]*cos(th[i]-Tth[j]) );
	part2= (z[i]*z[i]) + (Tz[j]*Tz[j]) - (2*z[i]*Tz[j]);
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
		
	if(dist<=L){
	  part3=(dist-L)/dist;
	  dr= ( r[i]  -   (  Tr[j]*(cos(th[i]-Tth[j]))  ) )*part3;
	  fr[i] -= dr;
	  dth=(r[i]*Tr[j])*sin(th[i]-Tth[j])*part3;
	  fth[i] -= dth;
	  dz=(z[i]-Tz[j])*part3;	
	  fz[i] -= dz;

	  //Force twisting and pushing away the image particles.
	  //Note, the factor of 0.5 since we count the interaction here
	  //and in the previous loop over the image particles.
	  dA=0.5*dth; //Equal and opposite -dth
	  fA -= dA;
	  dl= 0.5*dz; //Equal and opposite -dz 
	  fl -= dl;
	}
      }
    }

    // Pressure term
    dl=P*pi*((D/2.0)*(D/2.0));
    fl-=dl;

    //compute forces on particles due to the cylindrical boundary
    for(i=1; i<=NUM_PAR; i++){
      dist=r[i];
      distance_from_boundary=Rlim-dist;
      if(distance_from_boundary<=L){
	dr=-(BS)*(distance_from_boundary-L);
	fr[i]-=dr;
      }
    }

    //////////////////////////////////////////////////////////////////////////
    //Update the position of the particles
    //////////////////////////////////////////////////////////////////////////

    //Move the particles and the boundary.
    for(i=1;i<=NUM_PAR;i++){
      r[i]=r[i]+(lambda*fr[i]);
      th[i]=th[i]+(lambda*fth[i]);
      z[i]=z[i]+(lambda*fz[i]);      
    }
    A=A+(lambda*fA);
    Clength=Clength+(lambda*fl);



    ////////////////////////////////////////////////////////////////////////////
    //Apply boundary conditions again
    ////////////////////////////////////////////////////////////////////////////
    Clength=sqrt(Clength*Clength);
    for(i=1;i<=NUM_PAR;i++){
      if(z[i]>Clength){
	z[i]=Clength;
	fz[i]=0.0;
      }
      if(z[i]<0.0){
	z[i]=0.0;
	fz[i]=0.0;
      }
    }
    for(i=1;i<=NUM_PAR;i++){
      r[i]=sqrt(r[i]*r[i]);    
      if(r[i] > Rlim){
	r[i]=Rlim;
      }
    }
    A=modulo(A, 2*pi);
    ////////////////////////////////////////////////////////////////////////////



    ////////////////////////////////////////////////////////////////////////////
    //Print out state of the system
    ////////////////////////////////////////////////////////////////////////////
    if(ecounter==Emax){
      ecounter=0;
      fpt=fopen("energy", "a");
      fprintf(fpt, "%6.15f %6.15f\n", GDtime, enthalpy/(1.0*NUM_PAR));
      fclose(fpt);
    }

    denthalpy=enthalpy-enthalpy_old;
    absdenthalpy=sqrt(denthalpy*denthalpy);
    if(absdenthalpy<=Htol){
      break;
    }
    enthalpy_old=enthalpy;
    
  }//***** END OF SIMULATION *****///////////////////////////////////////////////

  printf("iter=%d\n", t);
  
  //Print out final state of the system
  fpt=fopen("gradient_descent_energy", "a");
  fprintf(fpt, "%6.15f %6.15f %d %6.15f\n",P, D, t, enthalpy/(1.0*NUM_PAR));
  fclose(fpt);

  fpt=fopen("enthalpy_breakdown", "a");
  fprintf(fpt, "%6.15f %6.15f %6.15f %6.15f %6.15f\n",P, D, energy/(1.0*NUM_PAR), pressure/(1.0*NUM_PAR), enthalpy/(1.0*NUM_PAR));
  fclose(fpt);

  //Print out the coordinates ...etc... to be used in the next loop....
  fpt=fopen("DATA", "w");
  fprintf(fpt, "%6.15f\n",P);
  fprintf(fpt, "%d\n", NUM_PAR);
  fprintf(fpt, "%6.15f\n", Rlim);
  fprintf(fpt, "%6.15f\n", Clength);
  for(i = 1; i <= NUM_PAR; i++){
    fprintf(fpt, "%6.15f %6.15f %6.15f\n",r[i], th[i], z[i]);
  }
  fprintf(fpt, "%6.15f\n", A);
  fclose(fpt);


  // Print out Geomview file........
  fpt=fopen("cg_gpic", "w");  
  fprintf(fpt, "LIST\n");
  for(i = 1; i <= NUM_PAR; i++){
    xo=r[i]*cos(th[i]);
    yo=r[i]*sin(th[i]);
    zo=z[i];
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", L/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fpt,"}\n");
    
    xo=r[i]*cos(th[i]+A);
    yo=r[i]*sin(th[i]+A);
    zo=Clength+z[i];
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", L/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fpt,"}\n");
    
    xo=r[i]*cos(th[i]-A);
    yo=r[i]*sin(th[i]-A);
    zo=-(Clength - z[i]);
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", L/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fpt,"}\n");

  }
  fclose(fpt);

 

}//END
