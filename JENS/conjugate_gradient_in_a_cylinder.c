#define NRANSI
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#define diagnostic 0
#define pdiagnostic 0
#define sub_diagnostic 0

#define BS 1.0 

//#include "nrutil.h"
//#include "nr.h"

#define NR_END 1
#define FREE_ARG char*
#define GOLD 1.618034l
#define GLIMIT 100.0
#define TINY 1.0e-20
#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);
#define CGOLD 0.3819660
#define ZEPS 1.0e-10
#define FTOL 1.0e-15
#define TOL 2.0e-7
//#define TOL 2.0e-10
//#define ITMAX 200
#define ITMAX 500000
#define EPS 1.0e-10
#define FREEALL free_vector(xi,1,n);free_vector(h,1,n);free_vector(g,1,n);

#define pi 3.141592653589793238462643383279502884

#define small 0.0000000001
#define L 1.0
#define RUNS 0

double modulo(double,double);

static double maxarg1,maxarg2;
#define FMAX(a,b) (maxarg1=(a),maxarg2=(b),(maxarg1) > (maxarg2) ?\
        (maxarg1) : (maxarg2))

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))



void nrerror(char error_text[])
/* Numerical Recipes standard error handler */
{
  FILE *gpt;
  fprintf(stderr,"Numerical Recipes run-time error...\n");
  fprintf(stderr,"%s\n",error_text);
  fprintf(stderr,"...now exiting to system...\n");
  gpt=fopen("FAIL", "w");
  fprintf(gpt,"0\n");
  fclose(gpt);
  exit(1);
}

double *vector(nl,nh)
long nh,nl;
/* allocate a double vector with subscript range v[nl..nh] */
{
	double *v;

	v=(double *)malloc((unsigned int) ((nh-nl+1+NR_END)*sizeof(double)));
	if (!v) nrerror("allocation failure in vector()");
	return v-nl+NR_END;
}

void free_vector(double *v, long nl, long nh)
/* free a double vector allocated with vector() */
{
	free((FREE_ARG) (v+nl-NR_END));
}

/******************************************************************************/
/*			  f1dim						                                      */ 
/******************************************************************************/
extern int ncom;
extern double *pcom,*xicom,(*nrfunc)(double []);

double f1dim(double x)
{
	int j;
	double f,*xt;

	xt=vector(1,ncom);
	for (j=1;j<=ncom;j++) xt[j]=pcom[j]+x*xicom[j];
	f=(*nrfunc)(xt);
	free_vector(xt,1,ncom);
	return f;
}

/* (C) Copr. 1986-92 Numerical Recipes Software 5.){2p491&].#RsL. */

/**************************************************************************/
/*			     mnbrac					  */
/**************************************************************************/
void mnbrak(double *ax, double *bx, double *cx, double *fa, double *fb, double *fc,
	double (*func)(double))
{
	double ulim,u,r,q,fu,dum;

	*fa=(*func)(*ax);
	*fb=(*func)(*bx);
	if (*fb > *fa) {
		SHFT(dum,*ax,*bx,dum)
		SHFT(dum,*fb,*fa,dum)
	}
	*cx=(*bx)+GOLD*(*bx-*ax);
	*fc=(*func)(*cx);
	while (*fb > *fc) {
		r=(*bx-*ax)*(*fb-*fc);
		q=(*bx-*cx)*(*fb-*fa);
		u=(*bx)-((*bx-*cx)*q-(*bx-*ax)*r)/
			(2.0*SIGN(FMAX(fabs(q-r),TINY),q-r));
		ulim=(*bx)+GLIMIT*(*cx-*bx);
		if ((*bx-u)*(u-*cx) > 0.0) {
			fu=(*func)(u);
			if (fu < *fc) {
				*ax=(*bx);
				*bx=u;
				*fa=(*fb);
				*fb=fu;
				return;
			} else if (fu > *fb) {
				*cx=u;
				*fc=fu;
				return;
			}
			u=(*cx)+GOLD*(*cx-*bx);
			fu=(*func)(u);
		} else if ((*cx-u)*(u-ulim) > 0.0) {
			fu=(*func)(u);
			if (fu < *fc) {
				SHFT(*bx,*cx,u,*cx+GOLD*(*cx-*bx))
				SHFT(*fb,*fc,fu,(*func)(u))
			}
		} else if ((u-ulim)*(ulim-*cx) >= 0.0) {
			u=ulim;
			fu=(*func)(u);
		} else {
			u=(*cx)+GOLD*(*cx-*bx);
			fu=(*func)(u);
		}
		SHFT(*ax,*bx,*cx,u)
		SHFT(*fa,*fb,*fc,fu)
	}
}

/* (C) Copr. 1986-92 Numerical Recipes Software 5.){2p491&].#RsL. */

/****************************************************************************/
/*    brent								    */
/****************************************************************************/
double brent(double ax, double bx, double cx, double (*f)(double), double tol,
	double *xmin)
{
	int iter;
	double a,b,d,etemp,fu,fv,fw,fx,p,q,r,tol1,tol2,u,v,w,x,xm;
	double e=0.0;

	a=(ax < cx ? ax : cx);
	b=(ax > cx ? ax : cx);
	x=w=v=bx;
	fw=fv=fx=(*f)(x);
	for (iter=1;iter<=ITMAX;iter++) {

		xm=0.5*(a+b);
		tol2=2.0*(tol1=tol*fabs(x)+ZEPS);
		if (fabs(x-xm) <= (tol2-0.5*(b-a))) {
			*xmin=x;
			return fx;
		}
		if (fabs(e) > tol1) {
			r=(x-w)*(fx-fv);
			q=(x-v)*(fx-fw);
			p=(x-v)*q-(x-w)*r;
			q=2.0*(q-r);
			if (q > 0.0) p = -p;
			q=fabs(q);
			etemp=e;
			e=d;
			if (fabs(p) >= fabs(0.5*q*etemp) || p <= q*(a-x) || p >= q*(b-x))
				d=CGOLD*(e=(x >= xm ? a-x : b-x));
			else {
				d=p/q;
				u=x+d;
				if (u-a < tol2 || b-u < tol2)
					d=SIGN(tol1,xm-x);
			}
		} else {
			d=CGOLD*(e=(x >= xm ? a-x : b-x));
		}
		u=(fabs(d) >= tol1 ? x+d : x+SIGN(tol1,d));
		fu=(*f)(u);
		if (fu <= fx) {
			if (u >= x) a=x; else b=x;
			SHFT(v,w,x,u)
			SHFT(fv,fw,fx,fu)
		} else {
			if (u < x) a=u; else b=u;
			if (fu <= fw || w == x) {
				v=w;
				w=u;
				fv=fw;
				fw=fu;
			} else if (fu <= fv || v == x || v == w) {
				v=u;
				fv=fu;
			}
		}
	}
	nrerror("Too many iterations in brent");
	*xmin=x;
	return fx;
}

/* (C) Copr. 1986-92 Numerical Recipes Software 5.){2p491&].#RsL. */

/*****************************************************************************/
/*	    linmin							     */
/*****************************************************************************/
int ncom;
double *pcom,*xicom,(*nrfunc)(double []);

void linmin(double p[], double xi[], int n, double *fret, double (*func)(double []))
{
	double brent(double ax, double bx, double cx,
		double (*f)(double), double tol, double *xmin);
	double f1dim(double x);
	void mnbrak(double *ax, double *bx, double *cx, double *fa, double *fb,
		double *fc, double (*func)(double));
	int j;
	double xx,xmin,fx,fb,fa,bx,ax;

	ncom=n;
	pcom=vector(1,n);
	xicom=vector(1,n);
	nrfunc=func;
	for (j=1;j<=n;j++) {
		pcom[j]=p[j];
		xicom[j]=xi[j];
	}
	ax=0.0;
	xx=1.0;
	mnbrak(&ax,&xx,&bx,&fa,&fx,&fb,f1dim);
	*fret=brent(ax,xx,bx,f1dim,TOL,&xmin);
	for (j=1;j<=n;j++) {
		xi[j] *= xmin;
		p[j] += xi[j];
	}
	free_vector(xicom,1,n);
	free_vector(pcom,1,n);
}

/* (C) Copr. 1986-92 Numerical Recipes Software 5.){2p491&].#RsL. */

/*****************************************************************************/
/*	 frprm								     */
/*****************************************************************************/
void frprmn(double p[], int n, double ftol, int *iter, double *fret,
	double (*func)(double []), void (*dfunc)(double [], double []))
{
	void linmin(double p[], double xi[], int n, double *fret,
		double (*func)(double []));
	int j,its;
	double gg,gam,fp,dgg;
	double *g,*h,*xi;
	static int kkk =0;	

        int hind; ////

	g=vector(1,n);
	h=vector(1,n);
	xi=vector(1,n);
	fp=(*func)(p);
	(*dfunc)(p,xi);
	for (j=1;j<=n;j++) {
		g[j] = -xi[j];
		xi[j]=h[j]=g[j];
	}
	for (its=1;its<=ITMAX;its++) {
		*iter=its;
	
		kkk++;

		if(sub_diagnostic==1){
		  printf("%d\n",kkk);
		}

		linmin(p,xi,n,fret,func);

		if (2.0*fabs(*fret-fp) <= ftol*(fabs(*fret)+fabs(fp)+EPS)) {
			FREEALL
			return;
		}
		fp=(*func)(p);
		(*dfunc)(p,xi);
		dgg=gg=0.0;
		for (j=1;j<=n;j++) {
			gg += g[j]*g[j];
                        dgg += xi[j]*xi[j]; //Fletcher-Reeves
			//dgg += (xi[j]+g[j])*xi[j]; //Polak-Ribiere
		}
		if (gg == 0.0) {
			FREEALL
			return;
		}
		gam=dgg/gg;
		for (j=1;j<=n;j++) {
			g[j] = -xi[j];
			xi[j]=h[j]=g[j]+gam*h[j];
		}
	}
	nrerror("Too many iterations in frprmn");          /*2*/
}

/* (C) Copr. 1986-92 Numerical Recipes Software 5.){2p491&].#RsL. */

/****************************************************************************/
/*          Driver							    */
/****************************************************************************/
double x[10000], a[10000], xterm;
double numtemp;
double cylinder, harmonic, energy, enthalpy;
double boundary, image, penergy;
double volume, diameter, pressure;
double distance_from_boundary;
double delta_x, delta_y;
double part1, part2, part3;
double dist, pre_dist;
int NDIM, NUM_PAR;
double CH[10000];
int i, j, k; 

int TimageID[5000], BimageID[5000];
int NT_images, NB_images;
double T[15000], B[15000];
double new_z, new_th, new_A;

double initial_Clength, Clength, Rlim, A;

double func(double x[])
{  
  double Etemp;

  //////////////////////////////////////////////////////////////////////////
  //IMPOSE BOUNDARY CONDITIONS
  //////////////////////////////////////////////////////////////////////////
  
  //Z BOUNDARY CONDITION ///////////////////////////////////////////////////////////////////////////////////
  for(i=1; i<=NUM_PAR; i++){
    //ensure that no particles can escape the system from the top of the cylinder
    if(x[i*3]> x[(NUM_PAR*3)+2]){
      x[i*3]= x[(NUM_PAR*3)+2];
    }
    if(x[i*3]<0.0){//ensure that no particles can escape the system from the bottom of the cylinder
      x[i*3]=0.0;
    }
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////

  //THETA BOUNDARY CONDITION ///////////////////////////////////////////////////////////////////////////////
  for(i=1;i<=NUM_PAR;i++){
    new_th=modulo(x[(i*3)-1],2*pi);
    x[(i*3)-1]=new_th;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////

  //RADIAL BOUNDARY CONDITION //////////////////////////////////////////////////////////////////////////////
  for(i=1;i<=NUM_PAR;i++){
    x[(i*3)-2]=sqrt(x[(i*3)-2]*x[(i*3)-2]);
    if(x[(i*3)-2] > Rlim){ //prevent particles from moving outside the cylinder
      x[(i*3)-2]=Rlim;
    }
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////

  //TWIST BOUNDARY CONDITION //////////////////////////////////////////////////////////////////////////////
  new_A=modulo(x[(NUM_PAR*3)+1],2*pi); // constrain twist angle to be between 0 and 2*Pi
  x[(NUM_PAR*3)+1]=new_A;
  //////////////////////////////////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  //COMPUTE ENERGY OF SYSTEM
  //////////////////////////////////////////////////////////////////////////

  ////ENERGY DUE TO INTERACTIONS WITHIN THE SIMULATION CELL ///////////////
  cylinder=0.0;
  for (i = 1; i <= NUM_PAR; i++){
    for (j = 1; j <= NUM_PAR; j++){
      if(i != j ){
	part1= (x[(i*3)-2]*x[(i*3)-2]) + (x[(j*3)-2]*x[(j*3)-2]) - ( 2*x[(i*3)-2]*x[(j*3)-2]*cos(x[(i*3)-1]-x[(j*3)-1]) );
	part2= (x[i*3]*x[i*3]) + (x[j*3]*x[j*3]) - (2*x[i*3]*x[j*3]);
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
	if(dist<=L){
	  Etemp = 0.5*(0.5*(dist-L)*(dist-L)); 
	  cylinder+=Etemp;
	}
      }
    }
  }
  ////////////////////////////////////////////////////////////////////////////


  //INTERACTIONS WITH IMAGE PARTICLES ////////////////////////////////////////
  
  //generate image particles//////////////////////////////////////////////////
  //find all the particles at the bottom of the cylinder
  k=0;
  for (i = 1; i <= NUM_PAR; i++){
    if(x[(i*3)] < L){
      k++;
      BimageID[k]=i;
      B[k*3]=x[(NUM_PAR*3)+2]+x[i*3];
      B[(k*3)-1]=modulo(x[(i*3)-1] + x[(NUM_PAR*3)+1], 2*pi);
      B[(k*3)-2]=x[(i*3)-2];
    }
  }
  NB_images=k;
  //find all the particles at the top of the cylinder
  k=0;
  for (i = 1; i <= NUM_PAR; i++){
    if(x[i*3] > x[(NUM_PAR*3)+2] - L){
      k++;
      TimageID[k]=i;
      T[k*3]=-(x[(NUM_PAR*3)+2]-x[i*3]);
      T[(k*3)-1]=modulo(x[(i*3)-1] - x[(NUM_PAR*3)+1], 2*pi);
      T[(k*3)-2]=x[(i*3)-2];
    }
  }
  NT_images=k;
  //////////////////////////////////////////////////////////////////////////////

  //compute interactions with bottom images////////////////////////////////////
  image=0.0;
  for(i=1;i<=NUM_PAR;i++){
    for(j=1;j<=NB_images;j++){
      part1= (x[(i*3)-2]*x[(i*3)-2]) + (B[(j*3)-2]*B[(j*3)-2]) - ( 2*x[(i*3)-2]*B[(j*3)-2]*cos(x[(i*3)-1]-B[(j*3)-1]) );
      part2= (x[i*3]*x[i*3]) + (B[j*3]*B[j*3]) - (2*x[i*3]*B[j*3]);
      pre_dist=part1+part2;
      dist=sqrt(pre_dist);
      if(dist<=L){
	Etemp = 0.5*(0.5*(dist-L)*(dist-L));
	image+=Etemp;
      }
    }
  }

  //compute interactions with top images//////////////////////////////////////
  for(i=1;i<=NUM_PAR;i++){
    for(j=1;j<=NT_images;j++){
      part1= (x[(i*3)-2]*x[(i*3)-2]) + (T[(j*3)-2]*T[(j*3)-2]) - ( 2*x[(i*3)-2]*T[(j*3)-2]*cos(x[(i*3)-1]-T[(j*3)-1]) );
      part2= (x[i*3]*x[i*3]) + (T[j*3]*T[j*3]) - (2*x[i*3]*T[j*3]);
      pre_dist=part1+part2;
      dist=sqrt(pre_dist);
      if(dist<=L){
	Etemp = 0.5*(0.5*(dist-L)*(dist-L));
	image+=Etemp;
      }
    }
  }
  ////////////////////////////////////////////////////////////////////////////
 
  
  ////Energy due to interaction between particles and the boundary///
  boundary=0.0;
  for(i=1; i<=NUM_PAR; i++){
    dist=x[(i*3)-2];
    distance_from_boundary=Rlim-dist;
    if(distance_from_boundary<=L){
      Etemp=BS*0.5*(distance_from_boundary-L)*(distance_from_boundary-L);
      boundary+=Etemp;
    }
  }
  ///////////////////////////////////////////////////////////////////


  ////Energy due to pressure term////////////////////////////////////
  penergy=0.0;
  volume=pi*((diameter/2.0)*(diameter/2.0))*x[(NUM_PAR*3)+2];
  penergy=pressure*volume;
  ///////////////////////////////////////////////////////////////////

  energy = cylinder + image + boundary;
  enthalpy=energy+penergy;
  
  return enthalpy;
}

void dfunc(double x[], double df[])
{
  
  double dr, dth, dz, dA, dl;

  //////////////////////////////////////////////////////////////////////////
  //IMPOSE BOUNDARY CONDITIONS
  //////////////////////////////////////////////////////////////////////////
  
  //Z BOUNDARY CONDITION ///////////////////////////////////////////////////////////////////////////////////
  for(i=1; i<=NUM_PAR; i++){
    //ensure that no particles can escape the system from the top of the cylinder
    if(x[i*3]>x[(NUM_PAR*3)+2]){
      x[i*3]=x[(NUM_PAR*3)+2];
    }
    if(x[i*3]<0.0){//ensure that no particles can escape the system from the bottom of the cylinder
      x[i*3]=0.0;
    }
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //THETA BOUNDARY CONDITION ///////////////////////////////////////////////////////////////////////////////
  for(i=1;i<=NUM_PAR;i++){
    new_th=modulo(x[(i*3)-1],2*pi);
    x[(i*3)-1]=new_th;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //RADIAL BOUNDARY CONDITION //////////////////////////////////////////////////////////////////////////////
  for(i=1;i<=NUM_PAR;i++){
    x[(i*3)-2]=sqrt(x[(i*3)-2]*x[(i*3)-2]);
    if(x[(i*3)-2] > Rlim){ //prevent particles from moving outside the cylinder
      x[(i*3)-2]=Rlim;
    }
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //TWIST BOUNDARY CONDITION //////////////////////////////////////////////////////////////////////////////
  new_A=modulo(x[(NUM_PAR*3)+1],2*pi); // constrain twist angle to be between 0 and 2*Pi
  x[(NUM_PAR*3)+1]=new_A;
  //////////////////////////////////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  //COMPUTE DERIVATIVES
  //////////////////////////////////////////////////////////////////////////

  //SET ALL DERIVATIVES TO ZERO ////////////////////////////////////////////
  for(i=1;i<= NUM_PAR;i++){
    df[i*3]=0.0; df[(i*3)-1]=0.0; df[(i*3)-2]=0.0;
  }
  df[(NUM_PAR*3)+1]=0.0;
  //////////////////////////////////////////////////////////////////////////
  

  //DERIVATIVES DUE TO CELL INTERACTIONS/////////////////////////////////////
  for(i=1;i<= NUM_PAR;i++){
    for(j=1;j<= NUM_PAR;j++){
      if(i != j){
	part1= (x[(i*3)-2]*x[(i*3)-2]) + (x[(j*3)-2]*x[(j*3)-2]) - ( 2*x[(i*3)-2]*x[(j*3)-2]*cos(x[(i*3)-1]-x[(j*3)-1]) );
	part2= (x[i*3]*x[i*3]) + (x[j*3]*x[j*3]) - (2*x[i*3]*x[j*3]);
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
	if(dist<=L){
	  part3=(dist-L)/dist;
	  dr=( x[(i*3)-2] - (  x[(j*3)-2]*(cos(x[(i*3)-1]-x[(j*3)-1]))  ) )*part3;
	  df[(i*3)-2]+=dr;
	  dth=(x[(i*3)-2]*x[(j*3)-2])*sin(x[(i*3)-1]-x[(j*3)-1])*part3;
	  df[(i*3)-1]+=dth;
	  dz=(x[i*3]-x[j*3])*part3;
	  df[i*3]+=dz;
	}
      }     
    } 
  }
  /////////////////////////////////////////////////////////////////////////////

  //DERIVATIVES DUE TO IMAGE INTERACTIONS//////////////////////////////////////
  //find all the image particles//
  k=0;
  for (i = 1; i <= NUM_PAR; i++){
    if(x[(i*3)] < L){
      k++;
      BimageID[k]=i;
      B[k*3]=x[(NUM_PAR*3)+2]+x[i*3];
      B[(k*3)-1]=modulo(x[(i*3)-1] + x[(NUM_PAR*3)+1], 2*pi);
      B[(k*3)-2]=x[(i*3)-2];
    }
  }
  NB_images=k;
  k=0;
  for (i = 1; i <= NUM_PAR; i++){
    if(x[i*3] > x[(NUM_PAR*3)+2] - L){
      k++;
      TimageID[k]=i;
      T[k*3]=-(x[(NUM_PAR*3)+2]-x[i*3]);
      T[(k*3)-1]=modulo(x[(i*3)-1] - x[(NUM_PAR*3)+1], 2*pi);
      T[(k*3)-2]=x[(i*3)-2];
    }
  }
  NT_images=k;
  ///////////////////////////////////

  //Compute the contribution to the derivatives//////////////
  for(i=1;i<=NUM_PAR;i++){
    for(j=1;j<=NB_images;j++){
	part1= (x[(i*3)-2]*x[(i*3)-2]) + (B[(j*3)-2]*B[(j*3)-2]) - ( 2*x[(i*3)-2]*B[(j*3)-2]*cos(x[(i*3)-1]-B[(j*3)-1]) );
	part2= (x[i*3]*x[i*3]) + (B[j*3]*B[j*3]) - (2*x[i*3]*B[j*3]);
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
	if(dist<=L){
	  part3=(dist-L)/dist;
	  dr = ( x[(i*3)-2]  -   (  B[(j*3)-2]*(cos(x[(i*3)-1]-B[(j*3)-1]))  ) )*part3;
	  df[(i*3)-2] += dr;
	  dth=(x[(i*3)-2]*B[(j*3)-2])*sin(x[(i*3)-1]-B[(j*3)-1])*part3;
	  df[(i*3)-1] += dth;
	  dz=(x[i*3]-B[j*3])*part3;
	  df[i*3] +=dz;
	  dA=-0.5*dth; //CHECK
	  df[(NUM_PAR*3)+1] += dA;
	  dl=-0.5*dz;
	  df[(NUM_PAR*3)+2] += dl;
	}
    }
  } 
  for(i=1;i<=NUM_PAR;i++){
    for(j=1;j<=NT_images;j++){
	part1= (x[(i*3)-2]*x[(i*3)-2]) + (T[(j*3)-2]*T[(j*3)-2]) - ( 2*x[(i*3)-2]*T[(j*3)-2]*cos(x[(i*3)-1]-T[(j*3)-1]) );
	part2= (x[i*3]*x[i*3]) + (T[j*3]*T[j*3]) - (2*x[i*3]*T[j*3]);
	pre_dist=part1+part2;
	dist=sqrt(pre_dist);
	if(dist<=L){
	  part3=(dist-L)/dist;
	  dr= ( x[(i*3)-2]  -   (  T[(j*3)-2]*(cos(x[(i*3)-1]-T[(j*3)-1]))  ) )*part3;
	  df[(i*3)-2] += dr;
	  dth=(x[(i*3)-2]*T[(j*3)-2])*sin(x[(i*3)-1]-T[(j*3)-1])*part3;
	  df[(i*3)-1] += dth;
	  dz=(x[i*3]-T[j*3])*part3;
	  df[i*3] +=dz;
	  dA=0.5*dth; //CHECK
	  df[(NUM_PAR*3)+1] += dA;
	  dl= 0.5*dz;
	  df[(NUM_PAR*3)+2] += dl;
	}
    }
  }
  /////////////////////////////////////////////////////////////////////////////

 
  //DERIVATIVES DUE TO BOUNDARY INTERACTIONS//////////////////////////////////
  for(i=1; i<=NUM_PAR; i++){
    dist=x[(i*3)-2];
    distance_from_boundary=Rlim-dist;
    if(distance_from_boundary<=L){
      dr=-(BS)*(distance_from_boundary-L);
      df[(i*3)-2]+=dr;
    }
  }
  /////////////////////////////////////////////////////////////////////////////
  
 
  //DERIVATIVE DUE TO PRESSURE TERM ///////////////////////////////////////////
  dl=pressure*pi*((diameter/2.0)*(diameter/2.0));
  df[(NUM_PAR*3)+2]+=dl;
  /////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  //IMPOSE BOUNDARY CONDITIONS AGAIN AND STOP MOTION BEYOND WALLS
  //////////////////////////////////////////////////////////////////////////
  
  //Z BOUNDARY CONDITION ///////////////////////////////////////////////////////////////////////////////////
  for(i=1; i<=NUM_PAR; i++){
    //ensure that no particles can escape the system from the top of the cylinder
    if(x[i*3]>x[(NUM_PAR*3)+2]){
      x[i*3]=x[(NUM_PAR*3)+2];
      df[i*3]=0.0;
    }
    if(x[i*3]<0.0){//ensure that no particles can escape the system from the bottom of the cylinder
      x[i*3]=0.0;
      df[i*3]=0.0;
    }
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //THETA BOUNDARY CONDITION ///////////////////////////////////////////////////////////////////////////////
  for(i=1;i<=NUM_PAR;i++){
    new_th=modulo(x[(i*3)-1],2*pi);
    x[(i*3)-1]=new_th;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //RADIAL BOUNDARY CONDITION //////////////////////////////////////////////////////////////////////////////
  for(i=1;i<=NUM_PAR;i++){
    x[(i*3)-2]=sqrt(x[(i*3)-2]*x[(i*3)-2]);
    if(x[(i*3)-2] > Rlim){ //prevent particles from moving outside the cylinder
      x[(i*3)-2]=Rlim;
      df[(i*3)-2]=0.0;
    }
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  //TWIST BOUNDARY CONDITION //////////////////////////////////////////////////////////////////////////////
  new_A=modulo(x[(NUM_PAR*3)+1],2*pi); // constrain twist angle to be between 0 and 2*Pi
  x[(NUM_PAR*3)+1]=new_A;
  //////////////////////////////////////////////////////////////////////////////////////////////////////////
  

}

double modulo(double num,double limit)
{
  return num-floor(num/limit)*limit;
}

int main (int argc, char *argv[])
{
  int iter,l;
  int junki;
  double junkf;
  double fret,*p;
  double tempenergy;
  double pos1, pos2, pos3;
  double rad, rad_temp;
  double Q[5000];
  double Pold;
  int NUM_D_CHARGES;
  int rep;
  long *seed;
  double rand;
  double xo, yo, zo;
  char filename[100];
  FILE *fpt;

  sscanf(argv[1],"%lf",&pressure); // Target pressure
  sscanf(argv[2],"%lf",&diameter); //Target diameter
	
  fpt=fopen("cylinder_seed", "r");
  fscanf(fpt, "%lf", &Pold); //This is the current pressure 
  fscanf(fpt, "%d", &NUM_PAR);
  fscanf(fpt, "%lf", &Rlim);
  fscanf(fpt, "%lf", &initial_Clength);
  if(diagnostic==1){
    printf("NUM_PAR=%d\nRlim=%6.15f\nClength=%6.15f\n", NUM_PAR, Rlim, initial_Clength);
  }
  NDIM=(NUM_PAR*3)+2;
  p=vector(1,NDIM);
  for(i = 1; i <= NUM_PAR; i++){
    fscanf(fpt, "%lf", &pos1);
    p[(i*3)-2]=pos1;
    fscanf(fpt, "%lf", &pos2);
    p[(i*3)-1]=pos2;
    fscanf(fpt, "%lf", &pos3);
    p[i*3]=pos3;
    if(diagnostic==1){
      printf("%6.15f %6.15f %6.15f\n", p[(i*3)-2], p[(i*3)-1], p[i*3]);
    }
  }
  fscanf(fpt, "%lf", &A);
  p[(NUM_PAR*3)+1]=A;
  
  Rlim=(diameter+1.0)/2.0; //Compute Rlim from the target diameter
  Clength=initial_Clength;
  p[(NUM_PAR*3)+2]=Clength;

  /////// RUN ////////////////////////////////////////////////////
  frprmn(p,NDIM,FTOL,&iter,&fret,func,dfunc);
  printf("Iterations: %3d\n",iter);
  ////////////////////////////////////////////////////////////////

  
  fpt=fopen("DATA", "w");
  fprintf(fpt, "%6.15f\n",pressure);
  fprintf(fpt, "%d\n", NUM_PAR);
  fprintf(fpt, "%6.15f\n", Rlim);
  fprintf(fpt, "%6.15f\n", p[(NUM_PAR*3)+2]);
  for(i = 1; i <= NUM_PAR; i++){
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", p[(i*3)-2], p[(i*3)-1], p[i*3]);
  }
  fprintf(fpt,"%6.15f\n", p[(NUM_PAR*3)+1]);
  fclose(fpt);

  //Print out final state of the system
  fpt=fopen("gradient_descent_energy", "a");
  fprintf(fpt, "%6.15f %6.15f %3d %6.15f\n",pressure, diameter, iter, fret/(1.0*NUM_PAR));
  fclose(fpt);

  fpt=fopen("cg_gpic", "w");
  fprintf(fpt, "LIST\n");
  for(i = 1; i <= NUM_PAR; i++){
    xo=p[(i*3)-2]*cos(p[(i*3)-1]);
    yo=p[(i*3)-2]*sin(p[(i*3)-1]);
    zo=p[(i*3)];
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", L/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fpt,"}\n");

    xo=p[(i*3)-2]*cos(p[(i*3)-1]+A);
    yo=p[(i*3)-2]*sin(p[(i*3)-1]+A);
    zo=p[(i*3)]+Clength;
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", L/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fpt,"}\n");

    xo=p[(i*3)-2]*cos(p[(i*3)-1]-A);
    yo=p[(i*3)-2]*sin(p[(i*3)-1]-A);
    zo=p[(i*3)]-Clength;
    fprintf(fpt, "{\n");
    fprintf(fpt, "SPHERE\n");
    fprintf(fpt,"%6.15f\n", L/2.0);
    fprintf(fpt,"%6.15f %6.15f %6.15f\n", xo, zo, yo );
    fprintf(fpt,"}\n");
  }
  fclose(fpt);

  fpt=fopen("FAIL", "w");
  fprintf(fpt, "1\n");
  fclose(fpt);
  
  free_vector(p,1,NDIM);
}








