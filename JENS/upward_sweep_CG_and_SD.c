#include <stdio.h>
#include <time.h>
#include <math.h>

//YOUR PATH HERE
#define MYPATH "/Users/jenswinkelmann/Desktop/TrinityCollege/Year_1/SoftSpherePacking/JENS/"

#define tol 0.000001
#define pi 3.141592653589793238462643383279502884

int main (int argc, char *argv[])
{
  int i, h, dcount, pcount;
  double startp, endp, startD, endD;
  double Dp, DD, Dpl, path_len;
  double D, p;
  double hdiff;
  double pf, Df, ef, hf;
  double pr, Dr, er, hr;
  double dsteplen, psteplen;  
  double JIGGLE;
  int count, c;
  int dsteps, psteps;
  int FNUM;
  FILE *fp, *gp, *hp;
  char filename[2000];
  char gfilename[2000];
  
  sscanf(argv[1],"%lf",&startp);//input starting pressure
  printf("starting pressure=%6.15f\n", startp);   
  sscanf(argv[2],"%lf",&startD);//input starting cylinder diameter 
  printf("starting diameter=%lf\n", startD); 
  sscanf(argv[3],"%lf",&dsteplen);//diameter step size 
  printf("dsteplength=%lf\n", dsteplen);
  sscanf(argv[4],"%d",&dsteps);//number of steps  
  printf("dsteps=%d\n", dsteps); 
  sscanf(argv[5],"%lf",&JIGGLE);// do you want to jiggle the packing before relaxation? If not then set to 0.0 
  printf("JIGGLE=%lf\n", JIGGLE); 
  sscanf(argv[6],"%d",&count);// after how many diamete steps should results be written out  
  printf("image_count=%d\n", count); 
  
  //copy the initial structure to cylinder_seed - all routines below execute on a file called "cylinder seed" 
  sprintf(filename,"%s","cp cylinder_seed_original cylinder_seed");
  fp = popen(filename, "w");
  pclose(fp);
  
  
  c=0; //start count c - for outputting data
  for(dcount=0; dcount<=dsteps; dcount++){ 
    c++;
    
    D=startD+(dcount*dsteplen); //change sign here to increase/decrease diameter
    p=startp;//pressure remains the same during thses simulations
    
    printf("D=%6.15f p=%6.15f\n", D, p);
    
    sprintf(filename,"%s%s%lf", MYPATH, "jiggle ", JIGGLE);//Jiggle the structure
    fp = popen(filename, "w");
    pclose(fp);
   
    //////////////////////////////////////////////////////////////////////////////////////////
    //Use the following for conjugate gradient AND gradient descent
    /////////////////////////////////////////////////////////////////////////////////////////
    /*
    sprintf(filename,"%s%s%lf%s%lf", MYPATH, "conjugate_gradient_in_a_cylinder ", p, " ", D);
    fp = popen(filename, "w");
    pclose(fp);
    
    fp=fopen("FAIL", "r");
    fscanf(fp, "%d", &FNUM);
    fclose(fp);
    
    if(FNUM==0){      
      
      sprintf(filename,"%s%s%lf%s%lf", MYPATH, "gradient_fixed_metric 0.01 1000000  ", p, " ", D);
      fp = popen(filename, "w");
      pclose(fp);
      
      sprintf(filename,"%s","rm energy");
      fp = popen(filename, "w");
      pclose(fp);
      
    }
    */
    //////////////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////////////////////////////
    //Use the following for gradient descent only
    //////////////////////////////////////////////////////////////////////////////////////////////  
    sprintf(filename,"%s%s%lf%s%lf", MYPATH, "gradient_fixed_metric 0.01 1000000  ", p, " ", D);
    fp = popen(filename, "w");
    pclose(fp);
    
    sprintf(filename,"%s","rm energy");
    fp = popen(filename, "w");
    pclose(fp);
    //////////////////////////////////////////////////////////////////////////////////////////////
    
    //copy the output to the expected input file - for the next iteration
    sprintf(filename,"%s","cp DATA cylinder_seed"); 
    fp = popen(filename, "w");
    pclose(fp); 
    

    if(c==count){//print out results
      c=0;
     
      //save outputs
      sprintf(filename,"%s%6.15f%s%6.15f","cp DATA DATA_",p,"_",D);
      fp = popen(filename, "w");
      pclose(fp);
      
      //save also as geomview files to visulise 3D structure
      sprintf(filename,"%s%d","mv cg_gpic cg_gpic_",dcount+10000);
      fp = popen(filename, "w");
      pclose(fp);
    }
  }

}
  


