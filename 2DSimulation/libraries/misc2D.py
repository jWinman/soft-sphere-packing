import numpy as np

################### Period. Boundary functions ###################
def foldZ(pos, L):
    x = pos[0]
    y = pos[1] - L * np.floor(pos[1] / L)
    poss = np.array([x, y])

    return poss

def shift(pos, L):
    x = pos[0]
    y = pos[1] + L
    poss = np.array([x, y])

    return poss

################### Energy functions ###################
def Harmonic(distances, selfy):
    eps = selfy.eps
    d = selfy.d
    harmonicLocal= lambda r: 0.5 * eps * (r - d)**2
    return np.where(distances > d, 0, harmonicLocal(distances))

################### Borders in 2D ###################
def borders2D(pos, selfy):
    """
    Takes arguments:
        pos: x and y positions of all disks
        selfy: object with other variables
    """

    d = selfy.d
    D = selfy.D
    eps = selfy.eps
    xs, ys = pos.T

    borderLocal = lambda x: np.exp(10 * x) - 1
    harmonicLocal = lambda x: 0.5 * eps * x**2

    energy  = np.sum(np.where(xs - d / 2. < 0, harmonicLocal(xs - d / 2.), 0))
    energy += np.sum(np.where(xs + d / 2. > D, harmonicLocal(xs + d / 2. - D), 0))
#    energy += np.sum(np.where(xs + d < 0, borderLocal(xs + d), 0))
#    energy += np.sum(np.where(xs - d > D, borderLocal(xs - d - D), 0))
    return energy

################### Initial functions ###################
def initial_Grid2D(R_z, L, N, damping):
    x = np.array([R_z * (-1)**i * damping for i in range(N)])
    y = np.linspace(0, L, N + 2)[1:-1]
    return x, y
