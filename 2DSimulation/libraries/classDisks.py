import numpy as np
import functools as ft
import scipy.spatial.distance as ssd
from scipy import optimize
import misc2D as misc

class DiskRectangular:
    def __init__(selfy, fileName, interaction="softsphere", borders="softborder"):
        import pylibconfig2 as pylib

        with open(fileName, "r") as f:
            cfgStr = f.read()

        cfg = pylib.Config(cfgStr)

        # static parameters
        selfy.D = cfg.cylinder.D
        selfy.p = cfg.cylinder.p
        selfy.N = cfg.spheres.N
        selfy.eps, selfy.d = cfg.spheres.eps, cfg.spheres.d
        selfy.niter = cfg.minimize.niter
        selfy.stepsize = cfg.minimize.stepsize
        selfy.T = cfg.minimize.T
        selfy.size = -1 # number of additional variable parameters

        # variable parameters
        selfy.positions = np.zeros(selfy.N * 2 + 2) # 2N position + alpha + L

        # energy interactions
        selfy.interaction = ft.partial(misc.Harmonic, selfy=selfy)
        selfy.borders = ft.partial(misc.borders2D, selfy=selfy)

        # Force interactions
#        selfy.interactionForce = ft.partial(misc.HarmonicForce, selfy=selfy)
#        selfy.borderForce = ft.partial(misc.border3DForce, selfy=selfy)
#        selfy.diffAlpha = misc.HarmonicDiffAlpha
#        selfy.diffL = misc.HarmonicDiffL

    def RandomInitial(selfy):
        import numpy.random
        x = numpy.random.uniform(0, selfy.D, selfy.N)
        y = numpy.random.uniform(0, selfy.N * selfy.d, selfy.N)
        L = numpy.random.uniform(0.5 * selfy.d, selfy.N * selfy.d)

        selfy.positions = np.append(np.array([x, y]).T.flatten(), np.array([L]))

    def GhostBoundary(selfy, x):
        xs = np.reshape(x[:selfy.size], (len(x[:selfy.size]) // 2, 2))
        L = x[selfy.size]

        xs = np.apply_along_axis(misc.foldZ, 1, xs, L)
        xAbove = np.apply_along_axis(misc.shift, 1, xs, L)
        xBelow = np.apply_along_axis(misc.shift, 1, xs, -L)
        return xs, np.concatenate((xs, xAbove, xBelow), axis=0)

    def Enthalpy(selfy, x):
        L = x[selfy.size]
        x, xGhost = selfy.GhostBoundary(x)

        # Energies between Ghost and Real spheres
        distances = ssd.cdist(x, xGhost, metric="euclidean")
        distances = np.delete(distances, [j + j * len(xGhost) for j in range(len(x))])
        energy = 0.5 * np.sum(selfy.interaction(distances))
#        print("interaction: ", energy / selfy.N)

        # Energies between opposing Ghost and Ghost spheres
#        distancesGhost = ssd.cdist(xGhost[selfy.N:2 * selfy.N],
#                              xGhost[2 * selfy.N:3 * selfy.N], metric="euclidean")

#        energy += np.sum(selfy.interaction(distancesGhost))
#        print("Ghost interaction: ", np.sum(selfy.interaction(distancesGhost)) / selfy.N)

        # Energy from Border
        energy += selfy.borders(x)
#        print("border energy: ", selfy.borders(x) / selfy.N)

        # pV term
        energy += selfy.p * selfy.D * L
#        print("pV: ", selfy.p * np.pi * selfy.R_z**2 * L / selfy.N)

#        print("energy: ", energy)

        return energy

    def minimize(selfy, method):
        bnds = [(0, selfy.D),
                (0, selfy.positions[-1])] * int((len(selfy.positions) + selfy.size) / 2)
        bnds.append((selfy.d / 2., selfy.N * selfy.d))

        if (method=="basinhopping"):
            minimizer_kwargs = {"method": "L-BFGS-B", "jac": None, "bounds": bnds}
            ret = optimize.basinhopping(selfy.Enthalpy, selfy.positions,
                                        minimizer_kwargs=minimizer_kwargs,
                                        niter=selfy.niter, stepsize=selfy.stepsize, T=selfy.T)

        elif (method == "CG"):
            ret = optimize.minimize(selfy.Enthalpy, selfy.positions, jac=None, method="CG")

        else:
            ret = optimize.minimize(selfy.Enthalpy, selfy.positions, jac=None, method="L-BFGS-B", bounds=bnds,
                                    options={"ftol": 1e-20, "gtol": 1e-20})

        selfy.positions = ret.x
        return ret.fun

    def setVariable(selfy, variable, value):
        exec("selfy.{} = {}".format(variable, value))

    def printToFile(selfy, final):
        x, y = selfy.GhostBoundary(selfy.positions)[0].T
        furtherArgs = selfy.positions[selfy.size:]

        np.savetxt("data/" + final + "Finalp{:.6f}D{:.4f}Pos".format(selfy.p, selfy.D), np.array([x, y]).T)
        np.savetxt("data/" + final + "Finalp{:.6f}D{:.4f}FurtherArgs".format(selfy.p, selfy.D), furtherArgs.T)

    def plot2D(selfy, path):
        import matplotlib.pyplot as plt

        pos, posGhost = selfy.GhostBoundary(selfy.positions)

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1, aspect=1, axisbg="w")
        ax.set_xlim(0, selfy.D)
        ax.set_ylim(0, selfy.positions[-1])

        for i, posi in enumerate(posGhost):
            circle = plt.Circle(posi, selfy.d / 2., color="k", fill=False)
            ax.add_artist(circle)

        for i, posi in enumerate(pos):
            circle = plt.Circle(posi, selfy.d / 2., color="r", fill=False)
            ax.add_artist(circle)

        plt.tight_layout()
        plt.show()

    def plot2DChannel(selfy, path, repetition):
        import matplotlib.pyplot as plt

        pos, posGhost = selfy.GhostBoundary(selfy.positions)

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1, aspect=1, axisbg="w")
        ax.set_xlim(0, selfy.D)
        ax.set_ylim(0, repetition * selfy.positions[-1])

        for i in range(repetition):
            for posi in pos:
                posRep = np.array([posi[0], 0])
                posRep[1] = posi[1] + i * selfy.positions[-1]
                circle = plt.Circle(posRep, selfy.d / 2., color="k", fill=False)
                ax.add_artist(circle)

#        for posi in pos:
#            circle = plt.Circle(posi, selfy.d / 2., color="r", fill=False)
#            ax.add_artist(circle)

        plt.tight_layout()
        plt.show()
