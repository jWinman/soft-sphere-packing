import numpy as np

import sys
sys.path.append("../libraries")
import classDisks

dataFile = "parameter.cfg"

mySystem = classDisks.DiskRectangular("parameter.cfg")
mySystem.setVariable("D", 2.)

Ns = np.array([6, 8])
Es = np.zeros(len(Ns))
pos = []
for i, N in enumerate(Ns):
    mySystem.setVariable("N", N)
    mySystem.RandomInitial()

    Es[i] = mySystem.minimize("basinhopping") / N
    print("Number: ", N, "Energy: ", Es[i])
    pos.append(mySystem.positions)

mySystem.positions = pos[np.argmin(Es)]
mySystem.setVariable("N", Ns[np.argmin(Es)])

mySystem.plot2DChannel("", 4)
